#!/usr/bin/env python
"""Usage: find_difference.py <list_1> <list_2>
Finds the elements present in the first list but not in the second.
The lists have to be provided as text files with one element per line.
The resulting elements are printed in alphabetical order.
"""

import sys

if len(sys.argv) != 3:
    sys.stderr.write("Wrong number of command-line arguments\n")
    sys.stderr.write(__doc__)
    sys.stderr.write("\n")
    sys.exit(1)

set1 = set([])
with open(sys.argv[1], "r") as f1:
    for line in f1:
        set1.add(line.strip())

set2 = set([])
with open(sys.argv[2], "r") as f2:
    for line in f2:
        set2.add(line.strip())

for elem in sorted(set1 - set2):
    sys.stdout.write("%s\n" % elem)

sys.exit(0)
