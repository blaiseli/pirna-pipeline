#!/usr/bin/env python
# vim: set fileencoding=<utf-8> :
"""This script reads sorted and indexed .bam files and files containing
corresponding normalization values to apply, a reference name, and makes
pileup graphs for this reference, at the same scale for each .bam file."""


import argparse
import sys
WRITE = sys.stdout.write
import warnings


def formatwarning(message, category, filename, lineno, line):
    """Formats warning messages."""
    return "%s:%s: %s:%s" % (filename, lineno, category.__name__, message)
warnings.formatwarning = formatwarning

import os
OPJ = os.path.join
OPB = os.path.basename

from collections import defaultdict
from itertools import izip
import csv
import matplotlib
# To be able to run the script without a defined $DISPLAY
#matplotlib.use("PDF")
matplotlib.use("Agg")
import pysam
import pylab
import numpy as np


class Pileup(object):
    """Container for pileup data."""
    __slots__ = (
        "experiment", "reference",
        "shortref", "basename",
        "ref_len", "norm_factor",
        "max_mm", "max_fwd", "max_rev",
        "counts_fwd", "counts_rev",
        "hists_fwd", "hists_rev")

    def __init__(self, input_file_name, reference, norm_factor, counts):
        self.experiment = OPB(input_file_name).split("_on_")[0]
        self.reference = reference
        self.norm_factor = norm_factor
        # Maximum number of mismatches recorded for this pileup
        self.max_mm = 0
        # Highest total F and R read counts
        # over numbers of mismatches
        # among read lengths.
        self.max_fwd = 0
        self.max_rev = 0
        # Only set once reference length is known
        # Length of the reference sequence
        self.ref_len = None
        # Normalized total read counts values for each position
        self.counts_fwd = None
        self.counts_rev = None
        # The same, but separated by number of mismatches
        self.hists_rev = None
        self.hists_fwd = None
        # For output purposes
        self.shortref = None
        self.basename = None
        self.get_data_from(input_file_name, counts)

    def get_data_from(self, input_file_name, counts):
        """Reads pileup data from an indexed and sorted bam file."""
        with pysam.Samfile(input_file_name, "rb") as samfile:
            # Search for the reference in the sam file
            # and get its length at the same time.
            found_ref = False
            for a_ref, a_ref_len in izip(samfile.references, samfile.lengths):
                shortref = a_ref.split(":")[0]
                if shortref == self.reference:
                    ref = a_ref
                    ref_len = a_ref_len
                    found_ref = True
                    # The reference has been found.
                    break
            assert found_ref, "Reference not found."
            self.shortref = Globals.cluster_names.get(shortref, shortref)
            self.ref_len = ref_len
            WRITE("%s (%s)\n" % (ref, self.shortref))
            # Adapt the output file names based on whether reads are
            # filtered by size or not.
            if Options.read_range == (0, -1):
                self.basename = (
                    "_on_".join(OPB(input_file_name).split("_on_")[:-1])
                    + "_on_%s" % self.shortref)
            else:
                self.basename = (
                    "_on_".join(OPB(input_file_name).split("_on_")[:-1])
                    + "_%d-%d" % Options.read_range + "_on_" + self.shortref)
            # Write headers for pileup data output.
            with open(OPJ(
                    Options.pileup_dir,
                    self.basename + "_pileup.txt"), "wb") as pileup_data_file:
                writer = csv.writer(pileup_data_file, dialect="excel-tab")
                writer.writerow(
                    ["#pileup for maps from %s on %s" % (input_file_name, ref)])
                writer.writerow(
                    ["#ref", "%s" % ref, ref_len])
                writer.writerow(
                    ["#normalization", self.norm_factor])
                writer.writerow(
                    ["position", "nb_F", "normalized", "nb_R", "normalized",
                    "nb_F_by_mm", "nb_R_by_mm"])
                # Prepare data storage structures.
                # To accomodate for positions with no reads
                # we have to pre-fill the arrays with zeros.
                # Normalized total read counts values for each position
                self.counts_fwd = np.zeros(ref_len)
                self.counts_rev = np.zeros(ref_len)
                # The same, but separated by number of mismatches
                self.hists_fwd = defaultdict(lambda: np.zeros(ref_len))
                self.hists_rev = defaultdict(lambda: np.zeros(ref_len))
                pos = 0
                # Loop over positions on the reference
                for pile in samfile.pileup(reference=ref):
                    # pile_pos: pile position on the reference
                    # mm_fwd: dictionary counting F mapping reads
                    # for each mismatch degree
                    # mm_rev: dictionary counting R mapping reads
                    # for each mismatch degree
                    pile_pos, mm_fwd, mm_rev = counts(pile)
                    # Check the code writing the *_pileup.txt files.
                    # It should differenciates mismatch categories
                    # Sum over the number of mismatches
                    # of the number of reads at that position
                    abs_fwd = sum(mm_fwd.values())
                    abs_rev = sum(mm_rev.values())
                    # Maximum number of mismatches for this pile
                    # (highest of the maximum keys)
                    if mm_fwd:
                        max_mm_fwd = max(mm_fwd)
                    else:
                        max_mm_fwd = 0
                    if mm_rev:
                        max_mm_rev = max(mm_rev)
                    else:
                        max_mm_rev = 0
                    max_mm = max(max_mm_fwd, max_mm_rev)
                    # Update the maximum for the whole pileup.
                    self.max_mm = max(self.max_mm, max_mm)
                    # Store normalized counts
                    norm_fwd = abs_fwd / self.norm_factor
                    norm_rev = abs_rev / self.norm_factor
                    self.counts_fwd[pile_pos] += norm_fwd
                    self.counts_rev[pile_pos] += norm_rev
                    # Update the maximum normalized counts.
                    self.max_fwd = max(self.max_fwd, norm_fwd)
                    self.max_rev = max(self.max_rev, norm_rev)
                    # Write rows for empty piles.
                    while pos < pile_pos:
                        row = [pos, 0, 0.0, 0, 0.0]
                        writer.writerow(row)
                        pos += 1
                    # Initiate row to write in output table.
                    row = [pile_pos, abs_fwd, norm_fwd, abs_rev, norm_rev]
                    for nb_mm in range(max_mm + 1):
                        counts_mm_fwd = mm_fwd[nb_mm]
                        counts_mm_rev = mm_rev[nb_mm]
                        # Alternating numbers of reads
                        # with nb_mm mismatches in F and R directions
                        row.extend([counts_mm_fwd, counts_mm_rev])
                        self.hists_fwd[nb_mm][pile_pos] \
                            += counts_mm_fwd / self.norm_factor
                        self.hists_rev[nb_mm][pile_pos] \
                            += counts_mm_rev / self.norm_factor
                    writer.writerow(row)
                    # Jump ahead so that we don't write at pile_pos twice.
                    pos += 1

    def slide_window(self, window_size=1000):
        """This is not used."""
        # TODO: Generate arrays with average F and R read densities
        # in sliding windows using self.counts_fwd and self.counts_rev
        # Then plot this as a curve, hoping it enables a
        # compromise between document size and plot accuracy.
        pass

    def plot(self, ymin, ymax, max_mm, linewidth=0.1):
        """Stack lines for various mismatch values."""
        out = OPJ(Options.pileup_dir, self.basename + "_pileup")
        # We want an array covering the full length of the reference.
        positions = np.arange(self.ref_len)
        y_span = ymax - ymin
        margin = Globals.margin_percent * y_span
        if Options.no_fwd:
            pylab.axis([0, self.ref_len, ymin - margin, ymax])
        elif Options.no_rev:
            pylab.axis([0, self.ref_len, ymin, ymax + margin])
        else:
            pylab.axis([0, self.ref_len, ymin - margin, ymax + margin])
        axis = pylab.gca()
        axis.set_autoscale_on(False)
        axis.spines["right"].set_color("none")
        axis.spines["top"].set_color("none")
        axis.xaxis.set_ticks_position("bottom")
        axis.yaxis.set_ticks_position("left")

        if Options.nomm:
            if not Options.no_fwd:
                axis.vlines(
                    positions, [0], self.counts_fwd,
                    edgecolor="red",
                    label="F mappers", linewidths=linewidth)
            if not Options.no_rev:
                axis.vlines(
                    positions, -self.counts_rev,
                    [0], edgecolor="blue",
                    label="R mappers", linewidths=linewidth)
        else:
            #max_mm_fwd = max(hists_fwd.keys())
            #max_mm_rev = max(hists_rev.keys())
            # No need to explicitly call the keys() method
            # This is slightly faster
            max_mm_fwd = max(self.hists_fwd)
            max_mm_rev = max(self.hists_rev)
            # To set the range of colour intensities.
            #max_mm = max(max_mm_fwd, max_mm_rev)

            if not Options.no_fwd:
                y_coords_fwd = np.empty((max_mm + 1, self.ref_len))
                y_coords_fwd[0] = self.hists_fwd[0]
                axis.vlines(
                    positions, [0], y_coords_fwd[0],
                    edgecolor=pylab.cm.Reds(1 / (max_mm + 1.0)),
                    label="F mappers 0 mm", linewidths=linewidth)
                for nb_mm in range(1, max_mm_fwd + 1):
                    y_coords_fwd[nb_mm] = [
                        sum(pair) for pair in izip(
                            y_coords_fwd[nb_mm - 1], self.hists_fwd[nb_mm])]
                    axis.vlines(
                        positions, y_coords_fwd[nb_mm - 1],
                        y_coords_fwd[nb_mm],
                        edgecolor=pylab.cm.Reds((nb_mm + 1) / (max_mm + 1.0)),
                        label="F mappers %d mm" % nb_mm, linewidths=linewidth)

            if not Options.no_rev:
                y_coords_rev = np.empty((max_mm + 1, self.ref_len))
                y_coords_rev[0] = self.hists_rev[0]
                axis.vlines(
                    positions, -y_coords_rev[0],
                    [0], edgecolor=pylab.cm.Blues(1 / (max_mm + 1.0)),
                    label="R mappers 0 mm", linewidths=linewidth)
                # lines for mismatch values above zero
                # have to stack on top of the previous ones
                for nb_mm in range(1, max_mm_rev + 1):
                    y_coords_rev[nb_mm] = [
                        sum(pair) for pair in izip(
                            y_coords_rev[nb_mm - 1], self.hists_rev[nb_mm])]
                    axis.vlines(
                        positions, -y_coords_rev[nb_mm],
                        -y_coords_rev[nb_mm - 1],
                        edgecolor=pylab.cm.Blues((nb_mm + 1) / (max_mm + 1.0)),
                        label="R mappers %d mm" % nb_mm, linewidths=linewidth)

        if not Options.no_legend:
            axis.legend(framealpha=0.5, ncol=2, prop={"size": 9}, loc=0)

        axis.set_xlabel("Position in %s sequence" % self.shortref)
        if Options.y_axis is not None:
            axis.set_ylabel(Options.y_axis)
        else:
            #if Options.byM:
            #    axis.set_ylabel("Reads per million normalizers")
            if Options.by_norm:
                axis.set_ylabel("Reads per %f normalizers" % Options.by_norm)
            else:
                axis.set_ylabel("Normalized read counts")
        #pylab.ylim([ymin, ymax])
        # quite slow:
        #pylab.bar(positions, counts_fwd, label="forward mappers")
        #pylab.bar(positions, -counts_rev, label="reverse mappers")
        #pylab.legend(loc="lower right")
        for fmt in Options.formats:
            try:
                pylab.savefig(out + "." + fmt)
            except ValueError:
                warnings.warn(
                    "Output format %s unrecognised, skipping it.\n" % fmt)
        #if Options.show_figs:
            #pylab.show()
        pylab.cla()

    def plotline(self, ymin, ymax, linewidth=0.1):
        """Pileup profile using line plot."""
        out = OPJ(Options.pileup_dir, self.basename + "_line_pileup")
        # We want an array covering the full length of the reference.
        positions = np.arange(self.ref_len)
        y_span = ymax - ymin
        margin = Globals.margin_percent * y_span
        if Options.no_fwd:
            pylab.axis([0, self.ref_len, ymin - margin, ymax])
        elif Options.no_rev:
            pylab.axis([0, self.ref_len, ymin, ymax + margin])
        else:
            pylab.axis([0, self.ref_len, ymin - margin, ymax + margin])
        #pylab.axis([0, self.ref_len, ymin, ymax])
        axis = pylab.gca()
        axis.set_autoscale_on(False)
        axis.spines["right"].set_color("none")
        axis.spines["top"].set_color("none")
        axis.xaxis.set_ticks_position("bottom")
        axis.yaxis.set_ticks_position("left")

        if not Options.no_fwd:
            axis.plot(
                positions, self.counts_fwd,
                color="red",
                label="F mappers", linewidth=linewidth)
        if not Options.no_rev:
            axis.plot(
                positions, -self.counts_rev,
                color="blue",
                label="R mappers", linewidth=linewidth)

        if not Options.no_legend:
            axis.legend(framealpha=0.5, ncol=2, prop={"size": 9}, loc=0)

        axis.set_xlabel("Position in %s sequence" % self.shortref)
        if Options.y_axis is not None:
            axis.set_ylabel(Options.y_axis)
        else:
            #if Options.byM:
            #    axis.set_ylabel("Reads per million normalizers")
            if Options.by_norm:
                axis.set_ylabel("Reads per %f normalizers" % Options.by_norm)
            else:
                axis.set_ylabel("Normalized read counts")
        #pylab.ylim([ymin, ymax])
        # quite slow:
        #pylab.bar(positions, counts_fwd, label="forward mappers")
        #pylab.bar(positions, -counts_rev, label="reverse mappers")
        #pylab.legend(loc="lower right")
        for fmt in Options.formats:
            try:
                pylab.savefig(out + "." + fmt)
            except ValueError:
                warnings.warn(
                    "Output format %s unrecognised, skipping it.\n" % fmt)
        #if Options.show_figs:
            #pylab.show()
        pylab.cla()


def pile_counter(read_range):
    """Generates a read counting function."""
    min_size, max_size = read_range
    min_score = Options.min_score
    if min_size <= max_size:
        if Options.max_edit_proportion == -1.0:
            # Do not filter on edit proportion
            def select(read):
                """Filter function.
                The read has to match at its first base."""
                ali = read.alignment
                #rlen = ali.rlen
                rlen = ali.query_length
                return (
                    #read.is_head
                    (min_size <= rlen <= max_size)
                    and (ali.opt("AS") >= min_score)
                    and (ali.opt("NM") <= Options.max_mm))
        else:
            def select(read):
                """Filter function.
                The read has to match at its first base."""
                ali = read.alignment
                #rlen = ali.rlen
                rlen = ali.query_length
                return (
                    #read.is_head
                    (ali.opt("NM") <= rlen * Options.max_edit_proportion)
                    and (min_size <= rlen <= max_size)
                    and (ali.opt("AS") >= min_score)
                    and (ali.opt("NM") <= Options.max_mm))
    else:
        # Don't filter by size
        if Options.max_edit_proportion == -1.0:
            # Do not filter on edit proportion
            def select(read):
                """Filter function.
                The read has to match at its first base."""
                ali = read.alignment
                return (
                    #read.is_head
                    (ali.opt("AS") >= min_score)
                    and (ali.opt("NM") <= Options.max_mm))
        else:
            def select(read):
                """Filter function.
                The read has to match at its first base."""
                ali = read.alignment
                #rlen = ali.rlen
                rlen = ali.query_length
                return (
                    #read.is_head
                    (ali.opt("NM") <= rlen * Options.max_edit_proportion)
                    and (ali.opt("AS") >= min_score)
                    and (ali.opt("NM") <= Options.max_mm))

    def mm_histo(pile):
        """Generates counts of forward and reverse mapping reads
        classified by number of mismatches."""
        ref_pos = pile.reference_pos
        # key: number of mismatches
        # value: number of aligned reads from the pile
        # with that number of mismatches
        mm_fwd = defaultdict(int)
        mm_rev = defaultdict(int)
        for read in pile.pileups:
            ali = read.alignment
            # Note: read.qpos and read.is_head are not reliable indications
            # for 5'end alignment testing
            #if read.qpos == 0:
            #    assert read.is_head
            if select(read):
                if ali.is_reverse:
                    if not Options.no_rev:
                        if ali.reference_end - 1 == ref_pos:
                            mm_rev[read.alignment.opt("NM")] += 1
                else:
                    if not Options.no_fwd:
                        if ali.reference_start == ref_pos:
                            mm_fwd[read.alignment.opt("NM")] += 1
        return ref_pos, mm_fwd, mm_rev
    return mm_histo


#####################################
# For better traceability,          #
# options and global variables      #
# can be stored as class attributes #
#####################################
class Globals(object):
    """This object holds global variables."""
    cluster_names = {"cluster1": "42AB", "cluster8": "flamenco"}
    margin_percent = 0.05


class Options(object):
    """This object contains the values of the global options."""
    formats = ["pdf"]
    #formats = ["png"]
    y_axis = None
    #byM = None
    by_norm = None
    no_fwd = None
    no_rev = None
    nomm = None
    no_legend = False
    # default value set so that pile_counter will not
    # select alignments based on edit proportion
    max_edit_proportion = None
    max_mm = None
    min_score = None
    pileup_dir = None
    # min and max lengths for a read to be counted
    # default value set so that pile_counter
    # will not select alignments based on read size
    read_range = None
    # set to True to display the figure
    # (this will pause the program until the figure window is closed)
    show_figs = False

#################
# main function #
#################


def main():
    """Main function of the program."""
    WRITE("%s\n" % " ".join(sys.argv))
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    #parser.add_argument(
    #    "--byM",
    #    action="store_true",
    #    help="Set this option to scale by million of normalizers "
    #    "instead of scaling by plain number of normalizers.")
    parser.add_argument(
        "-b", "--by_norm",
        type=int,
        help="Set this to count reads by multiples of normalizers.")
    parser.add_argument(
        "--nomm",
        action="store_true",
        help="Set this option to represent all degrees of mismatches "
        "using the same colour.")
    parser.add_argument(
        "--noF",
        action="store_true",
        help="Set this option to not plot the forward mapping reads.")
    parser.add_argument(
        "--noR",
        action="store_true",
        help="Set this option to not plot the reverse mapping reads.")
    parser.add_argument(
        "--no_legend",
        action="store_true",
        help="Set this option to not display a legend in the output.")
    parser.add_argument(
        "-i", "--in_files",
        nargs="*",
        required=True,
        help="Space-separated list of .bam files.")
    parser.add_argument(
        "-n", "--normalizations",
        nargs="*",
        required=True,
        help="Space-separated list of files containing the values used "
        "to normalize read count in a pile.\n"
        "Of course, the file number and order should correspond to that "
        "given in the option --in_files.")
    parser.add_argument(
        "-r", "--reference",
        required=True,
        help="Name of the reference for which pileups have to be generated."
        "\nThis name should be present in the references provided to "
        "bowtie2 when producing the sam mapping.")
    parser.add_argument(
        "-d", "--pileup_dir",
        required=True,
        help="Directory in which the pileups should be written.")
    # default value set so that pile_counter will not
    # select alignments based on edit proportion
    parser.add_argument(
        "--max_edit_proportion",
        type=float,
        default=-1.0,
        help="Maximum proportion of edits for an alignment to be recorded."
        "\nThe default is to accept any proportion of edits.")
    parser.add_argument(
        "--max_mm",
        type=int,
        default=5,
        help="Maximum number of mismatches for an alignment to be recorded."
        "\nThe default is to accept up to 5 mismatches.")
    parser.add_argument(
        "--min_score",
        type=int,
        default=-200,
        help="Minimum mapping score for an alignment to be recorded."
        "\nThe default value is -200.")
    # min and max lengths for a read to be counted
    # default value set so that pile_counter
    # will not select alignments based on read size
    parser.add_argument(
        "--read_range",
        nargs=2,
        default=[0, -1],
        help="Minimum and maximum lengths of the reads to take into account."
        "\nThe default is to take all sizes into account.")
    parser.add_argument(
        "-y", "--y_axis",
        help="Legend to use for the vertical axes.")
    parser.add_argument(
        "--linewidth",
        type=float,
        default=0.1,
        help="Width to use for pileup lines.")
    args = parser.parse_args()
    err_msg = "".join([
        "There must be the same number ",
        "of normalization values ",
        "as there are data input files."])
    assert len(args.normalizations) == len(args.in_files), err_msg
    # Give global access to some options
    #Options.byM = args.byM
    Options.by_norm = args.by_norm
    Options.no_fwd = args.noF
    Options.no_rev = args.noR
    Options.nomm = args.nomm
    Options.no_legend = args.no_legend
    Options.y_axis = args.y_axis
    Options.max_edit_proportion = args.max_edit_proportion
    Options.max_mm = args.max_mm
    Options.min_score = args.min_score
    Options.read_range = tuple(args.read_range)
    Options.pileup_dir = args.pileup_dir
    #Options.reference = args.reference
    #Options.references = argument.split(",")

    # To avoid repeatedly calling these as string methods:
    strip = str.rstrip
    split = str.split
    counts = pile_counter(Options.read_range)
    # key: experiment name
    # value: Pileup object
    data = {}

    #Globals.norm_factor = Options.normalize / 1000000.0
    for (input_file_name, norm_file_name) in izip(args.in_files,
                                                  args.normalizations):
        with open(norm_file_name, "r") as norm_file:
            norm = float(strip(norm_file.readline()))
            #if Options.byM:
            #    norm = norm / 1000000.0
            if Options.by_norm:
                norm = norm / Options.by_norm
        experiment = split(OPB(input_file_name), "_on_")[0]
        data[experiment] = Pileup(
            input_file_name,
            args.reference, norm, counts)

    # Determine overall ymin and ymax
    # to scale all pileups to the same size.
    if Options.no_fwd:
        ymax = 0.0
    else:
        ymax = max([pileup.max_fwd for pileup in data.values()])
    if Options.no_rev:
        ymin = 0.0
    else:
        ymin = -max([pileup.max_rev for pileup in data.values()])
    # Determine overall maximum number of mismatches
    # to use the same colour scale for all pileups.
    max_mm = max([pileup.max_mm for pileup in data.values()])

    if Options.nomm:
        for pileup in data.values():
            pileup.plotline(ymin, ymax, args.linewidth)
    else:
        for pileup in data.values():
            pileup.plot(ymin, ymax, max_mm, args.linewidth)

    return 0

if __name__ == "__main__":
    sys.exit(main())
