#!/usr/bin/env Rscript

args<-commandArgs(TRUE)

lib1 = args[1]
lib2 = args[2]
out_dir = args[3]
num_slices = args[4]
slice_size = args[5]
read_type = args[6]
mapping_ref = args[7]
ref = args[8]
min_overlap = args[9]
max_overlap = args[10]
overlap_range = paste(min_overlap, max_overlap, sep="-")

pp_dir = paste("ping_pong", num_slices, "slices", sep="_")

#lib1 = "shwhite"
#lib2 = "shpiwi"
#directory = "."
#num_slices = 4
#read_type = "piRNA_candidate"
#mapping_ref = "D_melanogaster_and_TE"

control = c()
for (slice_num in 1:num_slices) {
    directory = paste(lib1, pp_dir, ref, slice_size, sep="/")
    filename = paste(lib1, read_type, slice_num, "on", mapping_ref, overlap_range, "signature.xls", sep="_")
    #print(directory)
    #print(filename)
    data = read.table(paste(directory, filename, sep="/"), sep="\t", header = TRUE)
    #control = rbind(control, data$overlap_prob)
    control = rbind(control, data$probability)
}

mutant = c()
for (slice_num in 1:num_slices) {
    directory = paste(lib2, pp_dir, ref, slice_size, sep="/")
    filename = paste(lib2, read_type, slice_num, "on", mapping_ref, overlap_range, "signature.xls", sep="_")
    #print(directory)
    #print(filename)
    data = read.table(paste(directory, filename, sep="/"), sep="\t", header = TRUE)
    #mutant = rbind(mutant, data$overlap_prob)
    mutant = rbind(mutant, data$probability)
}


out_file = paste(lib1, lib2, read_type, "on", mapping_ref, overlap_range, "signature_boxplot.pdf", sep="_")

pdf(file=paste(out_dir, out_file, sep="/"), width=30, height=10)

boxplot(control, axes=FALSE, border="red", xlab=paste("overlap between F and R", mapping_ref, "mapping reads"), ylab="overlap probability")
par(new = TRUE)
boxplot(mutant, axes=FALSE, border="blue")
axis(1, labels = c(1:80), at = c(1:80))
axis(2)
legend(x="top", c(lib1, lib2), col=c("red", "blue"), pch=0)

dev.off()
