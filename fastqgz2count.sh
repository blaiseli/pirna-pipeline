#!/usr/bin/env bash
# This script needs fastq_to_fasta from fastx toolkit

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}


fastqgz=${1}
count_file="${fastqgz%.fastq.gz}_count.txt"

echo -e "Read\tCount" > ${count_file}

zcat ${fastqgz} \
    | fastq_to_fasta -n \
    | grep -v "^>" \
    | sort | uniq -c \
    | awk '{print $2"\t"$1}' \
    >> ${count_file}


exit 0
