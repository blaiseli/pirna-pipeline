#!/usr/bin/env python
# vim: set fileencoding=<utf-8> :
"""This script reads data from 2-columns tab-separated text files, as generated
by fastq2histo.awk. It also reads files containing corresponding normalization
values to apply and makes plots out of it, at the same scale."""


import argparse
import sys
WRITE = sys.stdout.write

# To avoid repeatedly calling these as string methods:
from string import split, strip
from re import sub

import warnings


def formatwarning(message, category, filename, lineno, line):
    """Used to format warning messages."""
    return "%s:%s: %s: %s" % (filename, lineno, category.__name__, message)
warnings.formatwarning = formatwarning

import os
OPJ = os.path.join
OPB = os.path.basename

from itertools import izip
import matplotlib
# To be able to run the script without a defined $DISPLAY
matplotlib.use("PDF")
#import pylab
import matplotlib.pyplot as plt


class Histogram(object):
    """Container for histogram data."""
    __slots__ = ("title", "norm_factor", "x_values", "y_values", "y_max")

    def __init__(self, input_file_name, norm_factor, title):
        self.title = title
        self.norm_factor = norm_factor
        self.x_values = []
        self.y_values = []
        self.y_max = 0
        self.get_data_from(input_file_name)

    def get_data_from(self, input_file_name):
        """Reads histogram data from a tab-separated file."""
        with open(input_file_name, "rb") as input_file:
            for line in map(strip, input_file):
                # Skip comments.
                if line[0] == "#":
                    continue
                fields = split(line)
                self.x_values.append(int(fields[0]))
                y_value = float(fields[1]) / self.norm_factor
                self.y_max = max(self.y_max, y_value)
                self.y_values.append(y_value)

    def plot(self, xmin, xmax, ymin, ymax, x_axis, y_axis,
             output_files=None,
             #legend_size=9,
             label_size=12, tick_size=12):
        """Plots the histogram into some graphic files."""
        # https://www.quantifiedcode.com/app/issue_class/3P0qV6OB
        output_files = output_files or [os.devnull]
        plt.axis([xmin - 0.5, xmax + 0.5, ymin, ymax])
        # TODO: set title font size
        # TODO: select colour
        # TODO: make bars thinner
        plt.title(self.title)
        axis = plt.gca()
        axis.set_autoscale_on(False)
        axis.bar(
            self.x_values, self.y_values,
            edgecolor="red",
            #facecolor=plt.cm.Oranges(1 / (xmax + 1.0)),
            facecolor="red",
            #label="reads",
            align="center")
        #axis.legend(framealpha=0.5, ncol=2, prop={"size": 9}, loc=9)
        # Automatic choice of legend placement: loc=0
        axis.xaxis.label.set_fontsize(label_size)
        axis.yaxis.label.set_fontsize(label_size)
        for tick in axis.get_xticklabels():
            tick.set_fontsize(tick_size)
        for tick in axis.get_yticklabels():
            tick.set_fontsize(tick_size)
        #axis.legend(framealpha=0.5, ncol=1, prop={"size": legend_size}, loc=0)
        axis.set_xlabel(x_axis)
        axis.set_ylabel(y_axis)
        plt.tight_layout()
        #plt.legend(loc="lower right")
        for out in output_files:
            plt.savefig(out)
        # To avoid successive plots from superimposing on one another
        plt.cla()


def main():
    """Main function of the program."""
    WRITE("%s\n" % " ".join(sys.argv))
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        "-i", "--in_files",
        nargs="*",
        required=True,
        help="Space-separated list of histogram files.\n"
        "Each file must be a 2-column tab-separated file.\n"
        "First column will be used as x-axis, and second as y-axis.")
    parser.add_argument(
        "-n", "--normalizations",
        nargs="*",
        required=True,
        help="Space-separated list of files containing the values used "
        "to normalize the counts in the histogram data.\n"
        "Of course, the file number and order should correspond to that "
        "given in the option --in_files.")
    parser.add_argument(
        "-t", "--titles",
        nargs="*",
        required=True,
        help="Space-separated list of double-quoted titles for the graphs.\n"
        "Of course, the file number and order should correspond to that "
        "given in the option --in_files.")
    parser.add_argument(
        "-d", "--hist_dir",
        required=True,
        help="Directory in which histograms should be written.")
    parser.add_argument(
        "--label_size",
        type=int,
        help="Font size for axis labels",
        #default=12)
        default=24)
    parser.add_argument(
        "--legend_size",
        type=int,
        help="Font size for legend labels",
        #default=9)
        default=18)
    parser.add_argument(
        "--tick_size",
        type=int,
        help="Font size for ticks",
        #default=12)
        default=18)
    parser.add_argument(
        "-x", "--x_axis",
        help="Label for the x axis of the histograms.",
        default="read length")
    parser.add_argument(
        "-y", "--y_axis",
        help="Label for the y axis of the histograms.",
        default="number of reads")
    #parser.add_argument("-s", "--switch",
    #        help="This is an optional boolean switch.",
    #        action="store_true")
    args = parser.parse_args()
    err_msg = "".join([
        "There must be the same number ",
        "of normalization values ",
        "as there are data input files."])
    assert len(args.normalizations) == len(args.in_files), err_msg
    err_msg = "".join([
        "There must be the same number of titles ",
        "as there are data input files."])
    assert len(args.titles) == len(args.in_files), err_msg
    # We match input files and titles together,
    # assuming the command-line order match.
    input_iterator = izip(
        args.in_files, args.normalizations, args.titles)
    # key: histogram title
    # value: Histogram object
    data = {}
    for (input_file_name, norm_file_name, title) in input_iterator:
        with open(norm_file_name, "r") as norm_file:
            norm = float(strip(norm_file.readline()))
        #experiment = split(OPB(input_file_name), "_on_")[0]
        data[title] = Histogram(input_file_name, norm, title)
    # Determine overall xmin, xmax, ymin, ymax
    # to scale all histograms to the same size
    #TODO: do this in one loop only if possible
    x_min = None
    x_max = 0
    for histo in data.values():
        if x_min is None:
            x_min = min(histo.x_values)
        else:
            x_min = min(x_min, min(histo.x_values))
        x_max = max(x_max, max(histo.x_values))
    y_min = 0
    y_max = max(histo.y_max for histo in data.values())

    for histo in data.values():
        out_pdf = OPJ(
            args.hist_dir,
            "%s.pdf" % (sub(" ", "_", histo.title)))
#        out_png = OPJ(
#            args.hist_dir,
#            "%s.png" % (sub(" ", "_", histo.title)))
        histo.plot(
            x_min, x_max, y_min, y_max,
            args.x_axis, args.y_axis,
            [out_pdf],
            #args.legend_size,
            args.label_size, args.tick_size)

if __name__ == "__main__":
    sys.exit(main())
