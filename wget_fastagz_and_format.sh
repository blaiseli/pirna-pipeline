#!/usr/bin/env bash
# wget_fastagz_and_format.sh <url of .fasta.gz> <local name without extensions>

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}


gz=`basename ${1}`
name=${gz%.fasta.gz}
new_name=${2}

cmd="wget --continue ${1}"
echo ${cmd}
eval ${cmd} || error_exit "${cmd} failed"

cmd="md5sum ${gz}"
echo ${cmd}
eval ${cmd} || error_exit "${cmd} failed"

cmd="gunzip ${gz}"
echo ${cmd}
eval ${cmd} || error_exit "${cmd} failed"

cmd="fuse_fasta_lines.py --in_file ${name}.fasta > ${name}.fa && rm -f ${name}.fasta"
echo ${cmd}
eval ${cmd} || error_exit "${cmd} failed"

cmd="ln -s ${name}.fa ${new_name}.fa"
echo ${cmd}
eval ${cmd} || error_exit "${cmd} failed"

cmd="bowtie2-build --seed 123 --packed ${new_name}.fa ${new_name}"
echo ${cmd}
eval ${cmd} || error_exit "${cmd} failed"

# No need to waste space:
# build_annotated_genome.py could use directly the fasta files
#grep "^>" ${new_name}.fa > ${new_name}.txt
