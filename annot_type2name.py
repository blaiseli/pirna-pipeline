#!/usr/bin/env python
"""This script gives an x annotation type short name when given as input an
annotation type as in FlyBase fasta filenames."""

import argparse
import sys
import warnings
def formatwarning(message, category, filename, lineno, line):
    """Generates the formatted warning message."""
    return "%s:%s: %s:%s" % (filename, lineno, category.__name__, message)
warnings.formatwarning = formatwarning

type2name = {
    "dmel-all-CDS" : "CDS",
    "dmel-all-five_prime_UTR" : "5UTR",
    "dmel-all-miRNA" : "miRNA",
    "dmel-all-miscRNA" : "miscRNA",
    "dmel-all-ncRNA" : "ncRNA",
    "dmel-all-three_prime_UTR" : "3UTR",
    "dmel-all-transcript" : "mRNA",
    "dmel-all-transposon" : "transposable_elements",
    "dmel-all-tRNA" : "tRNA",
    "dmel-dmel_mitochondrion_genome-CDS" : "mitochCDS",
    "dmel-dmel_mitochondrion_genome-miscRNA" : "mitochmiscRNA",
    "dmel-dmel_mitochondrion_genome-tRNA" : "mitochtRNA",
    "dmel-dmel_mitochondrion_genome-transcript" : "mitochtmRNA"}


def main():
    """Main function."""
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        "annot_type",
        help="Name of the annotation type as in FlyBase fasta filename.")
    args = parser.parse_args()
    sys.stdout.write("%s\n" % type2name[args.annot_type])
    return 0


if __name__ == "__main__":
    sys.exit(main())
