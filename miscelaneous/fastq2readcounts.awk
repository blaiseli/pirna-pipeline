#!/usr/bin/awk -f
# Usage: fastq2readcounts.awk <file.fastq>

#BEGIN {print "#read\tcount"}
# Take every second-out-of-four line
# and count its sequence in the histogram
NR%4==2 {histo[$0]++}
# At the end, display the results
END {for (seq in histo) print seq"\t"histo[seq]}
