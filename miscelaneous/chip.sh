#!/usr/bin/env bash
# Usage: chip.sh <library_name> <G_species> <number_of_cpus_to_use> [<annotation_dir>]

# To configure according to the local installation:
genomes_path=${GENOMES}

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}

PLACE=`pwd`
echo "Analyses taking place with base directory ${PLACE}"
echo -e "\n-----"
date +"%d/%m/%Y %T"

name=${1}
#mkdir -p ${name}

#TODO: one day, take more genomes into account, using a case
#genome="D_melanogaster"
genome=${2}
if [ ${genome} == "D_melanogaster" ]
then
    gshort="dm3"
else
    error_exit "${genome} not supported for the moment"
fi

md5sums="${name}/md5sums.txt"
compute_md5="nice -n 19 ionice -c2 -n7 md5sum"

# TODO: one day, make a function that calculates md5sums in background, queuing them?
#function next_md5
#{
#    nice -n 19 ionice -c2 -n7 md5sum ${1} >> ${md5sums} &
#    last_md5_pid=$!
#    exit 0
#}

# first argument: fastq file
function fastqc_report
{
    report_dir=`dirname ${1}`/fastqc_report
    if [ ! -e  ${report_dir} ]
    then
        mkdir ${report_dir}
        cmd="fastqc -o ${report_dir} --noextract -f fastq ${1}"
        nice -n 19 ionice -c2 -n7 ${cmd} || error_exit "${cmd} failed"
    else
        echo "${report_dir} already exists. Skipping fastqc reporting."
    fi
}

Input_1="raw_data/${name}/${name}_Input_1.fastq"
Input_2="raw_data/${name}/${name}_Input_2.fastq"
IP_1="raw_data/${name}/${name}_IP_1.fastq"
IP_2="raw_data/${name}/${name}_IP_2.fastq"

fastq_files="${Input_1} ${Input_2} ${IP_1} ${IP_2}"

#TODO: see if fastqc can deal with compressed files,
# and if so, avoid decompressing already compressed files
# and update the Input and IP variables accordingly for use with piPipes.
for fastq_file in ${fastq_files}
do
    echo "Testing existence of ${fastq_file}"
    if [ ! -e ${fastq_file} ]
    then
        echo "File not found. Testing existence of a compressed version."
        if [ ! -e ${fastq_file}.gz ]
        then
            if [ ! -e ${fastq_file}.bz2 ]
            then
                error_exit "No .gz nor .bz2 version found."
            else
                eval ${compute_md5} ${fastq_file}.bz2 >> ${md5sums} &
                cmd="bzcat ${fastq_file}.bz2 > ${fastq_file}"
                echo ${cmd}
                eval ${cmd} || error_exit "${cmd} failed"
            fi
        else
            eval ${compute_md5} ${fastq_file}.gz >> ${md5sums} &
            cmd="zcat ${fastq_file}.gz > ${fastq_file}"
            echo ${cmd}
            eval ${cmd} || error_exit "${cmd} failed"
        fi
    else
        eval ${compute_md5} ${fastq_file} >> ${md5sums} &
        echo "File found."
    fi
done

# Make fastqc report for the raw data
# Log this separately to re-use later
tmpdir=$(mktemp -dt "`basename $0`.XXXXXXXXXX")
cmd="fastqc_report ${fastq_files}"
echo ${cmd}
eval ${cmd} > ${tmpdir}/fastqc.log 2> ${tmpdir}/fastqc.err &


if [ $# = 4 ]
then
    annotation_path=${4}
else
    annotation_path="${genomes_path}/${genome}/Annotations/"
fi

echo -e "\n-----"
date +"%d/%m/%Y %T"

ncpu=${3}

# We only want to have ChIP profiles for piRNA clusters 1-8 and 17

beds=""
for n in "1" "2" "3" "4" "5" "6" "7" "9"
do
    beds="${beds}${genomes_path}/${genome}/Annotations/piRNA_clusters/piRNA_cluster${n}_annotation.bed,"
done
beds="${beds}${genomes_path}/${genome}/Annotations/piRNA_clusters/piRNA_cluster17_annotation.bed"

mkdir -p chip/${name}
#TODO: run piPipes chip with -M option on cluster bed files (in ${genomes_path}/${genome}/Annotations/piRNA_clusters/piRNA_cluster${n}_annotation.bed)
piPipes chip \
    -l ${IP_1} \
    -r ${IP_2} \
    -L ${Input_1} \
    -R ${Input_2} \
    -g ${gshort} \
    -M ${beds} \
    -c ${ncpu} \
    -B \
    -m \
    -o chip/${name} \

# Note: there is a -D option to delete intermediate file: maybe we could use it.

echo -e "\n\n-----"
date +"%d/%m/%Y %T"

# Wait for the end of fastqc reporting
echo -e "\nWaiting for ${fastq_file} fastqc report."
wait
# cat the fastqc logs to stdout and stderr
cat ${tmpdir}/fastqc.log
(1>&2 cat ${tmpdir}/fastqc.err)
rm -rf ${tmpdir}


exit 0
