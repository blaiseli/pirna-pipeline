#!/usr/bin/env python
# vim: set fileencoding=<utf-8> :
"""This script reads a bam file and splits it according to some criteria."""


import argparse
import sys
WRITE = sys.stdout.write
import warnings


def formatwarning(message, category, filename, lineno, line):
    """Formats warning messages."""
    return "%s:%s: %s:%s\n" % (filename, lineno, category.__name__, message)
warnings.formatwarning = formatwarning

import os
OPJ = os.path.join
OPB = os.path.basename

from pysam import AlignmentFile
#from Bio.Seq import reverse_complement
from string import maketrans
DNA_COMPL_TABLE = maketrans("ACGT", "TGCA")


def reverse_complement(seq):
    """Faster than Bio.Seq.reverse_complement"""
    return seq.translate(DNA_COMPL_TABLE)[::-1]


def write_reads(alis):
    """This function writes the alignments in the list *alis* as fastq
    sequences in the files corresponding to their status: first in pair, second
    in pair, or without present mate."""

    num_alis = len(alis)
    ali = alis[0]
    if num_alis == 1:
        Globals.fastq["s"].write(ali2fq(ali))
    elif num_alis == 2:
        if ali.is_read1:
            Globals.fastq["1"].write(ali2fq(ali))
            assert alis[1].is_read2
            Globals.fastq["2"].write(ali2fq(alis[1]))
        else:
            assert ali.is_read2
            Globals.fastq["2"].write(ali2fq(ali))
            assert alis[1].is_read1
            Globals.fastq["1"].write(ali2fq(alis[1]))
    else:
        raise NotImplementedError


def ali2fq(ali):
    """This function generates the fastq representation of an aligned read."""
    if ali.is_reverse:
        seq = reverse_complement(ali.seq)
        qual = ali.qual[::-1]
    else:
        seq = ali.seq
        qual = ali.qual
    return "@%s\n%s\n+\n%s\n" % (ali.qname, seq, qual)


#####################################
# For better traceability,          #
# options and global variables      #
# can be stored as class attributes #
#####################################
class Globals(object):
    """This object holds global variables."""


class Options(object):
    """This object contains the values of the global options."""

#################
# main function #
#################


def main():
    """Main function of the program."""
    WRITE("%s\n" % " ".join(sys.argv))
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        "in_file",
        help="Input bam file")
    parser.add_argument(
        "-p", "--fastq_prefix",
        required=True,
        help="Prefix to use for the fastq file names.")
    parser.add_argument(
        "-o", "--out_dir",
        required=True,
        help="Directory in which to write the fastq files.")
    args = parser.parse_args()

    Globals.fastq = {}
    Globals.fastq["1"] = open(OPJ(
        args.out_dir,
        "%s_1.fq" % args.fastq_prefix), "w")
    Globals.fastq["2"] = open(OPJ(
        args.out_dir,
        "%s_2.fq" % args.fastq_prefix), "w")
    Globals.fastq["s"] = open(OPJ(
        args.out_dir,
        "%s_s.fq" % args.fastq_prefix), "w")

    in_bam = AlignmentFile(args.in_file, "rb")
    alis = []
    for ali in in_bam.fetch(until_eof=True, multiple_iterators=False):
        read_name = ali.qname
        if alis:
            if read_name != alis[-1].qname:
                write_reads(alis)
                alis = [ali]
            else:
                alis.append(ali)
        else:
            alis.append(ali)
    if alis:
        write_reads(alis)

    for fastq_file in Globals.fastq.values():
        fastq_file.close()

    return 0

if __name__ == "__main__":
    sys.exit(main())
