#!/usr/bin/env bash
# Usage: install_raw_data.sh <library_name> <raw_data_file>
# Creates a "<library_name>" sub-directory in raw_data
# Puts a .fastq file or a symbolic link to a .fastq file inside
# the library sub-directory using the file <raw_data_file>
# <raw_data_file> has to be a fastq file or compressed fastq file.

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}


# Library name:
name=${1}

origin=${2}

dest="raw_data/${name}/${name}.fastq"

mkdir -p raw_data/${name}

if [ -f ${dest} -a ! -L ${dest} ]
then
    echo
    echo "${dest} already exists, and not as a symbolic link."
    echo "This script does not want to erase existing data."
    echo "Either remove the file manually first, either do not run this script."
    echo "Aborting"
    exit 1
else
    the_dir=`pwd`
    echo
    echo "Trying to install ${dest} in ${the_dir}."
    echo "Using ${origin} as raw data source".
    echo
fi

if [[ ${origin} =~ \.gz ]]
then
    cmd="zcat ${origin} > ${dest}"
    echo ${cmd}
    eval ${cmd} || error_exit "${cmd} failed"
elif [[ ${origin} =~ \.bz2 ]]
then
    cmd="bzcat ${origin} > ${dest}"
    echo ${cmd}
    eval ${cmd} || error_exit "${cmd} failed"
elif [[ ${origin} =~ \.fastq ]]
then
    cmd="ln -srf ${origin} ${dest}"
    echo ${cmd}
    eval ${cmd} || error_exit "${cmd} failed"
elif [[ ${origin} =~ \.fq ]]
then
    cmd="ln -srf ${origin} ${dest}"
    echo ${cmd}
    eval ${cmd} || error_exit "${cmd} failed"
elif [[ ${origin} =~ \.sra ]]
then
    cmd="fastq-dump --stdout -M 10 ${origin} > ${dest}"
    echo ${cmd}
    eval ${cmd} || error_exit "${cmd} failed"
elif [[ ${origin} =~ SRR* ]]
then
    cmd="fastq-dump --stdout -M 10 ${origin} > ${dest}"
    echo ${cmd}
    eval ${cmd} || error_exit "${cmd} failed"
else
    echo "The raw data file doesn't end in .gz, .bz2, .fastq or .fq"
    echo "Aborting"
    exit 1
fi

echo
echo "${dest} installed in ${the_dir}."
echo "Carefully check that file names and paths are OK before starting the analyses."
echo
echo "Note that the directory from which you will start the analyses should be the same as the directory from which you launched the present data-installation script."
echo "(${the_dir})"

exit 0
