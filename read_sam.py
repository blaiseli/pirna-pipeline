#!/usr/bin/env python
# pylint: disable=C0103
"""
usage: read_sam.py [options]

This script will read sam-formatted alignments and perform various actions.

available options:

    --help: prints this help text and exits.

    --in_file <file>: to specify the indexed .bam file containing the mapping
    information to process.

    --min_mapq: <int>: to specify the minimum value of mapq mapping quality for
    a read to be recorded. The default value is 0. (Not used)

    --min_score: <float>: to specify the minimum value mapping score for an
    alignment to be taken into account. The default value is -200.

    --hist_dir <directory>: to specify the directory in which lengths
    histograms of mapped reads should be written.

    --seq_dir <directory>: to specify the directory in which mapped reads
    should be written.

"""


import getopt
import os
OPJ = os.path.join
OPB = os.path.basename
#import re
import sys
WRITE = sys.stdout.write
import warnings
def formatwarning(message, category, filename, lineno, line):
    """Used to format warning messages."""
    return "%s:%s: %s: %s" % (filename, lineno, category.__name__, message)
warnings.formatwarning = formatwarning

from collections import defaultdict
from itertools import islice
from operator import or_ as union

import csv

import pysam

from Bio.Seq import reverse_complement


def oui(question):
    """oui(question) returns True if the user answers yes to the question
    <question>."""
    return (raw_input("\n" + question + " (y/n) ") in [
        "O", "o", "Oui", "oui", "OUI",
        "Y", "y", "Yes", "yes", "YES",
        "J", "j", "Ja", "ja", "JA",
        "S", "s", "Si", "si", "SI",
        "D", "d", "Da", "da", "DA",
        "T", "t", "Taip", "taip", "TAIP"])


# TODO: Check that 0mm and mm are dealt with correctly,
# and also in other scripts reading the generated histogram data
# plot_histos.py
def write_histos(name, max_mm, out=sys.stdout):
    """This function writes histogram data for reference <name>
    in <out> (by default sys.stdout).
    <max_mm> is the maximum recorded number of mismatches."""
    writer = csv.writer(out, dialect="excel-tab")
    writer.writerow(["#size histograms for maps from %s on %s" % (
        Options.in_file, name)])
    writer.writerow(["#ref", "%s" % name])
    headers = [
        "size", "F", "R", "F_0mm", "R_0mm", "F_mm", "R_mm"]
    for nb_mm in xrange(1, max_mm + 1):
        headers.extend(("F_%d_mm" % nb_mm, "R_%d_mm" % nb_mm))
    writer.writerow(headers)
    histo_fwd = Globals.histos_fwd[name]
    histo_rev = Globals.histos_rev[name]
    lens_fwd = reduce(
        union,
        [set(histo_fwd[nb_mm].keys()) for nb_mm in xrange(max_mm + 1)])
    lens_rev = reduce(
        union,
        [set(histo_rev[nb_mm].keys()) for nb_mm in xrange(max_mm + 1)])
    lengths = lens_fwd | lens_rev
    if lengths:
        minlen = min(lengths)
        maxlen = max(lengths)
        curr_len = minlen
        while curr_len <= maxlen:
            abs_fwd_0mm = histo_fwd[0][curr_len]
            abs_rev_0mm = histo_rev[0][curr_len]
            #abs_fwd_mm = 0
            #abs_rev_mm = 0
            extra_cols = []
            for nb_mm in xrange(1, max_mm + 1):
                extra_cols.extend(
                    (histo_fwd[nb_mm][curr_len],
                        histo_rev[nb_mm][curr_len]))
                #abs_fwd_mm += histo_fwd[nb_mm][curr_len]
                #abs_rev_mm += histo_rev[nb_mm][curr_len]
            #abs_fwd_mm = sum(compress(extra_cols, cycle([1, 0])))
            #abs_rev_mm = sum(compress(extra_cols, cycle([0, 1])))
            abs_fwd_mm = sum(islice(extra_cols, 0, None, 2))
            abs_rev_mm = sum(islice(extra_cols, 1, None, 2))
            abs_fwd = abs_fwd_mm + abs_fwd_0mm
            abs_rev = abs_rev_mm + abs_rev_0mm
            row = [
                curr_len, abs_fwd, abs_rev,
                abs_fwd_0mm, abs_rev_0mm, abs_fwd_mm, abs_rev_mm]
            row += extra_cols
            writer.writerow(row)
            curr_len += 1
    else:
        writer.writerow(["#empty"])

# Doesn't work, because, contrary to the definition given in
# http://bowtie-bio.sourceforge.net/bowtie2/manual.shtml#sam-output,
# the XS value for a non-best alignment seems to be the second-best AS value,
# so XS == AS doesn't guarantee that the alignment is best-scoring:
# is can be a second-best
#def best(ali):
#    opt = ali.opt
#    try:
#        score = opt("AS")
#    except KeyError:
#        # No score, so this alignment is not the best
#        return False
#    try:
#        return score >= opt("XS")
#    except KeyError:
#        # No best score for the others, because there are no others
#        return True


def report(to_report, low_score, fastq_info, ref, nb_refs, max_mm):
    """Report alignment results for a read on a given reference <ref>.
    <to_report> and <low_score> are lists of alignments of the read.
    All alignments in <to_report> or in <low_score> should be the
    best-scoring ex-aequo. At least one of the two lists should be empty.
    <fastq_info> should be a tuple (qname, seq_len, seq, qual).
    It is used to write a .fastq of the read,
    either in a '0mm', 'mm' or 'low_score' file.
    <nb_refs> is the number of references for which the read
    has alignments to report. This is used to weight global counts
    of the read.
    <max_mm> is the maximum number of mismatches encountered so far
    for a best-scoring alignement of a non-low-score read.
    """
    # TODO: ponderer les alignements F et R dans les histogrammes
    # par le nombre d'alignements retenus (F+R) DONE
    # TODO: check all
    (qname, seq_len, seq, qual) = fastq_info
    ## debug
    seq_written = False
    if to_report:
        assert len(low_score) == 0
        # To weight counts in the histograms
        # (in case several alignments for the same reference are reported):
        #tr = len(to_report)
        #mismatches = [ali.opt("NM") for ali in to_report]
        #least_mm = min(mismatches)
        # Note: we don't want to consider only those of least mismatches,
        # but all ex-aequo
        ## Note on the Note: Ex-aequo with equal number of mm are unlikely
        ## Besides, do we really want to count as 0mm on the ref
        ## the hypothetical ex-aequo mismatched alignments of a read
        ## that has at least a 0mm alignment on that ref?
        least_mm = min([ali.opt("NM") for ali in to_report])
        #if all([ali.opt("NM") for ali in to_report]):
        # Note that the fastq file will be the same for 0mm and mm
        # if Options.separate_0mm == False
        if least_mm:
            # There are no perfect matches (least_mm > 0)
            # Write the fastq file for mismatches
            fastq_file = open(Globals.fastq["mm"][ref], "a")
            fastq_file.write(
                "@%s (len=%d)\n%s\n+\n%s\n" % (qname, seq_len, seq, qual))
            fastq_file.close()
            seq_written = True
        else:
            # There is at least one alignment with 0 mismatches
            # Write the fastq file for no mismatches
            fastq_file = open(Globals.fastq["0mm"][ref], "a")
            fastq_file.write(
                "@%s (len=%d)\n%s\n+\n%s\n" % (qname, seq_len, seq, qual))
            fastq_file.close()
            seq_written = True
        max_mm = max(least_mm, max_mm)

        def is_least_mismatched(ali):
            """Checks whether <ali> has the least number of mismatches."""
            return ali.opt("NM") == least_mm
        ## select the alignments with the least mismatches
        # Take into account only the least mismatched alignments
        alis = [ali for ali in to_report if is_least_mismatched(ali)]
        # To weight counts in the histograms
        # (in case several alignments for the same reference are reported):
        # "floatify" to avoid integer division
        nb_alis = float(len(alis))
        alis_fwd = [ali for ali in alis if not ali.is_reverse]
        nb_fwd = len(alis_fwd)
        alis_rev = [ali for ali in alis if ali.is_reverse]
        nb_rev = len(alis_rev)
        # Weighting counts for "tot" by inverse number of references
        Globals.histos_fwd["tot"][least_mm][seq_len] += (
            nb_fwd / (nb_alis * nb_refs))
        Globals.histos_rev["tot"][least_mm][seq_len] += (
            nb_rev / (nb_alis * nb_refs))
        Globals.histos_fwd[ref][least_mm][seq_len] += (nb_fwd / nb_alis)
        Globals.histos_rev[ref][least_mm][seq_len] += (nb_rev / nb_alis)
        Globals.ref_counts_fwd["tot"][least_mm] += (nb_fwd / (nb_alis * nb_refs))
        Globals.ref_counts_rev["tot"][least_mm] += (nb_rev / (nb_alis * nb_refs))
        Globals.ref_counts_fwd[ref][least_mm] += (nb_fwd / nb_alis)
        Globals.ref_counts_rev[ref][least_mm] += (nb_rev / nb_alis)
    elif low_score:
        with open(Globals.fastq["low_score"][ref], "a") as fastq_file:
            fastq_file.write(
                "@%s (len=%d)\n%s\n+\n%s\n" % (qname, seq_len, seq, qual))
        seq_written = True
    else:
        pass
        #warnings.warn("No alignments for read %s on %s" % (qname, ref))
    return seq_written, max_mm

#####################################################################
# For better traceability,                                          #
# options and global variables should be stored as class attributes #
#####################################################################


class Globals(object):
    """This object holds global variables."""


class Options(object):
    """This object contains the values of the global options."""
    in_file = None
    min_mapq = 0
    min_score = -200
    hist_dir = None
    seq_dir = None
    # This has to be set to False if we want to be able to use slices of a
    # given type of read mapping on a given reference. The randomness of slices
    # is not valid if the file from which the slices are made is made from the
    # concatenation of several files.
    separate_0mm = False

#################
# main function #
#################


def main():
    """Main function of the program."""
    WRITE(" ".join(sys.argv) + "\n")
    try:
        opts, args = getopt.getopt(
            sys.argv[1:], "io:",
            ["help", "in_file=", "min_mapq=", "min_score=",
                "hist_dir=", "seq_dir="])
    except getopt.GetoptError:
        sys.stderr.write(__doc__)
        sys.exit(1)
    assert len(args) == 0, "Extra arguments not used."
    for option, argument in opts:
        if option == "--help":
            WRITE(__doc__)
            sys.exit()
        if option == "--in_file":
            Options.in_file = argument
        if option == "--min_mapq":
            Options.min_mapq = int(argument)
        if option == "--min_score":
            Options.min_score = float(argument)
        if option == "--hist_dir":
            Options.hist_dir = argument
        if option == "--seq_dir":
            Options.seq_dir = argument

    if Options.in_file is None:
        sys.stderr.write(
            "You should provide a sorted and indexed "
            "*_sorted.bam file with the option --in_file\n")
        sys.exit(1)
    if Options.hist_dir is None:
        sys.stderr.write(
            "You should provide a directory "
            "in which the histogram files will be written "
            "with the option --hist_dir\n")
        sys.exit(1)
    if Options.seq_dir is None:
        sys.stderr.write(
            "You should provide a directory "
            "in which the sequence files will be written "
            "with the option --seq_dir\n")
        sys.exit(1)
    if Options.in_file.endswith(".bam"):
        samfile = pysam.Samfile(Options.in_file, "rb")
    else:
        samfile = pysam.Samfile(Options.in_file, "r")
    refs = samfile.references
    # Number of sequences attributed to each reference
    # A sequence attributed to several references will be counted as one in
    # each of the corresponding dict, but distributed between F and R
    # Two dictionaries of dictionaries.
    # keys: references
    # values: dictionaries
    #   keys: number of mismatches
    #   values: number of reads for this ref
    #   for this direction for this number of mismatches
    #   (weighted by proportion of same-reference alignments
    #   of the read that have that direction)
    Globals.ref_counts_fwd = defaultdict(lambda: defaultdict(float))
    Globals.ref_counts_rev = defaultdict(lambda: defaultdict(float))
    getrname = samfile.getrname
    # length histograms for each region for each number of mismatches
    # key: ref
    # value: dict
    #        key: nb_mm
    #        value: dict
    #               key: length
    #               value: weighted count
    Globals.histos_fwd = defaultdict(
        lambda: defaultdict(lambda: defaultdict(float)))
    Globals.histos_rev = defaultdict(
        lambda: defaultdict(lambda: defaultdict(float)))
    # fasta file names
    Globals.fastq = {}
    Globals.fastq["0mm"] = {}
    Globals.fastq["mm"] = {}
    Globals.fastq["low_score"] = {}
    # keys are names of reference sequences, values are files
    # histo_file = {}
    # counts of QC and NM reads
    #QC = 0
    #NM = 0
    # for each region, a list of alignments of a given read
    # (the list is reset for each read, which requires
    # the alignments of a given read to be grouped in the file)
    to_report = {}
    low_score = {}
    # set of references a read has mapped to
    read_refs = set([])
    # to know whether a sequence has been recorded for the current read
    no_seq = True
    # counts of sub-maximal alignments (useless data)
    #submax = {}
    nb_seq_written = 0
    for ref in refs:
        basename = "_".join(OPB(Options.in_file).split("_")[:-1]) + "_" + ref
        # Creating file names
        # opening and closing the files
        # to overwrite possibly already existing ones
        # (will re-open later in "a" mode to append sequences)
        # (This is to avoid "IOError: [Errno 24] Too many open files")
        if Options.separate_0mm:
            # This distinguishes between 0mm and mm
            Globals.fastq["0mm"][ref] = OPJ(
                Options.seq_dir,
                "%s_0mm.fastq" % basename)
            fastq_file = open(Globals.fastq["0mm"][ref], "w")
            fastq_file.close()
            Globals.fastq["mm"][ref] = OPJ(
                Options.seq_dir,
                "%s_mm.fastq" % basename)
            fastq_file = open(Globals.fastq["mm"][ref], "w")
            fastq_file.close()
        else:
            # Maybe we don't want to bother with that distinction
            fastq_filename = OPJ(
                Options.seq_dir,
                "%s.fastq" % basename)
            Globals.fastq["0mm"][ref] = fastq_filename
            Globals.fastq["mm"][ref] = fastq_filename
            fastq_file = open(fastq_filename, "w")
            fastq_file.close()
        # For low_score, we don't bother
        Globals.fastq["low_score"][ref] = OPJ(
            Options.seq_dir,
            "%s_low_score.fastq" % basename)
        fastq_file = open(Globals.fastq["low_score"][ref], "w")
        fastq_file.close()
        #histo_file[ref] = open(OPJ(Options.hist_dir,
        #    basename + "_lengths.txt"), "wb")
        # Not necessary with defaultdict
        #Globals.histos_fwd[ref] = {}
        #Globals.histos_rev[ref] = {}
        #submax[ref] = 0
        # The order in the file can be a problem because sorted by ref
        # Solution: use .sam file from bowtie
        to_report[ref] = []
        low_score[ref] = []
    # this loop relies on the fact that alignments are grouped by query names
    # but it is not sure this will be the case when fetching by reference
    # (It may be that fetching by reference requires sam input
    # to be sorted by ref)
    #for ali in samfile.fetch(reference=ref):
    ## debug:
    last_qname = None
    qnames = set([])
    ##
    # maximum number of mismatches found among best-scoring alignments
    max_mm = 0
    # read attributes
    rlen = None
    seq = None
    qual = None
    for ali in samfile.fetch():
        if ali.qname != last_qname:
            if last_qname is not None:
                assert last_qname not in qnames
                # Deal with the read attribution,
                # sequence and histogram data recording
                # Calculate ponderation for the "tot" histogram.
                nb_refs = sum([
                    1 if len(to_report[ref]) else 0 for ref in read_refs])
                for ref in read_refs:
                    seq_written, max_mm = report(
                        to_report[ref], low_score[ref],
                        (last_qname, rlen, seq, qual),
                        ref, nb_refs, max_mm)
                    if seq_written:
                        nb_seq_written += 1
                        seq_written = False
                    # reset lists of best-scoring reads
                    to_report[ref] = []
                    low_score[ref] = []
                read_refs = set([])
                qnames.add(last_qname)
            last_qname = ali.qname
            ## debug
            #print last_qname
            ##
            no_seq = True
        # Note: We could implement a filtering on ali.mapq
        # -> Did not seem useful
        # Note: We could collect info about mapping position
        # using ali.pos or ali.positions
        # Note: We could collect information about the alignment
        # using ali.cigar
        if not (ali.is_qcfail or ali.is_unmapped):
            # determine the reference on which the read was aligned
            ref = getrname(ali.tid)
            # primary alignment may have been on another reference
            #if not ali.is_secondary:
            # Use the first mapped read to set rlen, seq and qual
            if no_seq:
                rlen = ali.rlen
                if ali.is_reverse:
                    seq = reverse_complement(ali.seq)
                    qual = ali.qual[::-1]
                else:
                    seq = ali.seq
                    qual = ali.qual
                no_seq = False
            # Note: A partir du moment ou il existe,
            # parmi les alignements du read,
            # un alignement de meilleur score qui n'a pas de mismatch,
            # c'est sur cette ref qu'il mappe, on le compte en "0mm",
            # et on oublie les autres alignements.
            # Si tous les alignements de meilleur score ont des mismatches,
            # on compte les alignements en "mm"
            # TODO: Implementer la mise a l'ecart des alignements de
            # opt("XS") > opt("AS")
            # DONE using the "best" function
            # TODO: Implementer le \forall opt("NM") < 0
            # vs \exists opt("NM") == 0
            # parmi les opt("XS") == opt("AS")
            # being DONE in the report function
            # Attention: si le read n'a qu'un alignement
            # opt("XS") n'est pas defini.
            ## debug
            #for r in refs:
            #    if len(low_score[r]):
            #        s = low_score[r][0].opt("AS")
            #        assert all([a.opt("AS") == s for a in low_score[r]])
            #        assert len(to_report[r]) == 0
            #    if len(to_report[r]):
            #        s = to_report[r][0].opt("AS")
            #        assert all([a.opt("AS") == s for a in to_report[r]])
            #        assert len(low_score[r]) == 0
            ##
            best = True
            ex_aequo = True
            score = ali.opt("AS")
            for read_ref in read_refs:
                for match in to_report[read_ref] + low_score[read_ref]:
                    if match.opt("AS") > score:
                        best = False
                        ex_aequo = False
                        break
                    if match.opt("AS") < score:
                        ex_aequo = False
                        break
            if best:
                if not ex_aequo:
                    # This alignment is best than all the already recorded
                    # for reporting, reset the reporting lists
                    for read_ref in read_refs:
                        to_report[read_ref] = []
                        low_score[read_ref] = []
                    read_refs = set([ref])
                else:
                    read_refs.add(ref)
                for match in to_report[ref]:
                    assert match.qname == last_qname
                    assert match.opt("AS") == ali.opt("AS")
                for match in low_score[ref]:
                    assert match.qname == last_qname
                    assert match.opt("AS") == ali.opt("AS")
                if ali.opt("AS") >= Options.min_score:
                    to_report[ref].append(ali)
                else:
                    low_score[ref].append(ali)
            else:
                pass
                #submax[ref] += 1
    # Calculate ponderation for the "tot" histogram.
    nb_refs = sum([1 if len(to_report[ref]) else 0 for ref in read_refs])
    #for ref in sorted(read_refs):
    for ref in read_refs:
        # report the last read's alignments
        seq_written, max_mm = report(
            to_report[ref], low_score[ref],
            (last_qname, rlen, seq, qual),
            ref, nb_refs, max_mm)
        if seq_written:
            nb_seq_written += 1
            seq_written = False
    # Trying to make a correct name for the ref_counts file
    with open(OPJ(
            Options.hist_dir,
            "%s_ref_counts.txt" % (
                ".".join(OPB(
                    Options.in_file).split(".")[:-1]))), "wb") as counts_file:
        counts_writer = csv.writer(counts_file, dialect="excel-tab")
        headers = [
            "#ref", "total", "total_F", "total_rev",
            "F_0mm", "R_0mm", "F_mm", "R_mm"]
        # Extend header with alternating F and R counts
        # for successive values of nb_mm
        for nb_mm in xrange(1, max_mm + 1):
            headers.extend(("F_%d_mm" % nb_mm, "R_%d_mm" % nb_mm))
        counts_writer.writerow(headers)
        refs = sorted(refs)
        refs.append("tot")
        for ref in refs:
            nb_0mm_fwd = Globals.ref_counts_fwd[ref][0]
            nb_tot_fwd = sum(Globals.ref_counts_fwd[ref].values())
            nb_mm_fwd = nb_tot_fwd - nb_0mm_fwd
            nb_0mm_rev = Globals.ref_counts_rev[ref][0]
            nb_tot_rev = sum(Globals.ref_counts_rev[ref].values())
            nb_mm_rev = nb_tot_rev - nb_0mm_rev
            row = [
                ref, nb_tot_fwd + nb_tot_rev, nb_tot_fwd, nb_tot_rev,
                nb_0mm_fwd, nb_0mm_rev, nb_mm_fwd, nb_mm_rev]
            # Extend row with alternating F and R counts
            # for successive values of nb_mm
            for nb_mm in xrange(1, max_mm + 1):
                row.extend(
                    (Globals.ref_counts_fwd[ref][nb_mm],
                        Globals.ref_counts_rev[ref][nb_mm]))
            counts_writer.writerow(row)
            # Trying to make the basename inference more robust
            basename = "%s_on_%s" % (
                "_on_".join(OPB(Options.in_file).split("_on_")[:-1]), ref)
            # dealing with the histograms too
            with open(
                    OPJ(Options.hist_dir, basename + "_lengths.txt"),
                    "wb") as histo_file:
                write_histos(ref, max_mm, histo_file)
    WRITE("%d sequence written\n" % nb_seq_written)
    return 0

if __name__ == "__main__":
    sys.exit(main())
