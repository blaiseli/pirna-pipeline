#!/usr/bin/env bash
# bash necessary to use the {1..30} syntax
# Usage: map_and_annotate.sh <library_name> <G_species>

# library name
name=${1}
genome=${2}
output="pi_clusters_unique_counts/${name}_unique_pi_clusters_counts.txt"

echo "counting piRNA uniquely mapping in pi_clusters for library $name"

mkdir -p pi_clusters_unique_counts

echo -e "#cluster\tcounts" > ${output}
for i in {1..30}; do
    awk '{print '$i'"\t"$1}' mapped_strict_dm/${name}_strict_on_${genome}_piRNA_cluster${i}.nbseq >> ${output}
done

echo "The counts are written in ${output}"
