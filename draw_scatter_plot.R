#!/usr/bin/Rscript --slave
# Script by Stephanie Pierson, to redraw data from piPipes rna2
# Instructions (from an e-mail to Severine Chambeyron, 22/03/2016):
# -----
# Le code prend en argument (dans cet ordre exactement) :
#
# 1 / fichier avec l’abondance de piRNAs pour chaque TE (normalisé) pour ta condition 1
#
# 2 / fichier avec l’abondance de piRNAs pour chaque TE (normalisé) pour ta condition 2
#
# 3 / Nom de ta condition 1 (apparaitra sur le labels des axes) [ex : ywEmb]
#
# 4 / Nom de ta condition 2 (apparaitra sur le labels des axes) [ex : ywOv]
#
# 5 / Le nom que tu veux donner au fichier pdf généré (sans pdf à la fin, le script le fait tout seul)
#
# Il ne mappe que les AS, dis-moi si tu veux mapper aussi les sens.
#
# 
#
# Exemple :
#
# Rscript --slave draw_scatter_plot.R  /path_to_your_directory/Name.piPipes_out/transposon_abundance/exp_ID1.normalized_by_mirna /path_to_your_directory/Name.piPipes_out/transposon_abundance/exp_ID2.normalized_by_mirna cond1 cond2 Name_of_your_graph
#
# -----

#library("gdata")
library("ggplot2")
library("grid")
library("ggthemes")
library("scales")
library("gridExtra")

roundUp <- function(x, nice=c(1,2,4,5,6,8,10)) {
     if(length(x) != 1) stop("'x' must be of length 1")
     10^floor(log10(x)) * nice[[which(x <= 10^floor(log10(x)) * nice)[[1]]]]
}

argv = commandArgs (TRUE)
sample1 = read.table (argv[1])
sample2 = read.table (argv[2])
name1 = argv[3]
name2 = argv[4]

main = basename (argv[5])
main = gsub ("\\."," ",main)
main = paste (strwrap(main, width = 80), collapse = "\n")

pdf (paste (argv[5],".pdf", sep=""), title=main )

colnames (sample1) = c("name", "group", "S1", "AS1")
colnames (sample2) = c("name", "group", "S2", "AS2")
sample = merge (sample1, sample2, by="name")
sample$S1 <- sample$S1+1
sample$S2 <- sample$S2+1
sample$AS1 <- sample$AS1+1
sample$AS2 <- sample$AS2+1 #add pseudocount

lim = roundUp (10*(max (sample$AS1, sample$AS2)))/10

ggplot(sample,aes(x=AS1,y=AS2)) + 
theme_bw()+
theme(axis.line = element_line(colour = "black"),panel.grid.major = element_blank(),panel.grid.minor = element_blank())+
geom_point(aes(colour=factor(group.x)),alpha = 0.5, na.rm=T) + 
scale_colour_manual(values=c("grey","yellow","green","black"))+
scale_x_log10 ( limits = c(0.1,lim), breaks = trans_breaks("log10", function(x) 10^x), labels = trans_format("log10", math_format(10^.x) ) ) +
scale_y_log10 ( limits = c(0.1,lim), breaks = trans_breaks("log10", function(x) 10^x), labels = trans_format("log10", math_format(10^.x) ) ) +
geom_abline (intercept=0, slope=1, colour="red", linetype='dashed')+
xlab ( substitute ( paste(italic(name1) ,"  antisense, normalized number of reads (log10)"), list(name1=name1, name2=name2))) +
ylab ( substitute ( paste(italic(name2), "  antisense, normalized number of reads (log10)"), list(name1=name1, name2=name2))) +
coord_fixed()

dev.off()

