#!/usr/bin/env bash
# Usage: select_fastq_size.sh <file.fastq> <min_size> <max_<size> <resulting_file.fastq>

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}


min=$2
max=$3

echo "homerTools trim -min $min ${1}"
homerTools trim -min $min ${1} || error_exit "homerTools trim failed"
echo  "rm -f ${1}.lengths"
rm -f ${1}.lengths
echo "homerTools trim -max $max ${1}.trimmed"
homerTools trim -max $max ${1}.trimmed || error_exit "homerTools trim failed"
echo "mv -f ${1}.trimmed.trimmed ${4}"
mv -f ${1}.trimmed.trimmed ${4}
echo "rm -f ${1}.trimmed.lengths"
rm -f ${1}.trimmed.lengths
echo "rm -f ${1}.trimmed"
rm -f ${1}.trimmed
