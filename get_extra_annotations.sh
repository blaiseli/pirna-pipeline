#!/usr/bin/env bash

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}

genome="D_melanogaster"
genome_dir="${GENOMES}/${genome}"
genome_name="${genome}_flybase_splitmRNA_and_TE"
TE_repo="https://github.com/cbergman"

cd ${genome_dir}/Annotations

echo -e "\nGetting canonical transposon sequences from ${TE_repo}"
# Get canonical transposon set
git clone ${TE_repo}/transposons.git || error_exit "Canonical TE download failed"
#ln -s transposons/current/transposon_sequence_set.embl.txt .
# Extract fasta sequences and index
embl2fasta.py transposons/current/transposon_sequence_set.embl.txt canonical_TE.fa || error_exit "Canonical TE sequence extraction failed"
bowtie2-build --seed 123 --packed canonical_TE.fa canonical_TE || error_exit "Canonical TE bowtie2 indexing failed"
#crac-index index canonical_TE canonical_TE.fa || error_exit "Canonical TE crac indexing failed"

# Extract annotation data from source:
# http://stackoverflow.com/questions/59895/can-a-bash-script-tell-what-directory-its-stored-in
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
custom_annots="${DIR}/custom_annotations"
echo -e "\nGetting extra data from ${custom_annots}"
orient_pi_clusters.py ${custom_annots}/pi_clusters.fa ${custom_annots}/pi_clusters_Brennecke_2007.csv  > pi_clusters_oriented.fa
bowtie2-build --seed 123 --packed pi_clusters_oriented.fa pi_clusters || error_exit "piRNA cluster bowtie2 indexing failed"
#crac-index index pi_clusters pi_clusters_oriented.fa || error_exit "piRNA cluster crac indexing failed"
cp ${custom_annots}/ovary_si_clusters.txt .

# Add canonical TE plus P{lArB}
cat canonical_TE.fa ${custom_annots}/P_lArB.fa > TE_extended.fa
# Add some cluster 1 features such as cl1-A and cl1-B
cat TE_extended.fa ${custom_annots}/42AB_features.fa > TE_plus.fa
# Create index for this TE_plus set of TE and sequences on which we may want to map.
bowtie2-build --seed 123 --packed TE_plus.fa TE_plus || error_exit "Extended canonical TE bowtie2 indexing failed"
#crac-index index TE_plus TE_plus.fa || error_exit "Extended canonical TE crac indexing failed"

# Add indices for individual TEs
TE_dir="individual_TE"
mkdir ${TE_dir}
cd ${TE_dir}
for header in `grep "^>" ../TE_plus.fa | awk '{print $1}'`
do
    TE_name=${header:1}
    TE_file="${TE_name}.fa"
    # \b is to match the boounday at the end of a word
    # This avoids matching rooA for roo
    grep -A 1 "^$header\b" ../TE_plus.fa > ${TE_file} || error_exit "${TE_name} fasta sequence extraction failed"
    bowtie2-build --seed 123 --packed ${TE_file} ${TE_name} || error_exit "${TE_name} bowtie2 indexing failed"
    #crac-index index ${TE_name} ${TE_file} || error_exit "${TE_name} crac indexing failed"
done
cd ..


echo -e "\nMaking extended genome annotation ${genome_name}"
# Make global .bed
cd Flybase
build_annotated_genome.py \
    --annotations 3UTR.fa,5UTR.fa,CDS.fa,miRNA.fa,miscRNA.fa,ncRNA.fa,transposable_elements.fa,tRNA.fa \
    --genome ${genome_name} \
    --pi_clusters ../pi_clusters_oriented.fa \
    --si_clusters ../ovary_si_clusters.txt \
    --fasta_TE ../TE_extended.fa \
    || error_exit "Extended genome annotation failed"

echo -e "\nExtracting piRNA cluster annotation"
# Extract .bed files for each piRNA cluster
mkdir ../piRNA_clusters
for i in {1..142}
do
    awk -v nb=${i} '$4=="piRNA_cluster@cluster"nb{print}' ${genome_name}_annot.bed \
        > ../piRNA_clusters/piRNA_cluster${i}_annotation.bed \
        || error_exit "piRNA cluster ${i} annotation extraction failed"
    bgzip -c ../piRNA_clusters/piRNA_cluster${i}_annotation.bed \
        > ../piRNA_clusters/piRNA_cluster${i}_annotation.bed.bgz \
        || error_exit "piRNA cluster ${i} annotation compression failed"
    tabix -p bed ../piRNA_clusters/piRNA_cluster${i}_annotation.bed.bgz \
        || error_exit "piRNA cluster ${i} annotation indexing failed"
done

echo -e "\nCompressing and indexing extended genome annotation ${genome_name}"
bgzip -c ${genome_name}_annot.bed > ${genome_name}_annot.bed.bgz \
    || error_exit "Extended genome annotation compression failed"
tabix -p bed ${genome_name}_annot.bed.bgz \
    || error_exit "Extended genome annotation indexing failed"



# Build a fasta file (and corresponding bowtie2 index) with the 3UTR transcript
# name instead of flybase ID as first element of the header:
make_flybase_fasta_name_header.py --in_file 3UTR.fa > 3UTR_named.fa \
    || error_exit "3UTR fasta reformatting failed"
bowtie2-build --seed 123 --packed 3UTR_named.fa 3UTR_named \
    || error_exit "3UTR bowtie2 indexing failed"
#crac-index index 3UTR_named 3UTR_named.fa \
#    || error_exit "3UTR crac indexing failed"
cd ..

ln -s Flybase/${genome_name}_annot* .
cd ..

exit 0
