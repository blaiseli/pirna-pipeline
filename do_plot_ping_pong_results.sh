#!/usr/bin/env bash
# Usage: do_plot_ping_pong_results.sh <ref_file> <num_slices> <lib1> <lib2>
# <ref_file> should be a file containing the list of references on which to perform ping-pong analysis.
# <num_slices> is the number of slices in which to divide the piRNA candidates
# The slice size will be determined based on the number of reads in the poorer libraries on a per-refrerence basis
# <lib1> and <lib2> are the names of the libraries for which to make the analyses

#TODO Clean script
# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}

genome="D_melanogaster"

ref_file=${1}
num_slices=${2}
libraries=${@:3}

lib_names=`python -c "import sys; print '_'.join(sys.argv[1:])" ${libraries}`

ref_text=`basename ${ref_file}`
results_dir="ping_pong_results/${lib_names}_${ref_text%.*}_${num_slices}_slices"

mkdir -p ${results_dir}

echo -e "\nPing-pong analysis will be done using references from ${ref_file}"
echo "Ping-pong analysis will be performed for libraries ${libraries}"

if [ ! -e ${results_dir}/slice_sizes.txt ]
then
    # [[:graph:]] -> class of the visible characters
    filenames=`echo ${libraries} | sed 's|\([[:graph:]][[:graph:]]*\)|\1/histos_piRNA_candidate_on_TE/\1_piRNA_candidate_on_TE_ref_counts.txt|g'`
    compute_slice_size.py -n ${num_slices} -i ${filenames} > ${results_dir}/slice_sizes.txt || error_exit "compute_slice_size.py failed."
fi

libnames_array=(${libraries})
num_libs=${#libnames_array[@]}
if [ ${num_libs} == 2 ]
then
    mkdir -p "${results_dir}/signature_plots"
    for ref in `cat ${ref_file}`
    do
        if [ ${ref} == "global" -o ${ref} == "all" ]
        then
            ref_to_count="tot"
            ref_for_mapping="${genome}_and_TE"
        else
            ref_to_count=${ref}
            ref_for_mapping=${ref}
        fi
        slice_size=`awk -v ref=${ref_to_count} '$1==ref{print $2}' ${results_dir}/slice_sizes.txt`
        cmd="pp_signal_boxplot.R ${libraries} ${results_dir}/signature_plots ${num_slices} ${slice_size} piRNA_candidate ${ref_for_mapping} ${ref} 1 80"
        echo ${cmd}
        eval ${cmd} || "${cmd} failed"
        cmd="pp_signal_boxplot.R ${libraries} ${results_dir}/signature_plots ${num_slices} ${slice_size} piRNA_candidate ${ref_for_mapping} ${ref} 1 21"
        echo ${cmd}
        eval ${cmd} || "${cmd} failed"
    done
else
    echo -e "\nFor the moment, ping-pong signature plotting is only implemented for a pair of libraries."
    echo "Skipping signature plotting."
fi

cmd="gather_pingpong_results.py \
    -r ${ref_file} \
    --mapping_genome ${genome}_and_TE \
    -l ${libraries} -n ${num_slices} \
    -s ${results_dir}/slice_sizes.txt \
    -m ${results_dir}/average_ping_pong_${lib_names}.xls \
    -d ${results_dir} -c green \
    --sort_by_size --draw_probs \
    > ${results_dir}/ping_pong_${lib_names}.xls"
echo ${cmd}
eval ${cmd} || error_exit "gather_pingpong_results.py failed."

curr_dir=`pwd`
cd ${results_dir}
#for category in "total" "F_0mm" "R_0mm" "F_mm" "R_mm"
for category in "total"
do
    texfot pdflatex ${lib_names}_${category}_percent.tex || error_exit "pdf compilation failed."
    texfot pdflatex ${lib_names}_${category}_percent.tex || error_exit "pdf compilation failed."
done

cd ${curr_dir}

exit 0
