#!/bin/sh
banque1=${1}
banque2=${2}
#col="2"
col=$3
transform=$4
norm=$5
genome=${6}
scatterdir="scatterplots"

in_files="siRNA_counts/${banque1}_siRNA_counts.txt,siRNA_counts/${banque2}_siRNA_counts.txt"

case ${norm} in
    mi)
        scatterplot_reads.py --min_counts 5 --column $col --in_files ${in_files} --normalizations mapped_${genome}/${banque1}_on_${genome}_miRNA.nbseq,mapped_${genome}/${banque2}_on_${genome}_miRNA.nbseq --x_axis "${banque1} (reads by miRNA reads)" --y_axis "${banque2} (reads by miRNA reads)" --plot ${scatterdir}/siRNA_${banque1}_vs_${banque2}_norm_mi_${transform}.tex
        cd ${scatterdir}
        pdflatex siRNA_${banque1}_vs_${banque2}_norm_mi_${transform}.tex
        cd ..
        ;;
    endosi)
        scatterplot_reads.py --min_counts 5 --column $col --in_files ${in_files} --normalizations mapped_${genome}/${banque1}_on_${genome}_siRNA.nbseq,mapped_${genome}/${banque2}_on_${genome}_siRNA.nbseq --x_axis "${banque1} (reads by endogenous siRNA reads)" --y_axis "${banque2} (reads by endogenous siRNA reads)" --plot ${scatterdir}/siRNA_${banque1}_vs_${banque2}_norm_endosi_${transform}.tex
        cd ${scatterdir}
        pdflatex siRNA_${banque1}_vs_${banque2}_norm_endosi_${transform}.tex
        cd ..
        ;;
    42AB)
        scatterplot_reads.py --min_counts 5 --column $col --in_files ${in_files} --normalizations mapped_strict_dm/${banque1}_strict_on_${genome}_piRNA_cluster1.nbseq,mapped_strict_dm/${banque2}_strict_on_${genome}_piRNA_cluster1.nbseq --x_axis "${banque1} (reads by 42AB piRNA reads)" --y_axis "${banque2} (reads by 42AB piRNA reads)" --plot ${scatterdir}/siRNA_${banque1}_vs_${banque2}_norm_42AB_${transform}.tex
        cd ${scatterdir}
        pdflatex siRNA_${banque1}_vs_${banque2}_norm_42AB_${transform}.tex
        cd ..
        ;;
    *)
        scatterplot_reads.py --min_counts 5 --column $col --in_files ${in_files} --x_axis "${banque1} (10000 reads)" --y_axis "${banque2} (10000 reads)" --plot ${scatterdir}/siRNA_${banque1}_vs_${banque2}_${transform}.tex
        cd ${scatterdir}
        pdflatex siRNA_${banque1}_vs_${banque2}_${transform}.tex
        cd ..
        ;;
esac
