#!/bin/sh

echo "Testing the -max option of 'homerTools trim' on a file with 3 sequences of lengths 10, 15 and 20."

echo "@test_10" > test.fastq
echo "ACGTACGTAC" >> test.fastq
echo "+" >> test.fastq
echo "AAAAAAAAAA" >> test.fastq
echo "@test_15" >> test.fastq
echo "ACGTACGTACGTACG" >> test.fastq
echo "+" >> test.fastq
echo "AAAAAAAAAAAAAAA" >> test.fastq
echo "@test_20" >> test.fastq
echo "ACGTACGTACGTACGTACGT" >> test.fastq
echo "+" >> test.fastq
echo "AAAAAAAAAAAAAAAAAAAA" >> test.fastq

select_fastq_size.sh test.fastq 15 20 test_15_20.fastq > /dev/null

nb_lines=`cat test_15_20.fastq | wc -l`

rm -f test_15_20.fastq
rm -f test.fastq

if [ $nb_lines -ne 8 ]
then
    echo "Your version of homerTools seems to not handle correctly the -max option of 'homerTools trim'."
    exit 1
else
    exit 0
fi
