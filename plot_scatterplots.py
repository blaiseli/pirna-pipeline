#!/usr/bin/env python
# vim: set fileencoding=<utf-8> :
"""This script reads data from *ref_counts.txt files. It also reads files
containing corresponding normalization values to apply and makes plots out of
it, at the same scale."""


import argparse
import sys
WRITE = sys.stdout.write

# To avoid repeatedly calling these as string methods:
from string import split, strip
#from re import sub

import warnings


def formatwarning(message, category, filename, lineno, line):
    """Used to format warning messages."""
    return "%s:%s: %s: %s\n" % (filename, lineno, category.__name__, message)
warnings.formatwarning = formatwarning

import os
OPJ = os.path.join
OPB = os.path.basename

from itertools import izip
from collections import OrderedDict, defaultdict
from operator import itemgetter
from scipy import stats
import matplotlib as mpl
# To be able to run the script without a defined $DISPLAY
mpl.use("PDF")

#mpl.rcParams["figure.figsize"] = 2, 4
mpl.rcParams["font.sans-serif"] = [
    "Arial", "Liberation Sans", "Bitstream Vera Sans"]
mpl.rcParams["font.family"] = "sans-serif"

#import pylab
from matplotlib import numpy as np
import matplotlib.pyplot as plt

#from chr_matplotlib import remappedColorMap as remap_cmap


# https://jakevdp.github.io/blog/2014/10/16/how-bad-is-your-colormap/
def grayify_cmap(cmap):
    """Return a grayscale version of the colormap"""
    cmap = plt.cm.get_cmap(cmap)
    colors = cmap(np.arange(cmap.N))
    # convert RGBA to perceived greyscale luminance
    # cf. http://alienryderflex.com/hsp.html
    rgb_weight = [0.299, 0.587, 0.114]
    luminance = np.sqrt(np.dot(colors[:, :3] ** 2, rgb_weight))
    colors[:, :3] = luminance[:, np.newaxis]
    return cmap.from_list(cmap.name + "_grayscale", colors, cmap.N)


class OrderedDefaultDict(OrderedDict, defaultdict):
    """Recipe for orderef defaultdict:
    http://stackoverflow.com/a/35968897/1878788
    """
    def __init__(self, default_factory=None, *args, **kwargs):
        #in python3 you can omit the args to super
        super(OrderedDefaultDict, self).__init__(*args, **kwargs)
        self.default_factory = default_factory


class Scatterplot(object):
    """A 2-dimension scatterplot."""
    __slots__ = ("x_values", "y_values", "by_norm")

    def __init__(self, by_norm):
        self.x_values = {}
        self.y_values = {}
        self.by_norm = by_norm

    def get_data(self, axis,
                 data_file_name, norm_file_name,
                 ref_col_num, col_num, min_counts):
        """Reads data for the axis *axis* from file *data_file_name* using
        normalization provided in file *norm_file_name*."""
        if norm_file_name is not None:
            with open(norm_file_name, "r") as norm_file:
                norm = float(strip(norm_file.readline())) / self.by_norm
        else:
            norm = 1.0 / self.by_norm
        if axis == "x":
            values = self.x_values
        elif axis == "y":
            values = self.y_values
        with open(data_file_name, "r") as data_file:
            name_converter = Globals.name_converter

            def convert_name(name):
                """Converts a name if conversion is defined in
                *Globals.name_converter*."""
                #if name in name_converter:
                #    print "converting %s to %s" % (name, name_converter[name])
                return name_converter.get(name, name)
            for line in map(strip, data_file):
                if line[0] == "#":
                    continue
                fields = split(line)
                point_id = convert_name(fields[ref_col_num - 1])
                msg = "Several data points with id %s. Aborting." % point_id
                assert point_id not in values, msg
                count = float(fields[col_num - 1])
                if count >= min_counts:
                    values[point_id] = count / norm

    def plot(self,
             x_axis_label, y_axis_label,
             point_colours=None,
             annotations=None,
             output_files=None,
             transform=False,
             legend_size=9,
             label_size=12,
             tick_size=12,
             marker_size=4):
        """Plots the scatterplot into some graphic files."""
        # https://www.quantifiedcode.com/app/issue_class/3P0qV6OB
        output_files = output_files or [os.devnull]
        # An element needs to have its two coordinates to be plottable.
        plottable = set(self.x_values) & set(self.y_values)
        if not len(plottable):
            warnings.warn("No elements to plot.")
        if annotations is None:
            annotations = {}
        #
        # values and colours for the scatter command
        #x_values = []
        #y_values = []
        #colours = []
        #for element, colour in point_colours.items():
        #    if element in plottable:
        #        x_values.append(self.x_values[element])
        #        y_values.append(self.y_values[element])
        #        colours.append(colour)
        #
        #xmin = min(x_values) * 0.9
        #xmax = max(x_values) * 1.1
        #ymin = min(y_values) * 0.9
        #ymin = min(y_values) * 1.1
        #plt.axis([xmin, xmax, ymin, ymax])
        #plt.axis([min(x_values), max(x_values), min(y_values), max(y_values)])
        #if transform:
        #    plt.loglog(basex=transform, basey=transform)
        # TODO: set title font size
        #plt.title(self.title)
        # gca(): Return the current axis instance
        axis = plt.gca()
        if transform:
            axis.set_xscale("log", basex=transform)
            axis.set_yscale("log", basey=transform)
            if transform == 2:
                plt.minorticks_off()
                #axis.set_xscale("log", basex=transform, subsx=[3])
                #axis.set_yscale("log", basey=transform)
        #axis.set_autoscale_on(False)
        axis.set_aspect('equal')
        #axis.set_xmargin(0.5)
        #axis.set_ymargin(0.5)
        all_x_values = []
        all_y_values = []
        if Globals.plot_diagonal:
            xmin = None
            xmax = None
            ymin = None
            ymax = None
        if point_colours is None:
            point_colours = {"black": list(plottable)}
        for colour, elements in point_colours.items():
            x_values = []
            y_values = []
            for element in elements:
                if element in plottable:
                    x_values.append(self.x_values[element])
                    y_values.append(self.y_values[element])
            if x_values and y_values:
                if Globals.plot_diagonal:
                    if xmin is None:
                        xmin = min(x_values)
                    else:
                        xmin = min(xmin, min(x_values))
                    if xmax is None:
                        xmax = max(x_values)
                    else:
                        xmax = max(xmax, max(x_values))
                    if ymin is None:
                        ymin = min(y_values)
                    else:
                        ymin = min(ymin, min(y_values))
                    if ymax is None:
                        ymax = max(y_values)
                    else:
                        ymax = max(ymax, max(y_values))
                #series.extend([x_values, y_values, "%s." % colour[0]])
                #axis.plot(x_values, y_values, "%s." % colour[0], label=colour)
                axis.plot(
                    x_values,
                    y_values,
                    ".",
                    color=colour,
                    markeredgecolor='black',
                    markersize=marker_size,
                    label=colour)
            all_x_values.extend(x_values)
            all_y_values.extend(y_values)
        if Globals.plot_diagonal:
            botleft = min(xmin, ymin)
            topright = max(xmax, ymax)
            axis.plot([botleft, topright], [botleft, topright], ":k")
        # Pearson's correlation coefficient
        # http://www.statsoft.com/textbook/glosp.html#Pearson%20Correlation
        r_value, _ = stats.pearsonr(all_x_values, all_y_values)
        plt.text(
            0.75,
            0.9,
            "Pearson's correlation coefficient: %.3f" % r_value,
            fontsize=legend_size,
            horizontalalignment="right",
            transform=axis.transAxes)
        #min_val = min(xmin, ymin)
        #max_val = max(xmax, ymax)
        #margin = 0.05 * (max_val - min_val)
        #margin = 0
        #v = min_val
        #axis.plot(
        #    v,
        #    v,
        #    ".",
        #    color="blue")
        #v = max_val + margin
        #axis.plot(
        #    v,
        #    v,
        #    ".",
        #    color="red")
        #while v < max_val + margin:
        #    axis.plot(
        #        v,
        #        v,
        #        ".",
        #        color="blue")
        #    v += margin
        #axis.plot(
        #    max_val + margin,
        #    max_val + margin,
        #    ".",
        #    color="blue")
        #axis.plot(
        #    min_val - margin,
        #    min_val - margin,
        #    ".",
        #    color="red")
        #axis.scatter(
        #    x_values, y_values, c=colours)
        for element in set(annotations.keys()) & plottable:
            axis.annotate(
                element,
                xy=(self.x_values[element],
                    self.y_values[element]),
                xytext=annotations[element],
                textcoords="offset points",
                ha="center",
                va="bottom",
                #bbox=dict(
                #    boxstyle='round,pad=0.2',
                #    fc='yellow', alpha=0.3),
                arrowprops=dict(
                    arrowstyle='->',
                    connectionstyle='arc3,rad=0.5',
                    color='blue',
                    alpha=0.5))
        for element in set(annotations.keys()) - plottable:
            warnings.warn("%s will not be plotted." % element)
        #axis.legend(framealpha=0.5, ncol=2, prop={"size": 9}, loc=9)
        # Automatic choice of legend placement: loc=0
        #axis.xaxis.label.set_fontsize(label_size)
        #axis.yaxis.label.set_fontsize(label_size)
        #for tick in axis.get_xticklabels():
        #    tick.set_fontsize(tick_size)
        #for tick in axis.get_yticklabels():
        #    tick.set_fontsize(tick_size)
        #axis.legend(framealpha=0.5, ncol=1, prop={"size": legend_size}, loc=0)
        #axis.legend(framealpha=0.5, ncol=1, loc=0)
        #axis.set_xlabel(x_axis_label, fontname="Liberation Sans")
        #axis.set_ylabel(y_axis_label, fontname="Helvetica")
        axis.set_xlabel(x_axis_label)
        axis.set_ylabel(y_axis_label)
        #axis.tick_params(
        #    which="major",
        #    direction="in",
        #    length=9,
        #    width=1)
        #axis.tick_params(
        #    which="minor",
        #    direction="in",
        #    length=4,
        #    width=1)
        axis.tick_params(
            which="major",
            direction="out",
            top=False,
            right=False,
            length=4,
            width=1)
        axis.tick_params(
            which="minor",
            direction="out",
            top=False,
            right=False,
            length=2,
            width=0.5)
        axis.spines["top"].set_visible(False)
        axis.spines["right"].set_visible(False)
        #plt.axis("tight")
        #plt.axis("equal")
        plt.axis("scaled")
        plt.tight_layout()
        #plt.legend(loc="lower right")
        for out in output_files:
            plt.savefig(out)
        # To avoid successive plots from superimposing on one another
        plt.cla()

    def fold_heatmap(self,
                     x_axis_label, y_axis_label,
                     point_colours,
                     cmap,
                     output_files=None,
                     transform=2,
                     out_names=os.devnull,
                     legend_size=9,
                     label_size=9,
                     tick_size=9,
                     do_remap_cmap=False):
        """Plots the fold heatmaps in graphic files.
        *extra_points* is a hack to attempt to force
        the fold scale to reach some deisred values."""
        # https://www.quantifiedcode.com/app/issue_class/3P0qV6OB
        output_files = output_files or [os.devnull]
        # An element needs to have its two coordinates to be plottable.
        pre_plottable = set(self.x_values) & set(self.y_values)
        if Globals.elems_order:
            plottable = set(Globals.elems_order) & pre_plottable
            eliminated = pre_plottable - plottable
            if len(eliminated):
                warnings.warn(
                    "The order of %d elements "
                    "cannot be determined. "
                    "They will not be plotted" % len(eliminated))
        else:
            plottable = pre_plottable
        # values and colours for the scatter command
        #x_values = []
        #y_values = []
        #colours = []
        #for element, colour in point_colours.items():
        #    if element in plottable:
        #        x_values.append(self.x_values[element])
        #        y_values.append(self.y_values[element])
        #        colours.append(colour)
        #
        #xmin = min(x_values) * 0.9
        #xmax = max(x_values) * 1.1
        #ymin = min(y_values) * 0.9
        #ymin = min(y_values) * 1.1
        #plt.axis([xmin, xmax, ymin, ymax])
        #plt.axis([min(x_values), max(x_values), min(y_values), max(y_values)])
        #if transform:
        #    plt.loglog(basex=transform, basey=transform)
        # Build lists of information correspondinrgd to points to plot.
        if not transform:
            log_func = np.log2
            label = "log2 (%s / %s)" % (y_axis_label, x_axis_label)
        elif transform == 2:
            log_func = np.log2
            label = "log2 (%s / %s)" % (y_axis_label, x_axis_label)
        elif transform == 10:
            log_func = np.log10
            label = "log10 (%s / %s)" % (y_axis_label, x_axis_label)
        else:
            warnings.warn("Using log2 instead of %s" % transform)
            log_func = np.log10
            label = "log2 (%s / %s)" % (y_axis_label, x_axis_label)
        x_values = []
        y_values = []
        names = []
        colours = []
        not_plottable = []
        for colour, elements in point_colours.items():
            for element in elements:
                if element in plottable:
                    x_values.append(self.x_values[element])
                    y_values.append(self.y_values[element])
                    names.append(element)
                    colours.append(colour)
                else:
                    not_plottable.append(element)
        warnings.warn(
            "These %d elements will not be plotted:\n%s" % (
                len(not_plottable),
                ", ".join(
                    not_plottable)))
        nb_to_plot = len(names)
        warnings.warn(
            "These %d elements will be plotted:\n%s" % (
                nb_to_plot, ", ".join(names)))
        col_len = len(x_values)
        if x_values and y_values:
            folds = np.asarray(y_values, dtype=np.float_) /\
                np.asarray(x_values, dtype=np.float_)
            assert len(folds) == col_len
            logfolds = log_func(folds)
            assert len(logfolds) == col_len
            if Globals.elems_order:
                # Sort according to user-provided order
                logfolds, colours, names = map(
                    np.asarray,
                    zip(*sorted(
                        zip(logfolds, colours, names),
                        key=Globals.triple2rank)))
            else:
                # Sort by folds
                logfolds, colours, names = map(
                    np.asarray,
                    zip(*sorted(zip(logfolds, colours, names), reverse=False)))
            if do_remap_cmap:
                if Globals.max_logfold is None:
                    vmin = np.nanmin(logfolds)
                    vmax = np.nanmax(logfolds)
                    max_abs = 1.00001 * max(abs(vmin), abs(vmax))
                else:
                    max_abs = 1.00001 * Globals.max_logfold
                #logfolds = np.asarray(list(logfolds) + [-max_abs, max_abs])
                logfolds = np.insert(logfolds, 0, -max_abs)
                names = np.insert(names, 0, "minlogfold")
                colours = np.insert(colours, 0, "purple")
                logfolds = np.append(logfolds, max_abs)
                names = np.append(names, "maxlogfold")
                colours = np.append(colours, "orange")

                assert len(logfolds) == nb_to_plot + 2, "%d != %d" % (
                    len(logfolds), nb_to_plot)
            else:
                assert len(logfolds) == nb_to_plot, "%d != %d" % (
                    len(logfolds), nb_to_plot)
            #print "\n".join(
            #    ["%s: %f (%f / %f)" % (
            #        name, logfold, y, x) for name, logfold, x, y in zip(
            #            names, logfolds, x_values, y_values)])
            column = np.reshape(logfolds, (-1, 1))
        # TODO: set title font size
        #plt.title(self.title)
        # gca(): Return the current axis instance
        axis = plt.gca()
        if do_remap_cmap:
            axis.set_ylim((1, len(names) - 1))
        else:
            axis.set_ylim((0, len(names)))
        #axis.set_autoscale_on(False)
        #if False:
        ##if do_remap_cmap:
        #    vmin = min(logfolds)
        #    absvmin = abs(min(logfolds))
        #    vmax = max(logfolds)
        #    if vmin < 0 < vmax:
        #        max_abs = max(absvmin, vmax)
        #        #cmap = remap_cmap(
        #        #    cmap, data=np.asarray([-max_abs, 0, max_abs]))
        #        #cmap = remap_cmap(cmap, data=logfolds)
        #        cmap = remap_cmap(cmap, start=0, midpoint=0.5, stop=1.0)
        #    else:
        #        midpoint = absvmin / (vmax + absvmin)
        #        cmap = remap_cmap(cmap, midpoint=midpoint)
        #base_cmap = plt.cm.RdYlGn
        #base_cmap = plt.cm.PuOr
        #base_cmap = plt.cm.RdYlBu_r
        heatmap = axis.pcolor(
            column,
            #cmap=remap_cmap(plt.cm.PuOr, midpoint=midpoint))
            #cmap=remap_cmap(plt.cm.RdYlGn, midpoint=midpoint))
            #cmap=grayify_cmap(remap_cmap(base_cmap, midpoint=midpoint)))
            cmap=cmap)
        if do_remap_cmap:
            axis.set_yticks(np.arange(1, len(names) - 1) + 0.5, minor=False)
            try:
                assert names[-1] == "maxlogfold"
                assert names[0] == "minlogfold"
                assert np.nanmin(logfolds) == -max_abs
                assert np.nanmax(logfolds) == max_abs
            except AssertionError:
                print "Some log folds seem to be larger (in absolute value),"
                print "than the imposed maximum."
                print "\n".join(["%s\t%s\t%s" % (n, l, c) for (n, l, c) in zip(
                    names, logfolds, colours)])
                sys.exit(1)
            #assert names[0] == "maxlogfold"
            #assert names[-1] == "minlogfold"
            axis.set_yticklabels(names[1:-1])
            with open(out_names, "w") as names_list:
                names_list.write("%s\n" % "\n".join(names[1:-1]))
            for tick, colour in zip(axis.get_yticklabels(), colours[1:-1]):
                tick.set_color(colour)
                #tick.set_fontsize(tick_size)
        else:
            axis.set_yticks(np.arange(len(names)) + 0.5, minor=False)
            axis.set_yticklabels(names)
            with open(out_names, "w") as names_list:
                names_list.write("%s\n" % "\n".join(names))
            for tick, colour in zip(axis.get_yticklabels(), colours):
                tick.set_color(colour)
                #tick.set_fontsize(tick_size)
        axis.set_xticks([])
        #axis.yaxis.label.set_fontsize(label_size)
        axis.tick_params(
            axis="y", left="off", right="off",
            labelsize=label_size)
        axis.set_aspect('equal')
        # Tells plt where it should find the color info.
        cbar = plt.colorbar(
            mappable=heatmap,
            orientation="vertical",
            fraction=0.1)
        cbar.set_label(
            label,
            #"log2 (%s / %s)" % (y_axis_label, x_axis_label),
            size=tick_size)
        cbar.ax.tick_params(axis="y", labelsize=tick_size)
        #cbar.ax.invert_yaxis()
        #axis.pcolor(folds, cmap=plt.cm.RdYlG)
        #axis.scatter(
        #    x_values, y_values, c=colours)
        #axis.plot(*series)
        #axis.legend(framealpha=0.5, ncol=2, prop={"size": 9}, loc=9)
        # Automatic choice of legend placement: loc=0
        #axis.xaxis.label.set_fontsize(label_size)
        #axis.yaxis.label.set_fontsize(label_size)
        #for tick in axis.get_xticklabels():
        #    tick.set_fontsize(tick_size)
        #for tick in axis.get_yticklabels():
        #    tick.set_fontsize(tick_size)
        #axis.legend(framealpha=0.5, ncol=1, prop={"size": legend_size}, loc=0)
        #axis.legend(framealpha=0.5, ncol=1, loc=0)
        #axis.tick_params(
        #    which="major",
        #    direction="in",
        #    length=9,
        #    width=1)
        #axis.tick_params(
        #    which="minor",
        #    direction="in",
        #    length=4,
        #    width=1)
        ##
        #plt.axis("tight")
        ##
        #plt.axis("equal")
        #plt.axis("scaled")
        ##
        #plt.tight_layout()
        ##
        #plt.legend(loc="lower right")
        for out in output_files:
            plt.savefig(out, dpi=300, bbox_inches='tight')
        # To avoid successive plots from superimposing on one another
        plt.cla()

#########################################################
# For better traceability, options and global variables #
# should be stored as class attributes                  #
#########################################################


class Globals(object):
    """This object holds global variables."""
    plot_diagonal = False
    max_logfold = None


def main():
    """Main function of the program."""
    WRITE("%s\n" % " ".join(sys.argv))
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        "-i", "--in_files",
        nargs=2,
        required=True,
        help="Space-separated pair of *ref_counts.txt files.\n"
        "Data in each file should consist in lines with at least "
        "two blank-separated fields as follows:\n"
        "name counts_1 [counts_2 etc.]\n"
        "The lines should be sorted on the name fields.\n"
        "The counts of the first file will be used, once normalized, "
        "as x-axis coordinates for the points.\n"
        "The counts of the second file will be used, once normalized, "
        "as y-axis coordinates for the points.\n"
        "Only the points represented in the two files will be used.")
    parser.add_argument(
        "-n", "--normalizations",
        nargs=2,
        help="Space-separated pair of files containing the values used "
        "to normalize the counts.\n"
        "Of course, the file number and order should correspond to that "
        "given in the option --in_files.")
    parser.add_argument(
        "-b", "--by_norm",
        type=int,
        default=1,
        help="Set this to count reads by multiples of normalizers.")
    parser.add_argument(
        "-d", "--plot_dir",
        required=True,
        help="Directory in which scatterplots should be written.")
    parser.add_argument(
        "-p", "--plot_name",
        help="Base name for the plot files.")
    parser.add_argument(
        "-f", "--fold_heatmap",
        help="Base name for the fold heatmap plot files.")
    parser.add_argument(
        "--name_conversion",
        help="Tab-separated file with 2 columns used to convert names.\n"
        "First column contains names as they appear in the files "
        "given in the option --in_file.\n"
        "Second column contains names as they appear in the files "
        "given in the options --element_lists and / or --elements_order.")
    parser.add_argument(
        "-e", "--element_lists",
        nargs="*",
        help="Space-separated list of files contining element names.\n"
        "A file groups elements by categories for colour attribution.\n"
        "There should be one element per line. These elements should "
        "correspond to entries in the *ref_counts.txt files.\n"
        "The colours attributed to the elements listed in a file will be "
        "defined by the  --colours option.")
    parser.add_argument(
        "-o", "--elements_order",
        help="A file contining element names.\n"
        "There should be one element per line. These elements should "
        "correspond to entries in the *ref_counts.txt files.\n"
        "If this option is set, the heatmap will display the elements "
        "in the order in which they appear in the file.")
    parser.add_argument(
        "-c", "--colours",
        nargs="*",
        help="Space-separated list of colours to use in the plots.\n"
        "The order of the colours should correspond to the order of "
        "the files listed with the option --element_lists.")
    parser.add_argument(
        "--cmap",
        help="Name of the pyplot colour map to use un the fold heatmaps.\n"
        "https://jakevdp.github.io/blog/2014/10/16/how-bad-is-your-colormap "
        "may help you making your choice.",
        default="RdYlBu_r")
    parser.add_argument(
        "--remap_cmap",
        help="Use this option to change the colour map "
        "so that it is centered on 0\n"
        "(Warning: this is an experimental feature, "
        "and may not exactly work as expected).",
        action="store_true")
    parser.add_argument(
        "-l", "--elements_to_label",
        help="File contining element names.\n"
        "There should be one element per line. These elements will "
        "be annotated on the scatterplot (provided they are on the plot).")
    parser.add_argument(
        "--ref_column",
        type=int,
        help="Colum (1-based including a series names column) "
        "to use as referenc names.\n",
        default=1)
    parser.add_argument(
        "--column",
        type=int,
        help="Colum (1-based including a series names column) "
        "to use as counts.\n"
        "First column headers in *ref_counts.txt files are:\n"
        "#ref   total   total_F total_R F_0mm   R_0mm   F_mm    R_mm\n"
        "Extra columns detail counts by growing number of mismatches "
        "starting from 1 and by successions of F and R read counts.",
        default=2)
    parser.add_argument(
        "--max_logfold",
        type=float,
        help="Maximal absolute value of the logfold.\n"
        "Only has effects in conjunction with --remap_cmap")
    parser.add_argument(
        "--min_counts",
        type=float,
        help="Minimal number of counts for an element to be represented "
        "on the scatter plot.",
        default=1)
    parser.add_argument(
        "--label_size",
        type=int,
        help="Font size for axis labels",
        #default=12)
        default=24)
    parser.add_argument(
        "--hm_label_size",
        type=int,
        help="Font size for fold heatmap labels",
        default=9)
    parser.add_argument(
        "--legend_size",
        type=int,
        help="Font size for legend labels",
        #default=18)
        default=9)
    parser.add_argument(
        "--marker_size",
        type=int,
        help="Size for markers on the plot",
        #default=12)
        default=4)
    parser.add_argument(
        "--tick_size",
        type=int,
        help="Font size for ticks",
        #default=12)
        default=18)
    parser.add_argument(
        "-x", "--x_axis",
        help="Label for the x axis of the plot.",
        required=True)
    parser.add_argument(
        "-y", "--y_axis",
        help="Label for the y axis of the plot.",
        required=True)
    parser.add_argument(
        "-t", "--transform",
        required=True,
        help="log2, log10, or a linear scale to apply.")
    parser.add_argument(
        "--plot_diagonal",
        help="Use this option to plot the x=y line.",
        action="store_true")
    #parser.add_argument("-s", "--switch",
    #        help="This is an optional boolean switch.",
    #        action="store_true")
    args = parser.parse_args()
    if args.plot_diagonal:
        Globals.plot_diagonal = True
    if args.max_logfold:
        Globals.max_logfold = args.max_logfold
    Globals.name_converter = {}
    if args.name_conversion:
        with open(args.name_conversion) as conversion_file:
            for line in map(strip, conversion_file):
                if line[0] == "#":
                    continue
                [in_name, out_name] = split(line)
                Globals.name_converter[in_name] = out_name
    # elems_by_colour = OrderedDict()
    elems_by_colour = OrderedDefaultDict(list)
    # Define the order of the elements in the heat map.
    Globals.elems_order = OrderedDict()
    if args.elements_order:
        with open(args.elements_order, "r") as order_file:
            for rank, line in enumerate(map(strip, order_file)):
                if line[0] == "#":
                    continue
                Globals.elems_order[line] = rank
        get_rank = Globals.elems_order.get
        # third element of a triple (logfold, colour, name)
        get_fold = itemgetter(2)

        def triple2rank(triple):
            """Returns the rank of an element
            whose name is at third position of a triple."""
            return get_rank(get_fold(triple))
        Globals.triple2rank = staticmethod(triple2rank)
    # elem2colours = OrderedDict()
    if args.element_lists:
        input_iterator = izip(args.element_lists, args.colours)
        colours = []
        for (elements_file_name, colour) in input_iterator:
            #elems_by_colour[colour] = []
            with open(elements_file_name, "r") as elements_file:
                for element in map(strip, elements_file):
                    if element[0] == "#":
                        continue
                    for prev_col in colours:
                        if element in elems_by_colour[prev_col]:
                            warnings.warn("Turning %s from %s to %s" % (
                                element, prev_col, colour))
                            elems_by_colour[prev_col].remove(element)
                    #elem2colours[line] = colour
                    elems_by_colour[colour].append(element)
            colours.append(colour)
    else:
        colours = ["black"]
        elems_by_colour = None
    annotations = {}
    if args.elements_to_label:
        with open(args.elements_to_label, "r") as annotate_file:
            for line in map(strip, annotate_file):
                if line[0] == "#":
                    continue
                fields = split(line)
                if len(fields) == 1:
                    annotations[fields[0]] = (-30, 30)
                else:
                    # assuming the two next fields contain
                    # the x and y label offset
                    annotations[fields[0]] = (float(fields[1]),
                                              float(fields[2]))
    x_file, y_file = args.in_files
    if args.normalizations:
        x_norm_file, y_norm_file = args.normalizations
    else:
        x_norm_file, y_norm_file = None, None
    plot_data = Scatterplot(args.by_norm)
    plot_data.get_data(
        "x", x_file, x_norm_file,
        args.ref_column, args.column, args.min_counts)
    plot_data.get_data(
        "y", y_file, y_norm_file,
        args.ref_column, args.column, args.min_counts)
    if args.transform == "log2":
        transform = 2
    elif args.transform == "log10":
        transform = 10
    else:
        transform = False
    if args.plot_name:
        out_pdf = OPJ(
            args.plot_dir,
            "%s.pdf" % args.plot_name)
        plot_data.plot(
            args.x_axis, args.y_axis,
            #elem2colours,
            elems_by_colour,
            annotations,
            [out_pdf],
            transform,
            args.legend_size,
            args.label_size,
            args.tick_size,
            args.marker_size)
    if args.fold_heatmap:
        out_names = OPJ(
            args.plot_dir,
            "%s_list.txt" % args.fold_heatmap)
        # Plot fold heatmaps
        out_pdf = OPJ(
            args.plot_dir,
            "%s.pdf" % args.fold_heatmap)
        plot_data.fold_heatmap(
            args.x_axis, args.y_axis,
            elems_by_colour,
            getattr(plt.cm, args.cmap),
            [out_pdf],
            transform,
            out_names,
            args.legend_size,
            args.hm_label_size,
            tick_size=9,
            do_remap_cmap=args.remap_cmap)

if __name__ == "__main__":
    sys.exit(main())
