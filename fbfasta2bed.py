#!/usr/bin/env python
# vim: set fileencoding=<utf-8> :
"""This script reads a fasta file from flybase, with annotated headers and will
generate a bed file with the fasta sequence coordinates."""


import argparse
import sys
WRITE = sys.stdout.write

from Bio.SeqIO import parse


#################
# main function #
#################


def main():
    """Main function of the program."""
    #WRITE("%s\n" % " ".join(sys.argv))
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        "in_file",
        help="Input fasta file")
    args = parser.parse_args()
    split = str.split
    strip = str.strip
    join = str.join

    for record in parse(args.in_file, "fasta"):
        description = split(record.description, " ")
        fb_id = description[0]
        annots = dict((split(
            strip(annot, ";"), "=") for annot in description[1:]))
        chrom, coords = split(annots["loc"], ":")
        if coords[:10] == "complement":
            strand = "-"
            start, end = split(coords[11:-1], "..")
        else:
            strand = "+"
            start, end = split(coords, "..")
        WRITE("%s\n" % join(
            "\t",
            ("chr%s" % chrom,
             str(int(start) - 1),
             end,
             fb_id,
             "1000",
             strand)))
    return 0

if __name__ == "__main__":
    sys.exit(main())
