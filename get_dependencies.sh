#!/usr/bin/env bash

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}

# http://stackoverflow.com/questions/59895/can-a-bash-script-tell-what-directory-its-stored-in
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
version_file=${DIR}/program_versions.txt

for dep in zlib pigz lbzip2 cython samtools cutadapt pysam bowtie2 sra-tools bedtools MACS2 piPipes
do
    cmd="${DIR}/install_scripts/get_${dep}.sh"
    [[ -e ${cmd} ]] || error_exit "${cmd} not found"
    eval ${cmd} ${version_file} || error_exit "${cmd} failed"
done

cd ${DIR}/homerTools_4.7.2_trim_corrected/
make || error_exit "homerTools build failed"
make install || error_exit "homerTools install failed"
echo -e "homerTools\t4.7.2_trim_corrected" >> ${version_file}
