#!/usr/bin/env python
"""
Usage: annotate_sam.py [options]

This script will read sam-formatted alignments, extract the mapped reads, and
write them to a file with annotation in their headers. The alignments of a same
read should be consecutive in the sam file. This is what is normally obtained
with bowtie2 (perhaps not if using parallel processing).

The annotations should be provided in the form of a .bed file with lines
formatted as follows (almost 6-columns bed format: extended with the
possibility of having 0 for the strand):

<chromosome>\t<start>\t<end>\t<annotation_type>@<feature_name>\t0\t<strand>
<strand> can be +, - or 0
0 is when the strand is not documented or undefined.

Warning: <start> is 0-based but <end> isn't (see
http://genome.ucsc.edu/FAQ/FAQformat#format1 about bed format).

A compressed .bgz version and an index .bgz.tbi of it should exist, created by
bgzip and tabix: http://samtools.sourceforge.net/tabix.shtml

Available options:

    --help: prints this help text and exits.

    --in_file <file>: to specify the .sam file containing the mapping
    information to process.
    Alignemnts of a same read should be successive lines of the file.

    --annot <file>: to secify the file containing the annotations in .bed
    format. There must be a .bed.bgz (or .bed.gz) compressed version and
    .bed.bgz.tbi (or .bed.gz.tbi) corresponding tabix index in the same
    directory.

    --annot_module <file>: to import a python module defining a customized
    Annotation object.

    --exclude_types: <type_1>,<type2>,...: to not use annotations of the types
    listed.

    --keep_chr: to keep the "chr" prefix on the reference names. This amounts
    to assuming that the annotations use the chr prefix too.

    --max_edit_proportion: <float>: to specify the maximum proportion of edits
    for an alignment to be recorded. The default value is 0.0. (Not used)

    --min_mapq: <int>: to specify the minimum value of mapq mapping quality for
    a read to be recorded.
    The default value is 0.
    (Not used)

    --min_score: <float>: to specify the minimum value mapping score for an
    alignment to be taken into account.
    The default value is -200.

    --pi_range: <int>,<int>: to specify the minimum and maximum size for reads
    considered as potential piRNAs.
    If this option is set, potential siRNA reads (coming from the same
    annotation group as the potential piRNAs, but of size 21) will also be
    written in a file.
    If not set, potential piRNA and siRNA reads will not be written in joint
    files.

    --histo_dir <directory>: to specify the directory in whiche the histogram
    data should be written.

    --seq_dir <directory>: to specify the directory in which mapped reads
    should be written.

    --unique_set_file <file>: to specify the .pickle file in which the names of
    unique mappers should be dumped.

"""

import pysam
OPT = pysam.AlignedSegment.opt
#from Bio.Seq import reverse_complement
from string import maketrans
DNA_COMPL_TABLE = maketrans("ACGT", "TGCA")


# Faster, but will not work correctly with IUPAC ambiguity codes.
def reverse_complement(seq):
    """Faster than Bio.Seq.reverse_complement"""
    return seq.translate(DNA_COMPL_TABLE)[::-1]

#import numpy as np

#import cProfile
#from guppy import hpy
#H = hpy()
##H.setref()
#H.setrelheap()

try:
    import pyximport
    pyximport.install(pyimport=True)
except ImportError:
    # https://www.quantifiedcode.com/app/issue_class/1sMHNVxe
    pass

#import copy
# to read command-line options
import getopt
# to import the annotation module
import imp
import os
OPJ = os.path.join
OPB = os.path.basename
OPD = os.path.dirname
#import re
#import subprocess
import sys
# see https://wiki.python.org/moin/PythonSpeed/PerformanceTips#Doing_Stuff_Less_Often
sys.setcheckinterval(1000)
WRITE = sys.stdout.write
import warnings


def formatwarning(message, category, filename, lineno, line):
    """Formats a warning message."""
    return "%s:%s: %s:%s" % (filename, lineno, category.__name__, message)
warnings.formatwarning = formatwarning

#from operator import or_

import csv
from cPickle import dump, HIGHEST_PROTOCOL

from string import split
#from bisect import bisect_left
from collections import defaultdict
from itertools import compress, ifilterfalse


###
# #
###
def fastaq_with_nb_mappings(qname, annot_string, rlen, seq, qual, nb_mappings):
    """Returns fasta and fastq formatted sequence.
    In the .fastq, the number of retained mappings of this read is recorded.
    This information can then later be used after re-mapping."""
    fasta = ">%s (annot=%s) (len=%d)\n%s\n" % (qname, annot_string, rlen, seq)
    fastq = "@%s_%d\n%s\n+\n%s\n" % (qname, nb_mappings, seq, qual)
    return fasta, fastq


def fastaq(qname, annot_string, rlen, seq, qual):
    """Returns fasta and fastq formatted sequence."""
    fasta = ">%s (annot=%s) (len=%d)\n%s\n" % (qname, annot_string, rlen, seq)
    fastq = "@%s\n%s\n+\n%s\n" % (qname, seq, qual)
    return fasta, fastq


def report(alis, ali_shortref, Annotation, fuse, fetch):
    """Does all the reporting job."""
    # Count for future ponderation all mappings above the threshold min_score,
    # not just the best alignments of the read.
    # For example, consider a read that maps in a pi_cluster and in TE copies.
    # These alignments will likely not all have the best alignment score.
    # But they have to be counted to downwheight
    #nb_mappings = len(alis)
    assert not all(ali.is_secondary for ali in alis)
    #fuse = Annotation.fuse
    # alis should be a list of same-score alignments for a same read
    max_score = max([OPT(ali, "AS") for ali in alis])
    def lower_score(ali):
        """Returns True is the alignment <ali> has a score
        strictly lower than max_score."""
        return OPT(ali, "AS") < max_score
    all_annotations = set([])
    # Is the read a unique mapper?
    unique = None
    # loop on the alignments that do not have a score
    # lower than the maximum score among the alignments
    for ali in ifilterfalse(lower_score, alis):
        if unique is None:
            # No mismatches allowed for a unique mapper.
            if OPT(ali, "NM") == 0:
                # A first perfect match is found.
                unique = ali
        elif unique:
            if OPT(ali, "NM") == 0:
                # Several perfect matches have been found.
                unique = False
        else:
            # unique stays False
            pass
        # To orient the annotations
        # according to the read alignment orientation:
        reverse = ali.is_reverse
        if not ali.is_secondary:
            # This is the primary alignment: some information
            # common to all alignments of the read are guaranteed to be there.
            # Let's collect them to later write fasta and fastq sequences
            qname = ali.qname
            if reverse:
                seq = reverse_complement(ali.seq)
                qual = ali.qual[::-1]
            else:
                seq = ali.seq
                qual = ali.qual
            rlen = ali.rlen
        # try using tabix, building compressed bed and index first
        # (http://samtools.sourceforge.net/tabix.shtml)

        def orient(annot):
            """Uses strand information to set orientation to F or R."""
            #annot: ["typ@nam", "strand"]
            #typ, nam = annot[0].split("@")
            #typnam = annot[0]
            strand = annot[1]
            if strand == "0":
                return "@".join(annot)
                #return (typ, nam, strand)
            elif reverse:
                if strand == "-":
                    return "%s@%s" % (annot[0], "F")
                    #return (typ, nam, "F")
                else:
                    return "%s@%s" % (annot[0], "R")
            else:
                if strand == "+":
                    return "%s@%s" % (annot[0], "F")
                else:
                    return "%s@%s" % (annot[0], "R")
        #matches = Globals.tabix_file.fetch(
        #    ali_shortref(ali), ali.pos, ali.pos + ali.rlen)
        # Parsing the output lines
        # Example output
        #severine@cubitus:~/Genomes/H_sapiens/Annotations$ \
        #    tabix H_sapiens_annot.bed.gz chr2:100000-101000
        #chr2	99983	100021	repeated_sequence@LTR88b	0	+
        #chr2	100167	100462	repeated_sequence@L2a	0	-
        #chr2	100461	100717	repeated_sequence@AluSx1	0	+
        #chr2	100716	100841	repeated_sequence@L2a	0	-
        #if matches:
        #    # compress from itertools is used to select the "columns"
        #    all_annotations.update(
        #        set([orient(
        #            list(compress(match.split(),
        #                [0, 0, 0, 1, 0, 1]))) for match in matches]))
        all_annotations.update(
            {orient(
                list(compress(
                    split(match),
                    [0, 0, 0, 1, 0, 1]))) for match in fetch(
                        ali_shortref(ali), ali.pos, ali.pos + ali.rlen)})
    # Annotation object with the following attributes:
    # - annots: set of the different tuples (typ, nam, orientation)
    # determined for the read
    # - group: name of a type group in the type hierarchy
    # - strict: name of the "strict" annotation type retained for the whole set
    # - priority_annots: set of the annotations extracted from the whole set
    # that correspond to the above-mentioned group
    if all_annotations:
        #print all_annotations
        #if True:
        try:
            # TODO: use Options.exclude_types to filter out
            read_annotation = reduce(
                fuse,
                [Annotation(
                    typ, nam, strand) for typ, nam, strand in (
                        split(a, "@") for a in all_annotations)])
            # TODO:
            # See if making this more functionnal is more efficient.
            # Something like:
            #read_annotation = reduce(
            #    fuse,
            #    [Annotation(*tpl) for tpl in imap(
            #        splitatat,
            #        all_annotations)])
            # With splitatat behaving like lamba a : split(a, "@")
            # and imap imported from itertools.
        except AssertionError:
            #print ref
            print "\n".join([str(a) for a in alis])
            #print "\n".join([str(a) for a in max_score_alis])
            sys.exit(1)
    else:
        read_annotation = Annotation("not_annotated", "not_annotated", "0")
    # Do this only after all Annotations have been fused
    try:
        read_annotation.set_strict()
    except AssertionError:
        print max_score, rlen
        print seq
        print all_annotations
    group = read_annotation.group
    annot_string = "|".join(sorted(all_annotations))
    if annot_string:
        pass
    else:
        annot_string = "not_annotated@not_annotated@0"
    #fasta, fastq = fastaq(qname, annot_string, rlen, seq, qual, nb_mappings)
    fasta, fastq = fastaq(qname, annot_string, rlen, seq, qual)
    Globals.group_fastas[group].write(fasta)
    Globals.group_fastqs[group].write(fastq)
    Globals.write_siRNAs(fasta, fastq, rlen, read_annotation.priority_annots)
    if unique is None:
        # No perfect match was found.
        unique = False
    if unique:
        # Write it in bam format.
        Globals.unique_sam.write(unique)
        Globals.unique_set.add(qname)
        unique = True
    Globals.write_piRNAs(fasta, fastq, rlen, group, unique)
    #TODO: update histograms
    #
    # key: annotation type
    # value: dictionary
    #        key: orientation
    #        value: list of annotation names
    #annot_by_type_and_orientation = {}
    annot_by_type_and_orientation = defaultdict(lambda: defaultdict(int))
    for typ, nam, orient in read_annotation.annots:
        #if typ not in annot_by_type_and_orientation:
        #    annot_by_type_and_orientation[typ] = {
        #        "R": [],
        #        "F": [],
        #        "0": []}
        annot_by_type_and_orientation[typ][orient] += 1
    for typ in annot_by_type_and_orientation.keys():
        # if a read gets several annotations of a given type,
        # they should be weighted in the histograms of this type,
        # to avoid counting a read several times
        num_rev = annot_by_type_and_orientation[typ]["R"]
        num_fwd = annot_by_type_and_orientation[typ]["F"]
        num_0 = annot_by_type_and_orientation[typ]["0"]
        tot = float(num_rev + num_fwd + num_0)
        Globals.type_histos["R"][typ][rlen] = \
            Globals.type_histos["R"][typ].get(rlen, 0) + (num_rev / tot)
        Globals.type_histos["F"][typ][rlen] = \
            Globals.type_histos["F"][typ].get(rlen, 0) + (num_fwd / tot)
        Globals.type_histos["0"][typ][rlen] = \
            Globals.type_histos["0"][typ].get(rlen, 0) + (num_0 / tot)
    #
    #TODO: check this
    priority_annot_by_orientation = {
        "R": 0,
        "F": 0,
        "0": 0}
    #strict_type = read_annotation.strict
    ## debug (should already have been determined earlier)
    assert group == read_annotation.group
    ##
    for typ, nam, orient in read_annotation.priority_annots:
        priority_annot_by_orientation[orient] += 1
    # if a read gets several annotations of a given type,
    # they should be weighted in the histograms of this type,
    # to avoid counting a read several times
    num_rev = priority_annot_by_orientation["R"]
    num_fwd = priority_annot_by_orientation["F"]
    num_0 = priority_annot_by_orientation["0"]
    tot = float(num_rev + num_fwd + num_0)
    Globals.group_histos["R"][group][rlen] = \
        Globals.group_histos["R"][group].get(
            rlen, 0) + (num_rev / tot)
    Globals.group_histos["F"][group][rlen] = \
        Globals.group_histos["F"][group].get(
            rlen, 0) + (num_fwd / tot)
    Globals.group_histos["0"][group][rlen] = \
        Globals.group_histos["0"][group].get(
            rlen, 0) + (num_0 / tot)
    #Globals.strict_type_histos["R"][strict_type][rlen] = \
    #    Globals.strict_type_histos["R"][strict_type].get(
    #        rlen, 0) + (num_rev / tot)
    #Globals.strict_type_histos["F"][strict_type][rlen] = \
    #    Globals.strict_type_histos["F"][strict_type].get(
    #        rlen, 0) + (num_fwd / tot)
    #Globals.strict_type_histos["0"][strict_type][rlen] = \
    #    Globals.strict_type_histos["0"][strict_type].get(
    #        rlen, 0) + (num_0 / tot)

# TODO: check that scripts using outputted histogram data deal correctly with
# the removal of the #normalization comment line


def write_type_histos(name, out=sys.stdout):
    """Writes histograms by annotation type."""
    writer = csv.writer(out, dialect="excel-tab")
    writer.writerow(
        ["#size histogram for maps from %s on %s (by type)" % (
            Options.in_file, name)])
    writer.writerow(["#ref", name])
    writer.writerow(["size", "nb_F", "nb_R", "nb_0"])
    gth = Globals.type_histos
    lengths = gth["F"][name].keys() + \
        gth["R"][name].keys() + gth["0"][name].keys()
    if lengths:
        #i = min(lengths)
        #while i <= max(lengths):
        for i in xrange(min(lengths), max(lengths) + 1):
            abs_fwd = gth["F"][name].get(i, 0)
            abs_rev = gth["R"][name].get(i, 0)
            abs_0 = gth["0"][name].get(i, 0)
            writer.writerow([i, abs_fwd, abs_rev, abs_0])
            #i += 1
    else:
        writer.writerow(["#empty"])


def write_group_histos(name, out=sys.stdout):
    """Writes histograms by annotation group."""
    writer = csv.writer(out, dialect="excel-tab")
    writer.writerow(
        ["#size histogram for maps from %s on %s (by group)" % (
            Options.in_file, name)])
    writer.writerow(["#ref", name])
    writer.writerow(["size", "nb_F", "nb_R", "nb_0"])
    ggh = Globals.group_histos
    lengths = ggh["F"][name].keys() + \
        ggh["R"][name].keys() + ggh["0"][name].keys()
    if lengths:
        #i = min(lengths)
        #while i <= max(lengths):
        for i in xrange(min(lengths), max(lengths) + 1):
            abs_fwd = ggh["F"][name].get(i, 0)
            #norm_F = abs_F / Globals.norm_factor
            abs_rev = ggh["R"][name].get(i, 0)
            #norm_R = abs_R / Globals.norm_factor
            abs_0 = ggh["0"][name].get(i, 0)
            #norm_0 = abs_0 / Globals.norm_factor
            writer.writerow([i, abs_fwd, abs_rev, abs_0])
            #i += 1
    else:
        writer.writerow(["#empty"])


#########################################################
# For better traceability, options and global variables #
# should be stored as class attributes                  #
#########################################################


class Globals(object):
    """This object holds global variables."""
    # default range impossible so that no potential piRNAs will be written
    pi_min = 1000000
    pi_max = -1
    #chrom_lengths = None

    @staticmethod
    def write_siRNAs(fasta, fastq, rlen, annots):
        """Does nothing. We don't want to write siRNA sequences."""
        pass

    @staticmethod
    def write_piRNAs(fasta, fastq, rlen, group, unique):
        """Does nothing. We don't want to write piRNA sequences."""
        pass


class Options(object):
    """This object contains the values of the global options."""
    annot_file = None
    annot_module_file = None
    annot_types_file = None
    exclude_types = set([])
    in_file = None
    keep_chr = False
    max_edit_proportion = 0.0
    min_mapq = 0
    min_score = -200
    pi_range = None
    histo_dir = None
    seq_dir = None
    unique_set_file = None

#################
# main function #
#################


def main():
    """Main function."""
    WRITE(" ".join(sys.argv) + "\n")
    try:
        opts, args = getopt.getopt(
            sys.argv[1:], "io:",
            [
                "help", "keep_chr",
                "annot=", "annot_module=", "exclude_types=",
                "in_file=", "max_edit_proportion=",
                "min_mapq=", "min_score=", "pi_range=",
                "histo_dir=", "seq_dir=", "unique_set_file="])
    except getopt.GetoptError:
        sys.stderr.write(__doc__)
        sys.exit(1)
    for option, argument in opts:
        if option == "--help":
            WRITE(__doc__)
            sys.exit()
        if option == "--annot":
            Options.annot_file = argument
        if option == "--annot_module":
            Options.annot_module_file = argument
        #if option == "--annot_types":
        #    Options.annot_types_file = argument
        if option == "--exclude_types":
            Options.exclude_types = set(split(argument, ","))
        if option == "--in_file":
            Options.in_file = argument
        if option == "--keep_chr":
            Options.keep_chr = True
        if option == "--max_edit_proportion":
            warnings.warn("--max_edit_proportion is ignored")
            Options.max_edit_proportion = float(argument)
        if option == "--min_mapq":
            warnings.warn("--min_mapq is ignored")
            Options.min_mapq = int(argument)
        if option == "--min_score":
            Options.min_score = float(argument)
        if option == "--pi_range":
            pi_min, pi_max = split(argument, ",")
            Globals.pi_min = int(pi_min)
            Globals.pi_max = int(pi_max)
            if Globals.pi_min > Globals.pi_max:
                sys.stderr.write(
                    "".join(
                        "%s is not a valid range " % argument,
                        "for piRNA read sizes.\n"))
                sys.exit(1)
        if option == "--histo_dir":
            Options.histo_dir = argument
        if option == "--seq_dir":
            Options.seq_dir = argument
        if option == "--unique_set_file":
            Options.unique_set_file = argument

    if Options.in_file is None:
        sys.stderr.write(
            "You should provide a .sam file "
            "with the option --in_file\n")
        sys.exit(1)
    if os.path.isfile("%s.bgz" % Options.annot_file):
        msg = "".join([
            "An index should have been generated ",
            "with tabix for %s.bgz." % Options.annot_file])
        assert os.path.isfile("%s.bgz.tbi" % Options.annot_file), msg
    elif os.path.isfile("%s.gz" % Options.annot_file):
        msg = "".join([
            "An index should have been generated ",
            "with tabix for %s.gz." % Options.annot_file])
        assert os.path.isfile("%s.gz.tbi" % Options.annot_file), msg
    else:
        warnings.warn(
            "Annotation file provided with the option --annot "
            "doesn't seem to have a bgzip-compressed "
            "and tabix-indexed version.\nExpect failures.\n")
    if Options.histo_dir is None:
        warnings.warn(
            "You haven't provided a directory in which "
            "the histogram data will be written with the option --histo_dir"
            "\nThe data will go to standard output.\n")
    if Options.seq_dir is None:
        sys.stderr.write(
            "You should provide a directory in which "
            "the output files will be written with the option --seq_dir\n")
        sys.exit(1)

    module_name = OPB(Options.annot_module_file)[:-3]
    WRITE("Loading module %s from file %s\n" % (
        module_name,
        Options.annot_module_file))
    #module_path = os.path.dirname(Options.annot_module_file)
    #module_desc = imp.find_module(module_name, [module_path])
    #annotation_module = imp.load_module(full_name, *module_desc)
    try:
        annotation_module = imp.load_source(
            module_name,
            Options.annot_module_file)
    except IOError:
        print os.listdir(".")
        sys.exit(1)
    #annotation_module = __import__(Options.annot_module_file[:-3])
    Annotation = annotation_module.Annotation
    # Define priority testers
    trans_table = maketrans("(-)", "___")
    for group_name in Annotation.groups:
        tester_name = "is_lower_than_%s" % group_name.translate(trans_table)
        setattr(
            Annotation,
            tester_name,
            staticmethod(Annotation.lower_priority_tester(group_name)))
    fuse = Annotation.fuse
    if not Annotation.siRNA_types:
        def write_siRNAs(fasta, fastq, rlen, annots):
            """Does nothing. We don't want to write siRNA sequences."""
            pass
    else:
        def write_siRNAs(fasta, fastq, rlen, annots):
            """Writes siRNA sequences to file."""
            if rlen == 21:
                if any(a[0] in Annotation.siRNA_types for a in annots):
                    Globals.group_fastas["siRNA"].write(fasta)
                    Globals.group_fastqs["siRNA"].write(fastq)
    Globals.write_siRNAs = staticmethod(write_siRNAs)
    if ((Globals.pi_max < Globals.pi_min)
            or (not Annotation.piRNA_groups)):
        def write_piRNAs(fasta, fastq, rlen, group, unique):
            """Does nothing. We don't want to write piRNA sequences."""
            pass
    else:
        def write_piRNAs(fasta, fastq, rlen, group, unique):
            """Writes piRNA sequences to file."""
            if unique:
                # Note that these reads are written
                # regardless of their annotation group
                if rlen == 21:
                    Globals.group_fastas["siRNA_unique"].write(fasta)
                    Globals.group_fastqs["siRNA_unique"].write(fastq)
                elif Globals.pi_min <= rlen <= Globals.pi_max:
                    Globals.group_fastas["piRNA_unique"].write(fasta)
                    Globals.group_fastqs["piRNA_unique"].write(fastq)
            if group in Annotation.piRNA_groups:
                if rlen == 21:
                    Globals.group_fastas["siRNA_candidate"].write(fasta)
                    Globals.group_fastqs["siRNA_candidate"].write(fastq)
                elif Globals.pi_min <= rlen <= Globals.pi_max:
                    Globals.group_fastas["piRNA_candidate"].write(fasta)
                    Globals.group_fastqs["piRNA_candidate"].write(fastq)
    Globals.write_piRNAs = staticmethod(write_piRNAs)
    Globals.unique_set = set([])
    #TODO: Use the Annotation module to set this.
    WRITE("Reading the list of valid annotation types.\n")
    #annot_types = set(["not_annotated"])
    #types_file = open(Options.annot_types_file, "r")
    #for line in types_file:
    #    annot_types.add(line.strip())
    #types_file.close()
    #annot_types = sorted(annot_types)

    #try:
    #    pysam.tabix_compress(
    #        Options.annot_file, "%s.bgz" % Options.annot_file)
    #except IOError:
    #    assert os.path.isfile(Options.annot_file)
    #    assert os.path.isfile("%s.bgz" % Options.annot_file)
    if os.path.isfile("%s.bgz" % Options.annot_file):
        tabix_file = pysam.TabixFile("%s.bgz" % Options.annot_file)
    elif os.path.isfile("%s.gz" % Options.annot_file):
        tabix_file = pysam.TabixFile("%s.gz" % Options.annot_file)
    else:
        tabix_file = pysam.TabixFile(Options.annot_file)
    #
    fetch_from_tabix = tabix_file.fetch
    #annotated_refs = tabix_file.contigs
    def fetch(reference, start, end):
        try:
            return fetch_from_tabix(reference, start, end)
        except ValueError:
            return []
    WRITE("Getting information about references in sam file...\n")
    #separate_RF = False
    if Options.in_file.endswith(".bam"):
        warnings.warn(
            "You provided a .bam file instead of a .sam."
            "\nIt is not guaranteed that annotation will be correct.\n")
        samfile = pysam.AlignmentFile(Options.in_file, "rb")
    else:
        samfile = pysam.AlignmentFile(Options.in_file, "r")
    #refs = samfile.references
    #lengths = samfile.lengths
    #opt = pysam.AlignedSegment.opt
    getrname = samfile.getrname
    #if all(ref.startswith("chr") for ref in refs) and not Options.keep_chr:
    if not Options.keep_chr:
        # We will have to remove the "chr" prefix
        # when looking for the annotation
        #remove_chr = True
        def shortname(ref):
            """Removes the 'chr' from a chromosome name."""
            if ref.startswith("chr"):
                return ref[3:]
            else:
                return ref
    else:
        #remove_chr = False
        def shortname(ref):
            """Just returns the reference name."""
            return ref
    #Globals.chrom_lengths = dict(
    #    zip([shortname(ref) for ref in refs], lengths))

    def ali_shortref(ali):
        """Returns the reference short name of an alignemnt <ali>."""
        return shortname(getrname(ali.tid))

    #print H.heap()

    # files for each group of annotated sequences
    Globals.group_fastas = {}
    Globals.group_fastqs = {}
    if Options.in_file[:-4].endswith("_sorted"):
        basename = "_".join(split(OPB(Options.in_file), "_")[:-1])
    else:
        basename = OPB(Options.in_file)[:-4]
    # File in which to write aligned unique piRNA reads:
    Globals.unique_sam = pysam.AlignmentFile(
        OPJ(
            OPD(Options.in_file),
            "%s_unique.bam" % basename),
        "wb",
        template=samfile)
    for group in Annotation.groups + [
            "low_score", "unmapped", "siRNA_unique", "piRNA_unique",
            "siRNA", "siRNA_candidate", "piRNA_candidate"]:
        Globals.group_fastas[group] = open(OPJ(
            Options.seq_dir,
            basename + "_%s.fasta" % group), "w")
        Globals.group_fastqs[group] = open(OPJ(
            Options.seq_dir,
            basename + "_%s.fastq" % group), "w")
    # length histograms for each type (or type group) of annotation
    Globals.type_histos = {}
    Globals.type_histos["F"] = {}
    Globals.type_histos["R"] = {}
    Globals.type_histos["0"] = {}
    #Globals.strict_type_histos = {}
    #Globals.strict_type_histos["F"] = {}
    #Globals.strict_type_histos["R"] = {}
    #Globals.strict_type_histos["0"] = {}
    Globals.group_histos = {}
    Globals.group_histos["F"] = {}
    Globals.group_histos["R"] = {}
    Globals.group_histos["0"] = {}
    # different histograms when grouping by annotation type
    # or by annotation group type
    for group in Annotation.groups:
        Globals.group_histos["F"][group] = {}
        Globals.group_histos["R"][group] = {}
        Globals.group_histos["0"][group] = {}
    #for typ in Annotation.strict_types:
    #    Globals.strict_type_histos["F"][typ] = {}
    #    Globals.strict_type_histos["R"][typ] = {}
    #    Globals.strict_type_histos["0"][typ] = {}
    for typ in Annotation.annot_types:
        Globals.type_histos["F"][typ] = {}
        Globals.type_histos["R"][typ] = {}
        Globals.type_histos["0"][typ] = {}
    # counts of QC and NM reads
    QC = 0
    NM = 0
    # chromosome names in Flybase annotations do not have
    # the initial "chr" found in UCSC genome
    #for ref in refs:
    #    if remove_chr:
    #        #fullref = ref
    #        ref = ref[3:]
    #    else:
    #        #fullref = ref
    #        pass
    last_qname = None
    to_report = []
    low_score = []
    qc = 0
    nm = 0
    aligned_reads = 0
    #info_threshold = 1000000
    #info_increment = 1000000
    info_threshold = 10000
    info_increment = 10000
    WRITE("Annotating aligned reads from file %s\n" % Options.in_file)
    # this loop relies on the fact that alignments are grouped by query names
    for ali in samfile.fetch():
        if last_qname != ali.qname:
            if to_report:
                assert qc == 0
                assert nm == 0
                report(to_report, ali_shortref, Annotation, fuse, fetch)
                aligned_reads += 1
                if aligned_reads == info_threshold:
                    msg = "".join([
                        "%s aligned reads " % aligned_reads,
                        "annotated (or not)...\n"])
                    WRITE(msg)
                    info_threshold += info_increment
            if low_score:
                assert qc == 0
                assert nm == 0
                assert len(low_score) == 1 and not low_score[0].is_secondary
                ls_ali = low_score[0]
                fasta, fastq = fastaq(
                    ls_ali.qname, "low_score@low_score@0",
                    ls_ali.rlen, ls_ali.seq, ls_ali.qual)
                Globals.group_fastas["low_score"].write(fasta)
                Globals.group_fastqs["low_score"].write(fastq)
                write_piRNAs(fasta, fastq, ls_ali.rlen, "low_score", False)
                aligned_reads += 1
                if aligned_reads == info_threshold:
                    msg = "".join([
                        "%s aligned reads " % aligned_reads,
                        "annotated (or not)...\n"])
                    WRITE(msg)
                    info_threshold += info_increment
            assert not (qc and nm) and qc <= 1 and nm <= 1
            #tot = len(to_report) + qc + nm + len(no_report)
            QC += qc
            NM += nm
            last_qname = ali.qname
            to_report = []
            low_score = []
            qc = 0
            nm = 0
        else:
            # last_qname == ali.qname
            if low_score:
                # The primary alignment for this read had a low score,
                # the others will not do better
                # We'll skip this alignment
                continue
        if ali.is_qcfail:
            qc += 1
            #QC[ref] += 1
        elif ali.is_unmapped:
            #fasta, fastq = fastaq(ali.qname, "unmapped@unmapped@0",
            #        ali.rlen, ali.seq, ali.qual, 0)
            fasta, fastq = fastaq(
                ali.qname, "unmapped@unmapped@0",
                ali.rlen, ali.seq, ali.qual)
            Globals.group_fastas["unmapped"].write(fasta)
            Globals.group_fastqs["unmapped"].write(fastq)
            write_piRNAs(fasta, fastq, ali.rlen, "unmapped", False)
            nm += 1
            #NM[ref] += 1
        else:
            # the reference name should be computed
            # for each reported alignment:
            # there was a bug by which all alignments of a given read
            # where considered to be on the same ref
            if OPT(ali, "AS") >= Options.min_score:
                to_report.append(ali)
            else:
                if not ali.is_secondary:
                    # the primary alignment has a low score,
                    # consider it should have been a non-mapper
                    low_score.append(ali)
    # deal with the last read:
    if to_report:
        assert qc == 0
        assert nm == 0
        report(to_report, ali_shortref, Annotation, fuse, fetch)
        aligned_reads += 1
        #if aligned_reads == info_threshold:
        #    write("%s aligned reads annotated...\n" % aligned_reads)
        #    info_threshold += info_increment
    if low_score:
        assert qc == 0
        assert nm == 0
        assert len(low_score) == 1 and not low_score[0].is_secondary
        ls_ali = low_score[0]
        fasta, fastq = fastaq(
            ls_ali.qname, "low_score@low_score@0",
            ls_ali.rlen, ls_ali.seq, ls_ali.qual)
        Globals.group_fastas["low_score"].write(fasta)
        Globals.group_fastqs["low_score"].write(fastq)
        write_piRNAs(fasta, fastq, ls_ali.rlen, "low_score", False)
        aligned_reads += 1
        #if aligned_reads == info_threshold:
        #    write("%s aligned reads annotated (or not)...\n" % aligned_reads)
        #    info_threshold += info_increment
    tabix_file.close()
    assert not (qc and nm) and qc <= 1 and nm <= 1
    #tot = len(to_report) + qc + nm + len(no_report)
    WRITE("%s aligned reads annotated (or not)...\n" % aligned_reads)
    QC += qc
    NM += nm
    #last_qname = ali.qname
    #to_report = []
    #no_report = []
    #qc = 0
    #nm = 0
    #if Options.in_file[:-4].endswith("_sorted"):
    #    basename = "_".join(OPB(Options.in_file).split("_")[:-1])
    #else:
    #    basename = OPB(Options.in_file)[:-4]
    WRITE("Writing size histograms...\n")
    #for typ in Annotation.strict_types:
    #    WRITE("Writing size histograms for strict type %s...\n" % typ)
    #    fich = open(OPJ(
    #        Options.histo_dir,
    #        basename + "_strict_%s_lengths.txt" % typ), "wb")
    #    write_strict_type_histos(typ, fich)
    #    fich.close()
    for typ in Annotation.annot_types:
        WRITE("Writing size histograms for type %s...\n" % typ)
        fich = open(OPJ(
            Options.histo_dir,
            basename + "_%s_lengths.txt" % typ), "wb")
        write_type_histos(typ, fich)
        fich.close()
    for group in Annotation.groups:
        WRITE("Writing size histograms for group %s...\n" % group)
        fich = open(OPJ(
            Options.histo_dir,
            basename + "_group_%s_lengths.txt" % group), "wb")
        write_group_histos(group, fich)
        fich.close()
    ##for group in Annotation.groups:
    #    Globals.group_fastas[group].close()
    #    Globals.group_fastqs[group].close()
    #Globals.group_fastas["low_score"].close()
    #Globals.group_fastqs["low_score"].close()
    #Globals.group_fastas["unmapped"].close()
    #Globals.group_fastqs["unmapped"].close()
    #Globals.group_fastas["siRNA_unique"].close()
    #Globals.group_fastqs["siRNA_unique"].close()
    #Globals.group_fastas["piRNA_unique"].close()
    #Globals.group_fastqs["piRNA_unique"].close()
    #Globals.group_fastas["siRNA"].close()
    #Globals.group_fastqs["siRNA"].close()
    #Globals.group_fastas["siRNA_candidate"].close()
    #Globals.group_fastqs["siRNA_candidate"].close()
    #Globals.group_fastas["piRNA_candidate"].close()
    #Globals.group_fastqs["piRNA_candidate"].close()
    for fasta_file in Globals.group_fastas.values():
        fasta_file.close()
    for fastq_file in Globals.group_fastqs.values():
        fastq_file.close()
    if Options.unique_set_file:
        WRITE("Pickling set of unique mappers...\n")
        with open(Options.unique_set_file, "wb") as pickle_file:
            dump(Globals.unique_set, pickle_file, HIGHEST_PROTOCOL)
    WRITE("qc_fail: %d\n" % QC)
    WRITE("unaligned: %d\n" % NM)
    WRITE("aligned: %d\n" % aligned_reads)
    return 0

if __name__ == "__main__":
    sys.exit(main())
    #cProfile.run("main()")
