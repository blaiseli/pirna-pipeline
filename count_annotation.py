#!/usr/bin/env python
"""
usage: count_annotation.py [options]

This script will read sam-formatted alignments and counts and extract the reads
mapping on a given annotation or set of annotations.

The annotations should be provided in the form of a .bed file with lines
formatted as follows (almost 6-columns bed format: extended with the
possibility of having 0 for the strand):

<chromosome>\t<start>\t<end>\t<annotation_type>@<feature_name>\t0\t<strand>
<strand> can be +, - or 0
0 is when the strand is not documented or undefined.

Warning: <start> is 0-based but <end> isn't (see
http://genome.ucsc.edu/FAQ/FAQformat#format1 about bed format).

A compressed .bgz version and an index .bgz.tbi of it should exist, created by
bgzip and tabix: http://samtools.sourceforge.net/tabix.shtml

The sam-formatted alignements must have entries grouped by query name (i. e.
alignments of a same query should not be dispersed).


Available options:

    --help: prints this help text and exits.

    --keep_chr: to keep the "chr" prefix on the reference names. This amounts
    to assuming that the annotations use the chr prefix too.

    --in_file <file>: to specify the .sam file containing the mapping
    information to process.

    --annot <file>: to secify the file containing the annotations in .bed
    format. There must be a .bed.bgz (or .bed.gz) compressed version and
    .bed.bgz.tbi (or .bed.gz.tbi) corresponding tabix index in the same
    directory.

    --max_edit_proportion: <float>: to specify the maximum proportion of edits
    for an alignment to be recorded.
    The default value is 0.0.

    --min_mapq: <int>: to specify the minimum value of mapq mapping quality for
    a read to be recorded.
    The default value is 0.

    --out_file: <file>: to specify the fastq file in which the uniquely mapping
    reads have to be written.

    --read_range <minlen>,<maxlen>: to specify the minimum and maximum lengths
    of the reads to take into account.
    The default range is 1,50

    --record_count <file>: file in which to write the count.

"""


import getopt
import os
#OPJ = os.path.join
#OPB = os.path.basename
#import re
import sys
# see https://wiki.python.org/moin/PythonSpeed/PerformanceTips#Doing_Stuff_Less_Often
sys.setcheckinterval(1000)
WRITE = sys.stdout.write
import warnings
def formatwarning(message, category, filename, lineno, line):
    """Formats a warning message."""
    return "%s:%s: %s:%s" % (filename, lineno, category.__name__, message)
warnings.formatwarning = formatwarning

from itertools import compress
#from operator import or_
from string import split

import pysam

from Bio.Seq import reverse_complement




#TODO: extract unique/non-unique reads to make pileups


##############################################################################################
# For better traceability, options and global variables should be stored as class attributes #
##############################################################################################


class Globals(object):
    """This object holds global variables."""
    chromosomes = {}


class Options(object):
    """This object contains the values of the global options."""
    annot_file = None
    check_ali_ends_only = False
    in_file = None
    keep_chr = False
    max_edit_proportion = 0.0
    min_mapq = 0
    out_file = None
    read_range = (1, 50)
    record_count = None

#################
# main function #
#################


def main():
    """The main function."""
    WRITE(" ".join(sys.argv) + "\n")
    try:
        opts, args = getopt.getopt(
            sys.argv[1:], "io:",
            [
                "help", "keep_chr",
                "annot=", "in_file=",
                "max_edit_proportion=", "min_mapq=",
                "out_file=", "read_range=", "record_count="])
    except getopt.GetoptError:
        sys.stderr.write(__doc__)
        sys.exit(1)
    for option, argument in opts:
        if option == "--help":
            WRITE(__doc__)
            sys.exit()
        if option == "--annot":
            Options.annot_file = argument
        if option == "--in_file":
            Options.in_file = argument
        if option == "--keep_chr":
            Options.keep_chr = True
        if option == "--max_edit_proportion":
            Options.max_edit_proportion = float(argument)
        if option == "--min_mapq":
            Options.min_mapq = int(argument)
        if option == "--out_file":
            Options.out_file = argument
        if option == "--read_range":
            minlen, maxlen = split(argument, ",")
            Options.read_range = (int(minlen), int(maxlen))
        if option == "--record_count":
            Options.record_count = argument

    if Options.in_file is None:
        sys.stderr.write(
            "You should provide a sam file "
            "with the option --in_file\n")
        sys.exit(1)

    if os.path.isfile("%s.bgz" % Options.annot_file):
        msg = "".join([
            "An index should have been generated ",
            "with tabix for %s.bgz." % Options.annot_file])
        assert os.path.isfile("%s.bgz.tbi" % Options.annot_file), msg
        tabix_file = pysam.Tabixfile("%s.bgz" % Options.annot_file)
    elif os.path.isfile("%s.gz" % Options.annot_file):
        msg = "".join([
            "An index should have been generated ",
            "with tabix for %s.gz." % Options.annot_file])
        assert os.path.isfile("%s.gz.tbi" % Options.annot_file), msg
        tabix_file = pysam.Tabixfile("%s.gz" % Options.annot_file)
    else:
        warnings.warn(
            "Annotation file provided with the option --annot "
            "doesn't seem to have a bgzip-compressed "
            "and tabix-indexed version.\nExpect failures.\n")
        tabix_file = pysam.Tabixfile(Options.annot_file)
    fetch = tabix_file.fetch
    fetchable = set(tabix_file.contigs)

    samfile = pysam.Samfile(Options.in_file, "r")
    refs = samfile.references
    getrname = samfile.getrname
    if not Options.keep_chr:
        # We will have to remove the "chr" prefix
        # when looking for the annotation
        #remove_chr = True
        def shortname(ref):
            """Removes the 'chr' from a chromosome name."""
            if ref.startswith("chr"):
                return ref[3:]
            else:
                return ref
    else:
        #remove_chr = False
        def shortname(ref):
            """Just returns the reference name."""
            return ref
    #if all(ref.startswith("chr") for ref in refs):
    #    # We will have to remove the "chr" prefix when looking for the annotation
    #    remove_chr = True
    #    def shortname(ref):
    #        return ref[3:]
    #else:
    #    remove_chr = False
    #    def shortname(ref):
    #        return ref
    def ali_shortref(ali):
        return shortname(getrname(ali.tid))
    # Define counting function
    def count(alis, qname, rlen, seq, qual):
        all_annotations = set([])
        for ali in alis:
            ref = ali_shortref(ali)
            if ref in fetchable:
                matches = list(
                    fetch(ref, ali.pos, ali.pos + ali.rlen))
                if matches:
                    reverse = ali.is_reverse
                    def orient(annot):
                        """Uses strand information to set orientation to F or R."""
                        #annot: ["typ@nam", "strand"]
                        #typ, nam = annot[0].split("@")
                        #typnam = annot[0]
                        strand = annot[1]
                        if strand == "0":
                            return "@".join(annot)
                            #return (typ, nam, strand)
                        elif reverse:
                            if strand == "-":
                                return "%s@%s" % (annot[0], "F")
                                #return (typ, nam, "F")
                            else:
                                return "%s@%s" % (annot[0], "R")
                        else:
                            if strand == "+":
                                return "%s@%s" % (annot[0], "F")
                            else:
                                return "%s@%s" % (annot[0], "R")
                    all_annotations.update(
                        {orient(
                            list(compress(
                                split(match),
                                [0, 0, 0, 1, 0, 1]))) for match in matches})
                else:
                    # An alignment of the read occurred outside a zone
                    # corresponding to annotations presend in the bed file
                    # provided with the option --annot
                    return 0
            else:
                # The reference on which ali occurs is not among
                # the references present in the bed file
                return 0
        assert all_annotations, "There should be annotations at this point."
        # write sequence
        with open(Options.out_file, "a") as f:
            f.write("@%s (len=%d)\n%s\n+\n%s\n" % (qname, rlen, seq, qual))
        return 1
    # To ensure file exists and starts empty
    f = open(Options.out_file, "w")
    f.close()
    # counts of QC and NM reads
    QC = 0
    NM = 0
    # chromosome names in Flybase annotations do not have
    # the initial "chr" found in UCSC genome
    #for ref in refs:
    #    if remove_chr:
    #        #fullref = ref
    #        ref = ref[3:]
    #    else:
    #        #fullref = ref
    #        pass
    last_qname = None
    to_report = []
    no_report = []
    counts = 0
    qc = 0
    nm = 0
    aligned_reads = 0
    # to know if a read already has an alignment
    # taken for fastq sequence output
    no_seq = True
    info_threshold = 1000000
    info_increment = 1000000
    WRITE("Annotating aligned reads from file %s\n" % Options.in_file)
    min_size, max_size = Options.read_range
    # This loop relies on the fact that alignments
    # are grouped by query names
    for ali in samfile.fetch():
        if last_qname != ali.qname:
            # time to deal with the last group of alignments
            # concerning a given read
            if to_report:
                assert qc == 0
                assert nm == 0
                # At this point to_report should contain
                # all the best-score alignments of a given read.
                counts += count(
                    to_report, last_qname, rlen, seq, qual)
                aligned_reads += 1
                if aligned_reads == info_threshold:
                    WRITE("%s aligned reads annotated (or not)...\n" % aligned_reads)
                    info_threshold += info_increment
            if no_report:
                assert qc == 0
                assert nm == 0
                for a in no_report:
                    if not a.is_secondary:
                        aligned_reads += 1
                if aligned_reads == info_threshold:
                    WRITE("%s aligned reads annotated (or not)...\n" % aligned_reads)
                    info_threshold += info_increment
            assert not (qc and nm) and qc <= 1 and nm <= 1
            #tot = len(to_report) + qc + nm + len(no_report)
            QC += qc
            NM += nm
            last_qname = ali.qname
            to_report = []
            no_report = []
            qc = 0
            nm = 0
            no_seq = True
        if ali.is_qcfail:
            qc += 1
            #assert ali.flag not in [0, 4, 16], "flag %d for is_qcfail" % ali.flag
        elif ali.is_unmapped:
            nm += 1
            #assert ali.flag not in [0, 16], "flag %d for is_unmapped" % ali.flag
        else:
            if no_seq:
                rlen = ali.rlen
                if ali.is_reverse:
                    seq = reverse_complement(ali.seq)
                    qual = ali.qual[::-1]
                else:
                    seq = ali.seq
                    qual = ali.qual
                no_seq = False
            l = ali.rlen
            if ali.mapq >= Options.min_mapq and (ali.opt("NM") <= l * Options.max_edit_proportion):
                # report only if the alignment score is among the highests
                try:
                    do_report = (ali.opt("AS") >= ali.opt("XS")) \
                        and (min_size <= l <= max_size)
                except KeyError:
                    # Absence of "XS" flag means
                    # that there is only one alignment
                    do_report = (min_size <= l <= max_size)
                if do_report:
                    to_report.append(ali)
            else:
                no_report.append(ali)
    # deal with the last read:
    if to_report:
        assert qc == 0
        assert nm == 0
        # At this point to_report should contain
        # all the best-score alignments of a given read.
        counts += count(to_report, last_qname, rlen, seq, qual)
        aligned_reads += 1
        #if aligned_reads == info_threshold:
        #    WRITE("%s aligned reads annotated...\n" % aligned_reads)
        #    info_threshold += info_increment
    if no_report:
        assert qc == 0
        assert nm == 0
        for a in no_report:
            if not a.is_secondary:
                aligned_reads += 1
    assert not (qc and nm) and qc <= 1 and nm <= 1
    #tot = len(to_report) + qc + nm + len(no_report)
    WRITE("%s aligned reads annotated (or not)...\n" % aligned_reads)
    QC += qc
    NM += nm
    #last_qname = ali.qname
    #to_report = []
    #no_report = []
    #qc = 0
    #nm = 0
    WRITE("qc_fail: %d\n" % QC)
    WRITE("unaligned: %d\n" % NM)
    WRITE("aligned: %d\n" % aligned_reads)
    WRITE("strictly mapping on %s: %d \n" % (Options.annot_file, counts))
    if Options.record_count is not None:
        with open(Options.record_count, 'w') as counts_f:
            counts_f.write("%d\n" % counts)
    return 0

if __name__ == "__main__":
    sys.exit(main())
