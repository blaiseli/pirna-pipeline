#!/usr/bin/env python

import sys
import warnings
def formatwarning(message, category, filename, lineno, line):
    return "%s:%s: %s:%s" % (filename, lineno, category.__name__, message)
warnings.formatwarning = formatwarning
#import decimal
#DEC = decimal.Decimal
from scipy import stats as stats
import numpy as np

## Not sure 0.10 is the correct limit
#if float(stats.__version__) < 0.10:
#    def ttest(a, b, axis=0, equal_var=True):
#        # TODO: replace by code adapted from new and old version
#        return stats.ttest_ind(a, b, axis=axis, equal_var=equan_var)
#else:
#    ttest = stats.ttest_ind
ttest = stats.ttest_ind

from itertools import combinations
from collections import defaultdict
from re import sub
from string import strip, split

infilename = sys.argv[1]
#basename = os.path.basename(infilename)


samples = defaultdict(list)
with open(infilename) as csv:
    for line in map(strip, csv):
        if not line:
            continue
        fields = split(line, "\t")
        samples[fields[1]].append(float(sub(",", ".", fields[2])))

for (condition_1, condition_2) in combinations(sorted(samples.keys()), 2):
    title = "Comparing conditions %s and %s" % (condition_1, condition_2)
    sys.stdout.write("=" * len(title) + "\n")
    sys.stdout.write(title + "\n")
    sys.stdout.write("=" * len(title) + "\n")
    # Effectifs des deux series
    #n1 = len(samples["1"])
    #n2 = len(samples["2"])
    n1 = len(samples[condition_1])
    n2 = len(samples[condition_2])

    # http://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.shapiro.html
    #
    # Perform the Shapiro-Wilk test for normality.
    #
    # The Shapiro-Wilk test tests the null hypothesis that the data was drawn from
    # a normal distribution.

    try:
        w_shapiro_1, p_shapiro_1 = stats.shapiro(samples[condition_1])

        if p_shapiro_1 < 0.05:
            normal_1 = False
        else:
            normal_1 = True
    except ValueError, e:
        warnings.warn("%s\n" % str(e))
        warnings.warn("Arbitrarily considering distribution as not normal\n")
        p_shapiro_1 = "ND"
        normal_1 = False

    try:
        w_shapiro_2, p_shapiro_2 = stats.shapiro(samples[condition_2])
        if p_shapiro_2 < 0.05:
            normal_2 = False
        else:
            normal_2 = True
    except ValueError, e:
        warnings.warn("%s\n" % str(e))
        warnings.warn("Arbitrarily considering distribution as not normal\n")
        p_shapiro_2 = "ND"
        normal_2 = False

    if normal_1 and normal_2:
        normal = True
    else:
        normal = False


    if normal:
        sys.stdout.write("The two series have a normal distribution (Shapiro-Wilk's p-values: %s and %s).\n" % (str(p_shapiro_1), str(p_shapiro_2)))
        sys.stdout.write("We perform a mean comparison test.\n")
        
        # http://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.levene.html
        #
        # Perform Levene test for equal variances.
        #
        # The Levene test tests the null hypothesis that all input samples are from
        # populations with equal variances. Levene's test is an alternative to
        # Bartlett's test bartlett in the case where there are significant deviations
        # from normality.
        #
        # Three variations of Levene's test are possible. The possibilities and their
        # recommended usages are:
        #
        # * 'median' : Recommended for skewed (non-normal) distributions>
        # * 'mean' : Recommended for symmetric, moderate-tailed distributions.
        # * 'trimmed' : Recommended for heavy-tailed distributions.

        w_levene, p_levene = stats.levene(samples[condition_1], samples[condition_2])
        if p_levene < 0.05:
            equal_var = False
            sys.stdout.write("Variances are not homogeneous (Levene's p-value: %f).\n" % p_levene)
            sys.stdout.write("We perform a Welch's t-test.\n")
        else:
            equal_var = True
            sys.stdout.write("Variances are homogeneous (Levene's p-value: %f).\n" % p_levene)
            sys.stdout.write("We perform a Student's t-test.\n")
        
        # http://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.ttest_ind.html
        #
        # Calculates the T-test for the means of TWO INDEPENDENT samples of scores.
        #
        # This is a two-sided test for the null hypothesis that 2 independent samples
        # have identical average (expected) values. This test assumes that the
        # populations have identical variances.
        #
        # We can use this test, if we observe two independent samples from the same or
        # different population, e.g. exam scores of boys and girls or of two ethnic
        # groups. The test measures whether the average (expected) value differs
        # significantly across samples. If we observe a large p-value, for example
        # larger than 0.05 or 0.1, then we cannot reject the null hypothesis of
        # identical average scores. If the p-value is smaller than the threshold, e.g.
        # 1%, 5% or 10%, then we reject the null hypothesis of equal averages.

        t_ind, p_value = stats.ttest_ind(samples[condition_1], samples[condition_2], equal_var=equal_var)

    else:
        sys.stdout.write("At least one of the two series does not have a normal distribution (Shapiro-Wilk's p-values: %s and %s).\n" % (str(p_shapiro_1), str(p_shapiro_2)))
        sys.stdout.write("We perform a Mann-Whitney rank-sum comparison test (with correction for continuity).\n")
        # http://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.mannwhitneyu.html
        #
        # Computes the Mann-Whitney rank test on samples x and y.
        #
        # Use only when the number of observation in each sample is > 20 and you have 2
        # independent samples of ranks. Mann-Whitney U is significant if the u-obtained
        # is LESS THAN or equal to the critical value of U.
        #
        # This test corrects for ties and by default uses a continuity correction. The
        # reported p-value is for a one-sided hypothesis, to get the two-sided p-value
        # multiply the returned p-value by 2.
        if n1 <= 20 or n2 <= 20:
            sys.stderr.write("sample size %d and %d: note that the Mann-Whitney test uses an invalid normal approximation for small sample sizes.\n" % (n1, n2))
            #u_mannwhitneyu = 0
            #for v1 in samples["1"]:
            #    for v2 in samples["2"]:
            #        if v2 < v1:
            #            u_mannwhitneyu += 1
            #        elif v2 == v1:
            #            u_mannwhitneyu += 0.5
            #        else:
            #            pass
        #import rpy
        #rpy.r.wilcox.test(samples["1"], samples["2"])
        #mannwhitney = rpy.r["wilcox.test"](samples["1"], samples["2"], exact=True)
        #p_manwhitney = mannwhitney["p.value"]
        #u_mannwhitneyu, p_mannwhitneyu = stats.mannwhitneyu(samples["1"], samples["2"])
        u_mannwhitneyu, p_value = stats.mannwhitneyu(samples[condition_2], samples[condition_1], use_continuity=True)
        p_value *= 2

    sys.stdout.write("Two-tailed p-value:\t%f\n\n" % p_value)


        # http://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.wilcoxon.html
        #
        # Calculate the Wilcoxon signed-rank test.
        #
        # The Wilcoxon signed-rank test tests the null hypothesis that two related
        # paired samples come from the same distribution. In particular, it tests
        # whether the distribution of the differences x - y is symmetric about zero. It
        # is a non-parametric version of the paired T-test.
        #
        # Because the normal approximation is used for the calculations, the samples
        # used should be large. A typical rule is to require that n > 20.

        # not what we want
        #if n1 + n2 <= 20:
        #    sys.stderr.write("%d et %d valeurs: effectifs insuffisants pour un test de Wilcoxon\n")
        #
        #t_wilcoxon, p_wilcoxon = stats.wilcoxon(samples["1"], samples["2"])
