#!/usr/bin/env python

"""This script will read sorted .bam files and determine lists of reads having
a ping-pong partner. Reads are partners if for at least one reference they
co-map in opposite direction with a 10 nt overlap, i. e. the reverse mapper
must map 9 nt after the forward mapper.
The output are the reads that have a partner, in .fastq format, in an order that
probably can be considered random.
"""

import argparse
import sys
import warnings
import os
OPJ = os.path.join
OPB = os.path.basename
import re
WRITE = sys.stdout.write
def formatwarning(message, category, filename, lineno, line):
    """Formats a warning message."""
    return "%s:%s: %s:%s" % (filename, lineno, category.__name__, message)
warnings.formatwarning = formatwarning


#import csv

import pysam

from collections import deque
#from itertools import product
from itertools import izip

from Bio.Seq import reverse_complement
from Bio.SeqIO.QualityIO import FastqGeneralIterator


def oui(question):
    """oui(question) returns True if the user answers yes to the question
    <question>."""
    return (raw_input("\n" + question + " (y/n) ") in [
        "O", "o", "Oui", "oui", "OUI",
        "Y", "y", "Yes", "yes", "YES",
        "J", "j", "Ja", "ja", "JA",
        "S", "s", "Si", "si", "SI",
        "D", "d", "Da", "da", "DA",
        "T", "t", "Taip", "taip", "TAIP"])


def fastaq(qname, annot_string, rlen, seq, qual):
    """Returns fasta and fastq formatted sequence."""
    fasta = ">%s (annot=%s) (len=%d)\n%s\n" % (qname, annot_string, rlen, seq)
    fastq = "@%s\n%s\n+\n%s\n" % (qname, seq, qual)
    return fasta, fastq


def fastq(qname, seq, qual):
    """Returns a fastq formatted sequence."""
    return "@%s\n%s\n+\n%s\n" % (qname, seq, qual)


def append_pile(pile, window, next_pos):
    """Appends the reads having their 5' end in the pile *pile*
    to the deque of lists of reads *window*.
    *nex_pos* is the position at which the next list of reads
    has to be read and appended to *window*. *next_pos* will be
    used to determine how many blanks need to be filled before
    the position of *pile* is reached. It is updated and returned."""
    ref_pos = pile.pos
    # Fill the blanks at the positions before the pile
    # with empty lists of reads
    while next_pos < ref_pos:
        window.append([])
        next_pos += 1
    # Make the list of reads having their 5' end
    # at the pile position
    window.append([])
    # Note: query_alignment_start is not 5' end
    # for reverse aligned reads
    for pileup_read in pile.pileups:
        ali = pileup_read.alignment
        if ali.is_reverse:
            if ali.reference_end - 1 == ref_pos:
                window[-1].append(pileup_read)
        else:
            if ali.reference_start == ref_pos:
                window[-1].append(pileup_read)
    next_pos += 1
    return next_pos


#########################################################
# For better traceability, options and global variables #
# should be stored as class attributes                  #
#########################################################
class Globals(object):
    """This object holds global variables."""


class Options(object):
    """This object contains the values of the global options."""
    in_file = None
    min_mapq = 0
    min_score = -200
    #normalize = None
    hist_dir = None
    seq_dir = None


#################
# main function #
#################
def main():
    """Main function."""
    WRITE(" ".join(sys.argv) + "\n")
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        "bam_files", nargs="+",
        help="Sorted and indexed *_sorted.bam file.")
    parser.add_argument(
        "-n", "--nopp_fastq",
        help="Fastq file where to write the non ping-ponger reads.",
        type=argparse.FileType(mode="w"))
    parser.add_argument(
        "-o", "--out_file",
        help="File in which to write the counts.",
        type=argparse.FileType(mode="w"))
    parser.add_argument(
        "-p", "--pp_fastq",
        help="Fastq file where to write the ping-ponger reads.",
        type=argparse.FileType(mode="w"))
    parser.add_argument(
        "-s", "--source_reads",
        help="Reads originally mapped to obtain the bam files.")
    parser.add_argument(
        "-m", "--min_score",
        help="Minimum score for alignments to be considered.",
        type=int,
        default=-10)
    parser.add_argument(
        "--overlap",
        help="Overlap between partners.",
        type=int,
        default=10)
    #parser.add_argument("-s", "--switch",
    #        help="This is an optional boolean switch.", action="store_true")
    args = parser.parse_args()
    # To avoid repeatedly calling these as string methods:
    #strip = str.rstrip
    split = str.split
    #with args.input_file as input_file:
    #    for line in input_file:
    #        fields = split(strip(line, "\n"), "\t")

    pp_reads = set([])
    for bam_file in args.bam_files:
        try:
            samfile = pysam.Samfile(bam_file, "rb")
        except ValueError, e:
            # Skip this file
            warnings.warn(
                "\n%s\n*Skipping %s which could not be opened.*\n" % (
                    e, bam_file))
            continue
        #getrname = samfile.getrname
        for ref, ref_len in izip(samfile.references, samfile.lengths):
            # TODO: simplify then compare with current results
            # piles of reads on a window of args.overlap nt on the reference
            window = deque([], maxlen=args.overlap)
            # Fill the window with args.overlap - 1 (possibly empty) piles
            # The first args.overlap - 1 positions will not contain relevant
            # R-mapping reads.
            # R-mapping reads at the args.overlap-th will have their
            # F-mapping partners at first position.
            # Positions are 0-based.
            # First postition for which to record 5'-mapping reads is 0
            next_pos = 0
            # Look at the first piles, filling the blanks if needed:
            # start and end are zero-based in pysam.AlignmentFile.pileup
            for pile in samfile.pileup(ref, start=0, end=args.overlap - 2):
                # 0-based position just after the pile
                next_pos = append_pile(pile, window, next_pos)
            # In case the last pile was at a position
            # before the one before the args.overlap-th:
            while len(window) < (args.overlap - 1):
                next_pos += 1
                window.append([])

            # Now we have an almost-filled window.
            # Next non-empty piles will yield R-mapping candidates that
            # can have F-mapping partners args.overlap - 1 nt upstream,
            # that is, at the first position of
            # the window deque of width args.overlap
            for pile in samfile.pileup(ref, start=next_pos, end=ref_len - 1):
                next_pos = append_pile(pile, window, next_pos)
                assert len(window) == args.overlap
                # compare F-mappers at the start of the
                # args.overlap-width deque with
                # reverse-mappers in the newly added pile,
                # that is, at the last position of the deque
                fwd_alis = list([ali for ali in (
                    pileup_read.alignment for pileup_read in window[0]) if not ali.is_reverse])
                rev_alis = list([ali for ali in (
                    pileup_read.alignment for pileup_read in window[-1]) if ali.is_reverse])
                if len(fwd_alis) * len(rev_alis):
                    # There is at least a couple of (fwd_ali, rev_ali) partners
                    for fwd_ali in fwd_alis:
                        pp_reads.add((
                            fwd_ali.qname,
                            fwd_ali.seq,
                            fwd_ali.qual))
                    for rev_ali in rev_alis:
                        pp_reads.add((
                            rev_ali.qname,
                            reverse_complement(rev_ali.seq),
                            rev_ali.qual[::-1]))
    WRITE("Writing and counting sequences in %s and %s\n" % (
        args.nopp_fastq.name, args.pp_fastq.name))
    nb_pp = 0
    nb_nopp = 0
    with open(args.source_reads, "r") as source_reads_file:
        for triplet in FastqGeneralIterator(source_reads_file):
            # triplet has the rest of the fasta header after the space
            # Remove this
            triplet = (split(triplet[0])[0], triplet[1], triplet[2])
            if triplet in pp_reads:
                args.pp_fastq.write(fastq(*triplet))
                nb_pp += 1
            else:
                args.nopp_fastq.write(fastq(*triplet))
                nb_nopp += 1
    try:
        freq_pp = (1.0 * nb_pp) / (nb_pp + nb_nopp)
        WRITE("nb_pp\t%d\nnb_nopp\t%d\npercent_pp\t%.2f\n" % (
            nb_pp, nb_nopp, 100 * freq_pp))
        args.out_file.write("%d\t%d\t%f\n" % (nb_pp, nb_nopp, freq_pp))
    except ZeroDivisionError:
        WRITE("nb_pp\t%d\nnb_nopp\t%d\npercent_pp\tNaN\n" % (nb_pp, nb_nopp))
        args.out_file.write("%d\t%d\tNA\n" % (nb_pp, nb_nopp))

    return 0


if __name__ == "__main__":
    sys.exit(main())
