#!/usr/bin/env python
"""Usage: find_union.py <list_1> <list_2> [<list_3> ...]
Finds the elements present in at least one of the lists.
The lists have to be provided as text files with one element per line.
The resulting elements are printed in alphabetical order.
"""

import sys

if len(sys.argv) < 3:
    sys.stderr.write("Wrong number of command-line arguments\n")
    sys.stderr.write(__doc__)
    sys.stderr.write("\n")
    sys.exit(1)

union = set([])
for filename in sys.argv[1:]:
    with open(filename, "r") as f:
        for line in f:
            union.add(line.strip())

for elem in sorted(union):
    sys.stdout.write("%s\n" % elem)
sys.exit(0)
