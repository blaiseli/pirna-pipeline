#!/bin/sh
# Usage: re_annotate.sh <library_name> <normalization_file> <sequence_output_dir> <histograms_output_dir>
# Some information helping understanding the script might be found in 00README.txt

# TODO: simplify names by removing "trimmed", update other scripts (at least map_on_repbase.sh) accordingly

# library name
name=${1}

echo $name

# file containing the counts used to perform normalizations
norm_file=${2}
norm=`cat ${norm_file}`


#############################################################################
# Annotating the reads and generating fastq files for each annotation group #
#############################################################################

# Will contain the sequences that mapped, in distinct files for distinct annotation groups
mkdir -p ${3}/${name}
seq_dir=${3}/${name}
# Will contain size histogram data for the various annotation groups
mkdir -p ${4}/${name}
histo_dir=${4}/${name}
# maximum read size
# If this environment variable is not defined, use 30 as default value
[ MAX_LEN ] || MAX_LEN="30"

#--min_score -10 was chosen in order to filter out alignments with too many mismatches.
# We had to use this proxy because filtering on the number of mismatches sometimes excluded the primary alignment.
# Also, the score is in theory a more suitable criterion than the number of mismatches, because it takes into account read quality.
#time annotate_sam.py --in_file mapped_dm/${name}_trimmed_on_D_melanogaster.sam --annot /Genomes/D_melanogaster/Annotations/Flybase/D_melanogaster_flybase_splitmRNA_annot.txt --exclude_types mRNA,ncRNA --min_score "-10" --normalize ${norm} --seq_dir mapped_reads_dm --histo_dir histos_mapped_dm
#time annotate_sam.py --in_file mapped_dm/${name}_trimmed_on_D_melanogaster.sam --annot /Genomes/D_melanogaster/Annotations/Flybase/D_melanogaster_flybase_splitmRNA_annot.txt --exclude_types mRNA,ncRNA,CDS,five_prime_untranslated_region --min_score "-10" --normalize ${norm} --seq_dir ${seq_dir} --histo_dir ${histos_dir}

lbunzip2 -k mapped_dm/${name}_trimmed_on_D_melanogaster_19-${MAX_LEN}.sam.bz2
time annotate_sam.py --in_file mapped_dm/${name}_trimmed_on_D_melanogaster_19-${MAX_LEN}.sam --annot /Genomes/D_melanogaster/Annotations/Flybase/D_melanogaster_flybase_splitmRNA_annot.txt --exclude_types mRNA,ncRNA,CDS,five_prime_untranslated_region --min_score "-10" --normalize ${norm} --seq_dir ${seq_dir} --histo_dir ${histo_dir}
rm -f mapped_dm/${name}_trimmed_on_D_melanogaster_19-${MAX_LEN}.sam

# To normalize by number of siRNA (annotated on siRNA ovary clusters, size 21)
#cat ${seq_dir}/${name}_trimmed_on_D_melanogaster_19-${MAX_LEN}_pi-si-TE-3UTR.fasta | grep "siRNA_cluster" | grep "(len=21)" | wc -l > mapped_dm/${name}_trimmed_on_D_melanogaster_siRNA.nbseq
