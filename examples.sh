#!/usr/bin/env bash
# Usage: examples.sh [get_the_data]
# This script shows example commands of how to perform piRNA analyses
# It is meant to be read, but can be executed.
# If you execute the script without arguments, data will not be downloaded.
# You can add an arbitrary command line argument to get the example data re-installed.
# This can be the basis for your own analyses automation.

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}

# http://stackoverflow.com/questions/59895/can-a-bash-script-tell-what-directory-its-stored-in
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

# By default, the directory for the example analyses and the pre-installed raw data should be present in the source directory
if [ $# = 1 ]
then
    # Go to the directory in which you will create the directory for the analyses.
    # Here it is the source directory:
    cd ${DIR}

    ######################
    # Directory creation #
    ######################

    # Create a directory for the analyses
    # Remove it first if it exists
    # (or move it somewhere else if you want to keep old analyses
    # that were done in a directory with the name you want to use now).
    rm -rf piRNA_analyses
    mkdir piRNA_analyses

    # Go into that directory
    cd piRNA_analyses

    # Create a raw_data directory
    mkdir raw_data

    # Create sub_directories for the example data
    mkdir raw_data/my_control
    mkdir raw_data/my_mutant

    ###########################
    # Geting the example data #
    ###########################

    # This can be slow due to a large amount of data being downloaded

    # Control 
    # -------
    # Information about the original library available from:
    # http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSM744629
    # strain: vret148-60/TM6
    # genotype/variation: Vret Heterozygote
    # tissue: ovaries

    cd raw_data/my_control
    wget --continue ftp://ftp-trace.ncbi.nlm.nih.gov/sra/sra-instant/reads/ByExp/sra/SRX%2FSRX079%2FSRX079821/SRR298536/SRR298536.sra
    fastq-dump -M 10 SRR298536.sra
    rm -f SRR298536.sra

    # For the full dataset, you could make a symbolic link
    #ln -s SRR298536.fastq my_control.fastq

    # For the example, extract the first million reads and remove the full .fastq file
    head -4000000 SRR298536.fastq > my_control_1M.fastq
    rm -f SRR298536.fastq
    # Create symbolic link to match the library name
    ln -s my_control_1M.fastq my_control.fastq

    # Return to the analyses directory
    cd ../..
    # or
    #cd ${DIR}/piRNA_analyses

    # Mutant 
    # ------
    # Information about the original library available from:
    # http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSM744630
    # strain: vret148-60/vret148-60
    # genotype/variation: Vret Mutant
    # tissue: ovaries

    cd raw_data/my_mutant
    wget --continue ftp://ftp-trace.ncbi.nlm.nih.gov/sra/sra-instant/reads/ByExp/sra/SRX%2FSRX079%2FSRX079822/SRR298537/SRR298537.sra
    fastq-dump -M 10 SRR298537.sra
    rm -f SRR298537.sra

    # For the full dataset, you could make a symbolic link
    #ln -s SRR298537.fastq my_mutant.fastq

    # For the example, extract the first million reads and remove the full .fastq file
    head -4000000 SRR298537.fastq > my_mutant_1M.fastq
    rm -f SRR298537.fastq
    # Create symbolic link to match the library name
    ln -s my_mutant_1M.fastq my_mutant.fastq

    # Return to the analyses directory
    cd ../..
    # or
    #cd ${DIR}/piRNA_analyses
else
    #########################################
    # Symbolic linking already present data #
    #########################################
    manual=""
    if [ ${manual} ]
    then
        # Manual method
        ################
        # Go to the directory in which you will create the directory for the analyses.
        # Here it is the source directory:
        cd ${DIR}/piRNA_analyses
        cd raw_data/my_control
        # Create symbolic link to match the library name
        # -f option is for replacing existing link
        ln -sf my_control_10m.fastq my_control.fastq
        # Return to the analyses directory
        cd ../..
        # or
        #cd ${DIR}/piRNA_analyses
        cd raw_data/my_mutant
        # Create symbolic link to match the library name
        # -f option is for replacing existing link
        ln -sf my_mutant_10m.fastq my_mutant.fastq
        # Return to the analyses directory
        cd ../..
        # or
        #cd ${DIR}/piRNA_analyses
    else
        # Script-helped method
        #######################
        # Alternatively, you can use the install_raw_data.sh script, that automatically makes the link
        # Go to the directory in which you will create the directory for the analyses.
        # Here it is the source directory:
        cd ${DIR}/piRNA_analyses
        install_raw_data.sh my_control raw_data/my_control/my_control_10m.fastq
        install_raw_data.sh my_control raw_data/my_mutant/my_mutant_10m.fastq
    fi
    # Cleaning previous analyses results
    #####################################
    # Remove previously generated directories
    # Don't do this if you intend to keep the old analyses
    rm -rf my_control
    rm -rf my_mutant
    rm -rf summaries
    rm -rf logs
fi

# Go to the directory from where the analyses will be started
cd ${DIR}/piRNA_analyses

#############################
# Running the main pipeline #
#############################

# According to the on-line information, for both libraries,
# the 3' adapter is CTGTAGGCACCATCAATC and
# no barcode has to be removed from the reads.
# For better tracability,
# you could store your parameters as lines in a file.
# Initialize the file with a header
#echo "#library_name adapter_sequence barcode_length" > libraries.txt
# Append the libraries information
#echo "my_control CTGTAGGCACCATCAATC 0" >> libraries.txt
#echo "my_mutant CTGTAGGCACCATCAATC 0" >> libraries.txt
# And you could then retrieve the line with grep
# and use it as arguments to the main pipeline script.
#time do_pipeline_small.sh `grep my_control libraries.txt`
#time do_pipeline_small.sh `grep my_mutant libraries.txt`
# More simply:
time do_pipeline_small.sh my_control CTGTAGGCACCATCAATC 0
time do_pipeline_small.sh my_mutant CTGTAGGCACCATCAATC 0

# "time" is optional, and will display the time used by the analyses.
# For information, on a Dell PowerEdge R730 server
# Disk: 4x2TB nearline SAS RAID5 array
# Filesystem: ext4
# Processor: Intel(R) Xeon(R) CPU E5-2603 v3 @ 1.60GHz
# RAM: 64 GB
# OS: Debian jessie
# 
# time output for my_control_1M:
# -----
# real    94m1.600s
# user    96m29.836s
# sys    0m53.092s
# -----
# 
# time output for my_mutant_1M:
# -----
# real    78m32.404s
# user    79m42.292s
# sys    0m38.816s
# -----
#
# For the 10m datasets, on the same system, time is less than 2 minutes

# Results are summarized in
# summaries/my_control_on_D_melanogaster_results.xls
# summaries/my_mutant_on_D_melanogaster_results.xls

# The table can be opened by a speadsheet program.
# We can also extrract information on the command-line using awk,
# because the table is in plain tab-separated format.

# Determine the list of transposable elements on which to calculate ping-pong
awk '{print $1}' my_control/histos_piRNA_candidate_on_TE/my_control_piRNA_candidate_on_TE_ref_counts.txt | grep -v "^#" | grep -v "^tot$" > TEs_my_control.txt
awk '{print $1}' my_mutant/histos_piRNA_candidate_on_TE/my_mutant_piRNA_candidate_on_TE_ref_counts.txt | grep -v "^#" | grep -v "^tot$" > TEs_my_mutant.txt
if [ `diff TEs_my_control.txt TEs_my_mutant.txt` ]
then
    # The lists differ.
    # Determmine the intersection of the two lists
    find_intersection.py TEs_my_control.txt TEs_my_mutant.txt > TE_list.txt
    # To use the union of the two lists, it would be:
    #find_union.py TEs_my_control.txt TEs_my_mutant.txt > TE_list.txt
    rm -f TEs_my_control.txt 
    rm -f TEs_my_mutant.txt 
else
    mv -f TEs_my_control.txt TE_list.txt
    rm -f TEs_my_mutant.txt 
fi

# Calculate ping-pong for the list of TEs, using 4 slices.
# The slice size is caclulated for each TE,
# based on the number of piRNA candidates available in the libraries for that TE.
time do_do_ping_pong.sh TE_list.txt 4 my_control my_mutant ping_pong_my_control_my_mutant_on_TE_list.xls

exit 0
