#!/usr/bin/env python
"""
Usage: build_annotated_genome.py [options]

This script will read annotated fasta headers (in a specific format) and create
an annotation .bed file with annotations sorted by chromosome, start position,
end position, and strand.

Note that "dmel_mitochondrion_genome" is converted into "M" in order to yield
chromosome names compatible with UCSC data.

Available options:

    --help: prints this help text and exits.

    --annotations <file>,<file>,<file>,...: list of files containing
    fasta headers with annotation information.
    The information should be formatted like in the headers
    of the files present in Flybase
    (ftp://flybase.net/genomes/Drosophila_melanogaster/current/fasta/)
    There should be a field "loc" indicating the chromosome and position,
    a field "type" indicating the type of feature
    and a field "name" giving the feature.
    type=transposable_element; loc=2R:2285599..2290131; name=Xanthias{}6297;
    type=transposable_element; loc=X:21391133..21392905; name=Y{}6298;
    type=transposable_element; loc=2RHet:complement(2572749..2574037); name=Y{}6299;
    type=transposable_element; loc=3L:23809390..23810305; name=Y{}6300;
    type=transposable_element; loc=3RHet:complement(195710..196683); name=Y{}6301;
    type=mRNA; loc=3R:join(17192696..17192928,complement(17200782..17201634),complement(17202324..17202463),complement(17202541..17202798),complement(17203010..17203121)); name=mod(mdg4)-RAE;

    --chromosomes <file>: to specify a list of chromosomes
    to take into account (by default, all).
    The file should have one chromosome name per line.

    --fasta_TE <file>: fasta file of canonical transposable elements (as
    obtained by extraction from https://github.com/cbergman/transposons.git
    using embl2fasta.py).
    Sequence names will be used as "chromosomes", and a "transposable_element"
    annotation will be generated for each sequence.
    These "chromosomes" will not be tested for belonging to the list provided
    with the --chromosomes option.

    --pi_clusters <file>: file containing piRNA clusters fasta headers.
    The headers are as follows:
    >cluster<n>:<chrom>:<start>..<end>

    --refgenes <file>: file containing gene annotation as in
    http://hgdownload.soe.ucsc.edu/goldenPath/hg38/database/refGene.txt.gz
    Column 2 should contain the gene name.
    Column 3 should contain the chromosome name.
    Column 4 should contain the strand.
    Column 5 should contain the transcription start position.
    Column 6 should contain the transcription end position.

    --rmsk <file>: file containing repeated elements annotation as in
    http://hgdownload.soe.ucsc.edu/goldenPath/hg38/database/rmsk.txt.gz
    Column 6 should contain the chromosome name.
    Column 7 should contain the start position.
    Column 8 should contain the end position.
    Column 10 should contain the strand.
    Column 11 should contain the repeated element name.

    --si_clusters <file>: file containing siRNA clusters fasta headers.
    The headers are as follows:
    ><organ>_cluster_<n>:<chrom>:<start>..<end>
    <organ> is the name of the organ in which this siRNA cluster is active.

    --genome <string>: to provide a name for the genome annotation to build.


"""


# bli@sei-lot:/extra/Genomes/D_melanogaster/Annotations/Flybase$ cat CDS.txt | head | grep X | awk '{print $2,$3,$4}'
# type=CDS; loc=X:6905942..6906127; name=CG42308-RA;
# type=CDS; loc=X:6905942..6906127; name=CG42308-RB;
# bli@sei-lot:/extra/Genomes/D_melanogaster/Annotations/Flybase$ cat mRNA.txt | head | grep X | awk '{print $2,$3,$5}'
# type=mRNA; loc=X:join(6905390..6905441,6905527..6905789,6905851..6906170); name=CG42308-RA;
# type=mRNA; loc=X:join(6905470..6905789,6905851..6906170); name=CG42308-RB;

import getopt
import re
import sys
WRITE = sys.stdout.write
import warnings
def formatwarning(message, category, filename, lineno, line):
    """Formats a warning message."""
    return "%s:%s: %s:%s" % (filename, lineno, category.__name__, message)
warnings.formatwarning = formatwarning

from bisect import insort
# To read plain fasta file
from Bio import SeqIO


def oui(question):
    """oui(question) returns True if the user answers yes to the question
    <question>."""
    return (raw_input("\n" + question + " (y/n) ") in [
        "O", "o", "Oui", "oui", "OUI",
        "Y", "y", "Yes", "yes", "YES",
        "J", "j", "Ja", "ja", "JA",
        "S", "s", "Si", "si", "SI",
        "D", "d", "Da", "da", "DA",
        "T", "t", "Taip", "taip", "TAIP"])


REG_TYP = re.compile(r"type=([^;]*);")
REG_NAM = re.compile(r"name=([^;]*);")

REG_LOC = re.compile(r"loc=([^;:]*):([^;]+?);")
REG_LOC_ELEMENTS = re.compile(r"(\d+\.\.\d+)|(join)|(complement)")
REG_COORDS = re.compile(r"(\d+)\.\.(\d+)")


def parse_coords(header):
    """This function reads a flybase-style annotated fasta header
    and returns a chromosome name, a list of coordinates ranges
    on which the annotated feature spanned, and a list of
    coordinates ranges on which it spanned reverse-complemented."""
    loc = REG_LOC.search(header)
    #chromosome = loc.group(1)
    #coordinates = loc.group(2)
    # Flag indicating if the next coordinates to be read
    # will be for a reverse-complement feature
    compl = False
    # In order to not switch back to non compl
    # after one coordinate has been read if the compl applies to all
    all_compl = False
    # Lists of coordinates
    coords = []
    compl_coords = []
    # Triples where the first item is the chain matching a pair of coordinates,
    # the second a chain matching "join",
    # and the third a chain matching "complement".
    # Only on of the three elements should be a non-empty chain.
    #triples = REG_LOC_ELEMENTS.findall(coordinates)
    triples = REG_LOC_ELEMENTS.findall(loc.group(2))
    if triples[0][2]:
        all_compl = True
    #for triple in REG_LOC_ELEMENTS.findall(loc.group(2)):
    for triple in triples:
        if triple[0]:
            #start, end = REG_COORDS.search(triple[0]).groups()
            if compl:
                compl_coords.append(REG_COORDS.search(triple[0]).groups())
                #print "compl(%s..%s)" % (start, end)
                compl = False or all_compl
            else:
                coords.append(REG_COORDS.search(triple[0]).groups())
                #print "%s..%s" % (start, end)
        elif triple[2]:
            compl = True
        else:
            #only joining
            pass
    # returning chromosome name and lists of
    # integer-converted pairs of (start, end) coordinates
    return loc.group(1), sorted(
        [(int(c[0]), int(c[1])) for c in coords]), sorted(
            [(int(c[0]), int(c[1])) for c in compl_coords])


def parse_annotation(header):
    """Extracts information from a Flybase fasta header."""
    try:
        typ = REG_TYP.search(header).groups()[0]
        nam = REG_NAM.search(header).groups()[0]
        chrom, plus, minus = parse_coords(header)
        return chrom, plus, minus, typ, nam
    except AttributeError:
        print "problem encountered while parsing the following header:"
        print header
        sys.exit(1)


class Options(object):
    """This object contains the values of the global options."""
    annotation_files = None
    chromosomes_file = None
    fasta_TE_file = None
    genome = None
    pi_clusters_file = None
    refgenes_file = None
    rmsk_file = None
    si_clusters_file = None

#################
# main function #
#################


def main():
    """Main function of the script."""
    try:
        opts, args = getopt.getopt(
            sys.argv[1:], "io:", [
                "help",
                "annotations=", "chromosomes=",
                "fasta_TE=", "genome=", "pi_clusters=",
                "refgenes=", "rmsk=", "si_clusters="])
    except getopt.GetoptError:
        sys.stderr.write(__doc__)
        sys.exit(1)
    for option, argument in opts:
        if option == "--help":
            WRITE(__doc__)
            sys.exit()
        if option == "--genome":
            Options.genome = argument
        if option == "--annotations":
            #print argument
            #print argument.split(",")
            Options.annotation_files = argument.split(",")
        if option == "--chromosomes":
            Options.chromosomes_file = argument
        if option == "--fasta_TE":
            Options.fasta_TE_file = argument
        if option == "--pi_clusters":
            Options.pi_clusters_file = argument
        if option == "--refgenes":
            Options.refgenes_file = argument
        if option == "--rmsk":
            Options.rmsk_file = argument
        if option == "--si_clusters":
            Options.si_clusters_file = argument

    chromosomes = {}
    # to sort on the "end" rather than on the "start" coordinate
    #rev_chromosomes = {}

    allowed_chromosomes = set([])
    if Options.chromosomes_file is not None:
        with open(Options.chromosomes_file, "r") as chrom_f:
            for line in chrom_f:
                allowed_chromosomes.add(line.strip())

    for f_name in Options.annotation_files:
        with open(f_name, "r") as annot_f:
            for line in annot_f:
                if line.startswith(">"):
                    chrom, plus, minus, typ, nam = parse_annotation(line)
                    if chrom == "dmel_mitochondrion_genome":
                        chrom = "M"
                    if allowed_chromosomes:
                        if chrom not in allowed_chromosomes:
                            continue
                    if typ == "transposable_element":
                        nam = re.sub("{}.*", "", nam)
                    if chrom not in chromosomes.keys():
                        chromosomes[chrom] = []
                    for coord in plus:
                        insort(chromosomes[chrom], (coord, "+", typ, nam))
                    for coord in minus:
                        insort(chromosomes[chrom], (coord, "-", typ, nam))
                else:
                    # Skip sequence lines
                    continue

    if Options.pi_clusters_file is not None:
        with open(Options.pi_clusters_file, "r") as clust_f:
            for line in clust_f:
                if line.startswith(">"):
                    line.strip()
                    nam, chrom, coords, strand = line.split(":")
                    if chrom == "dmel_mitochondrion_genome":
                        chrom = "M"
                    if allowed_chromosomes:
                        if chrom not in allowed_chromosomes:
                            continue
                    start, end = REG_COORDS.search(coords).groups()
                    insort(
                        chromosomes[chrom],
                        ((int(start), int(end)),
                        strand.strip(), "piRNA_cluster", nam[1:]))
                else:
                    # Skip sequence lines
                    continue

    if Options.refgenes_file is not None:
        with open(Options.refgenes_file, "r") as refgenes_f:
            for line in refgenes_f:
                fields = line.strip().split()
                name = fields[1]
                chrom = fields[2]
                if chrom == "dmel_mitochondrion_genome":
                    chrom = "M"
                if allowed_chromosomes:
                    if chrom not in allowed_chromosomes:
                        continue
                strand = fields[3]
                start = fields[4]
                end = fields[5]
                try:
                    insort(
                        chromosomes[chrom],
                        ((int(start), int(end)), strand, "mRNA", name))
                except KeyError:
                    chromosomes[chrom] = []
                    insort(
                        chromosomes[chrom],
                        ((int(start), int(end)), strand, "mRNA", name))

    if Options.rmsk_file is not None:
        with open(Options.rmsk_file, "r") as rmsk_f:
            for line in rmsk_f:
                fields = line.strip().split()
                chrom = fields[5]
                if chrom == "dmel_mitochondrion_genome":
                    chrom = "M"
                if allowed_chromosomes:
                    if chrom not in allowed_chromosomes:
                        continue
                start = fields[6]
                end = fields[7]
                strand = fields[9]
                name = fields[10]
                try:
                    insort(
                        chromosomes[chrom],
                        ((int(start), int(end)),
                        strand, "repeated_sequence", name))
                except KeyError:
                    chromosomes[chrom] = []
                    insort(
                        chromosomes[chrom],
                        ((int(start), int(end)),
                        strand, "repeated_sequence", name))

    if Options.si_clusters_file is not None:
        with open(Options.si_clusters_file, "r") as clust_f:
            for line in clust_f:
                if line.startswith(">"):
                    line.strip()
                    nam, chrom, coords = line.split(":")
                    if chrom == "dmel_mitochondrion_genome":
                        chrom = "M"
                    if allowed_chromosomes:
                        if chrom not in allowed_chromosomes:
                            continue
                    start, end = REG_COORDS.search(coords).groups()
                    insort(
                        chromosomes[chrom],
                        ((int(start), int(end)),
                        "0", "siRNA_cluster", nam[1:]))

    extra_chromosomes = {}
    if Options.fasta_TE_file is not None:
        for (TE_name, TE) in SeqIO.index(Options.fasta_TE_file, "fasta").items():
            chrom = TE_name
            start = 1
            end = len(TE.seq)
            strand = "+"
            msg = "TE name conflicting with already existing chromosome."
            assert TE_name not in chromosomes, msg
            msg = "Canonical TE name cannot appear more than once."
            assert TE_name not in extra_chromosomes, msg
            extra_chromosomes[TE_name] = (
                (1, len(TE.seq)),
                "+", "transposable_element",
                TE_name)

    with open(Options.genome + "_annot.bed", "w") as bed_f:
        for chr_name in sorted(chromosomes.keys()):
            for tpl in chromosomes[chr_name]:
                # bed format: chrom chromStart  chromEnd    name    score   strand
                # Here "score" is "0"
                # and "name" is "annotation_category@annotation_name"
                # "chromStart" is in 0-based coordinates,
                # "chromEnd" is in 1-based coordinates
                # "strand" is "0" if undefined or unknown
                # or not applicable or whatever, "+" / "-" otherwise.
                bed_f.write("%s\t%d\t%d\t%s@%s\t0\t%s\n" % (
                    chr_name, tpl[0][0] - 1, tpl[0][1],
                    tpl[2], tpl[3], tpl[1]))
        for TE in sorted(extra_chromosomes.keys()):
            tpl = extra_chromosomes[TE]
            bed_f.write("%s\t%d\t%d\t%s@%s\t0\t%s\n" % (
                TE, tpl[0][0] - 1, tpl[0][1],
                tpl[2], tpl[3], tpl[1]))
    return 0

if __name__ == "__main__":
    sys.exit(main())
