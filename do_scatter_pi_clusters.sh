#!/bin/sh
banque1=${1}
banque2=${2}
col=$3
transform=$4
norm=$5
genome=${6}
scatterdir="scatterplots"

in_files="pi_clusters_unique_counts/${banque1}_unique_pi_clusters_counts.txt,pi_clusters_unique_counts/${banque2}_unique_pi_clusters_counts.txt"

case ${norm} in
    mi)
        #scatterplot_reads.py --min_counts 100 --column $col --in_files histos_mapped_pi_clusters/${banque1}/${banque1}_22-29_on_ref_counts.txt,histos_mapped_pi_clusters/${banque2}/${banque2}_22-29_on_ref_counts.txt --normalizations mapped_dm/${banque1}_trimmed_on_D_melanogaster_siRNA.nbseq,mapped_dm/${banque2}_trimmed_on_D_melanogaster_siRNA.nbseq --x_axis "${banque1} (reads by endogenous siRNA reads)" --y_axis "${banque2} (reads by endogenous siRNA reads)" --plot ${scatterdir}/pi_clusters_${banque1}_vs_${banque2}_norm_endosi_col_${col}.tex
        scatterplot_reads.py --circled_names --min_counts 10 --column $col --in_files ${in_files} --normalizations mapped_${genome}/${banque1}_on_${genome}_miRNA.nbseq,mapped_${genome}/${banque2}_on_${genome}_miRNA.nbseq --x_axis "${banque1} (reads by miRNA reads)" --y_axis "${banque2} (reads by miRNA reads)" --plot ${scatterdir}/pi_clusters_${banque1}_vs_${banque2}_norm_mi_${transform}_col_${col}.tex --transform ${transform}
        cd ${scatterdir}
        pdflatex pi_clusters_${banque1}_vs_${banque2}_norm_mi_${transform}_col_${col}.tex
        cd ..
        ;;
    endosi)
        #scatterplot_reads.py --min_counts 100 --column $col --in_files histos_mapped_pi_clusters/${banque1}/${banque1}_22-29_on_ref_counts.txt,histos_mapped_pi_clusters/${banque2}/${banque2}_22-29_on_ref_counts.txt --normalizations mapped_dm/${banque1}_trimmed_on_D_melanogaster_siRNA.nbseq,mapped_dm/${banque2}_trimmed_on_D_melanogaster_siRNA.nbseq --x_axis "${banque1} (reads by endogenous siRNA reads)" --y_axis "${banque2} (reads by endogenous siRNA reads)" --plot ${scatterdir}/pi_clusters_${banque1}_vs_${banque2}_norm_endosi_col_${col}.tex
        scatterplot_reads.py --circled_names --min_counts 10 --column $col --in_files ${in_files} --normalizations mapped_${genome}/${banque1}_on_${genome}_siRNA.nbseq,mapped_${genome}/${banque2}_on_${genome}_siRNA.nbseq --x_axis "${banque1} (reads by endogenous siRNA reads)" --y_axis "${banque2} (reads by endogenous siRNA reads)" --plot ${scatterdir}/pi_clusters_${banque1}_vs_${banque2}_norm_endosi_${transform}_col_${col}.tex --transform ${transform}
        cd ${scatterdir}
        pdflatex pi_clusters_${banque1}_vs_${banque2}_norm_endosi_${transform}_col_${col}.tex
        cd ..
        ;;
    42AB)
        #scatterplot_reads.py --min_counts 100 --column $col --in_files histos_mapped_pi_clusters/${banque1}/${banque1}_22-29_on_ref_counts.txt,histos_mapped_pi_clusters/${banque2}/${banque2}_22-29_on_ref_counts.txt --normalizations mapped_strict_dm/${banque1}_trimmed_strict_on_D_melanogaster_piRNA_cluster1.nbseq,mapped_strict_dm/${banque2}_trimmed_strict_on_D_melanogaster_piRNA_cluster1.nbseq --x_axis "${banque1} (reads by 42 piRNA reads)" --y_axis "${banque2} (reads by 42A piRNA reads)" --plot ${scatterdir}/pi_clusters_${banque1}_vs_${banque2}_norm_42A_col_${col}.tex
        scatterplot_reads.py --circled_names --min_counts 10 --column $col --in_files ${in_files} --normalizations mapped_strict_dm/${banque1}_strict_on_${genome}_piRNA_cluster1.nbseq,mapped_strict_dm/${banque2}_strict_on_${genome}_piRNA_cluster1.nbseq --x_axis "${banque1} (reads by 42AB piRNA reads)" --y_axis "${banque2} (reads by 42AB piRNA reads)" --plot ${scatterdir}/pi_clusters_${banque1}_vs_${banque2}_norm_42AB_${transform}_col_${col}.tex --transform ${transform}
        cd ${scatterdir}
        pdflatex pi_clusters_${banque1}_vs_${banque2}_norm_42AB_${transform}_col_${col}.tex
        cd ..
        ;;
    *)
        #scatterplot_reads.py --min_counts 100 --column $col --in_files histos_mapped_pi_clusters/${banque1}/${banque1}_22-29_on_ref_counts.txt,histos_mapped_pi_clusters/${banque2}/${banque2}_22-29_on_ref_counts.txt --x_axis "${banque1} (10000 reads)" --y_axis "${banque2} (10000 reads)" --plot ${scatterdir}/pi_clusters_${banque1}_vs_${banque2}_col_${col}.tex
        scatterplot_reads.py --circled_names --min_counts 10 --column $col --in_files ${in_files} --x_axis "${banque1} (10000 reads)" --y_axis "${banque2} (10000 reads)" --plot ${scatterdir}/pi_clusters_${banque1}_vs_${banque2}_${transform}_col_${col}.tex --transform ${transform}
        cd ${scatterdir}
        pdflatex pi_clusters_${banque1}_vs_${banque2}_${transform}_col_${col}.tex
        cd ..
        ;;
esac
