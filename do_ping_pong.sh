#!/usr/bin/env bash
# Usage: do_ping_pong.sh <library_name> <num_slices> <slice_size> [<reference_name>]
# <reference_name> is the name of a reference
# It can also be a file with one reference per line,
# and ping-pong will be determined for each reference
# (not sure this option is useful, because one will likely want to adapt the slice size to the reference)


# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}

# Library name:
name=${1}

# Genome to use
#genome=${2}
genome="D_melanogaster"

# Number of piRNA candidate sequences to include in each slice
num_slices=${2}

slice_size=${3}

# Reference on which to compute ping-pong
# "all" or the name of a transposable element (or equivalent like "P_lArB_transgene")
if [ $# = 4 ]
then
    ref_name=${4}
else
    ref_name="global"
fi

mkdir -p "logs/${name}"

if [ -f ${ref_name} ]
then
    ref_names=`cat ${ref_name}`
else
    ref_names="${ref_name}"
fi

log_dir="logs/${name}/ping_pong_${num_slices}_slices"
mkdir -p ${log_dir}
for ref in ${ref_names}
do
    log="${log_dir}/ping_pong_${num_slices}_slices_${ref}.stdout"
    err="${log_dir}/ping_pong_${num_slices}_slices_${ref}.stderr"
    echo -e "\nRunning ping-pong analysis for reference ${ref} using ${num_slices} slices of ${slice_size} reads."
    echo "Logs outputted in the following files:"
    echo ${log}
    echo ${err}
    cmd="ping_pong.sh ${name} ${genome} ${ref} ${num_slices} ${slice_size}"
    echo ${cmd}
    eval ${cmd} > ${log} 2> ${err} || error_exit "${cmd} failed."
    echo -e "\nPing-pong analysis for reference ${ref} finished.\n"
done


exit 0
