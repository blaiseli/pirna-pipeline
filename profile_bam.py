#!/usr/bin/env python
# vim: set fileencoding=<utf-8> :
"""This script reads a sorted and indexed .bam file and searches for regions
where mapped reads are "more abundant" than in a background."""


import argparse
import sys
WRITE = sys.stdout.write
import warnings
def formatwarning(message, category, filename, lineno, line):
    """Formats warning messages."""
    return "%s:%s: %s:%s" % (filename, lineno, category.__name__, message)
warnings.formatwarning = formatwarning

import os
OPJ = os.path.join
OPB = os.path.basename

from collections import deque
from itertools import izip
import pysam
import numpy as np



def pile_counter(read_range):
    """Generates a read counting function."""
    min_size, max_size = read_range
    min_score = Options.min_score
    if min_size <= max_size:
        if Options.max_edit_proportion == -1.0:
            # Do not filter on edit proportion
            def select(read):
                """Filter function.
                The read has to match at its first base."""
                ali = read.alignment
                #rlen = ali.rlen
                rlen = ali.query_length
                # Introduced this in revision bdee801f3449adcee138692596d639ef811f1f57
                #sys.exit(2)
                return (
                    #read.is_head
                    (min_size <= rlen <= max_size)
                    and (ali.opt("AS") >= min_score)
                    and (ali.opt("NM") <= Options.max_mm))
        else:
            def select(read):
                """Filter function.
                The read has to match at its first base."""
                ali = read.alignment
                #rlen = ali.rlen
                rlen = ali.query_length
                return (
                    #read.is_head
                    (ali.opt("NM") <= rlen * Options.max_edit_proportion)
                    and (min_size <= rlen <= max_size)
                    and (ali.opt("AS") >= min_score)
                    and (ali.opt("NM") <= Options.max_mm))
    else:
        # Don't filter by size
        if Options.max_edit_proportion == -1.0:
            # Do not filter on edit proportion
            def select(read):
                """Filter function.
                The read has to match at its first base."""
                ali = read.alignment
                return (
                    #read.is_head
                    (ali.opt("AS") >= min_score)
                    and (ali.opt("NM") <= Options.max_mm))
        else:
            def select(read):
                """Filter function.
                The read has to match at its first base."""
                ali = read.alignment
                #rlen = ali.rlen
                rlen = ali.query_length
                return (
                    #read.is_head
                    (ali.opt("NM") <= rlen * Options.max_edit_proportion)
                    and (ali.opt("AS") >= min_score)
                    and (ali.opt("NM") <= Options.max_mm))

    def read_counter(pile, no_secondary=False):
        """Generates counts of forward and reverse 5' mapping reads."""
        ref_pos = pile.reference_pos
        # number of selected 5' aligned reads from the pile
        fwd_count = 0
        rev_count = 0
        for read in pile.pileups:
            ali = read.alignment
            # Note: read.qpos and read.is_head are not reliable indications
            # for 5'end alignment testing
            #if read.qpos == 0:
            #    assert read.is_head
            if select(read):
                if not (no_secondary and ali.is_secondary):
                    if ali.is_reverse:
                        if ali.reference_end - 1 == ref_pos:
                            rev_count += 1
                    else:
                        if ali.reference_start == ref_pos:
                            fwd_count += 1
        return ref_pos, fwd_count, rev_count
    return read_counter



#####################################
# For better traceability,          #
# options and global variables      #
# can be stored as class attributes #
#####################################


class Globals(object):
    """This object holds global variables."""
    cluster_names = {"cluster1": "42AB", "cluster8": "flamenco"}


class Options(object):
    """This object contains the values of the global options."""
    y_axis = None
    # default value set so that pile_counter will not
    # select alignments based on edit proportion
    max_edit_proportion = None
    max_mm = None
    min_score = None
    # min and max lengths for a read to be counted
    # default value set so that pile_counter
    # will not select alignments based on read size
    read_range = None

#################
# main function #
#################


def main():
    """Main function of the program."""
    WRITE("%s\n" % " ".join(sys.argv))
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        "-b", "--bam_file",
        required=True,
        help="Sorted and indexed .bam file.")
    parser.add_argument(
        "-o", "--out_bed",
        required=True,
        help="File in which to write bed information about high read density zones.",
        type=argparse.FileType("w"))
    parser.add_argument(
        "-d", "--densities",
        required=True,
        help="File in which to write densities.",
        type=argparse.FileType("w"))
    #parser.add_argument(
    #    "-e", "--exclude_bed",
    #    required=True,
    #    help="File containing coordinates of zones to exclude from the counts.\n"
    #    "There must exist a .bgz.tbi (or .gz.tbi) corresponding tabix index "
    #    "of this file.")
    parser.add_argument(
        "-L", "--background_length",
        type=int,
        default=20000,
        help="Length of the windows on which to evaluate background mapper density.")
    parser.add_argument(
        "-l", "--zone_length",
        type=int,
        default=1000,
        help="Length of the windows on which to assess above-background mapper density.")
    # default value set so that pile_counter will not
    # select alignments based on edit proportion
    parser.add_argument(
        "--max_edit_proportion",
        type=float,
        default=-1.0,
        help="Maximum proportion of edits for an alignment to be recorded."
        "\nThe default is to accept any proportion of edits.")
    parser.add_argument(
        "--max_mm",
        type=int,
        default=0,
        help="Maximum number of mismatches for an alignment to be counted.")
    parser.add_argument(
        "--min_score",
        type=int,
        default=-200,
        help="Minimum mapping score for an alignment to be recorded."
        "\nThe default value is -200.")
    # min and max lengths for a read to be counted
    # default value set so that pile_counter
    # will not select alignments based on read size
    parser.add_argument(
        "--read_range",
        nargs=2,
        type=int,
        default=[0, -1],
        help="Minimum and maximum lengths of the reads to take into account."
        "\nThe default is to take all sizes into account.")
    parser.add_argument(
        "--no_secondary",
        action="store_true",
        help="Do not count secondary alignments of reads.\n"
        "A read will only be counted once, but where it will be counted "
        "may be partially arbitrary.")
    parser.add_argument(
        "-e", "--exclude_refs",
        help="File containing a list of references to exclude from the profiling.\n"
        "One reference per line.",
        type=argparse.FileType("r"))
    args = parser.parse_args()
    err_msg = "".join([
        "The background window should be be at least ",
        "10 times larger than the inspected window."])
    assert args.background_length >= (10 * args.zone_length), err_msg
    bg_len = args.background_length
    z_len = args.zone_length
    # TODO: maybe a bit useless
    def window_coords(bg_start=None, bg_end=None):
        """Given a background window start position <bg_start> or end_position,
        this returns two pairs of (start, end) coordinates and the coordinate
        of the middle of the profiled zone. All the values are zero-based.
        First pair are background window start and end positions. Second pair
        are profiled window start and end positions."""
        if bg_end is None:
            bg_end = bg_start + (bg_len - 1)
        elif bg_start is None:
            bg_start = bg_end - (bg_len - 1)
        else:
            err_msg = "".join([
                "Incompatible start and end coordinates for ",
                "%d sized background window." % bg_len])
            assert bg_end - bg_start == (bg_len - 1), err_msg
        z_middle = (bg_end + bg_start) / 2
        z_start = z_middle - (z_len / 2)
        z_end = z_start + (z_len - 1)
        return (bg_start, bg_end), (z_start, z_end), z_middle

    # TODO: define "above", possibly using options
    def above(z_density, bg_density):
        """Returns True if z_density is considered above "bg_density"."""
        return z_density > (15 * bg_density)
    def compare_densities(z_sum, bg_sum, chrom, range_start, z_middle):
        """Calculates and compares background and profiled zone densities using the read counts in these windows.
        Writes densities to file.
        Writes bed output if a contiguous segment of high profiled zone density ends.
        Updates and returns range_start."""
        bg_density = (1.0 * bg_sum) / bg_len
        z_density = (1.0 * z_sum) / z_len
        args.densities.write("%s\t%d\t%f\t%f\n" % (chrom, z_middle, z_density, bg_density))
        #z_density = np.mean(background[rel_z_start:(rel_z_start + z_len)])
        if above(z_density, bg_density):
            if range_start is None:
                # Start a new range of high profiled zone density
                range_start = z_middle
        else:
            if range_start is not None:
                # Ends a current range of high profiled zone density
                # BED format uses zero-based indices like in python for coordinates,
                # So we use the current z_middle as end coordinate for the range.
                args.out_bed.write("%s\t%d\t%d\thigh_read_density\t1000\n" % (chrom, range_start, z_middle))
                WRITE("%s\t%d\t%d\thigh_read_density\t1000\n" % (chrom, range_start, z_middle))
            range_start = None
        return range_start
    # Give global access to some options
    Options.max_edit_proportion = args.max_edit_proportion
    Options.max_mm = args.max_mm
    Options.min_score = args.min_score
    Options.read_range = tuple(args.read_range)

    # To avoid repeatedly calling these as string methods:
    strip = str.rstrip
    #split = str.split
    counts = pile_counter(Options.read_range)
    # Make the list of references we want to skip.
    Globals.exclude_refs = []
    if args.exclude_refs:
        with args.exclude_refs as ref_file:
            for line in ref_file:
                ref_name = strip(line)
                if ref_name:
                    if ref_name[0] != "#":
                        Globals.exclude_refs.append(ref_name)
    Globals.exclude_refs = set(Globals.exclude_refs)

    with pysam.Samfile(args.bam_file) as samfile:
        for chrom, chrom_len in izip(samfile.references, samfile.lengths):
            if chrom in Globals.exclude_refs:
                # Skip references we're not interested in.
                #print "Skipping %s" % chrom
                continue
            WRITE("Chromosome: %s (len: %d)\n" % (chrom, chrom_len))
            # range_start will switch to some value
            # once the first position of a range
            # where profiled zone has peaks has been reached.
            range_start = None
            # Read counts, initiated with zeros
            #background = np.zeros(args.background_length, dtype="uint")
            #background = deque(np.zeros(bg_len, dtype="uint"), bg_len)
            background = np.zeros(bg_len, dtype="uint")
            # background and profiled zone start and end positions,
            # and middle position of the profiled zone
            (bg_start, bg_end), (z_start, z_end), z_middle = window_coords(bg_start=0)
            # Start index of the profiled zone within the background window
            rel_z_start = z_start - bg_start
            # Sum of read counts
            bg_sum = 0
            z_sum = 0
            #pos = 0
            piles = samfile.pileup(reference=chrom)
            try:
                pile = piles.next()
                # pos: pile position on the reference
                # fwd: 5' F mapping reads
                # rev: 5' R mapping reads
                pos, fwd, rev = counts(pile, args.no_secondary)
                #if fwd + rev:
                #    print chrom, pos, chrom_len
                #else:
                #    print pile.nsegments
            except StopIteration:
                # Not a single pile. Skip to next chromosome.
                continue
            # Fill background window
            while pos <= bg_end:
                background[pos] = fwd + rev
                bg_sum += (fwd + rev)
                try:
                    pile = piles.next()
                    pos, fwd, rev = counts(pile, args.no_secondary)
                    #if fwd + rev:
                    #    print chrom, pos, chrom_len
                    #else:
                    #    print pile.nsegments
                except StopIteration:
                    break
            # Either pos > bg_end, either we exited because there were no more piles.
            # The background window has been filled.
            # We can compute the sum of numbers of reads within
            # the profiled zone centered in the middle of the background window. 
            z_sum = np.sum(background[rel_z_start:(rel_z_start + z_len)])
            # Now we can compare densities.
            range_start = compare_densities(z_sum, bg_sum, chrom, range_start, z_middle)
            #if pos <= bg_end:
            #    # There has been a StopIteration exception.
            #    # There is no pile further away from the end of the first window
            #    print "No pile for %s." % chrom
            #    continue
            # Now pos > bg_end
            #assert pos > bg_end, "Ups!"
            # Convert background to deque (more efficient than np.array to make a sliding window).
            background = deque(background, bg_len)
            # Move background window to catch up with pos:
            while bg_end < pos:
                # Update coordinates
                bg_end += 1
                bg_start += 1
                z_start += 1
                # Not used
                #z_end += 1
                z_middle += 1
                # Update counts
                bg_sum -= background[0]
                # Note that random access is said to be inefficient in deque compared to list
                z_sum -= background[rel_z_start]
                z_sum += background[(rel_z_start + z_len)]
                # not efficient:
                #background = np.roll(background, -1)
                # slightly better
                #background = np.pad(background[1:], (0,1), "constant")
                # but using deque is largely better
                if bg_end == pos:
                    # Append to a full deque
                    background.append(fwd + rev)
                    #background[-1] = fwd + rev
                    bg_sum += (fwd + rev)
                else:
                    # Append to a full deque
                    background.append(0)
                    #background[-1] = 0
                #bg_sum += 0
                # Start index of the profiled zone within the background window
                # no need to re-compute this
                #rel_z_start = z_start - bg_start
                assert rel_z_start == z_start - bg_start
                # Compare densities around this z_middle coordinate
                range_start = compare_densities(z_sum, bg_sum, chrom, range_start, z_middle)
            # Now bg_end == pos
            # TODO: continue by updating by rolling (negative and reset to zero the new end) and updating bg_sum progressively
            # At each step, compute also the mean of a centered sub-window
            for pile in piles:
                pos, fwd, rev = counts(pile, args.no_secondary)
                #if fwd + rev:
                #    print chrom, pos, chrom_len
                #else:
                #    print pile.nsegments
                # Move background window to catch up with pos:
                # TODO: slow!!
                while bg_end < pos:
                    # Update coordinates
                    bg_end += 1
                    bg_start += 1
                    z_start += 1
                    # Not used
                    #z_end += 1
                    z_middle += 1
                    # Update counts
                    bg_sum -= background[0]
                    z_sum -= background[rel_z_start]
                    z_sum += background[(rel_z_start + z_len)]
                    #background = np.pad(background[1:], (0,1), "constant")
                    #background = np.roll(background, -1)
                    if bg_end == pos:
                        # Append to a full deque
                        background.append(fwd + rev)
                        #background[-1] = fwd + rev
                        bg_sum += (fwd + rev)
                    else:
                        # Append to a full deque
                        background.append(0)
                        #background[-1] = 0
                    #bg_sum += 0
                    # Start index of the profiled zone within the background window
                    # no need to re-compute this
                    #rel_z_start = z_start - bg_start
                    assert rel_z_start == z_start - bg_start
                    # Compare densities around this z_middle coordinate
                    range_start = compare_densities(z_sum, bg_sum, chrom, range_start, z_middle)
            if range_start is not None:
                # Deal with last open range
                args.out_bed.write("%s\t%d\t%d\thigh_read_density\t1000\n" % (chrom, range_start, z_middle))
                WRITE("%s\t%d\t%d\thigh_read_density\t1000\n" % (chrom, range_start, z_middle))
                # probably not necessary
                range_start = None
    return 0

if __name__ == "__main__":
    sys.exit(main())
