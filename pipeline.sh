#!/usr/bin/env bash
# Usage: pipeline.sh <library_name> <adapter> <tag_length_to_trim> <G_species> <number_of_cpus_to_use> [<annotation_file>]
# Some information helping understanding the script might be found in 00README.txt

# To configure according to the local installation:
genomes_path=${GENOMES}

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}


PLACE=`pwd`
echo "Analyses taking place with base directory ${PLACE}"

#banque="yb_RNAi"
banque=${1}
#adapter="TCGTATGCCGTCTTCTGCTTG"
adapter=${2}
#trim="0"
trim=${3}

echo "trim.sh raw_data/${banque}/${banque}.fastq ${adapter} ${trim}"
trim.sh raw_data/${banque}/${banque}.fastq ${adapter} ${trim} || error_exit "trim.sh failed"


#genome="D_melanogaster"
genome=${4}

if [ $# = 6 ]
then
    annotation_file=${6}
else
    #annotation_file="${genomes_path}/${genome}/Annotations/Flybase/${genome}_flybase_splitmRNA_annot.txt"
    annotation_file="${genomes_path}/${genome}/Annotations/Flybase/${genome}_flybase_splitmRNA_annot.bed"
fi

echo "map_and_annotate.sh ${banque} ${genome} ${annotation_file}"
map_and_annotate.sh ${banque} ${genome} ${annotation_file} || error_exit "map_and_annotate.sh failed"

#ncpu="4"
ncpu=${5}

#echo "map_and_count_unique.sh"
#map_and_count_unique.sh ${banque} ${genome} ${ncpu} || error_exit "map_and_count_unique.sh failed"
#
#echo "map_on_canonical_set.sh ${banque} ${genome}"
#map_on_canonical_set.sh ${banque} ${genome} || error_exit "map_on_canonical_set.sh failed"
#
#echo "map_on_3UTR.sh ${banque} ${genome}"
#map_on_3UTR.sh ${banque} ${genome} || error_exit "map_on_3UTR.sh failed"

# TODO: map sur DM, annoter, ce qui n'est pas annoté non-bonafide est mappé (de
# façon peu stringente) sur repbase: on trie les résultats entre les
# 0-mismatch (TE actifs) et les autres qui ont mappé mais pas si strictement
# (score min -10), on les mappe (pas trop stringeant) sur les pi_clusters, et
# on trie entre les 0-mismatch (qu'on utilise pour les pileups) et le reste
# (pour plus tard définir des pi_clusters spécifiques à notre souche)

# *Sauf que* un read qui mappe mal ou pas sur TE ce n'est pas pas forcément un
# unique mapper de pi_cluster
# Ça peut être un unique mapper de 3'UTR (il semblerait que des piRNA puissent provenir de là)
# Ça peut mapper en plusieurs endroits (pi_si_TE_3UTR et/ou low_score et/ou not_annotated)

# Pour garantir l'unicité d'un mapping, il faut passer par un bowtie -a (voir
# map_and_count_unique.sh).

exit 0
