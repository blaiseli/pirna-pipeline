#!/usr/bin/env python

import sys

plus = {}

with open(sys.argv[2]) as f:
    headers = f.readline()
    for line in f:
        fields = line.split(",")
        plus["cluster%s" % fields[0]] = float(fields[-2])

with open(sys.argv[1]) as f:
    for line in f:
        if line.startswith(">"):
            fields = line.split(":")
            sys.stdout.write(line.strip())
            p = plus[fields[0][1:]]
            if p >= 75:
                sys.stdout.write(":+\n")
            elif p <= 25:
                sys.stdout.write(":-\n")
            else:
                sys.stdout.write(":0\n")
        else:
            sys.stdout.write(line)

sys.exit(0)
