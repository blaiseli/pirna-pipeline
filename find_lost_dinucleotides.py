#!/usr/bin/env python
# vim: set fileencoding=<utf-8> :
"""This script reads two sorted and indexed .bam files.
It then tries to locate dinucleotides that have a decrease of
overlapping reads in the second .bam file."""


import argparse
import sys
WRITE = sys.stdout.write
import warnings


def formatwarning(message, category, filename, lineno, line):
    """Formats warning messages."""
    return "%s:%s: %s:%s" % (filename, lineno, category.__name__, message)
warnings.formatwarning = formatwarning

import os
OPJ = os.path.join
OPB = os.path.basename

from collections import defaultdict
from itertools import compress, imap, izip, product
import pysam
import numpy as np
from scipy.stats import chi2_contingency, fisher_exact

from Bio import SeqIO

# List of dinucleotides
#DINUCS = ["%s%s" % dinuc for dinuc in product("ACGTN", "ACGTN")]
DINUCS = ["%s%s" % dinuc for dinuc in product("ACGT", "ACGT")]


def pile_counter(read_range, min_overlap):
    """Generates a read counting function. *read_range* is a pair of values
    defining the minimum and maximum lengths for reads. *min_overlap* is the
    minimum number of bases on both the intron and exon side of the donor or
    acceptor site on which a read has to be aligned to be counted."""
    assert min_overlap >= 1
    min_size, max_size = read_range
    min_score = Options.min_score
    if min_size <= max_size:
        if Options.max_edit_proportion == -1.0:
            # Do not filter on edit proportion
            def select(read):
                """Filter function.
                The read has to match at its first base."""
                ali = read.alignment
                #rlen = ali.rlen
                rlen = ali.query_length
                return (
                    #read.is_head
                    (min_size <= rlen <= max_size)
                    and (ali.opt("AS") >= min_score)
                    and (ali.opt("NM") <= Options.max_mm))
        else:
            def select(read):
                """Filter function.
                The read has to match at its first base."""
                ali = read.alignment
                #rlen = ali.rlen
                rlen = ali.query_length
                return (
                    #read.is_head
                    (ali.opt("NM") <= rlen * Options.max_edit_proportion)
                    and (min_size <= rlen <= max_size)
                    and (ali.opt("AS") >= min_score)
                    and (ali.opt("NM") <= Options.max_mm))
    else:
        # Don't filter by size
        if Options.max_edit_proportion == -1.0:
            # Do not filter on edit proportion
            def select(read):
                """Filter function.
                The read has to match at its first base."""
                ali = read.alignment
                return (
                    #read.is_head
                    (ali.opt("AS") >= min_score)
                    and (ali.opt("NM") <= Options.max_mm))
        else:
            def select(read):
                """Filter function.
                The read has to match at its first base."""
                ali = read.alignment
                #rlen = ali.rlen
                rlen = ali.query_length
                return (
                    #read.is_head
                    (ali.opt("NM") <= rlen * Options.max_edit_proportion)
                    and (ali.opt("AS") >= min_score)
                    and (ali.opt("NM") <= Options.max_mm))

    def read_counter(pile):
        """Generates counts for reads overlapping a potential intron-exon junction.
        Returns the coordinate of the pile and the read count."""
        ref_pos = pile.reference_pos
        #donor_pos = pile.reference_pos
        #acceptor_pos = donor_pos - 2
        zone_start = ref_pos - min_overlap
        zone_end = ref_pos + min_overlap
        # reads overlapping at ref_pos (with min_overlap = 2):
        #                    - - - - - -
        #                - - - - - -
        #                  - - - - - -
        # reads not overlapping at ref_pos:
        # (One side of the site has less than min_overlap overlap.)
        #              - - - - - -
        #                      - - - - - -
        # potential junction site:
        #         zone_start|       |zone_end
        #                   v       v
        # ref_seq     5' N N N N|N N N N N 3'
        #                       ^
        #                    ref_pos
        counts = 0
        for read in pile.pileups:
            ali = read.alignment
            # Splice sites are oriented, because they are in the mRNA.
            if select(read) and not ali.is_reverse:
                if (ali.reference_start <= zone_start) \
                   and (zone_end <= ali.reference_end):
                    counts += 1
        return ref_pos, counts
    return read_counter


def count_reads_in(bamfile, counter, ref, ref_seq, start=None, end=None):
    #Globals.debug("%s:%d-%d (%d)\n" % (ref, start, end, (1 + end - start)))
    if start is not None:
        region_length = 1 + end - start
        #piles = bamfile.pileup(ref, start, end)
    else:
        region_length = Globals.ref_lengths[ref]
        #piles = bamfile.pileup(ref, start, end)
    # Key: dinucleotide
    # Value: array of integers corresponding to the counts
    # of overlapping reads at each position
    donor_counts = {}
    acceptor_counts = {}
    for dinuc in DINUCS:
        donor_counts[dinuc] = np.zeros(region_length)
        acceptor_counts[dinuc] = np.zeros(region_length)
    #for pile in piles:
    #    pos, counts = counter(pile)
    for (pos, counts) in imap(counter, bamfile.pileup(ref, start, end)):
        # donor site XY
        #                       pos + 2
        #                           v
        # ref_seq     5' N N N N[X Y N N N 3'
        #                exon   ^  intron
        #                      pos
        # site is at pos
        if pos <= region_length - 2:
            donor = ref_seq[pos:pos + 2]
            if "N" in donor:
                warnings.warn("Not counting dinucleotide %s\n" % donor)
            else:
                donor_counts[donor][pos] += counts
        # acceptor site XY
        #               pos - 2
        #                   v
        # ref_seq     5' N N X Y]N N N N N 3'
        #               intron  ^   exon
        #                      pos
        # site is at pos - 2
        if pos >= 2:
            acceptor = ref_seq[pos - 2:pos]
            if "N" in acceptor:
                warnings.warn("Not counting dinucleotide %s\n" % acceptor)
            else:
                acceptor_counts[acceptor][pos - 2] += counts
        Globals.debug("%d\r" % pos)
    Globals.debug("\n")
    return donor_counts, acceptor_counts


def test_coverage_loss(counts_ref, counts_other, tot_ref, tot_other, ref, dinuc):
    """For each index along the arrays *counts_ref* and *counts_other*, a
    statistical test is performed to determine whether the element in the
    second array is significantly more enriched with respect to background than
    it is in the first array. The values used as "background" are *tot_ref* and
    *tot_other*."""

    p_thresh = Globals.p_value
    def pos_where_different((idx, (nb_ref, nb_other))):
        if (nb_ref >= 5) and (nb_other >= 5):
            chi2, p_value, dof, expected_freqs = chi2_contingency(
                [[nb_ref, tot_ref - nb_ref],
                 [nb_other, tot_other - nb_other]])
            return (idx, int(1000 * (1 - p_value))), p_value < p_thresh
        elif (nb_ref >= 5):
            oddsratio, p_value = fisher_exact(
                [[nb_ref, tot_ref - nb_ref],
                 [nb_other, tot_other - nb_other]])
            return (idx, int(1000 * (1 - p_value))), p_value < p_thresh
        else:
            # We do not want to report a significant loss
            # in a site overlapped by so few reads.
            return (idx, 0), False

    freqs_ref = np.true_divide(counts_ref, tot_ref)
    freqs_other = np.true_divide(counts_other, tot_other)
    # Determine where the relative counts are lower in counts_other
    loss = np.less(freqs_other, freqs_ref)
    # Use this to filter data and hopefully save time
    for (pos, score) in compress(*izip(*imap(
        pos_where_different,
        compress(enumerate(izip(counts_ref, counts_other)), loss)))):
        yield "%s\t%d\t%d\t%s loss\t%d" % (ref, pos, pos + 2, dinuc, score)



#####################################
# For better traceability,          #
# options and global variables      #
# can be stored as class attributes #
#####################################


class Globals(object):
    """This object holds global variables."""
    p_value = 0.01


class Options(object):
    """This object contains the values of the global options."""
    # default value set so that pile_counter will not
    # select alignments based on edit proportion
    max_edit_proportion = None
    max_mm = None
    min_score = None
    # min and max lengths for a read to be counted
    # default value set so that pile_counter
    # will not select alignments based on read size
    read_range = None


#################
# main function #
#################
def main():
    """Main function of the program."""
    WRITE("%s\n" % " ".join(sys.argv))
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    ###################
    # Alignment input #
    ###################
    parser.add_argument(
        "--fasta_refs",
        required=True,
        help="File in fasta format where the sequences "
        "are those on which the reads where mapped.\n"
        "The sequence names should match the reference names "
        "in the .bam files.")
    parser.add_argument(
        "--ref_file",
        required=True,
        help="Sorted and indexed reference .bam file.")
    parser.add_argument(
        "--other_file",
        required=True,
        help="Sorted and indexed .bam file to compare with the reference.")
    ####################
    # Read count input #
    ####################
    parser.add_argument(
        "--ref_counts_ref",
        required=True,
        help="File containing the read counts for each reference "
        "in the first library.\n"
        "Each line shoud contain a reference and a count "
        "separated by a tabulation character.")
    parser.add_argument(
        "--ref_counts_other",
        required=True,
        help="File containing the read counts for each reference "
        "in the second library\n."
        "Each line shoud contain a reference and a count "
        "separated by a tabulation character.")
    parser.add_argument(
        "-d", "--bed_donor_loss",
        required=True,
        type=argparse.FileType('w'),
        help="File in which to write the positions of potential "
        "donor dinucleotides that are subject to a significant "
        "loss in the second library with respect to the first.")
    parser.add_argument(
        "-a", "--bed_acceptor_loss",
        required=True,
        type=argparse.FileType('w'),
        help="File in which to write the positions of potential "
        "acceptor dinucleotides that are subject to a significant "
        "loss in the second library with respect to the first.")
    #severine.chambeyron@yoshi:~/projet_Abdou$ for cluster in `seq 1 142`; do cluster_name=`sed -n ${cluster}p listes_refs/cluster_refs.txt`; count=`zcat shwhite/mapped_reads_D_melanogaster/shwhite_23-30_on_D_melanogaster_piRNA_candidate.fasta.gz | grep "^>" | grep "piRNA_cluster" | grep "@cluster${cluster}@" | wc -l`; echo -e "${cluster_name}\t$count"; done > shwhite/mapped_reads_D_melanogaster/shwhite_23-30_on_D_melanogaster_piRNA_candidate_piRNA_cluster_read_counts.txt
    ###########################
    # Options to filter reads #
    ###########################
    # default value set so that pile_counter will not
    # select alignments based on edit proportion
    parser.add_argument(
        "--max_edit_proportion",
        type=float,
        default=-1.0,
        help="Maximum proportion of edits for an alignment to be recorded."
        "\nThe default is to accept any proportion of edits.")
    parser.add_argument(
        "--max_mm",
        type=int,
        default=0,
        help="Maximum number of mismatches for an alignment to be counted.")
    parser.add_argument(
        "--min_score",
        type=int,
        default=-200,
        help="Minimum mapping score for an alignment to be recorded."
        "\nThe default value is -200.")
    # min and max lengths for a read to be counted
    # default value set so that pile_counter
    # will not select alignments based on read size
    parser.add_argument(
        "--read_range",
        nargs=2,
        type=int,
        default=[0, -1],
        help="Minimum and maximum lengths of the reads to take into account."
        "\nThe default is to take all sizes into account.")
    parser.add_argument(
        "--no_secondary",
        action="store_true",
        help="Do not count secondary alignments of reads.\n"
        "A read will only be counted once, but where it will be counted "
        "may be partially arbitrary.")
    parser.add_argument(
        "--min_overlap",
        type=int,
        default=2,
        help="Length of the minimal overlap at both sides of a potential "
        "exon-intron junction site for a read to be considered as"
        "overlapping the site.")
    parser.add_argument(
        "-e", "--exclude_refs",
        help="File containing a list of references "
        "to exclude from the profiling.\n"
        "One reference per line.",
        type=argparse.FileType("r"))
    parser.add_argument(
        "-v", "--verbose",
        action="store_true",
        help="Generate more output.")
    args = parser.parse_args()

    # Give global access to some options
    Options.max_edit_proportion = args.max_edit_proportion
    Options.max_mm = args.max_mm
    Options.min_score = args.min_score
    Options.read_range = tuple(args.read_range)

    if args.verbose:
        def debug(msg):
            sys.stderr.write(msg)
    else:
        def debug(msg):
            pass
    Globals.debug = staticmethod(debug)

    # To avoid repeatedly calling these as string methods:
    strip = str.rstrip
    split = str.split
    upper = str.upper

    counter = pile_counter(Options.read_range, args.min_overlap)

    ###############################################
    # Make the list of references we want to skip #
    ###############################################
    Globals.exclude_refs = []
    if args.exclude_refs:
        with args.exclude_refs as exclusion_file:
            for line in exclusion_file:
                ref_name = strip(line)
                if ref_name:
                    if ref_name[0] != "#":
                        Globals.exclude_refs.append(ref_name)
    Globals.exclude_refs = set(Globals.exclude_refs)

    ################################################
    # Determine reference lengths and total counts #
    ################################################
    # key: reference name
    # value: chromosome length
    Globals.ref_lengths = {}
    with pysam.Samfile(args.ref_file) as samfile:
        for ref, ref_len in izip(samfile.references, samfile.lengths):
            if ref in Globals.exclude_refs:
                # Skip references we're not interested in.
                #print "Skipping %s" % chrom
                continue
            else:
                Globals.ref_lengths[ref] = ref_len
                #Globals.ref_counts_ref[ref] = samfile.count(ref)
    # Checks using the other file
    with pysam.Samfile(args.other_file) as samfile:
        for ref, ref_len in izip(samfile.references, samfile.lengths):
            if ref in Globals.exclude_refs:
                # Skip references we're not interested in.
                #print "Skipping %s" % chrom
                continue
            else:
                if ref in Globals.ref_lengths:
                    assert Globals.ref_lengths[ref] == ref_len
                else:
                    Globals.ref_lengths[ref] = ref_len
                #Globals.ref_counts_other[ref] = samfile.count(ref)
    # key: reference name
    # value: total read counts for that reference
    # in the first library
    Globals.ref_counts_ref = {}
    with open(args.ref_counts_ref, "r") as counts_file:
        for line in counts_file:
            ref, count = split(strip(line))
            if ref not in Globals.exclude_refs:
                Globals.ref_counts_ref[ref] = int(count)
    # key: reference name
    # value: total read counts for that reference
    # in the second library
    Globals.ref_counts_other = {}
    with open(args.ref_counts_other, "r") as counts_file:
        for line in counts_file:
            ref, count = split(strip(line))
            if ref not in Globals.exclude_refs:
                Globals.ref_counts_other[ref] = int(count)

    with pysam.Samfile(args.ref_file) as ref_file, pysam.Samfile(args.other_file) as other_file:
        ref_records = SeqIO.index(args.fasta_refs, "fasta")
        for (ref, ref_len) in sorted(Globals.ref_lengths.items()):
            tot_ref = Globals.ref_counts_ref[ref]
            tot_other = Globals.ref_counts_other[ref]
            WRITE("Reference: %s (len: %d, tot_ref: %d, tot_other: %d)\n" % (
                ref, ref_len, tot_ref, tot_other))
            ref_seq = upper(str(ref_records[ref].seq))
            assert len(ref_seq) == ref_len, "Incoherent lengths."
            # histogram of dinucleotides to compare
            # with histograms of lost dinucleotides
            dinuc_counts = defaultdict(int)
            for idx in xrange(ref_len - 1): 
                dinuc_counts[ref_seq[idx:idx + 2]] += 1
            donor_counts_ref, acceptor_counts_ref = count_reads_in(
                ref_file, counter, ref, ref_seq)
            donor_counts_other, acceptor_counts_other = count_reads_in(
                other_file, counter, ref, ref_seq)
            #lost_donor_dinuc_counts = defaultdict(int)
            #lost_acceptor_dinuc_counts = defaultdict(int)
            lost_donor_dinuc_counts = {}
            lost_acceptor_dinuc_counts = {}
            for dinuc in DINUCS:
                WRITE("%s\n" % dinuc)
                bed_lines = list(test_coverage_loss(
                    donor_counts_ref[dinuc],
                    donor_counts_other[dinuc],
                    tot_ref, tot_other,
                    ref, dinuc))
                args.bed_donor_loss.write("\n".join(bed_lines))
                args.bed_donor_loss.write("\n")
                lost_donor_dinuc_counts[dinuc] = len(bed_lines)
                msg = "".join([
                    "%d potential %s donor positions " % (len(bed_lines), dinuc),
                    "with significant loss (p_value < %f).\n" % Globals.p_value])
                WRITE(msg)
                bed_lines = list(test_coverage_loss(
                    acceptor_counts_ref[dinuc],
                    acceptor_counts_other[dinuc],
                    tot_ref, tot_other,
                    ref, dinuc))
                args.bed_acceptor_loss.write("\n".join(bed_lines))
                args.bed_acceptor_loss.write("\n")
                lost_acceptor_dinuc_counts[dinuc] = len(bed_lines)
                msg = "".join([
                    "%d potential %s acceptor positions " % (len(bed_lines), dinuc),
                    "with significant loss (p_value < %f).\n" % Globals.p_value])
                WRITE(msg)
            dinucs_histogram = [dinuc_counts[dinuc] for dinuc in DINUCS]
            if any(np.less(dinucs_histogram, 5)):
                msg = "".join([
                    "Some dinucleotides have less than 5 counts.\n",
                    "chi^2 test needs high counts.\n"])
                warnings.warn(msg)
            lost_donor_dinucs_histogram = [
                lost_donor_dinuc_counts[dinuc] for dinuc in DINUCS]
            if any(np.less(lost_donor_dinucs_histogram, 5)):
                msg = "".join([
                    "Some significantly lost dinucleotides ",
                    "have less than 5 counts.\n",
                    "chi^2 test needs high counts.\n"])
                warnings.warn(msg)
            chi2, p_value, dof, expected_freqs = chi2_contingency(
                dinucs_histogram,
                lost_donor_dinucs_histogram)
            if p_value < 0.05:
                WRITE("".join(["Lost potential donor dinucleotide ",
                               "histogram significantly differs ",
                               "from overall dinucleotide histogram ",
                               "in %s\n" % ref]))
            else:
                WRITE("Nothing special in donor dinucleotide histogram.\n")
            lost_acceptor_dinucs_histogram = [
                lost_acceptor_dinuc_counts[dinuc] for dinuc in DINUCS]
            if any(np.less(lost_acceptor_dinucs_histogram, 5)):
                msg = "".join([
                    "Some significantly lost dinucleotides ",
                    "have less than 5 counts.\n",
                    "chi^2 test needs high counts.\n"])
                warnings.warn(msg)
            chi2, p_value, dof, expected_freqs = chi2_contingency(
                dinucs_histogram,
                lost_acceptor_dinucs_histogram)
            if p_value < 0.05:
                WRITE("".join(["Lost potential acceptor dinucleotide ",
                               "histogram significantly differs ",
                               "from overall dinucleotide histogram ",
                               "in %s\n" % ref]))
            else:
                WRITE("Nothing special in acceptor dinucleotide histogram.\n")
    return 0

if __name__ == "__main__":
    sys.exit(main())
