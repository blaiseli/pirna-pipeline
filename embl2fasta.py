#!/usr/bin/env python
# Usage: embl2fasta.py <embl file> <output file>

import re
import string
import sys

class Record(object):
    def __init__(self):
        self.name = None
        self.seq_parts = []
        self.seq = None
        #self.TIR5_coords = None
        #self.TIR3_coords = None
        #self.TIR5 = None
        #self.TIR3 = None
        #self.LTR5_coords = None
        #self.LTR3_coords = None
        #self.LTR5 = None
        #self.LTR3 = None
        #self.incomplete = False

    def __repr__(self):
        return ">%s\n%s" % (self.name, self.seq)

    def make_seq(self):
        self.seq = "".join(self.seq_parts)

    #def make_TIRs(self):
    #    if (self.TIR5_coords is not None) and (self.LTR3_coords is not None):
    #        self.TIR5 = self.seq[self.TIR5_coords[0]-1:self.TIR5_coords[1]]
    #        self.TIR3 = self.seq[self.TIR3_coords[0]-1:self.TIR3_coords[1]]
    #def make_LTRs(self):
    #    if (self.LTR5_coords is not None) and (self.LTR3_coords is not None):
    #        self.LTR5 = self.seq[self.LTR5_coords[0]-1:self.LTR5_coords[1]]
    #        self.LTR3 = self.seq[self.LTR3_coords[0]-1:self.LTR3_coords[1]]

with open(sys.argv[2], "w") as out, open(sys.argv[1], "r") as f:
    for line in f:
        fields = line.split()
        f_type = fields[0]
        if f_type == "ID":
            record = Record()
            parse_seq = False
        elif f_type == "DR":
            # remove the final dot
            try:
                record.name = fields[-1][:-1]
            except NameError:
                print line
        #elif f_type == "FT":
        #    if "terminal_inverted_repeat" in fields:
        #        if not record.TIR5_coords:
        #            try:
        #                record.TIR5_coords = map(int, fields[4].split(":")[-1].split(".."))
        #            except ValueError:
        #                record.incomplete = True
        #        else:
        #            try:
        #                record.TIR3_coords = map(int, fields[4].split(":")[-1].split(".."))
        #            except ValueError:
        #                record.incomplete = True
        #    if "five_prime_LTR" in fields:
        #        try:
        #            record.LTR5_coords = map(int, fields[4].split(":")[-1].split(".."))
        #        except ValueError:
        #            record.incomplete = True
        #    if "three_prime_LTR" in fields:
        #        try:
        #            record.LTR3_coords = map(int, fields[4].split(":")[-1].split(".."))
        #        except ValueError:
        #            record.incomplete = True
        elif f_type == "SQ":
            parse_seq = True
        elif f_type == "XX":
            if parse_seq:
                parse_seq = False
        elif f_type == "//":
            parse_seq = False
            record.make_seq()
            out.write(">%s\n%s\n" % (re.sub(r"\\", "_", record.name), string.upper(record.seq)))
            #record.make_TIRs()
            #record.make_LTRs()
            #if not record.incomplete:
            #    if record.TIR5 and record.TIR3:
            #        sys.stdout.write(">%s_TIR5\n%s\n>%s_TIR3\n%s\n" % (record.name, record.TIR5, record.name, record.TIR3))
            #    elif record.LTR5 and record.LTR3:
            #        out.write(">%s_LTR_junction\n%s%s\n" % (record.name, string.upper(record.seq[-150:]), string.upper(record.seq[:150])))
            #        sys.stdout.write(">%s_LTR5\n%s\n>%s_LTR3\n%s\n" % (record.name, string.upper(record.LTR5), record.name, string.upper(record.LTR3)))
            del record
        else:
            if parse_seq:
                record.seq_parts.extend(fields[:-1])
sys.exit(0)
