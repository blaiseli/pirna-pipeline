#!/usr/bin/env bash
# Usage: do_do_ping_pong.sh <ref_file> <num_slices> <lib1> [<lib2> ...]
# <ref_file> should be a file containing the list of references on which to perform ping-pong analysis.
# <num_slices> is the number of slices in which to divide the piRNA candidates
# Use 0 to only do the analyses on full libraries.
# The slice size will be determined based on the number of reads in the poorer libraries on a per-refrerence basis
# <lib1>, <lib2>, etc. are the names of the libraries for which to make the analyses

#TODO Clean script
# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}


ref_file=${1}
num_slices=${2}
libraries=${@:3}
#num_libs=`echo "$# - 3" | bc`
#libraries=${@:3:${num_libs}}
#output_file=${@:$#}

lib_names=`python -c "import sys; print '_'.join(sys.argv[1:])" ${libraries}`

ref_text=`basename ${ref_file}`
results_dir="ping_pong_results/${lib_names}_${ref_text%.*}_${num_slices}_slices"

[ ${num_slices} != "0" ] && mkdir -p ${results_dir}

mkdir -p "ping_pong_results/signature_plots_${lib_names}_${ref_text%.*}"

echo -e "\nPing-pong analysis will be done using references from ${ref_file}"
echo "Ping-pong analysis will be performed for libraries ${libraries}"
# [[:graph:]] -> class of the visible characters
filenames=`echo ${libraries} | sed 's|\([[:graph:]][[:graph:]]*\)|\1/histos_piRNA_candidate_on_TE/\1_piRNA_candidate_on_TE_ref_counts.txt|g'`

# Reset list of signature files for full ping-pong analyses
for lib in ${libraries}
do
    echo -e "#ref\tsignature_file" > "${lib}/ping_pong_full/signature_files_list.txt"
done

#http://stackoverflow.com/questions/10982911/creating-temporary-files-in-bash
#tmpdir=$(mktemp -dt "$0.XXXXXXXXXX")
# But:
#mktemp: invalid template, ‘/home/bli/src/pirna-pipeline/do_do_ping_pong.sh.XXXXXXXXXX’, contains directory separator
# So:
#tmpdir=$(mktemp -dt "`basename $0`.XXXXXXXXXX")
#
#compute_slice_size.py -n ${num_slices} -i ${filenames} > ${tmpdir}/slice_sizes.txt || error_exit "compute_slice_size.py failed."

if [ ${num_slices} != "0" ]
then
    compute_slice_size.py -n ${num_slices} -i ${filenames} > ${results_dir}/slice_sizes.txt || error_exit "compute_slice_size.py failed."
#cat ${tmpdir}/slice_sizes.txt
fi


for ref in `cat ${ref_file}`
do
    if [ ${ref} == "global" -o ${ref} == "all" ]
    then
        ref_to_count="tot"
    else
        ref_to_count=${ref}
    fi
    #TODO: gather percentages_pp from "${pp_dir}/${ref}/${lib}_pp_counts.txt"
    # over libs:
    # ref   lib1_slice_1    lib1_slice_2    etc...  lib2_slice_1    lib2_slice_2    etc...
    #slice_size=`awk -v ref=${ref_to_count} '$1==ref{print $2}' ${tmpdir}/slice_sizes.txt`
    if [ ${num_slices} != "0" ]
    then
        slice_size=`awk -v ref=${ref_to_count} '$1==ref{print $2}' ${results_dir}/slice_sizes.txt`
    fi
    signature_files_this_ref=" "
    for lib in ${libraries}
    do
        if [ ${num_slices} != "0" ]
        then
            pp_dir="${lib}/ping_pong_${num_slices}_slices"
            cmd="do_ping_pong.sh ${lib} ${num_slices} ${slice_size} ${ref}"
            echo -e "\nSlice size for ${ref}: ${slice_size}\n${cmd}"
            #echo ${cmd}
            eval ${cmd} || error_exit "do_ping_pong.sh failed."
        fi
        echo -e "\nDoing ping-pong analysis on full library."
        signature_files_this_lib="${lib}/ping_pong_full/signature_files_list.txt"
        cmd="ping_pong_full.sh ${lib} ${ref} ${signature_files_this_lib}"
        #echo ${cmd}
        eval ${cmd} || error_exit "ping_pong_full.sh failed."
        signature_file=`awk -v ref=${ref} -F "\t" '$1 == ref {print $2}' ${signature_files_this_lib}`
        signature_files_this_ref="${signature_files_this_ref} ${signature_file}"
    done
    plot_pp_signature.py \
        -i ${signature_files_this_ref} -l ${libraries} \
        -d ping_pong_results/signature_plots_${lib_names}_${ref_text%.*} \
        -p ${ref}_pp_signatures
done

#cmd="gather_pingpong_results.py -r \
#    ${ref_file} -l ${libraries} -n ${num_slices} \
#    -s ${results_dir}/slice_sizes.txt \
#    -m ${results_dir}/average_ping_pong_${lib_names}.xls \
#    -d ${results_dir} -c green \
#    --sort_by_size \
#    > ${results_dir}/ping_pong_${lib_names}.xls"
#echo ${cmd}
#eval ${cmd} || error_exit "gather_pingpong_results.py failed."
#
#curr_dir=`pwd`
#cd ${results_dir}
##for category in "total" "F_0mm" "R_0mm" "F_mm" "R_mm"
#for category in "total"
#do
#    texfot pdflatex ${lib_names}_${category}_percent.tex || error_exit "pdf compilation failed."
#    texfot pdflatex ${lib_names}_${category}_percent.tex || error_exit "pdf compilation failed."
#done
#
#cd ${curr_dir}

if [ ${num_slices} != "0" ]
then
    cmd="do_plot_ping_pong_results.sh ${ref_file} ${num_slices} ${libraries}"
    echo ${cmd}
    eval ${cmd} || error_exit "${cmd} failed."
fi


exit 0
