#!/usr/bin/env python
"""
This script will generate LaTeX code to make a scatterplot.
"""

import argparse
#import os
#opj = os.path.join
#opb = os.path.basename
# To avoid repeatedly calling these as string methods:
from string import split, strip
from operator import attrgetter
import re
import sys
WRITE = sys.stdout.write
import warnings


def formatwarning(message, category, filename, lineno, line):
    return "%s:%s: %s:%s" % (filename, lineno, category.__name__, message)
warnings.formatwarning = formatwarning

#import csv

from collections import defaultdict
from itertools import izip

import numpy as np
from scipy import stats
#from math import log, sqrt, floor, ceil
from math import log, floor, ceil


def log10(val):
    return log(val, 10)


def log2(val):
    return log(val, 2)


def trunc(val, decim=0, base=10):
    """<val>: value to truncate.
    <decim>: number of decimals to keep
    (negative to remove tens, hundreds, etc., by default: 0)
    <base>: base in which to count decimals (by default: 10)"""
    return floor((base ** decim) * val) / (base ** decim)


def fill(val, decim=0, base=10):
    """<val>: value to reach.
    <decim>: number of decimals of the number to reach
    (negative for tens, hundreds, etc., by defautt: 0)
    <base>: base in which to count decimals (by defaut: 10)"""
    return ceil((base ** decim) * val) / (base ** decim)


class Point(object):
    """Class representing a point in the scatterplot."""
    def __init__(self, name, x, y):
        self.name = name
        if Options.pool_variant_names:
            self.text_name = re.sub(
                "_", "\_", "-".join(split(self.name, "-")[:-1]))
        else:
            self.text_name = re.sub("_", "\_", self.name)
        self.raw_x = None
        self.raw_y = None
        self.norm_x = None
        self.norm_y = None
        self.x = x
        self.y = y
        self.fold = None
        self.print_name = False
        self.colour = None
        self.intensity = None
        self.style = None
        #self.shape = "shape=circle"

    def set_intensity(self, slope):
        """Calculates an 'intensity' measuring the distance
        of the point from a slope 1."""
        if self.norm_x == 0 or self.norm_y == 0:
            self.intensity = 0
        else:
            self.fold = self.raw_y / (self.raw_x * slope)
            #fold = self.norm_y / self.norm_x
            self.intensity = 1 - min(self.fold, 1 / self.fold)

    def set_colour(self):
        """Determines the colour of the point."""
        self.colour = Globals.elem2colour[self.name]

    def set_style(self, max_intensity):
        """Determines the style of the point."""
        if self.name in Globals.to_print:
            self.print_name = True
        if Options.colour_intensity:
            colour = "%s!%d!white" % (
                self.colour, int(floor(100 * self.intensity)))
        else:
            colour = self.colour
        shape = "shape=circle, inner sep=1pt, fill, draw"
        if self.print_name:
            if Options.label_below:
                pin_dir = "below"
                pin_type = "label"
            elif self.fold >= 1:
                pin_dir = "above left"
                pin_type = "pin"
            else:
                pin_dir = "below right"
                pin_type = "pin"
            pin = ", %s={[%s]%s:{\\tiny \\textsf{\\bf %s}}}" % (
                pin_type, self.colour, pin_dir, self.text_name)
        else:
            pin = ""
        self.style = ",".join([shape, colour]) + pin

    def tikznode(self):
        code = "\\node[%s] (%s) at (%.5f,%.5f) {};\n" % (
            self.style,
            re.sub("\.", "-", re.sub(r"\)", "-", re.sub(
                r"\(", "-", self.name))),
            Globals.transform(self.norm_x) * Globals.x_scale,
            Globals.transform(self.norm_y) * Globals.y_scale)
        if Options.tips:
            code += "\\node[] (%s_tip) at (%.5f,%.5f) {\pdftooltip[]{\phantom{X}}{%s}};\n" % (
                re.sub("\.", "-", re.sub(r"\)", "-", re.sub(
                    r"\(", "-", self.name))),
                Globals.transform(self.norm_x) * Globals.x_scale,
                Globals.transform(self.norm_y) * Globals.y_scale,
                self.text_name)
        return code


#########################################################
# For better traceability, options and global variables #
# should be stored as class attributes                  #
#########################################################


class Globals(object):
    """This object holds global variables."""
    @staticmethod
    def transform(val):
        return val

    @staticmethod
    def inverse_trans(val):
        return val

    library_names = {
        "yb_het": "fs(1) Yb heterozygote",
        "yb_hom": "fs(1) Yb mutant",
        "drosha_dmt": "TN-Drosha",
        "drosha_dw": "WT-Drosha",
        "dmt_771": "TN-Drosha G80",
        "dw_771": "WT-Drosha G80"}


class Options(object):
    """This object contains the values of the global options."""
    tips = True
    pool_variant_names = False
    #TODO: make this a command-line option
    globalscale = 1
    printname_intensity = 1.1
    draw_xequalsy = False
    #draw_xequalsy = True
    colour_intensity = False
    label_below = True
    print_regress = True


#################
# main function #
#################
#TODO: finish switching to argparse
def main():
    """Main function."""
    WRITE(" ".join(sys.argv) + "\n")
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        "-i", "--in_files",
        nargs=2,
        required=True,
        help="Space-separated pair of *ref_counts.txt files.\n"
        "Data in each file should consist in lines with at least "
        "two blank-separated fields as follows:\n"
        "name counts_1 [counts_2 etc.]\n"
        "The lines should be sorted on the name fields.\n"
        "The counts of the first file will be used, once normalized, "
        "as x-axis coordinates for the points.\n"
        "The counts of the second file will be used, once normalized, "
        "as y-axis coordinates for the points.")
    parser.add_argument(
        "--in_files_ref",
        nargs=2,
        help="Space-separated pair of files containing the read counts "
        "for reference sequencing banks.\n"
        "Use this to plot folds instead of counts.")
    parser.add_argument(
        "-l", "--library_names",
        nargs=2,
        required=True,
        help="Space-separated pair of library names.")
    parser.add_argument(
        "--library_names_ref",
        nargs=2,
        help="Space-separated pair of reference library names.")
    parser.add_argument(
        "-n", "--normalizations",
        nargs=2,
        help="Space-separated pair of files containing the values used "
        "to normalize the counts in the data.\n"
        "Of course, the file order should correspond to that "
        "given in the option --in_files.")
    parser.add_argument(
        "--normalizations_ref",
        nargs=2,
        help="Space-separated pair of files containing the values used "
        "to normalize the counts in the reference data.\n"
        "Of course, the file order should correspond to that "
        "given in the option --in_files_ref.")
    parser.add_argument(
        "-b", "--by_norm",
        type=int,
        default=1,
        help="Set this to count reads by multiples of normalizers.")
    parser.add_argument(
        "-p", "--plot",
        required=True,
        help="Name of the scatter plot file to write.")
    parser.add_argument(
        "--name_conversion",
        help="Tab-separated file with 2 columns used to convert names.\n"
        "First column contains names as they appear in the files "
        "given in the option --in_file.\n"
        "Second column contains names as they appear in the files "
        "given in the options --element_lists and / or --elements_order.")
    parser.add_argument(
        "-e", "--element_lists",
        nargs="*",
        help="Space-separated list of files contining element names.\n"
        "A file groups elements by categories for colour attribution.\n"
        "There should be one element per line. These elements should "
        "correspond to entries in the *ref_counts.txt filesa\n."
        "The colours attributed to the elements listed in a file will be "
        "defined by the  --colours option.")
    parser.add_argument(
        "-c", "--colours",
        nargs="*",
        help="Space-separated list of colours to use in the plots.\n"
        "The order of the colours should correspond to the order of "
        "the files listed with the option --element_lists.")
    parser.add_argument(
        "--ref_column",
        type=int,
        help="Colum (1-based including a series names column) "
        "to use as referenc names.\n",
        default=1)
    parser.add_argument(
        "--column",
        type=int,
        help="Colum (1-based including a series names column) "
        "to use as counts.\n"
        "First column headers in *ref_counts.txt files are:\n"
        "#ref   total   total_F total_R F_0mm   R_0mm   F_mm    R_mm\n"
        "Extra columns detail counts by growing number of mismatches "
        "starting from 1 and by successions of F and R read counts.",
        default=2)
    parser.add_argument(
        "--min_counts",
        type=float,
        help="Minimal number of counts for an element to be represented "
        "on the scatter plot.",
        default=1)
    parser.add_argument(
        "--pool_variant_names",
        action="store_true",
        help="Set this option to remove the last part of the elements names "
        "(-RA, -RB, etc.) that indicate the variants.")
    parser.add_argument(
        "--no_line",
        action="store_true",
        help="Set this option to not draw the regression line.")
    parser.add_argument(
        "--axis_font",
        help="Font size for axes labels.\n"
        "This should be a LaTeX font size name.",
        default="footnotesize")
    parser.add_argument(
        "--tick_font",
        help="Font size for ticks.\n"
        "This should be a LaTeX font size name.",
        default="tiny")
    parser.add_argument(
        "-x", "--x_axis",
        help="Label for the x axis of the plot.",
        default="number of mapping reads")
    parser.add_argument(
        "-y", "--y_axis",
        help="Label for the y axis of the plot.",
        default="number of mapping reads")
    parser.add_argument(
        "--x_scale",
        type=float,
        help="Scale for the x axis of the plot.",
        default=1.0)
    parser.add_argument(
        "--y_scale",
        type=float,
        help="Scale for the y axis of the plot.",
        default=1.0)
    parser.add_argument(
        "-t", "--transform",
        required=True,
        help="log2, log10, or a linear scale to apply.")
    #parser.add_argument("-s", "--switch",
    #        help="This is an optional boolean switch.",
    #        action="store_true")
    args = parser.parse_args()
    if args.no_line:
        Options.print_regress = False
    if args.pool_variant_names:
        Options.pool_variant_names = True
    Globals.x_scale = args.x_scale
    Globals.y_scale = args.y_scale
    Globals.axis_font = args.axis_font
    Globals.tick_font = args.tick_font
    if args.transform == "log10":
        def transform(val):
            return log10(val)

        def inverse_trans(val):
            return 10 ** val

        def tick(val):
            return "$10^{%d}$" % val

        def xtick(val):
            return tick(val)

        def ytick(val):
            return tick(val)
    elif args.transform == "log2":
        def transform(val):
            return log2(val)

        def inverse_trans(val):
            return 2 ** val

        def tick(val):
            #return "$2^{%d}$" % val
            #return "$%d$" % val
            #return "$\\frac{1}{%d}$" % (2 ** -val)
            if val < 0:
                #return "$1 / %s$" % (2 ** -val)
                return "$1 / %d$" % (2 ** -val)
            elif val > 0:
                return "$%s$" % (2 ** val)
            else:
                return "$1$"
            #return "$%.4f$" % (2 ** val)

        def xtick(val):
            return tick(val)

        def ytick(val):
            if val < 0:
                return "$\\frac{1}{%d}$" % (2 ** -val)
                #return "$1 / %s$" % (2 ** -val)
            elif val > 0:
                return "$%s$" % (2 ** val)
            else:
                return "$1$"
    elif args.transform:
        def transform(val):
            return val / float(args.transform)

        def inverse_trans(val):
            return val * float(args.transform)

        def tick(val):
            return "$%s$" % (val * float(args.transform))

        def xtick(val):
            return tick(val)

        def ytick(val):
            return tick(val)
    else:
        def transform(val):
            return val

        def inverse_trans(val):
            return val

        def tick(val):
            return "$%s$" % val

        def xtick(val):
            return tick(val)

        def ytick(val):
            return tick(val)
    Globals.transform = staticmethod(transform)
    Globals.inverse_trans = staticmethod(inverse_trans)
    Globals.tick = staticmethod(tick)
    Globals.xtick = staticmethod(xtick)
    Globals.ytick = staticmethod(ytick)
    xmin = None
    xmax = None
    ymin = None
    ymax = None
    Globals.name_converter = {}
    if args.name_conversion:
        with open(args.name_conversion) as conversion_file:
            for line in map(strip, conversion_file):
                if line[0] == "#":
                    continue
                [in_name, out_name] = split(line)
                Globals.name_converter[in_name] = out_name
    Globals.include = set([])
    # Default colour is "black"
    Globals.elem2colour = defaultdict(lambda: "black")
    Globals.to_print = set([])
    # Add other colours using options.
    for (list_file_name, colour) in izip(args.element_lists, args.colours):
        with open(list_file_name, "r") as list_file:
            for line in list_file:
                elem = strip(line)
                #if colour not in ["none", "white"]:
                #    Globals.to_print.add(elem)
                Globals.elem2colour[elem] = colour
                Globals.include.add(elem)

    if args.normalizations:
        try:
            Globals.norm_1 = float(args.normalizations[0]) / args.by_norm
        except ValueError:
            norm_f = open(args.normalizations[0], "r")
            Globals.norm_1 = float(strip(norm_f.readline())) / args.by_norm
            norm_f.close()
        try:
            Globals.norm_2 = float(args.normalizations[1]) / args.by_norm
        except ValueError:
            norm_f = open(args.normalizations[1], "r")
            Globals.norm_2 = float(strip(norm_f.readline())) / args.by_norm
            norm_f.close()
    else:
        Globals.norm_1 = 1.0 / args.by_norm
        Globals.norm_2 = 1.0 / args.by_norm

    if args.normalizations_ref is not None:
        msg = "".join([
            "Reference libraries have to be provided with option ",
            "--normalizations_ref if you provide normalizations for them."])
        assert args.in_files_ref, msg
        try:
            Globals.norm_1_ref = float(args.normalizations_ref[0]) / args.by_norm
        except ValueError:
            norm_f = open(args.normalizations_ref[0], "r")
            Globals.norm_1_ref = float(strip(norm_f.readline())) / args.by_norm
            norm_f.close()
        try:
            Globals.norm_2_ref = float(args.normalizations_ref[1]) / args.by_norm
        except ValueError:
            norm_f = open(args.normalizations_ref[1], "r")
            Globals.norm_2_ref = float(strip(norm_f.readline())) / args.by_norm
            norm_f.close()
    else:
        Globals.norm_1_ref = 1.0 / args.by_norm
        Globals.norm_2_ref = 1.0 / args.by_norm

    if args.library_names:
        if args.library_names_ref:
            msg = "".join([
                "Reference libraries have to be provided with option ",
                "--in_files_ref if you provide reference library names."])
            assert args.in_files_ref, msg
            name_1 = Globals.library_names.get(
                args.library_names[0],
                args.library_names[0])
            name_1_ref = Globals.library_names.get(
                args.library_names_ref[0],
                args.library_names_ref[0])
            x_axis_legend = "%s / %s (%s)" % (
                name_1, name_1_ref,
                args.x_axis)
            name_2 = Globals.library_names.get(
                args.library_names[1],
                args.library_names[1])
            name_2_ref = Globals.library_names.get(
                args.library_names_ref[1],
                args.library_names_ref[1])
            y_axis_legend = "%s / %s (%s)" % (name_2, name_2_ref, args.y_axis)
        else:
            name_1 = Globals.library_names.get(
                args.library_names[0],
                args.library_names[0])
            x_axis_legend = "%s (%s)" % (name_1, args.x_axis)
            name_2 = Globals.library_names.get(
                args.library_names[1],
                args.library_names[1])
            y_axis_legend = "%s (%s)" % (name_2, args.y_axis)
    else:
        x_axis_legend = args.x_axis
        y_axis_legend = args.y_axis

    data = {}
    data_f = open(args.in_files[0])
    name_converter = Globals.name_converter

    def convert_name(name):
        """Converts a name if conversion is defined in
        *Globals.name_converter*."""
        #if name in name_converter:
        #    print "converting %s to %s" % (name, name_converter[name])
        return name_converter.get(name, name)
    for line in data_f:
        if line[0] == "#" or len(strip(line)) == 0:
            continue
        fields = split(line)
        name = convert_name(fields[args.ref_column - 1])
        if name not in Globals.include:
            continue
        # Deal with piRNA cluster names
        name_parts = split(name, ":")
        if name_parts:
            if name_parts[0].startswith("cluster"):
                name = name_parts[0][7:]
        #name_parts = split(name, "_")
        #if name_parts[0].startswith("D") and len(name_parts) > 1:
        #    #print name
        #    continue
        # TODO: Optionize this
        #if name not in Colours.soma | Colours.inter:
        #    continue
        x = float(fields[args.column - 1])
        if x >= args.min_counts:
            #data[name] = data.get(name, Point(name, 0, 0))
            data[name] = Point(name, 0, 0)
            data[name].raw_x = x
            data[name].norm_x = x / Globals.norm_1
            data[name].x = Globals.transform(x / Globals.norm_1)
    data_f.close()
    if args.in_files_ref:
        ref_f = open(args.in_files_ref[0])
        for line in ref_f:
            if line[0] == "#" or len(strip(line)) == 0:
                continue
            fields = split(line)
            name = convert_name(fields[args.ref_column - 1])
            # Deal with piRNA cluster names
            name_parts = split(name, ":")
            if len(name_parts):
                if name_parts[0].startswith("cluster"):
                    name = name_parts[0][7:]
            #if name_parts[0].startswith("D") and len(name_parts) > 1:
            #    #print name
            #    continue
            # TODO: Optionize this
            #if name not in Colours.soma | Colours.inter:
            #    continue
            if name in data:
                x_ref = float(fields[args.column - 1])
                if x_ref >= args.min_counts:
                    #data[name] = data.get(name, Point(name, 0, 0))
                    data[name].raw_x /= x_ref
                    data[name].norm_x /= (x_ref / Globals.norm_1_ref)
                    data[name].x = Globals.transform(data[name].norm_x)
                else:
                    del data[name]
        ref_f.close()
    data_f = open(args.in_files[1])
    for line in data_f:
        if line[0] == "#" or len(strip(line)) == 0:
            continue
        fields = split(line)
        name = convert_name(fields[args.ref_column - 1])
        # Deal with piRNA cluster names
        name_parts = split(name, ":")
        if len(name_parts):
            if name_parts[0].startswith("cluster"):
                name = name_parts[0][7:]
        #if name_parts[0].startswith("D") and len(name_parts) > 1:
        #    #print name
        #    continue
        # TODO: Optionize this
        #if name not in Colours.soma | Colours.inter:
        #    continue
        if name in data:
            y = float(fields[args.column - 1])
            if y >= args.min_counts:
                #data[name] = data.get(name, Point(name, 0, 0))
                data[name].raw_y = y
                data[name].norm_y = y / Globals.norm_2
                data[name].y = Globals.transform(y / Globals.norm_2)
            else:
                del data[name]
        #y = float(fields[aargs.column - 1])
        #if y >= args.min_counts and data.has_key(name):
        #    #data[name] = data.get(name, Point(name, 0, 0))
        #    data[name].raw_y = y
        #    data[name].norm_y = y / Globals.norm_2
        #    data[name].y = Globals.transform(y / Globals.norm_2)
        #else:
        #    if data.has_key(name):
        #        del data[name]
    data_f.close()
    if args.in_files_ref:
        ref_f = open(args.in_files_ref[1])
        for line in ref_f:
            if line[0] == "#" or len(strip(line)) == 0:
                continue
            fields = split(line)
            name = convert_name(fields[args.ref_column - 1])
            # Deal with piRNA cluster names
            name_parts = split(name, ":")
            if len(name_parts):
                if name_parts[0].startswith("cluster"):
                    name = name_parts[0][7:]
            #if name_parts[0].startswith("D") and len(name_parts) > 1:
            #    #print name
            #    continue
            # TODO: Optionize this
            #if name not in Colours.soma | Colours.inter:
            #    continue
            if name in data:
                y_ref = float(fields[args.column - 1])
                if y_ref >= args.min_counts:
                    #data[name] = data.get(name, Point(name, 0, 0))
                    data[name].raw_y /= y_ref
                    data[name].norm_y /= (y_ref / Globals.norm_2_ref)
                    data[name].y = Globals.transform(data[name].norm_y)
                else:
                    del data[name]
        ref_f.close()

    for point in data.values():
        point.set_colour()

    x_values = [point.norm_x for point in data.values() if point.colour not in ["none", "white"]]
    x_min = min(x_values)
    x_max = max(x_values)
    x_span = x_max - x_min

    y_values = [point.norm_y for point in data.values() if point.colour not in ["none", "white"]]
    y_min = min(y_values)
    y_max = max(y_values)
    y_span = y_max - y_min

    Globals.left = int(Globals.transform(x_min) * args.x_scale - 0.5)
    Globals.right = int(Globals.transform(x_max) * args.x_scale + 0.5) + 0.5
    Globals.top = max([
        Globals.transform(y_max) * args.y_scale + 0.5,
        0.5])
    Globals.bottom = min([
        -0.5,
        Globals.transform(y_min) * args.y_scale - 0.5])
    below_space = 1
    #below_space = 0
    #
    header = """
\\documentclass{article}
\\usepackage{tikz}
\\usepackage[final]{pdfcomment}
\\usepackage[active,pdftex,tightpage]{preview}
\\usetikzlibrary{shapes,shapes.geometric}
\\usetikzlibrary{patterns}
\\usepackage{xcolor}

\\begin{document}

%%\\colorlet{green}{black!50!green}
%%\\colorlet{cyan}{black!25!cyan}
%%\\colorlet{red}{black!25!red}
%%\\colorlet{orange}{black!25!orange}

\\begin{preview}
\\begin{tikzpicture}[scale=%s]\n""" % Options.globalscale

    #x_axis = "\n\\draw[->] (%s,%s) -- coordinate (x axis mid) (%s,%s);" % (Globals.left - 1, Globals.bottom - 1, Globals.right, Globals.bottom - 1)
    x_ticks = ""
    start = trunc(Globals.transform(x_min))
    tr_x_max = Globals.transform(x_max)
    stop1 = trunc(tr_x_max)
    stop2 = fill(tr_x_max)
    #x_ticks += "\n\\draw[very thick,red,yshift=%scm] (%.5f,-2pt) -- (%.5f,2pt) node[anchor=north,font=\\tiny] {%s};" % (Globals.bottom - 1, stop1, stop1, Globals.tick(stop1))
    #x_ticks += "\n\\draw[very thick,red,yshift=%scm] (%.5f,-2pt) -- (%.5f,2pt) node[anchor=north,font=\\tiny] {%s};" % (Globals.bottom - 1, stop2, stop2, Globals.tick(stop2))
    #x_ticks += "\n\\draw[green,yshift=%scm] (%.5f,-2pt) -- (%.5f,2pt) node[anchor=north,font=\\tiny] {%s};" % (Globals.bottom - 1, tr_x_max, tr_x_max, Globals.tick(tr_x_max))
    diff1 = x_max - stop1
    diff2 = stop2 - x_max
    if diff1 > diff2:
        stop = stop2
    else:
        stop = stop1
    try:
        step = 10 ** floor(log10(stop - start))
    except ValueError:
        sys.stderr.write("It seems that the scaling factor you chose (%s) "
        "is not appropriate. Try something smaller.\n" % args.transform)
        return 1
    #i = floor((stop + step) / step) * step
    #i = ceil((stop - step) / step) * step
    i = stop
    while i - 2 * step < start:
        step = step / 2.0
    while i > start:
        #x_ticks += "\n\\draw[yshift=%scm] (%.5f,-1pt) -- (%.5f,1pt) node[anchor=north,font=\\tiny] {\\textsf{%s}};" % (Globals.bottom - 1, i, i, Globals.xtick(i))
        x_ticks += "\n\\draw[yshift=%scm] (%.5f,-1pt) -- (%.5f,1pt) node[anchor=north,font=\\%s] {\\textsf{%s}};" % (Globals.bottom - 1, i, i, args.tick_font, Globals.xtick(i))
        i -= step
    x_axis = "\n\\draw[] (%s,%s) -- coordinate (x axis mid) (%s,%s);" % (Globals.left - 1, Globals.bottom - 1, stop, Globals.bottom - 1)
    x_axis += x_ticks
    #x_axis += "\n\\node[below=%scm,font=\\footnotesize] at (x axis mid) {\\textsf{%s}};" % (below_space, re.sub("_", "\_", x_axis_legend))
    x_axis += "\n\\node[below=%scm,font=\\%s] at (x axis mid) {\\textsf{%s}};" % (below_space, args.axis_font, re.sub("_", "\_", x_axis_legend))

    #y_axis = "\n\\draw[->] (%s,%s) -- coordinate (y axis mid) (%s,%s);" % (Globals.left - 1, Globals.bottom - 1, Globals.left - 1, Globals.top)
    y_ticks = ""
    start = trunc(Globals.transform(y_min))
    stop1 = trunc(Globals.transform(y_max))
    stop2 = fill(Globals.transform(y_max))
    diff1 = y_max - stop1
    diff2 = stop2 - y_max
    if diff1 > diff2:
        stop = stop2
    else:
        stop = stop1
    step = 10 ** floor(log10(stop - start))
    #i = floor((stop + step) / step) * step
    #i = ceil((stop - step) / step) * step
    i = stop
    while i - 2 * step < start:
        step = step / 2.0
    while i > start:
        #y_ticks += "\n\\draw[xshift=%scm] (-1pt,%.5f) -- (1pt,%.5f) node[anchor=east,font=\\tiny] {\\textsf{%s}};" % (Globals.left - 1, i, i, Globals.ytick(i))
        y_ticks += "\n\\draw[xshift=%scm] (-1pt,%.5f) -- (1pt,%.5f) node[anchor=east,font=\\%s] {\\textsf{%s}};" % (Globals.left - 1, i, i, args.tick_font, Globals.ytick(i))
        i -= step
    y_axis = "\n\\draw[] (%s,%s) -- coordinate (y axis mid) (%s,%s);" % (Globals.left - 1, Globals.bottom - 1, Globals.left - 1, stop)
    y_axis += y_ticks
    #y_axis += "\n\\node[rotate=90,above=1cm,font=\\footnotesize] at (y axis mid) {\\textsf{%s}};" % (re.sub("_", "\_", y_axis_legend))
    y_axis += "\n\\node[rotate=90,above=1cm,font=\\%s] at (y axis mid) {\\textsf{%s}};" % (args.axis_font, re.sub("_", "\_", y_axis_legend))
    footer = """

\\end{tikzpicture}
\\end{preview}

\\end{document}
"""
    coords_f = open(args.plot + "_coords.xls", "w")
    coords_f.write("#ref\t%s\t%s\n" % (name_1, name_2))
    tikz_f = open(args.plot, "w")
    tikz_f.write(header)

    norm_x_values = [point.norm_x for point in data.values() if point.colour not in ["none", "white"]]
    norm_y_values = [point.norm_y for point in data.values() if point.colour not in ["none", "white"]]
    norm_x_min = min(norm_x_values)
    norm_x_max = max(norm_x_values)
    norm_y_min = min(norm_y_values)
    norm_y_max = max(norm_y_values)
    raw_x_values = [point.raw_x for point in data.values() if point.colour not in ["none", "white"]]
    raw_y_values = [point.raw_y for point in data.values() if point.colour not in ["none", "white"]]
    raw_x_min = min(raw_x_values)
    raw_x_max = max(raw_x_values)
    raw_y_min = min(raw_y_values)
    raw_y_max = max(raw_y_values)
    #slope, intercept, r_value, p_value, std_err = stats.linregress(raw_x_values, raw_y_values)
    params = np.linalg.lstsq(np.array([raw_x_values, np.zeros(len(raw_x_values))]).T, raw_y_values)[0]
    slope_forced = params[0]
    intercept_forced = params[1]
    slope = slope_forced
    # Pearson's correlation coefficient
    # http://www.statsoft.com/textbook/glosp.html#Pearson%20Correlation
    r_value, p_value = stats.pearsonr(raw_x_values, raw_y_values)
    #assert intercept_forced == 0

    for point in data.values():
        point.set_intensity(slope)
        #point.set_colour()
    max_intensity = max([point.intensity for point in data.values()])
    for point in data.values():
        point.set_style(max_intensity)
    #tikz_f.write("\n".join([point.tikznode() if point.colour != "black" else "" for point in sorted(data.values(), key=lambda point : point.intensity)]))
    #tikz_f.write("\n".join([point.tikznode() for point in sorted(data.values(), key=lambda point: point.intensity)]))
    # Sort points by intensity
    #for point in sorted(data.values(), key=attrgetter("intensity")):
    # Sort points by colour
    for point in sorted(data.values(), key=attrgetter("colour"), reverse=True):
        tikz_f.write("%s\n" % point.tikznode())
        coords_f.write("%s\t%s\t%s\n" % (point.name, point.norm_x, point.norm_y))

    bot = min(norm_x_min, norm_y_min)
    top = max(norm_x_max, norm_y_max)
    if Options.draw_xequalsy:
        #bot = min(raw_x_min, raw_y_min)
        #top = max(raw_x_max, raw_y_max)
        #tikz_f.write("\n\\draw (%.5f,%.5f) -- (%.5f,%.5f);" % (Globals.transform(bot / Globals.norm_1) * args.x_scale, Globals.transform(bot / Globals.norm_2) * args.y_scale, Globals.transform(top / Globals.norm_1) * args.x_scale, Globals.transform(top / Globals.norm_2) * args.y_scale))
        tikz_f.write("\n\\draw (%.5f,%.5f) -- (%.5f,%.5f);" % (Globals.transform(bot) * args.x_scale, Globals.transform(bot) * args.y_scale, Globals.transform(top) * args.x_scale, Globals.transform(top) * args.y_scale))
    #min_x = max(raw_x_min, float(args.min_counts - intercept) / slope, args.min_counts)
    #x2 = max(raw_x_min, float(20 * args.min_counts - intercept) / slope, 20 * args.min_counts)
    #x3 = max(raw_x_min, float(100 * args.min_counts - intercept) / slope, 100 * args.min_counts)
    if Options.print_regress:
        if (args.in_files_ref):
            tikz_f.write("\n\\draw[dotted, red] (%.5f,%.5f) -- (%.5f,%.5f);" % (Globals.transform(raw_x_min / Globals.norm_1) * args.x_scale, Globals.transform(slope_forced * raw_x_min / Globals.norm_2) * args.y_scale, Globals.transform(raw_x_max / Globals.norm_1) * args.x_scale, Globals.transform(slope_forced * raw_x_max / Globals.norm_2) * args.y_scale))
        #tikz_f.write("\n\\node[anchor=north west,font=\\footnotesize,align=left] (r_value) at (%.5f,%.5f) {\\textsf{Pearson's correlation}\\\\{}\\textsf{coefficient: $%.5f$}};" % (Globals.left, Globals.transform(top) * args.y_scale, r_value))
        tikz_f.write("\n\\node[anchor=north west,font=\\%s,align=left] (r_value) at (%.5f,%.5f) {\\textsf{Pearson's correlation}\\\\{}\\textsf{coefficient: $%.5f$}};" % (args.axis_font, Globals.left, Globals.transform(top) * args.y_scale, r_value))
    #tikz_f.write("\n\\draw[dotted, green] (%.5f,%.5f) -- (%.5f,%.5f);" % (Globals.transform(min_x / Globals.norm_1) * args.x_scale, Globals.transform((intercept + (slope * min_x)) / Globals.norm_2) * args.y_scale, Globals.transform(raw_x_max / Globals.norm_1) * args.x_scale, Globals.transform((intercept + (slope * raw_x_max)) / Globals.norm_2) * args.y_scale))
    #x_coord = min_x / Globals.norm_1
    #y_coord = (intercept + (min_x * slope)) / Globals.norm_2
    #x2_coord = x2 / Globals.norm_1
    #y2_coord = (intercept + (x2 * slope)) / Globals.norm_2
    #x3_coord = x3 / Globals.norm_1
    #y3_coord = (intercept + (x3 * slope)) / Globals.norm_2
    #x_coord_2 = raw_x_max / Globals.norm_1
    #y_coord_2 = (intercept + (raw_x_max * slope)) / Globals.norm_2
    #tikz_f.write("\n\\node[blue,circle,draw] at (%.5f,%.5f) {%.5f,%.5f};" % (Globals.transform(x_coord) * args.x_scale, Globals.transform(y_coord) * args.y_scale, x_coord, y_coord))
    #tikz_f.write("\n\\node[red,circle,draw] at (%.5f,%.5f) {%.5f,%.5f};" % (Globals.transform(x2_coord) * args.x_scale, Globals.transform(y2_coord) * args.y_scale, x2_coord, y2_coord))
    #tikz_f.write("\n\\node[red,circle,draw] at (%.5f,%.5f) {%.5f,%.5f};" % (Globals.transform(x3_coord) * args.x_scale, Globals.transform(y3_coord) * args.y_scale, x3_coord, y3_coord))
    #tikz_f.write("\n\\node[blue,circle,draw] at (%.5f,%.5f) {%.5f,%.5f};" % (Globals.transform(x_coord_2) * args.x_scale, Globals.transform(y_coord_2) * args.y_scale, x_coord_2, y_coord_2))
    #tikz_f.write("\n\\draw[dotted, blue] (%.5f,%.5f) -- (%.5f,%.5f);" % (Globals.transform(x_coord) * args.x_scale, Globals.transform(y_coord) * args.y_scale, Globals.transform(x_coord_2) * args.x_scale, Globals.transform(y_coord_2) * args.y_scale))

    #tikz_f.write(legend)
    tikz_f.write(x_axis)
    tikz_f.write(y_axis)
    tikz_f.write(footer)
    tikz_f.close()
    coords_f.close()

    return 0

if __name__ == "__main__":
    sys.exit(main())
