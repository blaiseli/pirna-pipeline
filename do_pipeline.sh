#!/usr/bin/env bash
# Usage: ./do_pipeline.sh <library_name> <adapter> <trim_nb> <G_species> <nb_processors> [<annotation_file>]
# raw_data/<library_name>/<library_name>.fastq should exist

# To configure according to the local installation:
genomes_path=${GENOMES}

# Library name:
name=${1}

# Adapter:
adapt=${2}

# Number of extra bases to trim on both sides:
trim=${3}

# Genome to use
genome=${4}

# Number of processors to use for some of the mappings:
nprocs=${5}

# Example:
#name="aub_RNAi"
#adapt="TCGTATGCCGTCTTCTGCTTG"
#trim="0"
#genome="D_melanogaster"
#nprocs="4"

# There is a default value for the annotation file
if [ $# = 6 ]
then
    annotation_file=${6}
else
    #annotation_file="${genome_path}/${genome}/Annotations/Flybase/${genome}_flybase_splitmRNA_annot.txt"
    annotation_file="${genome_path}/${genome}/Annotations/Flybase/${genome}_flybase_splitmRNA_annot.bed"
fi


mkdir -p "logs"
log="logs/${name}.stdout"
err="logs/${name}.stderr"

if [ ! -e ${annotation_file} ]
then
    echo "The annotation file ${annotation_file} is missing." > ${err}
    exit 1
else
    echo "Annotation file ${annotation_file} will be used."
fi

if [ ! -e ${genomes_path}/${genome}/${genome}.rev.2.bt2 ]
then
    echo "The genome bowtie2 index files ${genome_path}/${genome}/${genome}* seem to be missing."
    exit 1
else
    echo "Genome bowtie2 index files ${genome_path}/${genome}/${genome}* will be used."
fi

pipeline.sh ${name} ${adapt} ${trim} ${genome} ${nprocs} ${annotation_file} > ${log} 2> ${err}
#pipeline_small.sh ${name} ${adapt} ${trim} ${genome} ${nprocs} > ${log} 2> ${err}
