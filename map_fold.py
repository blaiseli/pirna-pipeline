#!/usr/bin/env python
# vim: set fileencoding=<utf-8> :
"""This script reads two sorted and indexed .bam files.
Using normalization values for the two libraries, it calculates the coverage
fold."""


import argparse
import sys
WRITE = sys.stdout.write
import warnings


def formatwarning(message, category, filename, lineno, line):
    """Formats warning messages."""
    return "%s:%s: %s:%s" % (filename, lineno, category.__name__, message)
warnings.formatwarning = formatwarning

import os
OPJ = os.path.join
OPB = os.path.basename

#from collections import defaultdict
from itertools import imap, izip
import pysam
import numpy as np


def pile_counter(read_range):
    """Generates a read counting function. *read_range* is a pair of values
    defining the minimum and maximum lengths for reads."""
    min_size, max_size = read_range
    min_score = Options.min_score
    if min_size <= max_size:
        if Options.max_edit_proportion == -1.0:
            # Do not filter on edit proportion
            def select(read):
                """Filter function.
                The read has to match at its first base."""
                ali = read.alignment
                #rlen = ali.rlen
                rlen = ali.query_length
                return (
                    #read.is_head
                    (min_size <= rlen <= max_size)
                    and (ali.opt("AS") >= min_score)
                    and (ali.opt("NM") <= Options.max_mm))
        else:
            def select(read):
                """Filter function.
                The read has to match at its first base."""
                ali = read.alignment
                #rlen = ali.rlen
                rlen = ali.query_length
                return (
                    #read.is_head
                    (ali.opt("NM") <= rlen * Options.max_edit_proportion)
                    and (min_size <= rlen <= max_size)
                    and (ali.opt("AS") >= min_score)
                    and (ali.opt("NM") <= Options.max_mm))
    else:
        # Don't filter by size
        if Options.max_edit_proportion == -1.0:
            # Do not filter on edit proportion
            def select(read):
                """Filter function.
                The read has to match at its first base."""
                ali = read.alignment
                return (
                    #read.is_head
                    (ali.opt("AS") >= min_score)
                    and (ali.opt("NM") <= Options.max_mm))
        else:
            def select(read):
                """Filter function.
                The read has to match at its first base."""
                ali = read.alignment
                #rlen = ali.rlen
                rlen = ali.query_length
                return (
                    #read.is_head
                    (ali.opt("NM") <= rlen * Options.max_edit_proportion)
                    and (ali.opt("AS") >= min_score)
                    and (ali.opt("NM") <= Options.max_mm))

    def read_counter(pile):
        """Generates counts for reads in a pile *pile*.
        Returns the coordinate of the pile and the read count."""
        ref_pos = pile.reference_pos
        fwd_count = 0
        rev_count = 0
        for read in pile.pileups:
            ali = read.alignment
            if select(read):
                if ali.is_reverse:
                    rev_count += 1
                else:
                    fwd_count += 1
        return ref_pos, fwd_count, rev_count
    return read_counter


def count_reads_in(bamfile, counter, ref, start=None, end=None):
    """Counts the F and R reads along a reference."""
    #Globals.debug("%s:%d-%d (%d)\n" % (ref, start, end, (1 + end - start)))
    if start is not None:
        region_length = 1 + end - start
        #piles = bamfile.pileup(ref, start, end)
    else:
        region_length = Globals.ref_lengths[ref]
        #piles = bamfile.pileup(ref, start, end)
    fwd_counts = np.zeros(region_length)
    rev_counts = np.zeros(region_length)
    for (pos, fwd_count, rev_count) in imap(
            counter, bamfile.pileup(ref, start, end)):
        fwd_counts[pos] += fwd_count
        rev_counts[pos] += rev_count
    return fwd_counts, rev_counts


def array2bedgraph(array, ref, outfile):
    """Uses the data in the one-dimension numpy array *array* to generate a
    bedgraph representation of it. Indices in the array are assumed to
    correspond to positions along *ref*.
    The function has side effects on the array: it replaces nan with 1.0
    and inf with thrice the max over non-inf."""

    # Replace 0/0 by 1.0 (assuming nan values all come from 0/0)
    are_nan = np.isnan(array)
    array[are_nan] = 1.0
    # Replace n/0 (inf) by max of non-infinite values
    #are_inf = np.isinf(array)
    #max_val = max(array[~are_inf])
    #array[are_inf] = 3 * max_val
    array = np.log(array)
    are_inf = np.isinf(array)
    are_posinf = np.isposinf(array)
    are_neginf = np.isneginf(array)
    max_val = max(array[~are_inf])
    min_val = min(array[~are_inf])
    array[are_posinf] = 3 * max_val
    array[are_neginf] = 3 * min_val

    pos = 0
    start = 0
    last_val = array[pos]
    while pos < Globals.ref_lengths[ref] - 1:
        pos += 1
        curr_val = array[pos]
        if curr_val != last_val:
            outfile.write("%s\t%d\t%d\t%f\n" % (ref, start, pos, last_val))
            start = pos
            last_val = curr_val
    if start < pos:
        # We didn't just write something
        outfile.write("%s\t%d\t%d\t%f\n" % (ref, start, pos, last_val))


#####################################
# For better traceability,          #
# options and global variables      #
# can be stored as class attributes #
#####################################
class Globals(object):
    """This object holds global variables."""
    p_value = 0.01


class Options(object):
    """This object contains the values of the global options."""
    # default value set so that pile_counter will not
    # select alignments based on edit proportion
    max_edit_proportion = None
    max_mm = None
    min_score = None
    # min and max lengths for a read to be counted
    # default value set so that pile_counter
    # will not select alignments based on read size
    read_range = None


#################
# main function #
#################
def main():
    """Main function of the program."""
    WRITE("%s\n" % " ".join(sys.argv))
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    ###################
    # Alignment input #
    ###################
    parser.add_argument(
        "-b", "--bam_files",
        nargs=2,
        required=True,
        help="Space-separated pair of sorted and indexed bam files.")
    #######################
    # Normalization input #
    #######################
    parser.add_argument(
        "-n", "--normalizations",
        nargs=2,
        required=True,
        type=argparse.FileType('r'),
        help="Space-separated pair of files containing the values used "
        "to normalize the counts in the data.\n"
        "Of course, the file order should correspond to that "
        "given in the option --bam_files.")
    ##########
    # Output #
    ##########
    parser.add_argument(
        "-f", "--fwd_bedgraph",
        required=True,
        type=argparse.FileType('w'),
        help="File in which to write the normalized fold values "
        "for forward-mapping reads along the references in bedgraph format.")
    parser.add_argument(
        "-r", "--rev_bedgraph",
        required=True,
        type=argparse.FileType('w'),
        help="File in which to write the normalized fold values "
        "for reverse-mapping reads along the references in bedgraph format.")
    ###########################
    # Options to filter reads #
    ###########################
    # default value set so that pile_counter will not
    # select alignments based on edit proportion
    parser.add_argument(
        "--max_edit_proportion",
        type=float,
        default=-1.0,
        help="Maximum proportion of edits for an alignment to be recorded."
        "\nThe default is to accept any proportion of edits.")
    parser.add_argument(
        "--max_mm",
        type=int,
        default=0,
        help="Maximum number of mismatches for an alignment to be counted.")
    parser.add_argument(
        "--min_score",
        type=int,
        default=-200,
        help="Minimum mapping score for an alignment to be recorded."
        "\nThe default value is -200.")
    # min and max lengths for a read to be counted
    # default value set so that pile_counter
    # will not select alignments based on read size
    parser.add_argument(
        "--read_range",
        nargs=2,
        type=int,
        default=[0, -1],
        help="Minimum and maximum lengths of the reads to take into account."
        "\nThe default is to take all sizes into account.")
    parser.add_argument(
        "--no_secondary",
        action="store_true",
        help="Do not count secondary alignments of reads.\n"
        "A read will only be counted once, but where it will be counted "
        "may be partially arbitrary.")
    parser.add_argument(
        "-e", "--exclude_refs",
        help="File containing a list of references "
        "to exclude from the profiling.\n"
        "One reference per line.",
        type=argparse.FileType("r"))
    parser.add_argument(
        "-v", "--verbose",
        action="store_true",
        help="Generate more output.")
    args = parser.parse_args()

    # Give global access to some options
    Options.max_edit_proportion = args.max_edit_proportion
    Options.max_mm = args.max_mm
    Options.min_score = args.min_score
    Options.read_range = tuple(args.read_range)

    if args.verbose:
        def debug(msg):
            sys.stderr.write(msg)
    else:
        def debug(msg):
            pass
    Globals.debug = staticmethod(debug)

    # To avoid repeatedly calling these as string methods:
    strip = str.rstrip
    #split = str.split
    #upper = str.upper

    counter = pile_counter(Options.read_range)

    ###############################################
    # Make the list of references we want to skip #
    ###############################################
    Globals.exclude_refs = []
    if args.exclude_refs:
        with args.exclude_refs as exclusion_file:
            for line in exclusion_file:
                ref_name = strip(line)
                if ref_name:
                    if ref_name[0] != "#":
                        Globals.exclude_refs.append(ref_name)
    Globals.exclude_refs = set(Globals.exclude_refs)

    # Read normalization factors
    norm_ref = float(strip(args.normalizations[0].readline()))
    norm_other = float(strip(args.normalizations[1].readline()))
    ref_filename = args.bam_files[0]
    other_filename = args.bam_files[1]

    with pysam.Samfile(ref_filename) as ref_file, \
            pysam.Samfile(other_filename) as other_file:
        ###############################
        # Determine reference lengths #
        ###############################
        # key: reference name
        # value: reference length
        Globals.ref_lengths = {}
        for ref, ref_len in izip(ref_file.references, ref_file.lengths):
            if ref in Globals.exclude_refs:
                # Skip references we're not interested in.
                continue
            else:
                Globals.ref_lengths[ref] = ref_len
                #Globals.ref_counts_ref[ref] = ref_file.count(ref)
        # Checks using the other file
        for ref, ref_len in izip(other_file.references, other_file.lengths):
            if ref in Globals.exclude_refs:
                # Skip references we're not interested in.
                continue
            else:
                if ref in Globals.ref_lengths:
                    assert Globals.ref_lengths[ref] == ref_len
                else:
                    Globals.ref_lengths[ref] = ref_len
                #Globals.ref_counts_other[ref] = other_file.count(ref)
        args.fwd_bedgraph.write("track type=bedGraph\n")
        args.rev_bedgraph.write("track type=bedGraph\n")
        for (ref, ref_len) in sorted(Globals.ref_lengths.items()):
            WRITE("Reference: %s (len: %d)\n" % (ref, ref_len))
            fwd_counts_ref, rev_counts_ref = count_reads_in(
                ref_file, counter, ref)
            fwd_counts_other, rev_counts_other = count_reads_in(
                other_file, counter, ref)
            # Normalize
            fwd_normal_ref = np.true_divide(fwd_counts_ref, norm_ref)
            fwd_normal_other = np.true_divide(fwd_counts_other, norm_other)
            # Calculate fold
            fwd_fold_array = fwd_normal_other / fwd_normal_ref
            # Make bedgraph
            array2bedgraph(fwd_fold_array, ref, args.fwd_bedgraph)
            # Normalize
            rev_normal_ref = np.true_divide(rev_counts_ref, norm_ref)
            rev_normal_other = np.true_divide(rev_counts_other, norm_other)
            # Calculate fold
            rev_fold_array = rev_normal_other / rev_normal_ref
            # Make bedgraph
            array2bedgraph(rev_fold_array, ref, args.rev_bedgraph)
    return 0

if __name__ == "__main__":
    sys.exit(main())
