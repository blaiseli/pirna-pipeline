#!/usr/bin/env bash
# Usage: map_on_3UTR.sh <library_name> <G_species>
# Some information helping understanding the script might be found in 00README.txt

# To configure according to the local installation:
genomes_path=${GENOMES}

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}


# library name
name=${1}
echo $name

genome=${2}
echo ${genome}

# file containing the counts used to perform normalizations
#norm_file=${2}
#norm=`cat ${norm_file}`

#ncpu=${3}
# maximum read size
# If this environment variable is not defined, use 30 as default value
[ MAX_LEN ] || MAX_LEN="30"


seq_dir="${name}/mapped_reads_${genome}"


## mapping the unmapped, low quality, mapped but not annotated, and mapped on piRNA clusters, siRNA clusters (for Drosophila ovaries), transposable elements and 3'UTRs
# Now just mapping "piRNA candidates"
# on 3'UTR sequences (see /extra/Genomes/D_melanogaster/Annotations/Annotations_Chambeyron/README_Annotations_Chambeyron.txt).
#mkdir -p mapped_3UTR

picandidates=${seq_dir}/${name}_23-${MAX_LEN}_on_${genome}_piRNA_candidate.fastq.gz
if [ -e ${piuniques%.gz} ]
then
    # Better use already uncompressed version.
    piuniques=${piuniques%.gz}
fi

piuniques=${seq_dir}/${name}_23-${MAX_LEN}_on_${genome}_piRNA_unique.fastq.gz
if [ -e ${piuniques%.gz} ]
then
    # Better use already uncompressed version.
    piuniques=${piuniques%.gz}
fi

sicandidates=${seq_dir}/${name}_21_on_${genome}_siRNA_candidate.fastq.gz
if [ -e ${sicandidates%.gz} ]
then
    # Better use already uncompressed version.
    sicandidates=${sicandidates%.gz}
fi

siuniques=${seq_dir}/${name}_21_on_${genome}_siRNA_unique.fastq.gz
if [ -e ${siuniques%.gz} ]
then
    # Better use already uncompressed version.
    siuniques=${siuniques%.gz}
fi

# Depending on their definition in the annotation module,
# the above piRNA and siRNA candidates may be subsets of pi-si-TE-3UTR.
# The whole set with sizes from 18 to ${MAX_LEN}:
pisiTE3UTR=${seq_dir}/${name}_18-${MAX_LEN}_on_${genome}_pi-si-TE-3UTR.fastq

refs="${genomes_path}/${genome}/Annotations/Flybase/3UTR_named"

echo -e "\nMapping piRNA candidates on 3'UTRs."
map_loosely_on_refs.sh ${name} ${name} ${picandidates} "piRNA_candidate" ${refs} "3UTR"

echo -e "\nMapping 23-${MAX_LEN} unique mappers on 3'UTRs."
map_loosely_on_refs.sh ${name} ${name} ${piuniques} "piRNA_unique" ${refs} "3UTR"

echo -e "\nMapping siRNA candidates on 3'UTRs."
map_loosely_on_refs.sh ${name} ${name} ${sicandidates} "siRNA_candidate" ${refs} "3UTR"

echo -e "\nMapping 21-nt unique mappers on 3'UTRs."
map_loosely_on_refs.sh ${name} ${name} ${siuniques} "siRNA_unique" ${refs} "3UTR"

echo -e "\nMapping pi-si-TE-3UTR on 3'UTRs."
map_loosely_on_refs.sh ${name} ${name} ${pisiTE3UTR} "pi-si-TE-3UTR" ${refs} "3UTR"


## extracting reads and building size histograms
#
## Will contain the sequences that mapped, in distinct files for distinct annotation groups
#mkdir -p mapped_reads_3UTR/${name}
#seq_dir=mapped_reads_3UTR/${name}
## Will contain size histogram data for the various annotation groups
#mkdir -p histos_mapped_3UTR/${name}
#histo_dir=histos_mapped_3UTR/${name}
#
#echo "read_sam.py --min_score "-10" --in_file mapped_3UTR/${name}_piRNA_candidate_on_3UTR.sam --seq_dir ${seq_dir} --hist_dir ${histo_dir}"
#read_sam.py --min_score "-10" --in_file mapped_3UTR/${name}_piRNA_candidate_on_3UTR.sam --seq_dir ${seq_dir} --hist_dir ${histo_dir} || error_exit "read_sam.py failed"
#
#echo "read_sam.py --min_score "-10" --in_file mapped_3UTR/${name}_siRNA_candidate_on_3UTR.sam --seq_dir ${seq_dir} --hist_dir ${histo_dir}"
#read_sam.py --min_score "-10" --in_file mapped_3UTR/${name}_siRNA_candidate_on_3UTR.sam --seq_dir ${seq_dir} --hist_dir ${histo_dir} || error_exit "read_sam.py failed"
#
#echo "read_sam.py --min_score "-10" --in_file mapped_3UTR/${name}_pi-si-TE-3UTR_on_3UTR.sam --seq_dir ${seq_dir} --hist_dir ${histo_dir}"
#read_sam.py --min_score "-10" --in_file mapped_3UTR/${name}_pi-si-TE-3UTR_on_3UTR.sam --seq_dir ${seq_dir} --hist_dir ${histo_dir} || error_exit "read_sam.py failed"
#
exit 0
