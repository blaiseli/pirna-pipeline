#!/bin/sh
# Usage: do_map_on_3UTR.sh <library_name>
#TODO: update paths once scripts are relocated

norm_file=mapped_dm/${1}_trimmed_on_D_melanogaster_siRNA.nbseq
echo "mapping the 'bona fide' reads on the 3'UTR sequences from Flybase"
./map_on_3UTR.sh ${1} ${norm_file}
