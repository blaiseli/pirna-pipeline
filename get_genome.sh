#!/usr/bin/env bash

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}

genome="D_melanogaster"
version="r5"
genome_dir="${GENOMES}/${genome}_${version}"

mkdir -p ${genome_dir}/Annotations/Flybase
ln -sf ${genome_dir} ${GENOMES}/${genome}
cd ${genome_dir}

case "${version}" in
"r5")
    get_cmd="wget --continue http://hgdownload.soe.ucsc.edu/goldenPath/dm3/bigZips/chromFa.tar.gz"
    extract_cmd="tar -xvzf chromFa.tar.gz"
    fb_version="r5.9"
    fb_date="FB2008_06"
    ;;
"r6")
    get_cmd="wget --continue ftp://ftp.flybase.net/genomes/Drosophila_melanogaster/current/fasta/dmel-all-chromosome-r6.03.fasta.gz"
    extract_cmd="gunzip dmel-all-chromosome-r6.03.fasta.gz"
    #get_cmd="http://hgdownload.cse.ucsc.edu/goldenPath/dm6/bigZips/dm6.fa.gz"
    fb_version="r6.03"
    fb_date="FB2014_06"
    ;;
*)
    error_exit "Unexpected version for ${genome} genome."
esac

echo ${get_cmd}
eval ${get_cmd} || error_exit "${genome} genome download failed"

echo ${extract_cmd}
eval ${extract_cmd} || error_exit "${genome} genome extraction failed"


# TODO: Reconsider this suppression of Uextra chromosome: there are some annotations, but no TE. 
# Exclude Uextra because no annotations are present in this "chromosome", resulting in a crash in annotation programs. 
fasta_files=`python -c "import os; fasta_files = [f for f in os.listdir('.') if f.split('.')[-1] in set(['fa', 'fas', 'fasta'])]; print ' '.join(sorted([f for f in fasta_files if f != 'chrUextra.fa']))"`
cat ${fasta_files} > ${genome}.fa || error_exit "${genome} genome concatenation failed"

#fasta_files=`python -c "import os; fasta_files = [f for f in os.listdir('.') if f.split('.')[-1] in set(['fa', 'fas', 'fasta'])]; print ','.join(sorted([f for f in fasta_files if f != 'chrUextra.fa']))"`
#
#bowtie2-build --seed 123 --packed ${fasta_files} ${genome} || error_exit "${genome} genome bowtie2 indexing failed"
bowtie2-build --seed 123 --packed ${genome}.fa ${genome} || error_exit "${genome} genome bowtie2 indexing failed"
crac-index index ${genome} ${genome}.fa || error_exit "${genome} genome crac indexing failed"


cd ${genome_dir}/Annotations/Flybase
#wget ftp://ftp.flybase.net/genomes/Drosophila_melanogaster/dmel_${fb_version}_${fb_date}/fasta/md5sum.txt

for annot_type in \
    "dmel-all-CDS" \
    "dmel-all-five_prime_UTR" \
    "dmel-all-miRNA" \
    "dmel-all-miscRNA" \
    "dmel-all-ncRNA" \
    "dmel-all-three_prime_UTR" \
    "dmel-all-transcript" \
    "dmel-all-transposon" \
    "dmel-all-tRNA" \
    "dmel-dmel_mitochondrion_genome-CDS" \
    "dmel-dmel_mitochondrion_genome-miscRNA" \
    "dmel-dmel_mitochondrion_genome-tRNA" \
    "dmel-dmel_mitochondrion_genome-transcript"
do
    wget_fastagz_and_format.sh ftp://ftp.flybase.net/genomes/Drosophila_melanogaster/dmel_${fb_version}_${fb_date}/fasta/${annot_type}-${fb_version}.fasta.gz `annot_type2name.py ${annot_type}` || error_exit "Annotation downloading and formatting failed"
done

get_extra_annotations.sh || error_exit "get_extra_annotations.sh failed"

# Canonical transposons have been installed by get_extra_annotations.sh and can now be used to make an extended genome index.
#bowtie2-build --seed 123 --packed ${fasta_files},Annotations/TE_plus.fa ${genome}_and_TE || error_exit "${genome}_and_TE extended genome bowtie2 indexing failed"
#bowtie2-build --seed 123 --packed ${fasta_files},Annotations/TE_extended.fa ${genome}_and_TE || error_exit "${genome}_and_TE extended genome bowtie2 indexing failed"
bowtie2-build --seed 123 --packed  ${genome}.fa,Annotations/TE_extended.fa ${genome}_and_TE || error_exit "${genome}_and_TE extended genome bowtie2 indexing failed"
crac-index index ${genome}_and_TE ${genome}.fa Annotations/TE_extended.fa || error_exit "${genome}_and_TE extended genome crac indexing failed"

exit 0
