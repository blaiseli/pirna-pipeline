#!/usr/bin/env bash
# Usage: get_pi_cluster_pp_counts.sh <library> <nb_slices> <cluster_num> <pp/nopp>

lib=${1}

nb_slices=${2}

num=${3}

pp_type=${4}

# Print header
echo -e "#ref\ttotal\ttotal_F\ttotal_R\tF_0mm\tR_0mm\tF_mm\tR_mm"
# Print lines corresponding to each slices
awk -F ":" -v num=${num} '$1 == "cluster"num {print}' ${lib}/ping_pong_${nb_slices}_slices/global/histos_${pp_type}_*_on_pi_clusters/${lib}_${pp_type}_*_on_pi_clusters_ref_counts.txt | awk '{print $1,"\t"$2"\t"$3"\t"$4"\t"$4"\t"$5"\t"$6"\t"$7}'

exit 0
