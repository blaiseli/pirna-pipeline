#!/usr/bin/env bash
# Usage: map_and_annotate_small.sh <library_name> <G_species> [<annotation_file>]

# To configure according to the local installation:
genomes_path=${GENOMES}

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}

function make_bam_index
{
    if [ ! -e ${1%.sam}_sorted.bam -o ! -e ${1%.sam}_sorted.bam.bai ]
    then
        nice -n 19 ionice -c2 -n7 sam2indexedbam.sh ${1} || error_exit "sam2indexedbam.sh failed"  
    fi
    if [ ! -e ${1%.sam}.bed ]
    then
         nice -n 19 ionice -c2 -n7 bedtools bamtobed -ed -i ${1%.sam}_sorted.bam > ${1%.sam}.bed || error_exit "bedtools bamtobed failed"
    fi
}


# library name
name=${1}
echo "Library: ${name}"

genome=${2}
echo "Genome: ${genome}"
#genome_db=${genomes_path}/${genome}/${genome}
genome_db="${genomes_path}/${genome}/${genome}_and_TE"
#annotation_file="${genomes_path}/${genome}/Annotations/${genome}_annot.txt"
#annotation_file="${genomes_path}/D_melanogaster/Annotations/Flybase/D_melanogaster_flybase_annot.txt"
#annotation_file="${genomes_path}/D_melanogaster/Annotations/Flybase/D_melanogaster_flybase_splitmRNA_annot.txt"
if [ $# = 3 ]
then
    annotation_file=${3}
else
    # This .bed file must have been compressed with bgzip and indexed with tabix
    # In bli@sei-lot:/extra/Genomes/D_melanogaster_r5/Annotations/Flybase:
    #build_annotated_genome.py --annotations \
    #    3UTR.txt,5UTR.txt,CDS.txt,miRNA.txt,miscRNA.txt,ncRNA.txt,transposable_elements.txt,tRNA.txt \
    #    --genome D_melanogaster_flybase_splitmRNA_and_TE --pi_clusters \
    #    ../Annotations_Chambeyron/pi_clusters_oriented.fa --si_clusters \
    #    ../Annotations_Chambeyron/ovary_si_clusters.txt --fasta_TE \
    #    ../canonical_TE.fa
    #bgzip -c D_melanogaster_flybase_splitmRNA_and_TE_annot.bed > D_melanogaster_flybase_splitmRNA_and_TE_annot.bed.bgz
    #tabix -p bed D_melanogaster_flybase_splitmRNA_and_TE_annot.bed.bgz
    annotation_file="${genomes_path}/${genome}/Annotations/Flybase/${genome}_flybase_splitmRNA_and_TE_annot.bed"
fi

echo "Annotation file: ${annotation_file}"
# maximum read size
# If this environment variable is not defined, use 30 as default value
[ MAX_LEN ] || MAX_LEN="30"

# TODO: this is not used any more with the tabix annotation system -> re-implement ?
#exclude_types="mRNA,ncRNA,CDS,five_prime_untranslated_region"
exclude_types="None"
echo "Types excluded from annotation: ${exclude_types}"

# http://stackoverflow.com/questions/59895/can-a-bash-script-tell-what-directory-its-stored-in
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
annot_module="${DIR}/annotation_piRNA.py"
echo "Module used to define annotation priorities: ${annot_module}"

#ncpu=${3}

fastq_in="${name}/trimmed/${name}_18-${MAX_LEN}.fastq.gz"
if [ ! -e ${fastq_in} ]
then
    echo -e "\nThis script requires that the sequences have already been trimmed and size-filtered and are present in ${fastq_in}"
    echo "(Use trim_small.sh to obtain such a file from raw fastq file.)"
    #echo "This script requires a file containing the number to use to normalize the read counts.\n(This could be resulting from the use of map_and_count_unique.sh)"
fi

#########################
# mapping on the genome #
#########################

# Will contain the .sam mapping result
map_dir="${name}/mapped_${genome}"
mkdir -p ${map_dir}
# Write them in the mapped_${genome} result while processing the .sam file
## Will contain the sequences that did not map
#mkdir -p unmapped_${genome}/${name}

sam_file="${map_dir}/${name}_18-${MAX_LEN}_on_${genome}.sam"
if [ ! -e ${sam_file} ]
then 
    if [ -e ${sam_file}.bz2 ]
    then
        echo -e "\nUncompressing existing ${sam_file}.bz2"
        echo "Be sure there is enough disk space for that."
        cmd="lbunzip2 -k ${sam_file}.bz2"
        echo ${cmd}
        eval ${cmd} || error_exit "${cmd} failed"
    else
        echo -e "\nMapping ${fastq_in} on ${genome_db} with bowtie2"
        cmd="bowtie2 --seed 123 -t -k 10 -L 6 -i S,1,0.8 -N 1 --mp 4,2 --rdg 5,2 --rfg 5,2"
        #echo "bowtie2 --seed 123 -t -k 10 -L 6 -i S,1,0.8 -N 1 --mp 4,2 --rdg 5,2 --rfg 5,2 -x ${genomes_path}/${genome}/${genome} -U trimmed/${name}_18-${MAX_LEN}.fastq -S mapped_${genome}/${name}_18-${MAX_LEN}_on_${genome}.sam --un unmapped_${genome}/${name}/${name}_18-${MAX_LEN}_not_on_${genome}.fastq"
        #bowtie2 --seed 123 -t -k 10 -L 6 -i S,1,0.8 -N 1 --mp 4,2 --rdg 5,2 --rfg 5,2 -x ${genomes_path}/${genome}/${genome} -U trimmed/${name}_18-${MAX_LEN}.fastq -S mapped_${genome}/${name}_18-${MAX_LEN}_on_${genome}.sam --un unmapped_${genome}/${name}/${name}_18-${MAX_LEN}_not_on_${genome}.fastq || error_exit "bowtie2 failed"
        echo "${cmd} -x ${genome_db} -U ${fastq_in} -S ${sam_file}"
        eval ${cmd} -x ${genome_db} -U ${fastq_in} -S ${sam_file} || error_exit "${cmd} failed"
    fi
else
    echo -e "\nUsing already existing ${sam_file}"
fi

# Make a sorted and indexed bam version in the background if necessary
# Log this separately to re-use later
tmpdir=$(mktemp -dt "`basename $0`.XXXXXXXXXX")
make_bam_index ${sam_file} > ${tmpdir}/indexing.log 2> ${tmpdir}/indexing.err &

#############################################################################
# Annotating the reads and generating fastq files for each annotation group #
#############################################################################

# Will contain the sequences that mapped, in distinct files for distinct annotation groups
seq_dir="${name}/mapped_reads_${genome}"
mkdir -p ${seq_dir}
unique_set="${seq_dir}/uniques.pickle"
# Will contain size histogram data for the various annotation groups
histo_dir="${name}/histos_mapped_${genome}"
mkdir -p ${histo_dir}

si_counts="${map_dir}/${name}_on_${genome}_siRNA.nbseq"
mi_counts="${map_dir}/${name}_on_${genome}_miRNA.nbseq"
si_candidates_counts="${map_dir}/${name}_on_${genome}_siRNA_candidate.nbseq"
pi_candidates_counts="${map_dir}/${name}_on_${genome}_piRNA_candidate.nbseq"
si_uniques_counts="${map_dir}/${name}_on_${genome}_siRNA_unique.nbseq"
pi_uniques_counts="${map_dir}/${name}_on_${genome}_piRNA_unique.nbseq"
# TODO: plot this histogram
pisiTE3UTR_histo="${histo_dir}/${name}_18-${MAX_LEN}_on_${genome}_group_pi-si-TE-3UTR_lengths.txt"

#if [ ! -e ${si_counts} -o ! -e ${mi_counts} -o ! -e ${si_candidates_counts} -o ! -e ${pi_candidates_counts} -o ! -e ${si_uniques_counts} -o ! -e ${pi_uniques_counts} -o ! -e ${pisiTE3UTR_histo} -o ! -e ${unique_set} ]
if [ ! -e ${unique_set} -o ! -e ${pisiTE3UTR_histo} -o ! -e ${si_counts} -o ! -e ${mi_counts} -o ! -e ${si_candidates_counts} -o ! -e ${pi_candidates_counts} -o ! -e ${si_uniques_counts} -o ! -e ${pi_uniques_counts} ]
then
    echo -e "\nProcessing results from ${sam_file}"
    #--min_score -10 was chosen in order to filter out alignments with too many mismatches.
    # We had to use this proxy because filtering on the number of mismatches
    # sometimes excluded the primary alignment.
    # Also, the score is in theory a more suitable criterion than the number of mismatches,
    # because it takes into account read quality.
    #TODO: Better deal with exclude_types (take from file ?)
    cmd="annotate_sam.py --in_file ${sam_file} --annot ${annotation_file} --annot_module ${annot_module} --exclude_types ${exclude_types} --min_score "-10" --pi_range 23,${MAX_LEN} --seq_dir ${seq_dir} --histo_dir ${histo_dir} --unique_set_file ${unique_set}"
    echo ${cmd}
    time eval ${cmd} || error_exit "${cmd} failed"

    # To normalize by number of siRNA (annotated on siRNA ovary clusters, size 21)
    echo -e "\nCounting siRNA reads for library ${name}"
    # length 21 and siRNA_cluster in priority annots
    mv ${seq_dir}/${name}_18-${MAX_LEN}_on_${genome}_siRNA.fasta ${seq_dir}/${name}_21_on_${genome}_siRNA.fasta
    mv ${seq_dir}/${name}_18-${MAX_LEN}_on_${genome}_siRNA.fastq ${seq_dir}/${name}_21_on_${genome}_siRNA.fastq
    nb_si=`grep -c "^>" ${seq_dir}/${name}_21_on_${genome}_siRNA.fasta`
    pigz ${seq_dir}/${name}_21_on_${genome}_siRNA.fasta
    pigz ${seq_dir}/${name}_21_on_${genome}_siRNA.fastq
    # length 21 and siRNA_cluster in their annot_string (all annotations)
    grep "siRNA_cluster" ${seq_dir}/${name}_18-${MAX_LEN}_on_${genome}_pi-si-TE-3UTR.fasta | grep -c "(len=21)" > ${si_counts}
    counts=`cat ${si_counts}`
    echo "The counts (${counts}) are written in ${si_counts}"
    nb_si_bis=`cat ${si_counts}`
    if [ ! ${nb_si} -eq ${nb_si_bis} ]
    then
        echo "${nb_si} reads in ${seq_dir}/${name}_21_on_${genome}_siRNA.fastq but ${nb_si_bis} siRNA reads counted in ${seq_dir}/${name}_18-${MAX_LEN}_on_${genome}_pi-si-TE-3UTR.fasta" >&2
        #echo "The second may include reads that were also annotated in a prioritary category." >&2
    fi

    # And to normalize by number of (pre-)miRNA
    echo -e "\nCounting miRNA reads for library $name"
    # All present sequences headers seem to match the grep
    grep -c "^>.*miRNA.*$" ${seq_dir}/${name}_18-${MAX_LEN}_on_${genome}_\(pre_\)miRNA.fasta > ${mi_counts}
    #pigz ${seq_dir}/${name}_18-${MAX_LEN}_on_${genome}_\(pre_\)miRNA.fasta
    counts=`cat ${mi_counts}`
    echo "The counts (${counts}) are written in ${mi_counts}"

    # Counting the number of siRNA candidates
    echo -e "\nCounting siRNA candidates for library ${name}"
    mv ${seq_dir}/${name}_18-${MAX_LEN}_on_${genome}_siRNA_candidate.fasta ${seq_dir}/${name}_21_on_${genome}_siRNA_candidate.fasta
    mv ${seq_dir}/${name}_18-${MAX_LEN}_on_${genome}_siRNA_candidate.fastq ${seq_dir}/${name}_21_on_${genome}_siRNA_candidate.fastq
    grep -c "^>" ${seq_dir}/${name}_21_on_${genome}_siRNA_candidate.fasta > ${si_candidates_counts}
    pigz ${seq_dir}/${name}_21_on_${genome}_siRNA_candidate.fasta
    pigz ${seq_dir}/${name}_21_on_${genome}_siRNA_candidate.fastq
    counts=`cat ${si_candidates_counts}`
    echo "The counts (${counts}) are written in ${si_candidates_counts}"

    # Counting the number of piRNA candidates
    echo -e "\nCounting piRNA candidates for library ${name}"
    mv ${seq_dir}/${name}_18-${MAX_LEN}_on_${genome}_piRNA_candidate.fasta ${seq_dir}/${name}_23-${MAX_LEN}_on_${genome}_piRNA_candidate.fasta
    mv ${seq_dir}/${name}_18-${MAX_LEN}_on_${genome}_piRNA_candidate.fastq ${seq_dir}/${name}_23-${MAX_LEN}_on_${genome}_piRNA_candidate.fastq
    grep -c "^>" ${seq_dir}/${name}_23-${MAX_LEN}_on_${genome}_piRNA_candidate.fasta > ${pi_candidates_counts}
    pigz ${seq_dir}/${name}_23-${MAX_LEN}_on_${genome}_piRNA_candidate.fasta
    pigz ${seq_dir}/${name}_23-${MAX_LEN}_on_${genome}_piRNA_candidate.fastq
    counts=`cat ${pi_candidates_counts}`
    echo "The counts (${counts}) are written in ${pi_candidates_counts}"

    # Counting the number of unique mappers of size 21
    echo -e "\nCounting uniquely mapping 21-nt reads for library ${name}"
    mv ${seq_dir}/${name}_18-${MAX_LEN}_on_${genome}_siRNA_unique.fasta ${seq_dir}/${name}_21_on_${genome}_siRNA_unique.fasta
    mv ${seq_dir}/${name}_18-${MAX_LEN}_on_${genome}_siRNA_unique.fastq ${seq_dir}/${name}_21_on_${genome}_siRNA_unique.fastq
    grep -c "^>" ${seq_dir}/${name}_21_on_${genome}_siRNA_unique.fasta > ${si_uniques_counts}
    pigz ${seq_dir}/${name}_21_on_${genome}_siRNA_unique.fasta
    pigz ${seq_dir}/${name}_21_on_${genome}_siRNA_unique.fastq
    counts=`cat ${si_uniques_counts}`
    echo "The counts (${counts}) are written in ${si_uniques_counts}"

    # Counting the number of unique mappers of size 23-${MAX_LEN}
    echo -e "\nCounting uniquely mapping 23-${MAX_LEN} reads for library ${name}"
    mv ${seq_dir}/${name}_18-${MAX_LEN}_on_${genome}_piRNA_unique.fasta ${seq_dir}/${name}_23-${MAX_LEN}_on_${genome}_piRNA_unique.fasta
    mv ${seq_dir}/${name}_18-${MAX_LEN}_on_${genome}_piRNA_unique.fastq ${seq_dir}/${name}_23-${MAX_LEN}_on_${genome}_piRNA_unique.fastq
    grep -c "^>" ${seq_dir}/${name}_23-${MAX_LEN}_on_${genome}_piRNA_unique.fasta > ${pi_uniques_counts}
    pigz ${seq_dir}/${name}_23-${MAX_LEN}_on_${genome}_piRNA_unique.fasta
    pigz ${seq_dir}/${name}_23-${MAX_LEN}_on_${genome}_piRNA_unique.fastq
    counts=`cat ${pi_uniques_counts}`
    echo "The counts (${counts}) are written in ${pi_uniques_counts}"
else
    echo -e "\nResults in ${sam_file} have already been processed."
    echo -e "\nCounting siRNA reads for library ${name}"
    counts=`cat ${si_counts}`
    echo "The counts (${counts}) are written in ${si_counts}"
    echo -e "\nCounting miRNA reads for library $name"
    counts=`cat ${mi_counts}`
    echo "The counts (${counts}) are written in ${mi_counts}"
    echo -e "\nCounting siRNA candidates for library ${name}"
    counts=`cat ${si_candidates_counts}`
    echo "The counts (${counts}) are written in ${si_candidates_counts}"
    echo -e "\nCounting piRNA candidates for library ${name}"
    counts=`cat ${pi_candidates_counts}`
    echo "The counts (${counts}) are written in ${pi_candidates_counts}"
    echo -e "\nCounting uniquely mapping 21-nt reads for library ${name}"
    counts=`cat ${si_uniques_counts}`
    echo "The counts (${counts}) are written in ${si_uniques_counts}"
    echo -e "\nCounting uniquely mapping 23-${MAX_LEN} reads for library ${name}"
    counts=`cat ${pi_uniques_counts}`
    echo "The counts (${counts}) are written in ${pi_uniques_counts}"
fi

# Wait for the end of map file indexing before saving space
echo -e "\nWaiting for ${sam_file} indexation to complete."
wait
# cat the indexing logs to stdout and stderr
cat ${tmpdir}/indexing.log
(1>&2 cat ${tmpdir}/indexing.err)
rm -rf ${tmpdir}

# Saving space
if [ ! -e ${sam_file}.bz2 ]
then
    echo -e "\nCompressing ${sam_file} with lbzip2"
    cmd="lbzip2 ${sam_file}"
    echo ${cmd}
    eval ${cmd} || error_exit "${cmd} failed"
else
        echo -e "\nRemoving ${sam_file} file, of which a compressed ${sam_file}.bz2 version exists"
        rm -f ${sam_file}
fi

exit 0
