#!/usr/bin/env python
# vim: set fileencoding=<utf-8> :
"""This script reads data from 3-columns tab-separated text files, as generated
by Christophe Antoniewski's signature.py script. It then plots the third column
(overlap probability) as a function of the first (overlap between reads)."""


import argparse
import sys
WRITE = sys.stdout.write

# To avoid repeatedly calling these as string methods:
from string import split, strip
#from re import sub

import warnings


def formatwarning(message, category, filename, lineno, line):
    """Used to format warning messages."""
    return "%s:%s: %s: %s\n" % (filename, lineno, category.__name__, message)
warnings.formatwarning = formatwarning

import os
OPJ = os.path.join
OPB = os.path.basename

from itertools import cycle, izip
import matplotlib as mpl
# To be able to run the script without a defined $DISPLAY
mpl.use("PDF")
mpl.rcParams["figure.figsize"] = 4, 3
mpl.rcParams["font.sans-serif"] = [
    "Arial", "Liberation Sans", "Bitstream Vera Sans"]
mpl.rcParams["font.family"] = "sans-serif"

#import pylab
import matplotlib.pyplot as plt


def main():
    """Main function of the program."""
    WRITE("%s\n" % " ".join(sys.argv))
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        "-i", "--in_files",
        nargs="*",
        required=True,
        help="Space-separated list of *signature.xls files.\n"
        "Data in each file should consist in one header line (that will be "
        "ignored) and lines with three blank-separated fields as follows:\n"
        "overlap number_of_pairs overlap_probability.")
    parser.add_argument(
        "-l", "--lib_names",
        nargs="*",
        required=True,
        help="Space-separated list of library names corresponding to the "
        "*signature.xls files provided with the option --in_files.\n"
        "These names will be used in the legend of the plot.")
    parser.add_argument(
        "-d", "--plot_dir",
        required=True,
        help="Directory in which the plot should be written.")
    parser.add_argument(
        "-p", "--plot_name",
        required=True,
        help="Base name for the plot file.")
    parser.add_argument(
        "-c", "--colours",
        nargs="*",
        help="Space-separated list of colours to use in the plots.\n"
        "The order of the colours should correspond to the order of "
        "the files listed with the option --in_files.")
    parser.add_argument(
        "--label_size",
        type=int,
        help="Font size for axis labels",
        default=12)
        #default=24)
    parser.add_argument(
        "--legend_size",
        type=int,
        help="Font size for legend labels",
        default=9)
        #default=18)
    parser.add_argument(
        "--tick_size",
        type=int,
        help="Font size for ticks",
        default=12)
        #default=18)
    parser.add_argument(
        "-x", "--x_axis",
        help="Label for the x axis of the plot.",
        default="overlap")
    parser.add_argument(
        "-y", "--y_axis",
        help="Label for the y axis of the plot.",
        default="overlap probability")
    #parser.add_argument("-s", "--switch",
    #        help="This is an optional boolean switch.",
    #        action="store_true")
    args = parser.parse_args()
    axis = plt.gca()
    if args.colours:
        colours = cycle(args.colours)
    else:
        colours = cycle(["blue", "green", "red", "cyan", "magenta", "yellow"])
    input_iterator = izip(args.in_files, colours, args.lib_names)
    for (signature_file_name, colour, lib_name) in input_iterator:
        with open(signature_file_name, "r") as signature_file:
            # Skip header
            signature_file.readline()
            overlaps, probs = zip(*[(int(overlap), float(prob))
                                    for (overlap, _, prob)
                                    in map(split, map(strip, signature_file))])
            axis.plot(overlaps, probs, colour, label=lib_name)
    axis.set_xlabel(args.x_axis)
    axis.set_ylabel(args.y_axis)
    out_pdf = OPJ(
        args.plot_dir,
        "%s.pdf" % args.plot_name)
    plt.axis("tight")
    #plt.axis("equal")
    #plt.axis("scaled")
    axis.xaxis.label.set_fontsize(args.label_size)
    axis.yaxis.label.set_fontsize(args.label_size)
    for tick in axis.get_xticklabels():
        tick.set_fontsize(args.tick_size)
    for tick in axis.get_yticklabels():
        tick.set_fontsize(args.tick_size)
    plt.legend(loc=0, prop={"size": args.legend_size})
    plt.tight_layout()
    plt.savefig(out_pdf, dpi=300)
    # To avoid successive plots from superimposing on one another
    #plt.cla()

if __name__ == "__main__":
    sys.exit(main())
