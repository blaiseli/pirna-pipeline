What is this?
^^^^^^^^^^^^^

This is a distribution of the homerTools program from the homer suite version
4.7.2 written by Christopher Benner (http://homer.salk.edu/homer/download.html)
with a modified "homerTools trim" function that doesn't discard reads of size
equal to the -max option (turned "<" to "<=" on lines 333 and 534 of
homerTools.cpp). I have removed all files that did not seem necessary to
compile homerTools and modified the Makefile accordingly. I also tried to
eliminate compilation warnings by altering bits of homerTools.cpp
Clustering.cpp and SeqTag.cpp. I haven't tested possible wrong effects.

How to install?
^^^^^^^^^^^^^^^

Under linux, compile using the command "make" in this directory, and copy the
homerTools executable in a directory present in your $PATH.

Note that I also have modified the compile flags in the Makefile to use
-march=native. It seems to work for me. Beware that the executable may not be
able to run in another computer than the one on which it was compiled.

Contact
^^^^^^^

blaise.li@igh.cnrs.fr (or blaise.li@normalesup.org if my present institutional
e-mail doesn't work any more)

Blaise Li
Équipes Chambeyron et Seitz
Institut de Génétique Humaine
141 rue de la Cardonille
34396 Montpellier Cedex 5
Montpellier
France

Montpellier, 19/11/2014
