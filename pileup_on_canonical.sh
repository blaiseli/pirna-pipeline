#!/usr/bin/env bash
# Usage: pileup_on_canonical.sh <library_name> <reads_category_name> <ref1>[,<ref2>,...] <norm_file>

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}


# library name
name=${1}
echo $name

name_reads=${2}

references=${3}
echo ${references}

# file containing the counts used to perform normalizations
#norm_file = mapped_strict_${genome}/${name}_strict_on_${genome}_piRNA_cluster1.nbseq
norm_file=${4}

pileup_on_refs.sh ${name} ${name_reads} TE ${references} ${norm_file}

