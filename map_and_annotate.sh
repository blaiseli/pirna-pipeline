#!/usr/bin/env bash
# Usage: map_and_annotate.sh <library_name> <G_species> [<annotation_file>]
# Some information helping understanding the script might be found in 00README.txt

# To configure according to the local installation:
genomes_path=${GENOMES}

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}


# library name
name=${1}
echo "Library: ${name}"

genome=${2}
echo "Genome: ${genome}"

#annotation_file="${genomes_path}/${genome}/Annotations/${genome}_annot.txt"
#annotation_file="${genomes_path}/D_melanogaster/Annotations/Flybase/D_melanogaster_flybase_annot.txt"
#annotation_file="${genomes_path}/D_melanogaster/Annotations/Flybase/D_melanogaster_flybase_splitmRNA_annot.txt"
if [ $# = 3 ]
then
    annotation_file=${3}
else
    # This .bed file must have been compressed with bgzip and indexed with tabix
    # Example in bli@sei-lot:/extra/Genomes/D_melanogaster_r5/Annotations/Flybase:
    #build_annotated_genome.py --annotations \
    #    3UTR.txt,5UTR.txt,CDS.txt,miRNA.txt,miscRNA.txt,ncRNA.txt,transposable_elements.txt,tRNA.txt \
    #    --genome D_melanogaster_flybase_splitmRNA_and_TE --pi_clusters \
    #    ../Annotations_Chambeyron/pi_clusters_oriented.fa --si_clusters \
    #    ../Annotations_Chambeyron/ovary_si_clusters.txt --fasta_TE \
    #    ../canonical_TE.fa
    #bgzip -c D_melanogaster_flybase_splitmRNA_and_TE_annot.bed > D_melanogaster_flybase_splitmRNA_and_TE_annot.bed.bgz
    #tabix -p bed D_melanogaster_flybase_splitmRNA_and_TE_annot.bed.bgz
    annotation_file="${genomes_path}/${genome}/Annotations/Flybase/${genome}_flybase_splitmRNA_annot.bed"
fi

echo "Annotation file: ${annotation_file}"

exclude_types="None"
echo "Types excluded from annotation: ${exclude_types}"

annot_module="annotation.py"
echo "Module used to define annotation priorities: ${annot_module}"

#ncpu=${3}

fastq_in="${name}/trimmed/${name}_trimmed.fastq"
echo -e "This script requires that the sequences have already been trimmed and are present in ${fastq_in}\n(Use trim.sh to obtain such a file from raw fastq file.)"
#echo "This script requires a file containing the number to use to normalize the read counts.\n(This could be resulting from the use of map_and_count_unique.sh)"

#########################
# mapping on the genome #
#########################

# Will contain the .sam mapping result
map_dir="${name}/mapped_${genome}"
mkdir -p ${map_dir}
# Will contain the sequences that did not map
mkdir -p unmapped_${genome}/${name}

sam_file="${map_dir}/${name}_trimmed_on_${genome}.sam"
if [ ! -e ${sam_file} ]
then 
    echo "Mapping ${fastq_in} on ${genomes_path}/${genome}/${genome} with bowtie2"
    echo "bowtie2 --seed 123 -t -k 10 -L 11 -i S,1,0.8 -N 1 --mp 4,2 -x  ${genomes_path}/${genome}/${genome} -U ${fastq_in} -S ${sam_file} --un unmapped_${genome}/${name}/${name}_trimmed_not_on_${genome}.fastq"
    bowtie2 --seed 123 -t -k 10 -L 11 -i S,1,0.8 -N 1 --mp 4,2 -x  ${genomes_path}/${genome}/${genome} -U ${fastq_in} -S ${sam_file} --un unmapped_${genome}/${name}/${name}_trimmed_not_on_${genome}.fastq || error_exit "bowtie2 failed"
else
    echo "Using already existing ${sam_file}"
fi

echo "Counting the number of mapping reads in ${sam_file} using samtools view."
echo "samtools view -Sc -F 260 ${sam_file} > ${map_dir}/${name}_trimmed_on_${genome}.nbmappers"
## count the alignments that are not from reads without alignments (flag 4 i.e. that align!) and that are not secondary (flag 256)
samtools view -Sc -F 260 ${sam_file} > ${map_dir}/${name}_trimmed_on_${genome}.nbmappers || error_exit "samtools view failed"


#############################################################################
# Annotating the reads and generating fastq files for each annotation group #
#############################################################################

# Will contain the sequences that mapped, in distinct files for distinct annotation groups
seq_dir="${name}/mapped_reads_${genome}"
mkdir -p ${seq_dir}
# Will contain size histogram data for the various annotation groups
histo_dir="${name}/histos_mapped_${genome}"
mkdir -p ${histo_dir}

#--min_score -10 was chosen in order to filter out alignments with too many mismatches.
# We had to use this proxy because filtering on the number of mismatches
# sometimes excluded the primary alignment.
# Also, the score is in theory a more suitable criterion than the number of mismatches,
# because it takes into account read quality.
#TODO: deal with exclude_types correctly (take from file ?)
echo "annotate_sam.py --in_file ${sam_file} --annot ${annotation_file} --annot_module ${annot_module} --exclude_types ${exclude_types} --keep_chr --min_score "-12" --seq_dir ${seq_dir} --histo_dir ${histo_dir}"
time annotate_sam.py --in_file ${sam_file} --annot ${annotation_file} --annot_module ${annot_module} --exclude_types ${exclude_types} --keep_chr --min_score "-12" --seq_dir ${seq_dir} --histo_dir ${histo_dir} || error_exit "annotate_sam.py failed"


exit 0
