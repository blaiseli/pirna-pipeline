#!/bin/sh
banque1=${1}
ref1=${2}
banque2=${3}
ref2=${4}
#col="2"
col=$5
transform=$6
norm=$7
genome=${8}
scatterdir="scatterplots"

in_files="siRNA_counts/${banque1}_siRNA_counts.txt,siRNA_counts/${banque2}_siRNA_counts.txt"
in_files_ref="siRNA_counts/${ref1}_siRNA_counts.txt,siRNA_counts/${ref2}_siRNA_counts.txt"

case ${norm} in
    mi)
        scatterplot_reads.py --min_counts 5 --column $col --in_files ${in_files} --normalizations mapped_${genome}/${banque1}_on_${genome}_miRNA.nbseq,mapped_${genome}/${banque2}_on_${genome}_miRNA.nbseq --in_files_ref ${in_files_ref} --normalizations_ref mapped_${genome}/${ref1}_on_${genome}_miRNA.nbseq,mapped_${genome}/${ref2}_on_${genome}_miRNA.nbseq --x_axis "${banque1} / ${ref1} (fold of reads by miRNA reads)" --y_axis "${banque2} / ${ref2} (fold of reads by miRNA reads)" --plot ${scatterdir}/siRNA_folds_${banque1}_vs_${banque2}_norm_mi_${transform}.tex
        cd ${scatterdir}
        pdflatex siRNA_folds_${banque1}_vs_${banque2}_norm_mi_${transform}.tex
        cd ..
        ;;
    endosi)
        scatterplot_reads.py --min_counts 5 --column $col --in_files ${in_files} --normalizations mapped_${genome}/${banque1}_on_${genome}_siRNA.nbseq,mapped_${genome}/${banque2}_on_${genome}_siRNA.nbseq --in_files_ref ${in_files_ref} --normalizations_ref mapped_${genome}/${ref1}_on_${genome}_siRNA.nbseq,mapped_${genome}/${ref2}_on_${genome}_siRNA.nbseq --x_axis "${banque1} / ${ref1} (fold of reads by endogenous siRNA reads)" --y_axis "${banque2} / ${ref2} (fold of reads by endogenous siRNA reads)" --plot ${scatterdir}/siRNA_folds_${banque1}_vs_${banque2}_norm_endosi_${transform}.tex
        cd ${scatterdir}
        pdflatex siRNA_folds_${banque1}_vs_${banque2}_norm_endosi_${transform}.tex
        cd ..
        ;;
    42AB)
        scatterplot_reads.py --min_counts 5 --column $col --in_files ${in_files} --normalizations mapped_strict_dm/${banque1}_strict_on_${genome}_piRNA_cluster1.nbseq,mapped_strict_dm/${banque2}_strict_on_${genome}_piRNA_cluster1.nbseq --in_files_ref ${in_files_ref} --normalizations_ref mapped_strict_dm/${ref1}_strict_on_${genome}_piRNA_cluster1.nbseq,mapped_strict_dm/${ref2}_strict_on_${genome}_piRNA_cluster1.nbseq --x_axis "${banque1} / ${ref1} (fold of reads by 42AB piRNA reads)" --y_axis "${banque2} / ${ref2} (fold of reads by 42AB piRNA reads)" --plot ${scatterdir}/siRNA_folds_${banque1}_vs_${banque2}_norm_42AB_${transform}.tex
        cd ${scatterdir}
        pdflatex siRNA_folds_${banque1}_vs_${banque2}_norm_42AB_${transform}.tex
        cd ..
        ;;
    *)
        scatterplot_reads.py --min_counts 5 --column $col --in_files ${in_files} --in_files_ref ${in_files_ref} --x_axis "${banque1} / ${ref1} (fold of reads)" --y_axis "${banque2} / ${ref2} (fold of reads)" --plot ${scatterdir}/siRNA_folds_${banque1}_vs_${banque2}_${transform}.tex
        cd ${scatterdir}
        pdflatex siRNA_folds_${banque1}_vs_${banque2}_${transform}.tex
        cd ..
        ;;
esac
