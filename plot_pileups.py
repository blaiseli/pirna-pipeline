#!/usr/bin/env python
"""
usage: plot_pileups.py [options]

This script will read tab separated RNA-Seq read position data and plot the corresponding pileup graphics.

available options:

    --help: prints this help text and exits.

    --in_files <file1>,<file2>,<file3>,etc.: to specify the files containing the pileup data.

    --formats <fmt1><fmt2><fmt3>,etc.: to specify graphical output formats. These should be recognised by matplotlib.
    Examples: emf, eps, jpeg, jpg, pdf, pgf, png, ps, raw, rgba, svg, svgz, tif, tiff
    Only tab separated .txt data will be generated if this option is not set.

    --pileup_dir <directory>: to specify the directory in which the pileup plot files should be written.

    --x_axis "string": to provide a label for the x axis of the histograms.
    Default: "position in <ref> sequence" where <ref> is inferred from the headers of the csv files.

    --y_axis "string": to provide a label for the y axis of the histograms.
    Default: "reads per million reference reads"

"""

# TODO: update the code writing the *_pileup.txt files so that it
# differenciates mismatch categories and update the code in the present script
# to take that into account


import getopt
import os
opj = os.path.join
opb = os.path.basename
import re
import sys
write = sys.stdout.write
import warnings
def formatwarning(message, category, filename, lineno, line):
    return "%s:%s: %s:%s" % (filename, lineno, category.__name__, message)
warnings.formatwarning = formatwarning

import csv

import pylab
import numpy as np
# To get pgf export working (http://stackoverflow.com/questions/20000427/matplotlib-pgf-oserror-no-such-file-or-directory-in-subprocess-py)
import matplotlib
pgf_with_rc_fonts = {"pgf.texsystem": "pdflatex"}
matplotlib.rcParams.update(pgf_with_rc_fonts)
matplotlib.rc('text', usetex=True)

def oui(question):
    """oui(question) returns True if the user answers yes to the question
    <question>."""    
    return (raw_input("\n" + question + " (y/n) ") in ["O", "o", "Oui", "oui", "OUI", "Y", "y", "Yes", "yes", "YES", "J", "j", "Ja", "ja", "JA", "S", "s", "Si", "si", "SI", "D", "d", "Da", "da", "DA", "T", "t", "Taip", "taip", "TAIP"])

def read_pileups(f):
    reader = csv.reader(f, dialect="excel-tab")
    fields = reader.next()
    fields = reader.next()
    ref = fields[1]
    ref_len = int(fields[2])
    fields = reader.next()
    #norm_number = int(fields[1])
    #norm_factor = float(fields[2])
    fields = reader.next()
    assert fields[0] == "position"
    assert fields[1] == "nb_F"
    assert fields[3] == "nb_R"
    data = []
    while True:
        try:
            fields = reader.next()
            if fields[0] == "#empty":
                break
            else:
                # deal with missing entries (some positions have no reads mapping on them, so no line in the pileup data file)
                pos = int(fields[0])
                if data:
                    i = data[-1][0] + 1
                else:
                    i = 0
                while i < pos:
                    data.append((i, 0.0, 0.0))
                    i += 1
                # pos, normalized counts for forwards and reverse mapping
                data.append((pos, float(fields[2]), float(fields[4])))
        except StopIteration:
            break
    if data:
        # deal with final missing entries
        pos += 1
        while pos < ref_len:
            data.append((pos, 0.0, 0.0))
            pos += 1
        #print ref
        positions = np.arange(ref_len)
        minposition = data[0][0]
        assert minposition == 0
        maxposition = data[-1][0]
        #print len(data), ref_len, maxposition
        assert maxposition == positions[-1]
        #positions = np.arange(minposition, maxposition + 1)
        counts_fwd = np.array([d[1] for d in data])
        counts_rev = np.array([d[2] for d in data])
        #print len(counts_fwd), len(counts_rev), len(positions)
        assert len(positions) == len(counts_fwd)
        assert len(positions) == len(counts_rev)
        max_fwd = max(counts_fwd)
        max_rev = max(counts_rev)
        return ref, minposition, maxposition, positions, counts_fwd, counts_rev, max_fwd, max_rev 
    else:
        return ref, None, None, None, None, None, None, None


class Pileup(object):
    def __init__(self, experiment, ref, positions, counts_fwd, counts_rev):
        self.experiment = experiment
        self.ref = ref
        self.positions = positions
        self.counts_fwd = counts_fwd
        self.counts_rev = counts_rev
    def plot(self, xmin, xmax, ymin, ymax, out=os.devnull):
        """positions, counts_F, counts_R are same-lenghts arrays."""
        pylab.axis([xmin, xmax, ymin, ymax])
        ax = pylab.gca()
        ax.set_autoscale_on(False)
        ax.spines["right"].set_color("none")
        ax.spines["top"].set_color("none")
        ax.xaxis.set_ticks_position("bottom")
        ax.yaxis.set_ticks_position("left")
        ax.vlines(self.positions, [0], self.counts_fwd, edgecolor='red', label="forward mappers")
        ax.vlines(self.positions, [0], -self.counts_rev, edgecolor='green', label="reverse mappers")
        if Options.x_axis:
            ax.set_xlabel(re.sub("_", "\_", Options.x_axis))
        else:
            ax.set_xlabel("position in %s sequence" % re.sub("_", "\_", self.ref))
        ax.set_ylabel(Options.y_axis)
        for fmt in Options.formats:
            try:
                pylab.savefig(out + "." + fmt)
            except ValueError:
                warnings.warn("Output format %s unrecognised, skipping it.\n" % fmt)
        #pylab.savefig(out + ".pdf")
        #pylab.savefig(out + ".svg")
        #pylab.savefig(out + ".svg")
        if Options.show_figs:
            pylab.show()
        # trying to avoid successive plots from superimposing on one another
        pylab.cla()


##############################################################################################
# For better traceability, options and global variables should be stored as class attributes #
##############################################################################################

class Globals(object):
    """This object holds global variables."""
    cluster_names = {"cluster1" : "42AB", "cluster8" : "flamenco"}

class Options(object):
    """This object contains the values of the global options."""
    in_files = None
    formats = []
    pileup_dir = None
    # set to True to count reads only when their 5' maps at the position
    strict = True
    # set to True to display the figure (this will pause the program until the figure window is closed)
    #show_figs = True
    show_figs = False
    x_axis = None
    y_axis = "reads per million reference reads"

#################
# main function #
#################

def main():
    write(" ".join(sys.argv) + "\n")
    try:
        opts, args = getopt.getopt(sys.argv[1:], "io:", ["help", "in_files=", "formats=", "pileup_dir=", "x_axis=", "y_axis="])
    except getopt.GetoptError:
        sys.stderr.write(__doc__)
        sys.exit(1)
    for option, argument in opts:
        if option == "--help":
            write(__doc__)
            sys.exit()
        if option == "--in_files":
            Options.in_files = argument.split(",")
        if option == "--formats":
            Options.formats = argument.split(",")
        if option == "--pileup_dir":
            Options.pileup_dir = argument
        if option == "--y_axis":
            Options.y_axis = argument
        if option == "--x_axis":
            Options.x_axis = argument

    if Options.in_files is None:
        sys.stderr.write("You should provide a comma-separated list of pileup data files with the option --in_files\n")
        sys.exit(1)
    if Options.pileup_dir is None:
        sys.stderr.write("You should provide a directory in which the pileup files will be written with the option --pileup_dir\n")
        sys.exit(1)

    xmin = None
    xmax = None
    ymin = None
    ymax = None
    data = {}

    for f_name in Options.in_files:
        experiment = "_on_".join(opb(f_name).split("_on_")[:-1])
        #print experiment
        with open(f_name, "rb") as f:
            (ref, minposition, maxposition, positions,
             counts_fwd, counts_rev, max_fwd, max_rev) = read_pileups(f) 
        shortref = ref.split(":")[0]
        shortref = Globals.cluster_names.get(shortref, shortref)
        if positions is not None:
            # To use the same ranges for all the plots
            if xmin is None:
                xmin = minposition
                xmax = maxposition
                ymin = -max_rev
                ymax = max_fwd
            else:
                xmin = min(xmin, minposition)
                xmax = max(xmax, maxposition)
                ymin = min(ymin, -max_rev)
                ymax = max(ymax, max_fwd)
            data[experiment] = Pileup(experiment, shortref, positions, counts_fwd, counts_rev)
    for h in data.values():
        out = opj(Options.pileup_dir, "%s_on_%s_pileup" % (h.experiment, h.ref))
        h.plot(xmin, xmax, ymin, ymax, out)
    return 0

if __name__ == "__main__":
    sys.exit(main())
