#!/usr/bin/env bash
# Usage: pipeline_small.sh <library_name> <adapter> <tag_length_to_trim> <G_species> <number_of_cpus_to_use> [<annotation_file>]
# Some information helping understanding the script might be found in README.md

# To configure according to the local installation:
genomes_path=${GENOMES}

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}

PLACE=`pwd`
echo "Analyses taking place with base directory ${PLACE}"

#name="yb_RNAi"
name="${1}"
#adapter="TCGTATGCCGTCTTCTGCTTG"
adapter="${2}"
#trim="0"
trim="${3}"
# maximum read size
# If this environment variable is not defined, use 30 as default value
[ MAX_LEN ] || MAX_LEN="30"

# Check that the version of "homerTools trim" correctly handles the "-max" option
echo -e "\n-----"
date +"%d/%m/%Y %T"
test_select_fastq_size.sh || error_exit "test_select_fastq_size.sh failed. Your version of homerTools might not trim reads correctly."


echo -e "\n-----"
date +"%d/%m/%Y %T"
md5sums="${name}/md5sums.txt"
compute_md5="nice -n 19 ionice -c2 -n7 md5sum"

# TODO: one day, make a function that calculates md5sums in background, queuing them?
#function next_md5
#{
#    nice -n 19 ionice -c2 -n7 md5sum ${1} >> ${md5sums} &
#    last_md5_pid=$!
#    exit 0
#}

# first argument: fastq file
# second argument: file containing the adapter
function fastqc_report
{
    report_dir="$(dirname ${1})/fastqc_report"
    if [ ! -e  ${report_dir} ]
    then
        mkdir ${report_dir}
        cmd="fastqc -o ${report_dir} --noextract -f fastq -a ${2} ${1}"
        nice -n 19 ionice -c2 -n7 ${cmd} || error_exit "${cmd} failed"
    else
        echo "${report_dir} already exists. Skipping fastqc reporting."
    fi
}

fastq_file="raw_data/${name}/${name}.fastq"
trimmed_file="${name}/trimmed/${name}_trimmed.fastq"
echo "Testing existence of ${fastq_file}"
if [ ! -e ${fastq_file} -a ! -e ${trimmed_file} ]
then
    echo "File not found. Testing existence of a compressed version."
    if [ ! -e ${fastq_file}.gz ]
    then
        if [ ! -e ${fastq_file}.bz2 ]
        then
            error_exit "No .gz nor .bz2 version found."
        else
            eval ${compute_md5} ${fastq_file}.bz2 >> ${md5sums} &
            cmd="bzcat ${fastq_file}.bz2 > ${fastq_file}"
            echo ${cmd}
            eval ${cmd} || error_exit "${cmd} failed"
        fi
    else
        eval ${compute_md5} ${fastq_file}.gz >> ${md5sums} &
        cmd="zcat ${fastq_file}.gz > ${fastq_file}"
        echo ${cmd}
        eval ${cmd} || error_exit "${cmd} failed"
    fi
else
    eval ${compute_md5} ${fastq_file} >> ${md5sums} &
    echo "File found."
fi

# Make fastqc report for the raw data
# Log this separately to re-use later
tmpdir=$(mktemp -dt "`basename $0`.XXXXXXXXXX")
adapter_file="$tmpdir/adapter.txt"
echo -e "adapter\t${adapter}" > ${adapter_file}
cmd="fastqc_report ${fastq_file} ${adapter_file}"
echo ${cmd}
eval ${cmd} > ${tmpdir}/fastqc.log 2> ${tmpdir}/fastqc.err &


trim_dir=${name}/trimmed
echo "trim_small.sh ${fastq_file} ${adapter} ${trim}"
trim_small.sh "${fastq_file}" "${adapter}" "${trim}" || error_exit "trim_small.sh failed"
eval ${compute_md5} ${trim_dir}/${name}_18-${MAX_LEN}.fastq.gz >> ${md5sums} &


# TODO: plot this from 7 to ${MAX_LEN}
lib_histo="${trim_dir}/${name}_trimmed.lengths"

#genome="D_melanogaster"
genome=${4}

if [ $# = 6 ]
then
    annotation_file=${6}
else
    #annotation_file="${genomes_path}/${genome}/Annotations/Flybase/${genome}_flybase_splitmRNA_annot.txt"
    annotation_file="${genomes_path}/${genome}/Annotations/Flybase/${genome}_flybase_splitmRNA_and_TE_annot.bed"
fi

echo -e "\n-----"
date +"%d/%m/%Y %T"
echo "map_and_annotate_small.sh ${name} ${genome} ${annotation_file}"
map_and_annotate_small.sh "${name}" "${genome}" "${annotation_file}" || error_exit "map_and_annotate_small.sh failed"
picandidates="${name}/mapped_reads_${genome}/${name}_23-${MAX_LEN}_on_${genome}_piRNA_candidate.fastq.gz"
piuniques="${name}/mapped_reads_${genome}/${name}_23-${MAX_LEN}_on_${genome}_piRNA_unique.fastq.gz"
sicandidates="${name}/mapped_reads_${genome}/${name}_21_on_${genome}_siRNA_candidate.fastq.gz"
eval ${compute_md5} ${picandidates} ${piuniques} ${sicandidates} >> ${md5sums} &

# TODO: plot this histogram (or do this in map_and_annotate_small.sh ?)
pisiTE3UTR_histo="${name}/histos_mapped_${genome}/${name}_18-${MAX_LEN}_on_${genome}_group_pi-si-TE-3UTR_lengths.txt"

ncpu=${5}
#ncpu="4"

# We only want to count uniques up to pi_cluster 17
#max_clust_nb=17

# We only want to count uniques for piRNA clusters 1-8 and 17
clust_list_file="cluster_list.txt"

if [ ! -e ${clust_list_file} ]
then
    echo "1" > ${clust_list_file}
    echo "2" >> ${clust_list_file}
    echo "3" >> ${clust_list_file}
    echo "4" >> ${clust_list_file}
    echo "5" >> ${clust_list_file}
    echo "6" >> ${clust_list_file}
    echo "7" >> ${clust_list_file}
    echo "8" >> ${clust_list_file}
    echo "17" >> ${clust_list_file}
fi

echo -e "\n\n-----"
date +"%d/%m/%Y %T"
echo "map_and_count_unique.sh"
map_and_count_unique.sh "${name}" "${genome}" "${clust_list_file}" "${ncpu}" || error_exit "map_and_count_unique.sh failed"

echo -e "\n\n-----"
date +"%d/%m/%Y %T"
echo "map_on_canonical_set.sh ${name} ${genome}"
map_on_canonical_set.sh "${name}" "${genome}" || error_exit "map_on_canonical_set.sh failed"

echo -e "\n\n-----"
date +"%d/%m/%Y %T"
echo "map_on_pi_clusters.sh ${name} ${genome}"
map_on_pi_clusters.sh "${name}" "${genome}" || error_exit "map_on_3UTR.sh failed"

echo -e "\n\n-----"
date +"%d/%m/%Y %T"
echo "map_on_3UTR.sh ${name} ${genome}"
map_on_3UTR.sh "${name}" "${genome}" || error_exit "map_on_3UTR.sh failed"

echo -e "\n\n-----"
mkdir -p summaries
# http://stackoverflow.com/questions/59895/can-a-bash-script-tell-what-directory-its-stored-in
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
annot_module="${DIR}/annotation_piRNA.py"
echo "Module used to define annotation priorities: ${annot_module}"
cmd="summarize_library.py -a ${annot_module} -l ${name} -g ${genome} -s ${name}/mapped_reads_${genome}"
echo ${cmd}
eval ${cmd} > summaries/${name}_on_${genome}_results.xls || error_exit "${cmd} failed"

# Wait for the end of fastqc reporting
echo -e "\nWaiting for ${fastq_file} fastqc report."
wait
# cat the fastqc logs to stdout and stderr
cat ${tmpdir}/fastqc.log
(1>&2 cat ${tmpdir}/fastqc.err)
rm -rf ${tmpdir}


exit 0
