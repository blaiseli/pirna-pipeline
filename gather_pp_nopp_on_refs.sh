#!/usr/bin/env bash
# Usage: gather_pp_nopp_on_refs.sh <lib1> <lib2> <num_slices> <ref_type> <refs>

lib1=${1}
lib2=${2}
num_slices=${3}
ref_type=${4}
refs=${5}

refs_basename=`basename ${refs}`
results_dir="ping_pong_results/pp_nopp_${lib1}_${lib2}_on_${refs_basename%.txt}"
mkdir -p ${results_dir}

slice_size=`awk '$1=="tot"{print $2}' ping_pong_results/${lib1}_${lib2}_${refs_basename%.txt}_${num_slices}_slices/slice_sizes.txt`

for ref in `cat ${refs}`
do
    if [ $ref != "global" ]
    then
        for lib in ${lib1} ${lib2}
        do
            for slice in `seq 1 ${num_slices}`
            do
                echo "#slice_${slice}_${lib}"
                echo -e "#size\tpp_F_0mm\tpp_R_0mm\tnopp_F_0mm\tnopp_R_0mm"
                for size in `seq 23 30`
                do
                    pp_type="pp"
                    histo_file=${lib}/ping_pong_${num_slices}_slices/global/${slice_size}/histos_${pp_type}_${slice}_on_${ref_type}/${lib}_${pp_type}_${slice}_on_${ref}_lengths.txt
                    pp_F_0mm=`awk -v size=${size} '$1==size{print $4}' ${histo_file}`
                    pp_R_0mm=`awk -v size=${size} '$1==size{print $5}' ${histo_file}`
                    pp_type="nopp"
                    histo_file=${lib}/ping_pong_${num_slices}_slices/global/${slice_size}/histos_${pp_type}_${slice}_on_${ref_type}/${lib}_${pp_type}_${slice}_on_${ref}_lengths.txt
                    nopp_F_0mm=`awk -v size=${size} '$1==size{print $4}' ${histo_file}`
                    nopp_R_0mm=`awk -v size=${size} '$1==size{print $5}' ${histo_file}`
                    echo -e "${size}\t${pp_F_0mm}\t${pp_R_0mm}\t${nopp_F_0mm}\t${nopp_R_0mm}"
                done
            done > ${results_dir}/histo_0mm_${lib}_on_${ref}.txt
        done
    fi
done
