#!/usr/bin/env bash
# Usage: do_scatterplot.sh <lib1> <lib2> <read_type> <ref_type> <normalization_type> <transform_type> [<element_lists> <colours> <column>]
# <read_type> can be "pi-si-TE-3UTR", "TE", "piRNA_candidate", "piRNA_unique" or "siRNA_candidate"
# <ref_type> can be "TE", "pi_clusters", or "3UTR"
# <normalization_type> can be "none", "mappers", "mi", "mt", "endosi", or a piRNA cluster number
# <transform_type> can be "log2", "log10" or a number (linear scale)
# <element_lists> and <colours> are comma-seprated lists of files and colours respectively.
# The files are lists of elements, one per line.
# The colours are the names of the colours to attribute to the elements in the corresponding file.
# The correspondence between colours and lists of elements is made by shared rank in the comma-separated lists.
# <column> is the column number in the input tables. By default, 2.

genome="D_melanogaster"

if [ ! ${OUT_DIR} ]
then
    scatterdir="scatterplots"
else
    scatterdir="${OUT_DIR}"
fi

if [ ! ${BY} -o ${BY}=="1" ]
then
    by=""
    BY=""
else
    by="-b ${BY}"
    BY="${BY} "
fi

if [ ! ${MIN_COUNTS} ]
then
    MIN_COUNTS="1000"
fi

if [ ! ${MAX_LOGFOLD} ]
then
    max_logfold=""
else
    max_logfold="--max_logfold ${MAX_LOGFOLD}"
fi

if [ ! ${MARKER_SIZE} ]
then
    MARKER_SIZE="4"
fi

if [ ! ${AXIS_FONT} ]
then
    AXIS_FONT="footnotesize"
fi

if [ ! ${TICK_FONT} ]
then
    TICK_FONT="tiny"
fi

if [ ! ${HM_LABEL} ]
then
    HM_LABEL="9"
fi

if [ ! ${CMAP} ]
then
    CMAP=""
else
    CMAP="--cmap ${CMAP}"
fi

if [ ! ${CENTER_CMAP} ]
then
    REMAP=""
else
    REMAP="--remap_cmap"
fi

if [ ! ${PLOT_DIAGONAL} ]
then
    DIAG=""
else
    DIAG="--plot_diagonal"
fi

if [ ! ${NAME_CONV} ]
then
    NAME_CONV=""
else
    if [ -e ${NAME_CONV} ]
    then
        NAME_CONV="--name_conversion ${NAME_CONV}"
    else
        echo "File ${NAME_CONV} not found! Aborting." 1>&2
        exit 1
    fi
fi

if [ ! ${ORDER} ]
then
    ORDER=""
else
    if [ -e ${ORDER} ]
    then
        ORDER="--elements_order ${ORDER}"
    else
        echo "File ${ORDER} not found! Aborting." 1>&2
        exit 1
    fi
fi

if [ ${NO_LINE} ]
then
    NO_LINE="--no_line"
fi

mkdir -p ${scatterdir}

lib1=${1}
lib2=${2}

summaries1="summaries/${lib1}_on_${genome}_results.xls"
summaries2="summaries/${lib2}_on_${genome}_results.xls"

read_type=${3}
ref_type=${4}
norm=${5}
transform=${6}


if [ $# = 7 ]
then
    element_lists=""
    colours=""
    col=${7}
elif [ $# = 8 ]
then
    el_files=`echo ${7} | tr "," " "`
    element_lists="--element_lists ${el_files}"
    colour_files=`echo ${8} | tr "," " "`
    colours="--colours ${colour_files}"
    col=2
elif [ $# = 9 ]
then
    el_files=`echo ${7} | tr "," " "`
    element_lists="--element_lists ${el_files}"
    colour_files=`echo ${8} | tr "," " "`
    colours="--colours ${colour_files}"
    col=${9}
else
    element_lists=""
    colours=""
    col=2
fi

case "${col}" in
    "2")
        reads="reads"
        ;;
    "3")
        reads="sense reads"
        ;;
    "4")
        reads="antisense reads"
        ;;
    "5")
        reads="sense 0mm reads"
        ;;
    "6")
        reads="antisense 0mm reads"
        ;;
    "7")
        reads="sense mm reads"
        ;;
    "8")
        reads="antisense mm reads"
        ;;
    *)
        if [ ${read_type} = "ping-pong" ]
        then
            reads="ping-pong signature"
        else
            reads="reads"
        fi
        ;;
esac

if [ ${read_type} = "ping-pong" ]
then
    #TODO: gather from ${lib}/ping_pong_full/${ref}/${lib1}_piRNA_candidate_remapped_on_${ref}_1-21_signature.xls
    # -> Do this from the ping-pong script(s)
    #TODO: document this option once it works
    infile1="${lib1}/histos_${read_type}_on_${ref_type}/${lib1}_${read_type}_on_${ref_type}_ref_counts.txt"
    infile2="${lib2}/histos_${read_type}_on_${ref_type}/${lib2}_${read_type}_on_${ref_type}_ref_counts.txt"
else
    infile1="${lib1}/histos_${read_type}_on_${ref_type}/${lib1}_${read_type}_on_${ref_type}_ref_counts.txt"
    infile2="${lib2}/histos_${read_type}_on_${ref_type}/${lib2}_${read_type}_on_${ref_type}_ref_counts.txt"
fi

infiles="${infile1} ${infile2}"

case "${norm}" in
    "mappers")
        norm_reads="mappers"
        legend1="${lib1} ${reads} by ${BY}${norm_reads}"
        legend2="${lib2} ${reads} by ${BY}${norm_reads}"
        norm1="${lib1}/mapped_${genome}/${lib1}_on_${genome}_mappers.nbseq"
        norm2="${lib2}/mapped_${genome}/${lib2}_on_${genome}_mappers.nbseq"
        if [ ! -e ${norm1} ]
        then
            awk -F "\t" '$1=="mappers"{print $2}' ${summaries1} > ${norm1}
        fi
        if [ ! -e ${norm2} ]
        then
            awk -F "\t" '$1=="mappers"{print $2}' ${summaries2} > ${norm2}
        fi
        normfiles="${norm1} ${norm2}"
        normalization="--normalizations ${normfiles}"
        texfile="${read_type}_on_${ref_type}_${lib1}_vs_${lib2}_norm_mappers_${transform}_col_${col}_min_${MIN_COUNTS}_with_names.tex"
        pdffile="${read_type}_on_${ref_type}_${lib1}_vs_${lib2}_norm_mappers_${transform}_col_${col}_min_${MIN_COUNTS}"
        foldheat="${read_type}_on_${ref_type}_${lib1}_vs_${lib2}_norm_mappers_${transform}_fold_col_${col}_min_${MIN_COUNTS}"
        ;;
    "mi")
        norm_reads="miRNA reads"
        legend1="${lib1} ${reads} by ${BY}${norm_reads}"
        legend2="${lib2} ${reads} by ${BY}${norm_reads}"
        norm1="${lib1}/mapped_${genome}/${lib1}_on_${genome}_miRNA.nbseq"
        norm2="${lib2}/mapped_${genome}/${lib2}_on_${genome}_miRNA.nbseq"
        if [ ! -e ${norm1} -o ! -e ${norm2} ]
        then
            echo "One of the normalization files is missing." 1>&2
            echo "Aborting" 1>&2
            exit 1
        fi
        normfiles="${norm1} ${norm2}"
        normalization="--normalizations ${normfiles}"
        texfile="${read_type}_on_${ref_type}_${lib1}_vs_${lib2}_norm_miRNA_${transform}_col_${col}_min_${MIN_COUNTS}_with_names.tex"
        pdffile="${read_type}_on_${ref_type}_${lib1}_vs_${lib2}_norm_miRNA_${transform}_col_${col}_min_${MIN_COUNTS}"
        foldheat="${read_type}_on_${ref_type}_${lib1}_vs_${lib2}_norm_miRNA_fold_col_${col}_min_${MIN_COUNTS}"
        ;;
    "mt")
        norm_reads="mitochondrial reads"
        legend1="${lib1} ${reads} by ${BY}${norm_reads}"
        legend2="${lib2} ${reads} by ${BY}${norm_reads}"
        norm1="${lib1}/mapped_${genome}/${lib1}_on_${genome}_mt.nbseq"
        norm2="${lib2}/mapped_${genome}/${lib2}_on_${genome}_mt.nbseq"
        if [ ! -e ${norm1} -o ! -e ${norm2} ]
        then
            echo "One of the normalization files is missing." 1>&2
            echo "Aborting" 1>&2
            exit 1
        fi
        normfiles="${norm1} ${norm2}"
        normalization="--normalizations ${normfiles}"
        texfile="${read_type}_on_${ref_type}_${lib1}_vs_${lib2}_norm_mt_${transform}_col_${col}_min_${MIN_COUNTS}_with_names.tex"
        pdffile="${read_type}_on_${ref_type}_${lib1}_vs_${lib2}_norm_mt_${transform}_col_${col}_min_${MIN_COUNTS}"
        foldheat="${read_type}_on_${ref_type}_${lib1}_vs_${lib2}_norm_mt_fold_col_${col}_min_${MIN_COUNTS}"
        ;;
    "endosi")
        norm_reads="endogenous siRNA reads"
        legend1="${lib1} ${reads} by ${BY}${norm_reads}"
        legend2="${lib2} ${reads} by ${BY}${norm_reads}"
        norm1="${lib1}/mapped_${genome}/${lib1}_on_${genome}_siRNA.nbseq"
        norm2="${lib2}/mapped_${genome}/${lib2}_on_${genome}_siRNA.nbseq"
        if [ ! -e ${norm1} -o ! -e ${norm2} ]
        then
            echo "One of the normalization files is missing." 1>&2
            echo "Aborting" 1>&2
            exit 1
        fi
        normfiles="${norm1} ${norm2}"
        normalization="--normalizations ${normfiles}"
        texfile="${read_type}_on_${ref_type}_${lib1}_vs_${lib2}_norm_siRNA_${transform}_col_${col}_min_${MIN_COUNTS}_with_names.tex"
        pdffile="${read_type}_on_${ref_type}_${lib1}_vs_${lib2}_norm_siRNA_${transform}_col_${col}_min_${MIN_COUNTS}"
        foldheat="${read_type}_on_${ref_type}_${lib1}_vs_${lib2}_norm_siRNA_fold_col_${col}_min_${MIN_COUNTS}"
        ;;
    "none")
        legend1="${lib1} ${reads}"
        legend2="${lib2} ${reads}"
        normalization=""
        texfile="${ref_type}_${lib1}_vs_${lib2}_${transform}_col_${col}.tex"
        ;;
    *)
        case "${norm}" in
            "1")
                norm_reads="42AB reads"
                legend1="${lib1} ${reads} by ${BY}${norm_reads}"
                legend2="${lib2} ${reads} by ${BY}${norm_reads}"
                texfile="${read_type}_on_${ref_type}_${lib1}_vs_${lib2}_norm_42AB_${transform}_col_${col}_min_${MIN_COUNTS}_with_names.tex"
                pdffile="${read_type}_on_${ref_type}_${lib1}_vs_${lib2}_norm_42AB_${transform}_col_${col}_min_${MIN_COUNTS}"
                foldheat="${read_type}_on_${ref_type}_${lib1}_vs_${lib2}_norm_42AB_fold_col_${col}_min_${MIN_COUNTS}"
                ;;
            "8")
                norm_reads="flamenco reads"
                legend1="${lib1} ${reads} by ${BY}${norm_reads}"
                legend2="${lib2} ${reads} by ${BY}${norm_reads}"
                texfile="${read_type}_on_${ref_type}_${lib1}_vs_${lib2}_norm_flamenco_${transform}_col_${col}_min_${MIN_COUNTS}_with_names.tex"
                pdffile="${read_type}_on_${ref_type}_${lib1}_vs_${lib2}_norm_flamenco_${transform}_col_${col}_min_${MIN_COUNTS}"
                foldheat="${read_type}_on_${ref_type}_${lib1}_vs_${lib2}_norm_flamenco_fold_col_${col}_min_${MIN_COUNTS}"
                ;;
            *)
                norm_reads="piRNA cluster ${norm} reads"
                legend1="${lib1} ${reads} by ${BY}${norm_reads}"
                legend2="${lib2} ${reads} by ${BY}${norm_reads}"
                texfile="${read_type}_on_${ref_type}_${lib1}_vs_${lib2}_norm_cluster_${norm}_${transform}_col_${col}_min_${MIN_COUNTS}_with_names.tex"
                pdffile="${read_type}_on_${ref_type}_${lib1}_vs_${lib2}_norm_cluster_${norm}_${transform}_col_${col}_min_${MIN_COUNTS}"
                foldheat="${read_type}_on_${ref_type}_${lib1}_vs_${lib2}_norm_cluster_${norm}_fold_col_${col}_min_${MIN_COUNTS}"
                ;;
        esac
        norm1="${lib1}/mapped_strict_${genome}/${lib1}_strict_on_${genome}_piRNA_cluster${norm}.nbseq"
        norm2="${lib2}/mapped_strict_${genome}/${lib2}_strict_on_${genome}_piRNA_cluster${norm}.nbseq"
        if [ ! -e ${norm1} -o ! -e ${norm2} ]
        then
            echo "One of the normalization files is missing." 1>&2
            echo "Aborting" 1>&2
            exit 1
        fi
        normfiles="${norm1} ${norm2}"
        normalization="--normalizations ${normfiles}"
        ;;    
esac

if [[ ${NOHEAT} ]]
then
    foldheat=""
else
    foldheat="-f ${foldheat}"
fi

if [[ ${NOSCATTER} ]]
then
    pdffile=""
else
    pdffile="-p ${pdffile}"
fi

date >> ${scatterdir}/log

# pyplot version
plot_scatterplots.py --min_counts ${MIN_COUNTS} ${by} \
    --column $col \
    --in_files ${infiles} ${normalization} ${colours} ${element_lists} \
    --x_axis "${legend1}" \
    --y_axis "${legend2}" \
    -t ${transform} \
    -d ${scatterdir} \
    ${pdffile} ${foldheat} ${CMAP} ${REMAP} ${DIAG} ${max_logfold} \
    --marker_size ${MARKER_SIZE} --hm_label_size ${HM_LABEL} ${NAME_CONV} ${ORDER} \
    >> ${scatterdir}/log \
    || exit 1

if [[ ! ${NOTEX} ]]
then
    # TikZ version, with names appearing as tip tool (in acrobat reader)
    scatterplot_reads.py --min_counts ${MIN_COUNTS} ${by} \
        --column $col ${NO_LINE} \
        --in_files ${infiles} ${normalization} ${colours} ${element_lists} \
        --library_names ${lib1} ${lib2} \
        --x_axis "${legend1}" \
        --y_axis "${legend2}" \
        --x_scale "2.5" \
        --y_scale "2.5" \
        --transform ${transform} \
        --axis_font ${AXIS_FONT} \
        --tick_font ${TICK_FONT} \
        --plot ${scatterdir}/${texfile} \
        >> ${scatterdir}/log \
        || exit 1

    if [ ! -e ${scatterdir}/${texfile} ]
    then
        echo "Missing scatterplot .tex code." 1>&2
        exit 1
    fi

    compile="Yes"
    #compile=""

    if [ ${compile} ]
    then
        curr_dir=`pwd`
        cd ${scatterdir}
        texfot pdflatex ${texfile}
        texfot pdflatex ${texfile}
        cd ${curr_dir}
        echo "Scatterplot written in ${scatterdir}/${texfile%.tex}.pdf"
    fi
fi

exit 0
