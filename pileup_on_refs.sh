#!/usr/bin/env bash
# Usage: pileup_on_refs.sh <read_type> <ref_type> <normalization_type> <mm/nomm> <element_list> <lib1> [<lib2>,...]
# <read_type> can be "pi-si-TE-3UTR", "piRNA_candidate", "piRNA_unique" or "siRNA_candidate"
# <ref_type> can be "TE", "pi_clusters", or "3UTR"
# <normalization_type> can be "none", "mappers", "mi", "mt", "endosi", or a piRNA cluster number
# <mm/nomm> is used to select whether to use colour intensity to distinguish degrees of mismatches.
# <elements_list> is a file containing a list of elements, one per line, for which the pileups have to be plotted.
# It can also be a single reference name.
# <lib1>, <lib2>, etc. are the library names for which the pileups are to be plotted.
# For each element in the list, the pileups for all these libraries will be at the same vertical scale.

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}

genome="D_melanogaster"


read_type=${1}
ref_type=${2}
norm=${3}

if [ ! ${OUT_DIR} ]
then
    pileups_dir="pileups_${read_type}_on_${ref_type}_norm_${norm}"
else
    pileups_dir="${OUT_DIR}"
fi


mm=${4}
elements_list=${5}
libraries=${@:6}

# Environment variable to scale by a certain number of normalizers
if [ ! ${BY} -o ${BY}=="1" ]
then
    by=""
    BY=""
else
    by="-b ${BY}"
    if [[ ${BY} == "1000000" ]]
    then
        BY="1M "
    else
        BY="${BY} "
    fi
fi

# Environment variables to not plot F or R reads
if [ ! ${NOF} ]
then
    noF=""
else
    noF="--noF"
fi
if [ ! ${NOR} ]
then
    noR=""
else
    noR="--noR"
fi

if [ -e ${elements_list} ]
then
    references=`cat ${elements_list}`
else
    references=${elements_list}
fi
for ref in ${references}
do
    # Define y-axis legend and prepare normalization option
    case "${norm}" in
        "mappers")
            legend="Reads by ${BY}mappers"
            normalization="-n"
            ;;
        "mi")
            legend="Reads by ${BY}miRNA reads"
            normalization="-n"
            ;;
        "mi")
            legend="Reads by ${BY}mitochondrial reads"
            normalization="-n"
            ;;
        "endosi")
            legend="Reads by ${BY}endogenous siRNA reads"
            normalization="-n"
            ;;
        "none")
            legend="Reads"
            normalization=""
            ;;
        *)
            case "${norm}" in
                "1")
                    legend="Reads by ${BY}42AB reads"
                    ;;
                "8")
                    legend="Reads by ${BY}flamenco reads"
                    ;;
                *)
                    legend="Reads by ${BY}piRNA cluster ${norm} reads"
                    ;;
            esac
            normalization="-n"
            ;;    
    esac

    # Define list of input files and extend normalization option
    input="-i"
    for lib in ${libraries}
    do
        name_mapped="${read_type}_on_${ref_type}"
        map_dir="${lib}/mapped_${name_mapped}"
        sam_file="${map_dir}/${lib}_${name_mapped}.sam"
        sorted_bam="${sam_file%.sam}_sorted.bam"
        if [ ! -e ${sorted_bam} -o ! -e ${sorted_bam}.bai ]
        then
            echo -e "\nMaking a sorted and indexed ${sorted_bam} file."
            sam2indexedbam.sh ${sam_file} || error_exit "sam2indexedbam.sh failed"
        else
            echo -e "\nUsing existing sorted and indexed ${sorted_bam} file."
        fi
        input="${input} ${sorted_bam}"
        summaries="summaries/${lib}_on_${genome}_results.xls"
        case "${norm}" in
            "mappers")
                norm_file="${lib}/mapped_${genome}/${lib}_on_${genome}_mappers.nbseq"
                if [ ! -e ${norm_file} ]
                then
                    awk -F "\t" '$1=="mappers"{print $2}' ${summaries} > ${norm_file}
                fi
                ;;
            "mi")
                norm_file="${lib}/mapped_${genome}/${lib}_on_${genome}_miRNA.nbseq"
                if [ ! -e ${norm_file} ]
                then
                    awk -F "\t" '$1=="miRNA_reads"{print $2}' ${summaries} > ${norm_file}
                fi
                ;;
            "mt")
                norm_file="${lib}/mapped_${genome}/${lib}_on_${genome}_mt.nbseq"
                if [ ! -e ${norm_file} ]
                then
                    awk '$2~"@mt"{print $1}' ${lib}/mapped_reads_${genome}/${lib}_on_${genome}_rRNA.fasta ${lib}/mapped_reads_${genome}/${lib}_on_${genome}_tRNA.fasta ${lib}/mapped_reads_${genome}/${lib}_on_${genome}_CDS.fasta | sort | uniq | wc -l > ${norm_file}
                    #cat ${lib}/mapped_reads_${genome}/${lib}_on_${genome}_rRNA.fasta ${lib}/mapped_reads_${genome}/${lib}_on_${genome}_tRNA.fasta ${lib}/mapped_reads_${genome}/${lib}_on_${genome}_CDS.fasta | grep "^>" | awk '{print $1}' | sort | uniq | wc -l > ${norm_file}
                fi
                ;;
            "endosi")
                norm_file="${lib}/mapped_${genome}/${lib}_on_${genome}_siRNA.nbseq"
                if [ ! -e ${norm_file} ]
                then
                    awk -F "\t" '$1=="siRNA_reads"{print $2}' ${summaries} > ${norm_file}
                fi
                ;;
            "none")
                norm_file=""
                ;;
            *)
                norm_file="${lib}/mapped_strict_${genome}/${lib}_strict_on_${genome}_piRNA_cluster${norm}.nbseq"
                if [ ! -e ${norm_file} ]
                then
                    echo "One of the normalization files is missing."
                    echo "Aborting"
                    exit 1
                fi
                ;;    
        esac
        normalization="${normalization} ${norm_file}"
    done


    mkdir -p ${pileups_dir}

    if [ ${mm} == "nomm" ]
    then
        build_pileups.py ${input} ${normalization} ${by} \
            -y "${legend}" \
            -d ${pileups_dir} \
            -r ${ref} ${noF} ${noR} \
            --nomm --no_legend --linewidth 0.2
    else
        build_pileups.py ${input} ${normalization} ${by} \
            -y "${legend}" \
            -d ${pileups_dir} \
            -r ${ref} ${noF} ${noR} \
            --linewidth 0.2
    fi
done

exit 0
