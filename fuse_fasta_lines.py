#!/usr/bin/env python
"""
usage: fuse_fasta_lines.py [options]

This script will takes a fasta file as input and outputs the corresponding fasta sequences where lines have been fused.

available options:

    --help: prints this help text and exits.

    --in_file <file>: to indicate the name of the file containing the list of the fasta files
    to concatenate.


"""


import getopt
import os
opj = os.path.join
opb = os.path.basename
import re
import sys
write = sys.stdout.write
import warnings
def formatwarning(message, category, filename, lineno, line):
    return "%s:%s: %s:%s" % (filename, lineno, category.__name__, message)
warnings.formatwarning = formatwarning


##############################################################################################
# For better traceability, options and global variables should be stored as class attributes #
##############################################################################################

class Globals(object):
    """This object holds global variables."""

class Options(object):
    """This object contains the values of the global options."""
    in_file = None

#################
# main function #
#################

def main():
    try:
        opts, args = getopt.getopt(sys.argv[1:], "io:", ["help", "in_file="])
    except getopt.GetoptError:
        sys.stderr.write(__doc__)
        sys.exit(1)
    for option, argument in opts:
        if option == "--help":
            write(__doc__)
            sys.exit()
        if option == "--in_file":
            Options.in_file = argument

    with open(Options.in_file, "r") as f:
        headers = []
        # first header to be found should not have an extra "\n" before
        first = True
        for line in f:
            line = line.strip()
            if line.startswith(">"):
                headers.append(re.sub("\n", "", line[1:]))
                if first:
                    write(">%s\n" % re.sub("^\s*", "", headers[-1]))
                    first = False
                else:
                    write("\n>%s\n" % re.sub("^\s*", "", headers[-1]))
            else:
                write(re.sub("\n", "", line))

    write("\n")
    return 0

if __name__ == "__main__":
    sys.exit(main())
