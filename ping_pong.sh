#!/usr/bin/env bash
# vim: set fileencoding=<utf-8> :
# Usage: ping_pong.sh <library_name> <G_species> <ref_name> <num_slices> <slice_size>

# To configure according to the local installation:
genomes_path=${GENOMES}

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}


# library name
name=${1}
echo "Library: ${name}"

genome=${2}
echo "Genome: ${genome}"

# Reference on which to compute ping-pong
# "all" (or "tot" or "global") or the name of a transposable element (or equivalent like "P_lArB_transgene")
ref_name=${3}

num_slices=${4}
# number of reads to include in each slice on which the ping-pong is measured
# the goal is to have something comparable between libraries
slice_size=${5}
# maximum read size
# If this environment variable is not defined, use 30 as default value
[ MAX_LEN ] || MAX_LEN="30"

# Overlap between F and R partners
overlap=10

#mkdir -p ${name}/ping_pong
#pp_dir="${name}/ping_pong_${num_slices}_${slice_size}"
pp_dir="${name}/ping_pong_${num_slices}_slices"
mkdir -p ${pp_dir}

#echo "This script requires a file containing the number to use to normalize the read counts.\n(This could be resulting from the use of map_and_count_unique.sh)"

# Note: This section is not active unless a 6th (arbitrary) argument is given
# TODO: handle this more cleanly
if [ $# = 6 ]
then
    seq_dir=${name}/mapped_reads_${genome}

    echo -e "This script requires that potential piRNA reads have already been identified and stored in ${seq_dir}/${name}_18-${MAX_LEN}_on_${genome}_pi-si-TE-3UTR.fasta and ${seq_dir}/${name}_23-${MAX_LEN}_on_${genome}_piRNA_candidate.fastq\n(map_and_annotate_small.sh generates such files.)\nNote that these are not exactly the same sets of reads: piRNA_candidates include reads that were not mapped or annotated."
    # Extracting potential piRNA reads
    mkdir -p ${name}/piRNA_reads

    # Note: The reads are not the same corpus as those used for the co-mapping method.
    # Clean reads are those that have only A, C, G or T
    echo "fastq2fasta.py -m 23 -M ${MAX_LEN} -c ${seq_dir}/${name}_18-${MAX_LEN}_on_${genome}_pi-si-TE-3UTR.fasta > piRNA_reads/${name}_on_${genome}_pi-si-TE-3UTR_23-${MAX_LEN}_clean.fasta"
    fastq2fasta.py -m 23 -M ${MAX_LEN} -c ${seq_dir}/${name}_18-${MAX_LEN}_on_${genome}_pi-si-TE-3UTR.fasta > ${name}/piRNA_reads/${name}_on_${genome}_pi-si-TE-3UTR_23-${MAX_LEN}_clean.fasta || error_exit "clean piRNA reads extraction failed"
    # Counting the number of clean reads
    cmd="grep -c \"^>\" piRNA_reads/${name}_on_${genome}_pi-si-TE-3UTR_23-${MAX_LEN}_clean.fasta > piRNA_reads/${name}_on_${genome}_pi-si-TE-3UTR_23-${MAX_LEN}_clean.nbreads"
    echo ${cmd}
    eval ${cmd} || error_exit "counting clean reads failed"
    #echo "cat piRNA_reads/${name}_on_${genome}_pi-si-TE-3UTR_23-${MAX_LEN}_clean.fasta | grep \"^>\" | wc -l > piRNA_reads/${name}_on_${genome}_pi-si-TE-3UTR_23-${MAX_LEN}_clean.nbreads"
    #cat ${name}/piRNA_reads/${name}_on_${genome}_pi-si-TE-3UTR_23-${MAX_LEN}_clean.fasta | grep "^>" | wc -l > ${name}/piRNA_reads/${name}_on_${genome}_pi-si-TE-3UTR_23-${MAX_LEN}_clean.nbreads


    echo "Attempt to calculate some ping-pong stats based on perfect reverse-complement read matching on 10 nt"
    cmd="ping_pong.py -b 100 -o ping_pong -s ${slice_size} ${name}/piRNA_reads/${name}_on_${name}_pi-si-TE-3UTR_23-${MAX_LEN}_clean.fasta > ${pp_dir}/${name}_sumproduct.txt"
    echo ${cmd}
    eval ${cmd} || error_exit "ping_pong.py failed"
    #ping_pong.py -b 100 -o ping_pong -s ${slice_size} ${name}/piRNA_reads/${name}_on_${name}_pi-si-TE-3UTR_23-${MAX_LEN}_clean.fasta > ${name}/ping_pong/${name}_sumproduct.txt || error_exit "ping_pong.py failed"

fi

############################
# re-mapping on the genome #
############################
# Note: cannot re-use results of map_on_canonical_set.sh because things have to be done from slices of "random" piRNA candidates.
# mapping on the genome -k 10: a read may map at several locations.
# The number of places where a read has been considered as mapper needs to be preserved in the fastq header of
# mapped_reads_${genome}/${name}/${name}_18-${MAX_LEN}_on_${genome}_pi-si-TE-3UTR.fastq
# Use this to weight the alignments of that read in the canonical TEs:
# A read that maps at several places is not as reliably attributed at each of these places that a read mapping at one of these places only.
# One may not re-count the number of mappings of a read if one sorts the .bam by reference. And the number of mappings of a read on the genome is not the same as on the canonical TEs.
# Problem: when the alignments of a read are made on a same "class" of copies of a same TE and remnants in piRNA clusters, one should actually not weight.

# Other approach:
# Take all that is not something else:
# unmapped_${genome}/${name}/${name}_22-29_not_on_${genome}.fastq
# ${seq_dir}/${name}_22-29_on_${genome}_not_annotated.fastq
# ${seq_dir}/${name}_22-29_on_${genome}_low_score.fastq
# ${seq_dir}/${name}_22-29_on_${genome}_pi-si-TE-3UTR.fastq
# Concatenate in one .fastq
# cat unmapped_${genome}/${name}/${name}_22-29_not_on_${genome}.fastq ${seq_dir}/${name}_22-29_on_${genome}_not_annotated.fastq ${seq_dir}/${name}_22-29_on_${genome}_low_score.fastq ${seq_dir}/${name}_22-29_on_${genome}_pi-si-TE-3UTR.fastq > piRNA_reads/${name}_piRNA_candidates.fastq
# Map on genome + canonical TEs
# Sort by reference
# Look for reverse-compl co-mappers, shifted by 9 for each reference.
# As soon as a read is implicated in at least a co-mapping, it is identified as having a partner.
# Note: in the pipeline of Claudia Armenise, the signal is computed taking the proportion of 9-shifted over all shifted.
# Proportion of pp or multiplicative "signal" ?

# If one wants to work on slices of a given size, there must be no bias between slices.
# Since piRNA candidates come from 4 distinct files, the files cannot simply be concatenated. They must be shuffled.
# One solution is to directly generate a file containing piRNA candidates during read annotation. The reads then appear in the order they appeared in the sequencing data (except non-mappers?)
# -> ${seq_dir}/${name}_18-${MAX_LEN}_on_${genome}_piRNA_candidate.fast{a,q}
#mapped_reads_${genome}/${name}/${name}_18-${MAX_LEN}_on_${genome}_piRNA_candidate.fastq


# Determining piRNA candidates file and name to give to the mapping
####################################################################

if [ ${ref_name} == "all" -o ${ref_name} == "global" -o ${ref_name} == "tot" ]
then
    # Global ping-pong
    seq_dir=${name}/mapped_reads_${genome}
    candidates_full=${seq_dir}/${name}_23-${MAX_LEN}_on_${genome}_piRNA_candidate.fastq
    ref="${genome}_and_TE"
else
    # Reference-wise ping-pong
    seq_dir="${name}/reads_piRNA_candidate_on_TE"
    candidates_full=${seq_dir}/${name}_piRNA_candidate_on_${ref_name}.fastq
    ref="${ref_name}"
fi

# Checking compatibility of slicing scheme with number of source piRNA
#######################################################################

if [ ! -e ${candidates_full} ]
then
    nb_lines=`zcat ${candidates_full}.gz | wc -l`
else
    nb_lines=`cat ${candidates_full} | wc -l`
fi
nb_candidates=`echo "${nb_lines}/4" | bc`

to_take=`echo "${num_slices}*${slice_size}" | bc`
if [ ${to_take} -gt ${nb_candidates} ]
then
    error_exit "Not enough reads in ${candidates_full} (${nb_candidates}) to take ${num_slices} of ${slice_size} from it."
fi


# Mapping on genome + canonical TE (complemented genome)
#########################################################

echo -e "\nTaking ${num_slices} slices of ${slice_size} reads from ${candidates_full} (that contains ${nb_candidates} reads) to search for ping-pongers."
lines_in_slice=`echo "${slice_size}*4" | bc`
mkdir -p "${pp_dir}/${ref_name}"
# To avoid overwriting when running several analyses, use sub-directories with slice size as name
pp_dir_this_ref="${pp_dir}/${ref_name}/${slice_size}"
mkdir -p "${pp_dir_this_ref}"
# This file will accumulate the counts over the slices
# Each line will be for a slice
# Each line will have 3 tab-separated columns
# nb_pp, nb_nopp and freq_pp
results_file="${pp_dir_this_ref}/${name}_pp_counts.txt"
echo -e "#nb_pp\tnb_nopp\tfreq_pp" > ${results_file}
#pp_ref_counts_file="${pp_dir}/${ref_name}/${name}_pp_ref_counts.txt"
#echo -e "#ref\ttotal\ttotal_F\ttotal_R\tF_0mm\tR_0mm\tF_mm\tR_mm" > ${pp_ref_counts_file}
#nopp_ref_counts_file="${pp_dir}/${ref_name}/${name}_nopp_ref_counts.txt"
#echo -e "#ref\ttotal\ttotal_F\ttotal_R\tF_0mm\tR_0mm\tF_mm\tR_mm" > ${nopp_ref_counts_file}
mkdir -p "${pp_dir}/reads"
# The following directory could be considered temporary data and deleted at the end
input_slices_dir="${pp_dir}/reads/${slice_size}"
mkdir -p "${input_slices_dir}"
for slice in `seq 1 ${num_slices}`
do
    candidates="${input_slices_dir}/piRNA_${ref_name}_${slice}.fastq"
    if [ ! -e ${candidates} -a ! -e ${candidates}.gz ]
    then
        end_line=`echo "${slice}*${lines_in_slice}" | bc`
        if [ ! -e ${candidates_full} ]
        then
            #zcat ${candidates_full}.gz | head -${end_line} | tail -${lines_in_slice} > ${candidates}
            cmd="zcat ${candidates_full}.gz | head -${end_line} | tail -${lines_in_slice} > ${candidates}"
        else
            #cat ${candidates_full} | head -${end_line} | tail -${lines_in_slice} > ${candidates}
            cmd="cat ${candidates_full} | head -${end_line} | tail -${lines_in_slice} > ${candidates}"
        fi
        echo ${cmd}
        eval ${cmd} || error_exit "Candidates extraction failed"
    fi
    map_dir="${pp_dir_this_ref}/mapped_piRNA_candidate_${slice}_on_${ref}"
    mkdir -p ${map_dir}
    sam_file="${map_dir}/${name}_piRNA_candidate_${slice}_on_${ref}.sam"
    bam_file="${sam_file%.sam}_sorted.bam"

    if [ ! -e ${bam_file} ]
    then
        if [ ! -e ${sam_file} ]
        then
            # If not "all", "global" or "tot", ${ref_name} must be in that genome index
            genome_db="${genomes_path}/${genome}/${genome}_and_TE"
            if [ -e ${candidates} ]
            then
                # No need to use a compressed version
                map_loosely_on_refs.sh ${name} ${pp_dir_this_ref} ${candidates} "piRNA_candidate_${slice}" ${genome_db} "${ref}" "no_process" || error_exit "map_loosely_on_ref.sh failed"
            else
                # At least a compressed version should exist
                map_loosely_on_refs.sh ${name} ${pp_dir_this_ref} ${candidates}.gz "piRNA_candidate_${slice}" ${genome_db} "${ref}" "no_process" || error_exit "map_loosely_on_ref.sh failed"
            fi
        else
            echo "Using already existing ${sam_file}"
        fi
        echo -e "\nSorting and indexing alignments."
        #indexed=""
        #while [ ! ${indexed} ]
        #do
        #    sam2indexedbam.sh ${sam_file} && indexted="OK"
        #done || error_exit "sam2indexedbam.sh failed"
        sam2indexedbam.sh ${sam_file} || error_exit "sam2indexedbam.sh failed"
        # Save space
        # Actually, this could be useful to apply the signature.py
        # script from Christophe Antoniewski
        signature_file="${pp_dir_this_ref}/${name}_piRNA_candidate_${slice}_on_${ref}_1-80_signature.xls"
        cmd="signature.py \
            --input ${sam_file} \
            --inputFormat sam \
            --minquery 23 \
            --maxquery ${MAX_LEN} \
            --mintarget 23 \
            --maxtarget ${MAX_LEN} \
            --minscope 1 \
            --maxscope 80 \
            --outputOverlapDataframe ${signature_file} \
            --referenceGenome ${genome_db} \
            --extract_index \
            --graph global \
            --rcode /dev/null"
        #cmd="signature.py ${sam_file} 23 ${MAX_LEN} 1 80 ${signature_file}"
        echo ${cmd}
        eval ${cmd} || error_exit "signature.py failed"
        signature_file="${pp_dir_this_ref}/${name}_piRNA_candidate_${slice}_on_${ref}_1-21_signature.xls"
        cmd="signature.py \
            --input ${sam_file} \
            --inputFormat sam \
            --minquery 23 \
            --maxquery ${MAX_LEN} \
            --mintarget 23 \
            --maxtarget ${MAX_LEN} \
            --minscope 1 \
            --maxscope 21 \
            --outputOverlapDataframe ${signature_file} \
            --referenceGenome ${genome_db} \
            --extract_index \
            --graph global \
            --rcode /dev/null"
        #cmd="signature.py ${sam_file} 23 ${MAX_LEN} 1 21 ${signature_file}"
        echo ${cmd}
        eval ${cmd} || error_exit "signature.py failed"
        #rm -f ${sam_file}
    else
        echo -e "\nUsing already existing ${bam_file}"
    fi

    pp_file="${pp_dir_this_ref}/${name}_pp_${slice}.fastq"
    nopp_file="${pp_dir_this_ref}/${name}_nopp_${slice}.fastq"
    # tab-delimited file with one line and 3 columns: nb_pp, nb_nopp, freq_pp
    result_file="${pp_dir_this_ref}/${name}_pp_counts_${slice}.txt"
    if [ ! -e ${pp_file} -o ! -e ${nopp_file} ]
    then
        echo -e "\nIdentifying reads having ping-pong partners by co-mapping on the genome or on canonical TE elements."
        if [ ! -e ${candidates} ]
        then
            gunzip -k ${candidates}.gz
        fi
        #cmd="bam2pingpong.py -s ${candidates} -o ${result_file} -p ${pp_file} -n ${nopp_file} ${bam_file}"
        cmd="bam2pingpong.py --overlap ${overlap} -s ${candidates} -o ${result_file} -p ${pp_file} -n ${nopp_file} ${bam_file}"
        echo "${cmd}"
        eval ${cmd} || error_exit "${cmd} failed"
        #echo -e "\nRecording counts in file ${results_file}"
        #cat ${result_file} >> ${results_file}
        #rm -f ${result_file}
    fi
    echo -e "\nRecording counts in file ${results_file}"
    cat ${result_file} >> ${results_file}
    #rm -f ${result_file}
    # Saving space
    if [ ! -e ${candidates}.gz ]
    then
        pigz ${candidates} || error_exit "pigz failed"
    else
        # A compressed version exists
        rm -f ${candidates}
    fi
    echo -e "\nReads with ping-pong partners in slice ${slice} are in ${pp_file}."
    echo -e "Reads without ping-pong partners in slice ${slice} are in ${nopp_file}."
    # Actually, a mapping should be done also for pre-assigned piRNAs because the R/F information is absent
    if [ ${ref_name} == "all" -o ${ref_name} == "global" -o ${ref_name} == "tot" ]
    then
        #refs="${genomes_path}/${genome}/Annotations/canonical_TE"
        #refs="${genomes_path}/${genome}/Annotations/Annotations_Chambeyron/TE_plus"
        refs="${genomes_path}/${genome}/Annotations/TE_plus"
        # pp
        echo -e "\nMapping the ping-pongers from slice ${slice} on canonical TEs."
        map_loosely_on_refs.sh ${name} ${pp_dir_this_ref} ${pp_file} "pp_${slice}" ${refs} "TE" || error_exit "map_loosely_on_ref.sh failed"
        # nopp
        echo -e "\nMapping the non ping-pongers from slice ${slice} on canonical TEs."
        map_loosely_on_refs.sh ${name} ${pp_dir_this_ref} ${nopp_file} "nopp_${slice}" ${refs} "TE" || error_exit "map_loosely_on_ref.sh failed"
        refs="${genomes_path}/${genome}/Annotations/pi_clusters"
        # pp
        echo -e "\nMapping the ping-pongers from slice ${slice} on piRNA clusters."
        map_loosely_on_refs.sh ${name} ${pp_dir_this_ref} ${pp_file} "pp_${slice}" ${refs} "pi_clusters" || error_exit "map_loosely_on_ref.sh failed"
        # nopp
        echo -e "\nMapping the non ping-pongers from slice ${slice} on piRNA clusters."
        map_loosely_on_refs.sh ${name} ${pp_dir_this_ref} ${nopp_file} "nopp_${slice}" ${refs} "pi_clusters" || error_exit "map_loosely_on_ref.sh failed"
    else
        refs="${genomes_path}/${genome}/Annotations/individual_TE/${ref}"
        # pp
        echo -e "\nMapping the ping-pongers from slice ${slice} on ${ref}."
        map_loosely_on_refs.sh ${name} ${pp_dir_this_ref} ${pp_file} "pp_${slice}" ${refs} ${ref} || error_exit "map_loosely_on_ref.sh failed"
        #awk -v ref=${ref} '$1==ref{print $2}' ${pp_dir}/${ref}/histos_pp_${slice}_on_${ref}/${name}_pp_${slice}_on_${ref}_ref_counts.txt >> ${pp_ref_counts_file}
        # nopp
        echo -e "\nMapping the non ping-pongers from slice ${slice} on ${ref}."
        map_loosely_on_refs.sh ${name} ${pp_dir_this_ref} ${nopp_file} "nopp_${slice}" ${refs} ${ref} || error_exit "map_loosely_on_ref.sh failed"
        #awk -v ref=${ref} '$1==ref{print $2}' ${pp_dir}/${ref}/histos_nopp_${slice}_on_${ref}/${name}_nopp_${slice}_on_${ref}_ref_counts.txt >> ${nopp_ref_counts_file}
    fi
done

exit 0
