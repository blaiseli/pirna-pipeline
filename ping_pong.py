#!/usr/bin/env python
"""This script will perform a ping-pong analysis."""
# TODO: Implement sum of product of partner counts

import argparse
import sys
import warnings
def formatwarning(message, category, filename, lineno, line):
    return "%s:%s: %s:%s" % (filename, lineno, category.__name__, message)
warnings.formatwarning = formatwarning

import os

from Bio.Seq import reverse_complement
import numpy as np
import matplotlib.pyplot as plt

#from random import getrandbits, randint, seed
from random import getrandbits

def letter2bits_old(letter):
    """Returns a base-4 number for a given base.
"""
    if letter == "A":
        return "0"
    elif letter == "C":
        return "3"
    elif letter == "G":
        return "2"
    elif letter == "T":
        return "1"
    else:
        raise NotImplementedError, "Letter %s not valid." % letter

def hash_DNA_old(seq):
    """Use letter2bits to compute a number between 0 and 2^20 - 1
to be used as a hash for a 10-mer."""
    # "ACGT" -> "00111001"
    # "TGCA" -> "01101100"
    return int("".join(letter2bits_old(base) for base in seq), 4)

def letter2bits(letter):
    """Returns a base-4 number for a given base.
"""
    if letter == "A":
        return 0
    elif letter == "C":
        return 3
    elif letter == "G":
        return 2
    elif letter == "T":
        return 1
    else:
        raise NotImplementedError, "Letter %s not valid." % letter

#def hash_DNA(seq):
#    """Use letter2bits to compute a number between 0 and 2^20 - 1
#to be used as a hash for a 10-mer."""
#    # "ACGT" -> "00111001"
#    # "TGCA" -> "01101100"
#    h = 0
#    for letter in seq:
#        h = (4 * h) + letter2bits(letter) 
#    return h
def hash_DNA(seq):
    """Compute a number between 0 and 4^n - 1 to be used as a hash for a n-mer."""
    h = 0
    for letter in seq:
        if letter == "A":
            h = (4 * h) + 0
        elif letter == "C":
            h = (4 * h) + 1
        elif letter == "G":
            h = (4 * h) + 2
        elif letter == "T":
            h = (4 * h) + 3
        else:
            raise NotImplementedError, "Letter %s not valid." % letter
    return h

#def reverse_hash(num, seq_len):
#    """This function returns the hash of the reverse complement
#of the sequence of length <seq_len> whose hash is <num>."""
#    h = 0
#    while seq_len:
#        seq_len -= 1
#        h += (3 - (num % 4)) * (4 ** seq_len)
#        num = num / 4
#    return h

def reverse_hash(num):
    """This function returns the hash of the reverse complement
of the sequence of length 10 whose hash is <num>."""
    seq_len = 10
    h = 0
    while seq_len:
        seq_len -= 1
        h += (3 - (num % 4)) * (4 ** seq_len)
        num = num / 4
    return h

val2letter = {0 : "A", 1 : "C", 2 : "G", 3 : "T"}

def unhash(num):
    letters = []
    for i in xrange(10):
        letters.append(val2letter[num % 4])
        num = num / 4
    return "".join(reversed(letters))

class Globals(object):
    """This class can be used to hold global variables as its class attributes."""

def main():
    parser = argparse.ArgumentParser(description=__doc__,
            formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("piRNA_reads",
            help="File in fasta format containing the reads identified as piRNAs.",
            type=argparse.FileType("r"))
    #parser.add_argument("ratio_hist",
    #        help="File where to write the ratios of ping-pong partners.",
    #        type=argparse.FileType("w"))
    parser.add_argument("-b", "--bins",
            help="Number of bins to use for the histogram.",
            type=int,
            default=25)
    parser.add_argument("-s", "--slicesize",
            help="Number of reads to include in each slice.",
            type=int)
    #parser.add_argument("-p", "--histplot",
    #        help="File in which to plot the histogram.",
    #        type=argparse.FileType("w"))
    #parser.add_argument("-c", "--cumplot",
    #        help="File in which to plot the histogram.",
    #        type=argparse.FileType("w"))
    parser.add_argument("-o", "--output_dir",
            help="Directory in which to output the results",
            type=str)
    #parser.add_argument("-s", "--switch",
    #        help="This is an optional boolean switch.", action="store_true")
    args = parser.parse_args()
    basename = os.path.basename(args.piRNA_reads.name)[:-6]
    # To avoid repeatedly calling these as string methods:
    #strip = str.rstrip
    #split = str.split
    counts = np.zeros(4**10, dtype="uint32")
    rand_counts = np.zeros(4**10, dtype="uint32")
    ## key: slice
    ## value: set of random ints
    #rand_int_dict = {}
    ## table giving the hash of the reverse complementary,
    ## initially filled with an impossible hash value
    hash2revcompl = np.full(4**10, 4**10, dtype="uint32")
    # Better make it initially "empty"?
    # table giving the hash of the reverse complementary,
    # initially filled with an impossible hash value
    #hash2revcompl = np.empty(4**10, dtype="uint32")
    # set of encountered 10-mers
    # in the form of pairs (hash, sequence)
    ten_mers = set([])
    ## debug
    #seed(12345)
    ##
    rand_ints = set([])
    slicenum = 1
    randseqs_filename = os.path.join(args.output_dir, "%s_%d_randseqs.txt" % (basename, slicenum))
    rand_seqs_file = open(randseqs_filename, "w")
    counts_filename = os.path.join(args.output_dir, "%s_%d_counts.txt" % (basename, slicenum))
    counts_file = open(counts_filename, "w")
    nbreads = 0
    for line in args.piRNA_reads:
        if line[0] != ">":
            if nbreads == args.slicesize:
                # Report
                assert sum(counts) == args.slicesize
                assert sum(rand_counts) == args.slicesize
                ratios = np.empty(len(ten_mers), dtype="float")
                products = np.empty(len(ten_mers), dtype="uint64")
                # Existence of a partner is a wrong way of measuring ping-pong
                # because large random data will tend to have a high value.
                has_partner = 0
                for (i, (int_val, ten_mer)) in enumerate(ten_mers):
                    # debug
                    assert ten_mer == reverse_complement(unhash(hash2revcompl[int_val]))
                    #
                    ten_mer_counts = counts[int_val]
                    partner_counts = counts[hash2revcompl[int_val]]
                    products[i] = ten_mer_counts * partner_counts
                    if products[i] != 0:
                        has_partner += 1
                    if ten_mer_counts < partner_counts:
                        ratios[i] = 1.0 * ten_mer_counts / partner_counts
                    else:
                        ratios[i] = 1.0 * partner_counts / ten_mer_counts

                rand_ratios = np.empty(len(rand_ints), dtype="float")
                rand_products = np.empty(len(rand_ints), dtype="uint64")
                rand_has_partner = 0
                #rand_int_dict[slicenum] = sorted(rand_ints)
                #for (i, int_val) in enumerate(rand_int_dict[slicenum]):
                for (i, int_val) in enumerate(sorted(rand_ints)):
                    ten_mer = unhash(int_val)
                    partner = unhash(hash2revcompl[int_val])
                    ten_mer_counts = rand_counts[int_val]
                    partner_counts = rand_counts[hash2revcompl[int_val]]
                    rand_products[i] = ten_mer_counts * partner_counts
                    if rand_products[i] != 0:
                        rand_has_partner += 1
                    counts_file.write("%s\t%d\t%s\t%d\t%d\n" % (ten_mer, ten_mer_counts, partner, partner_counts, rand_products[i]))
                    if ten_mer_counts < partner_counts:
                        rand_ratios[i] = 1.0 * ten_mer_counts / partner_counts
                    else:
                        rand_ratios[i] = 1.0 * partner_counts / ten_mer_counts
                fig = plt.figure()
                ax = fig.add_subplot(111)
                plt.xlim((0.0, 1.0))
                plt.ylim((0.0, 1.0))
                ax.hist(ratios, args.bins, normed = 1, histtype="stepfilled", color="blue", cumulative=True)
                ax.hist(rand_ratios, args.bins, normed = 1, histtype="step", color="red", cumulative=True)
                cumplot_filename = os.path.join(args.output_dir, "%s_%d_ppratio_cumplot.png" % (basename, slicenum))
                plt.savefig(cumplot_filename)
                ax.cla()
                sys.stdout.write("%d\t%d\t%d\t%f\t%f\n" % (slicenum, sum(products), sum(rand_products), (1.0 * has_partner) / len(ten_mers), (1.0 * rand_has_partner) / len(rand_ints)))
                ## debug
                #sys.stderr.write(", ".join(["%d:%d" % (slnum, len(truc)) for slnum, truc in rand_int_dict.items()]))
                ##
                # Reset
                ten_mers = set([])
                ## debug
                #seed(12345)
                ##
                rand_ints = set([])
                counts = np.zeros(4**10, dtype="uint32")
                rand_counts = np.zeros(4**10, dtype="uint32")
                slicenum += 1
                rand_seqs_file.close()
                randseqs_filename = os.path.join(args.output_dir, "%s_%d_randseqs.txt" % (basename, slicenum))
                rand_seqs_file = open(randseqs_filename, "w")
                counts_filename = os.path.join(args.output_dir, "%s_%d_counts.txt" % (basename, slicenum))
                counts_file = open(counts_filename, "w")
                nbreads = 0
            #try:
            # Record
            ten_mer = line[:10]
            int_val = hash_DNA(ten_mer)
            ten_mers.add((int_val, ten_mer))
            if not (counts[int_val] or rand_counts[int_val]):
                # Set the address of the reverse complementary
                # first time this 10-mer is seen
                hash2revcompl[int_val] = hash_DNA(reverse_complement(ten_mer))
                assert hash2revcompl[int_val] == reverse_hash(int_val)
                #counts[int_val] = 1
            #else:
                #counts[int_val] += 1
            counts[int_val] += 1
            rand_int = getrandbits(20)
            # Slower
            #rand_int = randint(0, 2**20 - 1)
            rand_seqs_file.write("%s\n" % unhash(rand_int))
            rand_ints.add(rand_int)
            if not (rand_counts[rand_int] or counts[rand_int]):
                #ten_mers.add((int_val, ten_mer))
                # Set the address of the reverse complementary
                # first time this 10-mer is seen
                hash2revcompl[rand_int] = reverse_hash(rand_int)
                #rand_counts[rand_int] = 1
            #else:
                #rand_counts[rand_int] += 1
            rand_counts[rand_int] += 1
            nbreads += 1
            #except NotImplementedError:
            #    pass
    rand_seqs_file.close()
    counts_file.close()
    
    
    #hist, bin_edges = np.histogram(ratios, bins=args.bins)
    #for i in range(len(hist)):
    #    args.ratio_hist.write("%f\t%d\n" % (bin_edges[i], hist[i]))
    
    #if args.histplot:
    #    fig = plt.figure()
    #    ax = fig.add_subplot(111)
    #    ax.hist(ratios, args.bins)
    #    plt.savefig(args.histplot)
    
    #plt.show()

if __name__ == "__main__":
    sys.exit(main())
