#!/usr/bin/env bash
# Usage: do_plot_histos.sh <read_type> <ref_type> <normalization_type> <mm/nomm> <element_list> <lib1> [<lib2>,...]
# <read_type> can be "pi-si-TE-3UTR", "piRNA_candidate", "piRNA_unique" or "siRNA_candidate"
# <ref_type> can be "TE", "pi_clusters", or "3UTR"
# <normalization_type> can be "none", "mappers", "mi", "mt", "endosi", or a piRNA cluster number
# <mm/nomm> is used to select whether to use colour intensity to distinguish degrees of mismatches.
# <elements_list> is a file containing a list of elements, one per line, for which the histograms have to be plotted.
# It can also be a single reference name, or "tot".
# <lib1>, <lib2>, etc. are the library names for which the histograms are to be plotted.
# For each element in the list, the histograms for all these libraries will be at the same vertical scale.

genome="D_melanogaster"

read_type=${1}
ref_type=${2}
norm=${3}
data_histos_dir="histos_${read_type}_on_${ref_type}"
plot_histos_dir="histos_${read_type}_on_${ref_type}_norm_${3}"
mm=${4}
elements_list=${5}
libraries=${@:6}

# Environment variable to scale by a certain number of normalizers
if [ ! ${BY} -o ${BY}=="1" ]
then
    by=""
    BY=""
else
    by="-b ${BY}"
    if [[ ${BY} == "1000000" ]]
    then
        BY="1M "
    else
        BY="${BY} "
    fi
fi

if [ -e ${elements_list} ]
then
    references=`cat ${elements_list}`
else
    references=${elements_list}
fi
for ref in ${references}
do
    # Define y-axis legend and prepare normalization option
    case "${norm}" in
        "mappers")
            legend="${ref} reads by ${BY}mappers"
            normalization="-n"
            ;;
        "mi")
            legend="${ref} reads by ${BY}miRNA reads"
            normalization="-n"
            ;;
        "mt")
            legend="${ref} reads by ${BY}mitochondrial reads"
            normalization="-n"
            ;;
        "endosi")
            legend="${ref} reads by ${BY}endogenous siRNA reads"
            normalization="-n"
            ;;
        "none")
            if [[ ${BY} ]]
            then
                echo "BY environment variable not valid without normalization"
                exit 1
            fi
            legend="${ref} reads"
            normalization=""
            ;;
        *)
            case "${norm}" in
                "1")
                    legend="${ref} reads by ${BY}42AB reads"
                    ;;
                "8")
                    legend="${ref} reads by ${BY}flamenco reads"
                    ;;
                *)
                    legend="${ref} reads by ${BY}piRNA cluster ${norm} reads"
                    ;;
            esac
            normalization="-n"
            ;;    
    esac

    # Define list of input files and extend normalization option
    input="-i"
    for lib in ${libraries}
    do
        histo_file=${lib}/${data_histos_dir}/${lib}_${read_type}_on_${ref}_lengths.txt
        input="${input} ${histo_file}"
        summaries="summaries/${lib}_on_${genome}_results.xls"
        case "${norm}" in
            "mappers")
                norm_file="${lib}/mapped_${genome}/${lib}_on_${genome}_mappers.nbseq"
                if [ ! -e ${norm_file} ]
                then
                    awk -F "\t" '$1=="mappers"{print $2}' ${summaries} > ${norm_file}
                fi
                ;;
            "mi")
                norm_file="${lib}/mapped_${genome}/${lib}_on_${genome}_miRNA.nbseq"
                if [ ! -e ${norm_file} ]
                then
                    awk -F "\t" '$1=="miRNA_reads"{print $2}' ${summaries} > ${norm_file}
                fi
                ;;
            "mt")
                norm_file="${lib}/mapped_${genome}/${lib}_on_${genome}_mt.nbseq"
                if [ ! -e ${norm_file} ]
                then
                    awk '$2~"@mt"{print $1}' ${lib}/mapped_reads_${genome}/${lib}_on_${genome}_rRNA.fasta ${lib}/mapped_reads_${genome}/${lib}_on_${genome}_tRNA.fasta ${lib}/mapped_reads_${genome}/${lib}_on_${genome}_CDS.fasta | sort | uniq | wc -l > ${norm_file}
                    #cat ${lib}/mapped_reads_${genome}/${lib}_on_${genome}_rRNA.fasta ${lib}/mapped_reads_${genome}/${lib}_on_${genome}_tRNA.fasta ${lib}/mapped_reads_${genome}/${lib}_on_${genome}_CDS.fasta | grep "^>" | awk '{print $1}' | sort | uniq | wc -l > ${norm_file}
                fi
                ;;
            "endosi")
                norm_file="${lib}/mapped_${genome}/${lib}_on_${genome}_siRNA.nbseq"
                if [ ! -e ${norm_file} ]
                then
                    awk -F "\t" '$1=="siRNA_reads"{print $2}' ${summaries} > ${norm_file}
                fi
                ;;
            "none")
                norm_file=""
                ;;
            *)
                norm_file="${lib}/mapped_strict_${genome}/${lib}_strict_on_${genome}_piRNA_cluster${norm}.nbseq"
                if [ ! -e ${norm_file} ]
                then
                    echo "One of the normalization files is missing."
                    echo "Aborting"
                    exit 1
                fi
                ;;    
        esac
        normalization="${normalization} ${norm_file}"
    done

    mkdir -p ${plot_histos_dir}

    if [ ${mm} == "nomm" ]
    then
        plot_length_histos.py ${input} ${normalization} ${by} \
            -d ${plot_histos_dir} -y "${legend}" --nomm
    else
        plot_length_histos.py ${input} ${normalization} ${by} \
            -d ${plot_histos_dir} -y "${legend}"
    fi
done

exit 0
