#!/usr/bin/env python
# vim: set fileencoding=<utf-8> :
"""This script reads two sorted and indexed .bam file and searches for regions
where mapped reads are "more abundant" in the second than in the first."""


import argparse
import sys
WRITE = sys.stdout.write
import warnings
def formatwarning(message, category, filename, lineno, line):
    """Formats warning messages."""
    return "%s:%s: %s:%s" % (filename, lineno, category.__name__, message)
warnings.formatwarning = formatwarning

import os
OPJ = os.path.join
OPB = os.path.basename

from collections import deque
from itertools import izip
import pysam
import numpy as np
from scipy import stats

# http://chriskiehl.com/article/parallelism-in-one-line/
from multiprocessing import Pool

def pile_counter(read_range):
    """Generates a read counting function."""
    min_size, max_size = read_range
    min_score = Options.min_score
    if min_size <= max_size:
        if Options.max_edit_proportion == -1.0:
            # Do not filter on edit proportion
            def select(read):
                """Filter function.
                The read has to match at its first base."""
                ali = read.alignment
                #rlen = ali.rlen
                rlen = ali.query_length
                # Introduced this in revision bdee801f3449adcee138692596d639ef811f1f57
                #sys.exit(2)
                return (
                    #read.is_head
                    (min_size <= rlen <= max_size)
                    and (ali.opt("AS") >= min_score)
                    and (ali.opt("NM") <= Options.max_mm))
        else:
            def select(read):
                """Filter function.
                The read has to match at its first base."""
                ali = read.alignment
                #rlen = ali.rlen
                rlen = ali.query_length
                return (
                    #read.is_head
                    (ali.opt("NM") <= rlen * Options.max_edit_proportion)
                    and (min_size <= rlen <= max_size)
                    and (ali.opt("AS") >= min_score)
                    and (ali.opt("NM") <= Options.max_mm))
    else:
        # Don't filter by size
        if Options.max_edit_proportion == -1.0:
            # Do not filter on edit proportion
            def select(read):
                """Filter function.
                The read has to match at its first base."""
                ali = read.alignment
                return (
                    #read.is_head
                    (ali.opt("AS") >= min_score)
                    and (ali.opt("NM") <= Options.max_mm))
        else:
            def select(read):
                """Filter function.
                The read has to match at its first base."""
                ali = read.alignment
                #rlen = ali.rlen
                rlen = ali.query_length
                return (
                    #read.is_head
                    (ali.opt("NM") <= rlen * Options.max_edit_proportion)
                    and (ali.opt("AS") >= min_score)
                    and (ali.opt("NM") <= Options.max_mm))

    def read_counter(pile, no_secondary=False):
        """Generates counts of forward and reverse 5' mapping reads."""
        ref_pos = pile.reference_pos
        # number of selected 5' aligned reads from the pile
        fwd_count = 0
        rev_count = 0
        for read in pile.pileups:
            ali = read.alignment
            # Note: read.qpos and read.is_head are not reliable indications
            # for 5'end alignment testing
            #if read.qpos == 0:
            #    assert read.is_head
            if select(read):
                if not (no_secondary and ali.is_secondary):
                    if ali.is_reverse:
                        if ali.reference_end - 1 == ref_pos:
                            rev_count += 1
                    else:
                        if ali.reference_start == ref_pos:
                            fwd_count += 1
        return ref_pos, fwd_count, rev_count
    return read_counter


def count_reads_in(bamfile, counter, ref, start, end, no_secondary=True):
    Globals.debug("%s:%d-%d (%d)\n" % (ref, start, end, (1 + end - start)))
    counts = 0
    #if no_secondary:
    #    piles = bamfile.pileup(ref, start, end, stepper="pass")
    #else:
    #    piles = bamfile.pileup(ref, start, end)
    piles = bamfile.pileup(ref, start, end)
    for pile in piles:
        pos, fwd, rev = counter(pile, no_secondary)
        Globals.debug("%d\r" % pos)
        counts += (fwd + rev)
    Globals.debug("\n")
    return counts


def count_total(pair):
    (bamfile_name, no_secondary) = pair
    counter = pile_counter(Options.read_range)
    chromosomes = {}
    total_count = 0
    Globals.debug("Counting valid reads in %s:\n" % bamfile_name)
    with pysam.AlignmentFile(bamfile_name) as samfile:
        for chrom, chrom_len in izip(samfile.references, samfile.lengths):
            if chrom in Globals.exclude_refs:
                # Skip references we're not interested in.
                #print "Skipping %s" % chrom
                continue
            else:
                Globals.debug("\n%s (%d)\n" % (chrom, chrom_len))
                chromosomes[chrom] = chrom_len
                total_count += count_reads_in(samfile, counter, chrom, 0, chrom_len, no_secondary)
    Globals.debug("%d\n" % total_count)
    if total_count < 5:
        warnings.warn("Only %d valid reads in %s\n" % (total_count, bamfile_name))
    return total_count, chromosomes

#####################################
# For better traceability,          #
# options and global variables      #
# can be stored as class attributes #
#####################################


class Globals(object):
    """This object holds global variables."""
    cluster_names = {"cluster1": "42AB", "cluster8": "flamenco"}


class Options(object):
    """This object contains the values of the global options."""
    y_axis = None
    # default value set so that pile_counter will not
    # select alignments based on edit proportion
    max_edit_proportion = None
    max_mm = None
    min_score = None
    # min and max lengths for a read to be counted
    # default value set so that pile_counter
    # will not select alignments based on read size
    read_range = None

#################
# main function #
#################


def main():
    """Main function of the program."""
    WRITE("%s\n" % " ".join(sys.argv))
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        "ref_file",
        help="Sorted and indexed reference .bam file.")
    parser.add_argument(
        "other_file",
        help="Sorted and indexed .bam file to compare with the reference.")
    parser.add_argument(
        "--tot_ref",
        type=int,
        help="Total number of valid reads to use for first library.")
    parser.add_argument(
        "--tot_other",
        type=int,
        help="Total number of valid reads to use for second library.")
    parser.add_argument(
        "--bed_higher",
        required=True,
        help="File in which to write bed information about zones "
        "where the read density is higher in the second bam file.",
        type=argparse.FileType("w"))
    parser.add_argument(
        "--bed_lower",
        required=True,
        help="File in which to write bed information about zones "
        "where the read density is lower in the second bam file.",
        type=argparse.FileType("w"))
    #parser.add_argument(
    #    "-d", "--densities",
    #    required=True,
    #    help="File in which to write densities.",
    #    type=argparse.FileType("w"))
    parser.add_argument(
        "-l", "--zone_length",
        type=int,
        default=1000,
        help="Length of the windows on which to assess differences in mapper density.")
    # default value set so that pile_counter will not
    # select alignments based on edit proportion
    parser.add_argument(
        "--max_edit_proportion",
        type=float,
        default=-1.0,
        help="Maximum proportion of edits for an alignment to be recorded."
        "\nThe default is to accept any proportion of edits.")
    parser.add_argument(
        "--max_mm",
        type=int,
        default=0,
        help="Maximum number of mismatches for an alignment to be counted.")
    parser.add_argument(
        "--min_score",
        type=int,
        default=-200,
        help="Minimum mapping score for an alignment to be recorded."
        "\nThe default value is -200.")
    # min and max lengths for a read to be counted
    # default value set so that pile_counter
    # will not select alignments based on read size
    parser.add_argument(
        "--read_range",
        nargs=2,
        type=int,
        default=[0, -1],
        help="Minimum and maximum lengths of the reads to take into account."
        "\nThe default is to take all sizes into account.")
    parser.add_argument(
        "-v", "--verbose",
        action="store_true",
        help="Generate more output.")
    parser.add_argument(
        "--no_secondary",
        action="store_true",
        help="Do not count secondary alignments of reads.\n"
        "A read will only be counted once, but where it will be counted "
        "may be partially arbitrary.")
    parser.add_argument(
        "-e", "--exclude_refs",
        help="File containing a list of references to exclude from the profiling.\n"
        "One reference per line.",
        type=argparse.FileType("r"))
    args = parser.parse_args()
    z_len = args.zone_length

    # Give global access to some options
    Options.max_edit_proportion = args.max_edit_proportion
    Options.max_mm = args.max_mm
    Options.min_score = args.min_score
    Options.read_range = tuple(args.read_range)

    if args.verbose:
        def debug(msg):
            sys.stderr.write(msg)
    else:
        def debug(msg):
            pass
    Globals.debug = staticmethod(debug)

    # To avoid repeatedly calling these as string methods:
    strip = str.rstrip
    #split = str.split
    counter = pile_counter(Options.read_range)
    # Make the list of references we want to skip.
    Globals.exclude_refs = []
    if args.exclude_refs:
        with args.exclude_refs as exclusion_file:
            for line in exclusion_file:
                ref_name = strip(line)
                if ref_name:
                    if ref_name[0] != "#":
                        Globals.exclude_refs.append(ref_name)
    Globals.exclude_refs = set(Globals.exclude_refs)
    # key: chromosome name
    # value: chromosome length
    chromosomes = {}
    if args.tot_ref:
        # Use user-provided total.
        tot_ref = args.tot_ref
        with pysam.Samfile(args.ref_file) as samfile:
            for chrom, chrom_len in izip(samfile.references, samfile.lengths):
                if chrom in Globals.exclude_refs:
                    # Skip references we're not interested in.
                    #print "Skipping %s" % chrom
                    continue
                else:
                    chromosomes[chrom] = chrom_len
        tot_other = args.tot_other
        with pysam.Samfile(args.other_file) as samfile:
            for chrom, chrom_len in izip(samfile.references, samfile.lengths):
                if chrom in Globals.exclude_refs:
                    # Skip references we're not interested in.
                    #print "Skipping %s" % chrom
                    continue
                else:
                    if chrom in chromosomes:
                        assert chromosomes[chrom] == chrom_len
                    else:
                        chromosomes[chrom] = chrom_len
    else:
        # Count total
        pool = Pool(2)
        counts_and_chromosomes = pool.map(count_total, [(args.ref_file, args.no_secondary), (args.other_file, args.no_secondary)])
        pool.close()
        pool.join()
        tot_ref, chromosomes = counts_and_chromosomes[0]
        tot_other, other_chromosomes = counts_and_chromosomes[0]
        for chrom, chrom_len in other_chromosomes.items():
            if chrom in chromosomes:
                assert chromosomes[chrom] == chrom_len
            else:
                chromosomes[chrom] = chrom_len

    with pysam.Samfile(args.ref_file) as ref_file, pysam.Samfile(args.other_file) as other_file, args.bed_lower as bed_lower, args.bed_higher as bed_higher:
        for (chrom, chrom_len) in sorted(chromosomes.items()):
            WRITE("Chromosome: %s (len: %d)\n" % (chrom, chrom_len))
            start = 0
            end = z_len - 1
            while end < chrom_len:
                counts_ref = count_reads_in(ref_file, counter, chrom, start, end, args.no_secondary)
                counts_other = count_reads_in(other_file, counter, chrom, start, end, args.no_secondary)
                if ((1.0 * counts_other) / (counts_other + tot_other)) > ((1.0 * counts_ref) / (counts_ref + tot_ref)): 
                    write_to_bed = bed_higher.write
                elif ((1.0 * counts_other) / (counts_other + tot_other)) < ((1.0 * counts_ref) / (counts_ref + tot_ref)):
                    write_to_bed = bed_lower.write
                else:
                    #print counts_other, counts_ref
                    write_to_bed = None
                if write_to_bed is not None:
                    # TODO: stats
                    if (counts_ref >= 5) and (counts_other >= 5):
                        chi2, p_value, dof, expected_freqs = stats.chi2_contingency([[counts_ref, tot_ref - counts_ref], [counts_other, tot_other - counts_other]])
                        if p_value < 0.05:
                            bed_line = "%s\t%d\t%d\tdifferent_read_density\t%d\n" % (chrom, start, end + 1, int(1000 * (1 - p_value)))
                            write_to_bed(bed_line)
                            WRITE(bed_line)
                    elif counts_other >= 5:
                        oddsratio, p_value = stats.fisher_exact([[counts_ref, tot_ref - counts_ref], [counts_other, tot_other - counts_other]])
                        if p_value < 0.05:
                            bed_line = "%s\t%d\t%d\tdifferent_read_density\t%d\n" % (chrom, start, end + 1, int(1000 * (1 - p_value)))
                            write_to_bed(bed_line)
                            WRITE(bed_line)
                    else:
                        # We do not want to report a peak in a zone with so few reads.
                        pass
                else:
                    # No need to make stats when both zones represent
                    # the same proportion of reads of the total of their bam file.
                    pass
                # Jump to next window
                start += z_len
                end += z_len

    return 0

if __name__ == "__main__":
    sys.exit(main())
