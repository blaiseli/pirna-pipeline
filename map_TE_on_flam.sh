#!/bin/sh
TE=$1
#
# Mapping TE
#
echo "bowtie2 --seed 123 -t -k 10 -L 6 -i S,1,0.8 -N 1 --mp 4,2 --rdg 5,2 --rfg 5,2 -x /Genomes/D_melanogaster/Annotations/Annotations_Chambeyron/pi_clusters -U mapped_reads_repbase/drosha_dmt_on_${TE}_F.fastq -S mapped_pi_clusters/drosha_dmt_on_${TE}_F_on_pi_clusters.sam"
bowtie2 --seed 123 -t -k 10 -L 6 -i S,1,0.8 -N 1 --mp 4,2 --rdg 5,2 --rfg 5,2 -x /Genomes/D_melanogaster/Annotations/Annotations_Chambeyron/pi_clusters -U mapped_reads_repbase/drosha_dmt_on_${TE}_F.fastq -S mapped_pi_clusters/drosha_dmt_on_${TE}_F_on_pi_clusters.sam

echo "bowtie2 --seed 123 -t -k 10 -L 6 -i S,1,0.8 -N 1 --mp 4,2 --rdg 5,2 --rfg 5,2 -x /Genomes/D_melanogaster/Annotations/Annotations_Chambeyron/pi_clusters -U mapped_reads_repbase/drosha_dmt_on_${TE}_R.fastq -S mapped_pi_clusters/drosha_dmt_on_${TE}_R_on_pi_clusters.sam"
bowtie2 --seed 123 -t -k 10 -L 6 -i S,1,0.8 -N 1 --mp 4,2 --rdg 5,2 --rfg 5,2 -x /Genomes/D_melanogaster/Annotations/Annotations_Chambeyron/pi_clusters -U mapped_reads_repbase/drosha_dmt_on_${TE}_R.fastq -S mapped_pi_clusters/drosha_dmt_on_${TE}_R_on_pi_clusters.sam

echo "bowtie2 --seed 123 -t -k 10 -L 6 -i S,1,0.8 -N 1 --mp 4,2 --rdg 5,2 --rfg 5,2 -x /Genomes/D_melanogaster/Annotations/Annotations_Chambeyron/pi_clusters -U mapped_reads_repbase/drosha_dw_on_${TE}_F.fastq -S mapped_pi_clusters/drosha_dw_on_${TE}_F_on_pi_clusters.sam"
bowtie2 --seed 123 -t -k 10 -L 6 -i S,1,0.8 -N 1 --mp 4,2 --rdg 5,2 --rfg 5,2 -x /Genomes/D_melanogaster/Annotations/Annotations_Chambeyron/pi_clusters -U mapped_reads_repbase/drosha_dw_on_${TE}_F.fastq -S mapped_pi_clusters/drosha_dw_on_${TE}_F_on_pi_clusters.sam

echo "bowtie2 --seed 123 -t -k 10 -L 6 -i S,1,0.8 -N 1 --mp 4,2 --rdg 5,2 --rfg 5,2 -x /Genomes/D_melanogaster/Annotations/Annotations_Chambeyron/pi_clusters -U mapped_reads_repbase/drosha_dw_on_${TE}_R.fastq -S mapped_pi_clusters/drosha_dw_on_${TE}_R_on_pi_clusters.sam"
bowtie2 --seed 123 -t -k 10 -L 6 -i S,1,0.8 -N 1 --mp 4,2 --rdg 5,2 --rfg 5,2 -x /Genomes/D_melanogaster/Annotations/Annotations_Chambeyron/pi_clusters -U mapped_reads_repbase/drosha_dw_on_${TE}_R.fastq -S mapped_pi_clusters/drosha_dw_on_${TE}_R_on_pi_clusters.sam

echo "bowtie2 --seed 123 -t -k 10 -L 6 -i S,1,0.8 -N 1 --mp 4,2 --rdg 5,2 --rfg 5,2 -x /Genomes/D_melanogaster/Annotations/Annotations_Chambeyron/pi_clusters -U mapped_reads_repbase/drosha_wt_on_${TE}_F.fastq -S mapped_pi_clusters/drosha_wt_on_${TE}_F_on_pi_clusters.sam"
bowtie2 --seed 123 -t -k 10 -L 6 -i S,1,0.8 -N 1 --mp 4,2 --rdg 5,2 --rfg 5,2 -x /Genomes/D_melanogaster/Annotations/Annotations_Chambeyron/pi_clusters -U mapped_reads_repbase/drosha_wt_on_${TE}_F.fastq -S mapped_pi_clusters/drosha_wt_on_${TE}_F_on_pi_clusters.sam

echo "bowtie2 --seed 123 -t -k 10 -L 6 -i S,1,0.8 -N 1 --mp 4,2 --rdg 5,2 --rfg 5,2 -x /Genomes/D_melanogaster/Annotations/Annotations_Chambeyron/pi_clusters -U mapped_reads_repbase/drosha_wt_on_${TE}_R.fastq -S mapped_pi_clusters/drosha_wt_on_${TE}_R_on_pi_clusters.sam"
bowtie2 --seed 123 -t -k 10 -L 6 -i S,1,0.8 -N 1 --mp 4,2 --rdg 5,2 --rfg 5,2 -x /Genomes/D_melanogaster/Annotations/Annotations_Chambeyron/pi_clusters -U mapped_reads_repbase/drosha_wt_on_${TE}_R.fastq -S mapped_pi_clusters/drosha_wt_on_${TE}_R_on_pi_clusters.sam

echo "bowtie2 --seed 123 -t -k 10 -L 6 -i S,1,0.8 -N 1 --mp 4,2 --rdg 5,2 --rfg 5,2 -x /Genomes/D_melanogaster/Annotations/Annotations_Chambeyron/pi_clusters -U mapped_reads_repbase/Gtsf1_hom_on_${TE}_F.fastq -S mapped_pi_clusters/Gtsf1_hom_on_${TE}_F_on_pi_clusters.sam"
bowtie2 --seed 123 -t -k 10 -L 6 -i S,1,0.8 -N 1 --mp 4,2 --rdg 5,2 --rfg 5,2 -x /Genomes/D_melanogaster/Annotations/Annotations_Chambeyron/pi_clusters -U mapped_reads_repbase/Gtsf1_hom_on_${TE}_F.fastq -S mapped_pi_clusters/Gtsf1_hom_on_${TE}_F_on_pi_clusters.sam

echo "bowtie2 --seed 123 -t -k 10 -L 6 -i S,1,0.8 -N 1 --mp 4,2 --rdg 5,2 --rfg 5,2 -x /Genomes/D_melanogaster/Annotations/Annotations_Chambeyron/pi_clusters -U mapped_reads_repbase/Gtsf1_hom_on_${TE}_R.fastq -S mapped_pi_clusters/Gtsf1_hom_on_${TE}_R_on_pi_clusters.sam"
bowtie2 --seed 123 -t -k 10 -L 6 -i S,1,0.8 -N 1 --mp 4,2 --rdg 5,2 --rfg 5,2 -x /Genomes/D_melanogaster/Annotations/Annotations_Chambeyron/pi_clusters -U mapped_reads_repbase/Gtsf1_hom_on_${TE}_R.fastq -S mapped_pi_clusters/Gtsf1_hom_on_${TE}_R_on_pi_clusters.sam

echo "bowtie2 --seed 123 -t -k 10 -L 6 -i S,1,0.8 -N 1 --mp 4,2 --rdg 5,2 --rfg 5,2 -x /Genomes/D_melanogaster/Annotations/Annotations_Chambeyron/pi_clusters -U mapped_reads_repbase/Gtsf1_het_on_${TE}_F.fastq -S mapped_pi_clusters/Gtsf1_het_on_${TE}_F_on_pi_clusters.sam"
bowtie2 --seed 123 -t -k 10 -L 6 -i S,1,0.8 -N 1 --mp 4,2 --rdg 5,2 --rfg 5,2 -x /Genomes/D_melanogaster/Annotations/Annotations_Chambeyron/pi_clusters -U mapped_reads_repbase/Gtsf1_het_on_${TE}_F.fastq -S mapped_pi_clusters/Gtsf1_het_on_${TE}_F_on_pi_clusters.sam

echo "bowtie2 --seed 123 -t -k 10 -L 6 -i S,1,0.8 -N 1 --mp 4,2 --rdg 5,2 --rfg 5,2 -x /Genomes/D_melanogaster/Annotations/Annotations_Chambeyron/pi_clusters -U mapped_reads_repbase/Gtsf1_het_on_${TE}_R.fastq -S mapped_pi_clusters/Gtsf1_het_on_${TE}_R_on_pi_clusters.sam"
bowtie2 --seed 123 -t -k 10 -L 6 -i S,1,0.8 -N 1 --mp 4,2 --rdg 5,2 --rfg 5,2 -x /Genomes/D_melanogaster/Annotations/Annotations_Chambeyron/pi_clusters -U mapped_reads_repbase/Gtsf1_het_on_${TE}_R.fastq -S mapped_pi_clusters/Gtsf1_het_on_${TE}_R_on_pi_clusters.sam
#
# making the .bam files
cd mapped_pi_clusters
for f in *${TE}*.sam; do echo "$f converted into ${f%.sam}.bam"; samtools view -bS $f > ${f%.sam}.bam; done
for f in *${TE}*.bam; do echo "Sorting $f"; samtools sort $f ${f%.bam}_sorted; done 
for f in *${TE}*_sorted.bam; do echo "Building index for $f"; samtools index $f; done
#
# merging the results
#
samtools merge drosha_dmt_on_${TE}_on_pi_clusters.bam drosha_dmt_on_${TE}_F_on_pi_clusters_sorted.bam drosha_dmt_on_${TE}_R_on_pi_clusters_sorted.bam
samtools merge drosha_dw_on_${TE}_on_pi_clusters.bam drosha_dw_on_${TE}_F_on_pi_clusters_sorted.bam drosha_dw_on_${TE}_R_on_pi_clusters_sorted.bam
samtools merge drosha_wt_on_${TE}_on_pi_clusters.bam drosha_wt_on_${TE}_F_on_pi_clusters_sorted.bam drosha_wt_on_${TE}_R_on_pi_clusters_sorted.bam
samtools merge Gtsf1_hom_on_${TE}_on_pi_clusters.bam Gtsf1_hom_on_${TE}_F_on_pi_clusters_sorted.bam Gtsf1_hom_on_${TE}_R_on_pi_clusters_sorted.bam
samtools merge Gtsf1_het_on_${TE}_on_pi_clusters.bam Gtsf1_het_on_${TE}_F_on_pi_clusters_sorted.bam Gtsf1_het_on_${TE}_R_on_pi_clusters_sorted.bam
samtools index drosha_dmt_on_${TE}_on_pi_clusters.bam 
samtools index drosha_dw_on_${TE}_on_pi_clusters.bam 
samtools index drosha_wt_on_${TE}_on_pi_clusters.bam 
samtools index Gtsf1_hom_on_${TE}_on_pi_clusters.bam 
samtools index Gtsf1_het_on_${TE}_on_pi_clusters.bam 
cd ..
#
#
build_pileups.py --in_file mapped_pi_clusters/drosha_dmt_on_${TE}_on_pi_clusters.bam --pileup_dir pileups_pi_clusters --normalize `cat trimmed/drosha_dmt_trimmed.nbseq` --read_range 23,29 --max_edit_proportion 0.0 --references cluster8
build_pileups.py --in_file mapped_pi_clusters/drosha_dw_on_${TE}_on_pi_clusters.bam --pileup_dir pileups_pi_clusters --normalize `cat trimmed/drosha_dw_trimmed.nbseq` --read_range 23,29 --max_edit_proportion 0.0 --references cluster8
build_pileups.py --in_file mapped_pi_clusters/drosha_wt_on_${TE}_on_pi_clusters.bam --pileup_dir pileups_pi_clusters --normalize `cat trimmed/drosha_wt_trimmed.nbseq` --read_range 23,29 --max_edit_proportion 0.0 --references cluster8
build_pileups.py --in_file mapped_pi_clusters/Gtsf1_hom_on_${TE}_on_pi_clusters.bam --pileup_dir pileups_pi_clusters --normalize `cat trimmed/Gtsf1_hom_trimmed.nbseq` --read_range 23,29 --max_edit_proportion 0.0 --references cluster8
build_pileups.py --in_file mapped_pi_clusters/Gtsf1_het_on_${TE}_on_pi_clusters.bam --pileup_dir pileups_pi_clusters --normalize `cat trimmed/Gtsf1_het_trimmed.nbseq` --read_range 23,29 --max_edit_proportion 0.0 --references cluster8
#
plot_pileups.py --in_files pileups_pi_clusters/drosha_dmt_on_${TE}_23-29_on_flamenco_pileup.txt,pileups_pi_clusters/drosha_dw_on_${TE}_23-29_on_flamenco_pileup.txt,pileups_pi_clusters/drosha_wt_on_${TE}_23-29_on_flamenco_pileup.txt,pileups_pi_clusters/Gtsf1_hom_on_${TE}_23-29_on_flamenco_pileup.txt,pileups_pi_clusters/Gtsf1_het_on_${TE}_23-29_on_flamenco_pileup.txt --pileup_dir pileups_pi_clusters --formats png
