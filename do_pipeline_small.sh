#!/usr/bin/env bash
# Usage: do_pipeline_small.sh <library_name> <adapter> <trim_nb> [<annotation_file>]
# raw_data/<library_name>/<library_name>.fastq should exist

# To configure according to the local installation:
genomes_path=${GENOMES}

# Library name:
name="${1}"

# Adapter:
adapt="${2}"

# Number of extra bases to trim on both sides:
trim="${3}"

# Genome to use
#genome=${4}
genome="D_melanogaster"

# Number of processors to use for some of the mappings:
#nprocs=${5}
nprocs="1"

# Example:
#name="aub_RNAi"
#adapt="TCGTATGCCGTCTTCTGCTTG"
#trim="0"
#genome="D_melanogaster"
#nprocs="4"

# There is a default value for the annotation file
if [ $# = 4 ]
then
    annotation_file=${5}
else
    #annotation_file="${genomes_path}/${genome}/Annotations/Flybase/${genome}_flybase_splitmRNA_annot.txt"
    annotation_file="${genomes_path}/${genome}/Annotations/Flybase/${genome}_flybase_splitmRNA_and_TE_annot.bed"
fi

PLACE=`pwd`
echo "The analyses will take place from within the base directory ${PLACE}"

mkdir -p "logs/${name}"
log="logs/${name}/${name}.stdout"
err="logs/${name}/${name}.stderr"

echo "Logs will be found in the two following files:"
echo ${log}
echo ${err}

if [ ! -e ${annotation_file} ]
then
    echo "The annotation file ${annotation_file} is missing." > ${err}
    echo "Aborting." >> ${err}
    echo "Analysis prematurely stopped. Check the logs."
    exit 1
fi

if [ ! -e ${genomes_path}/${genome}/${genome}.rev.2.bt2 ]
then
    echo "The genome bowtie2 index files ${genomes_path}/${genome}/${genome}* seem to be missing." > ${err}
    echo "Aborting." >> ${err}
    echo "Analysis prematurely stopped. Check the logs."
    exit 1
fi

#pipeline.sh ${name} ${adapt} ${trim} ${genome} ${nprocs} > ${log} 2> ${err}
pipeline_small.sh "${name}" "${adapt}" "${trim}" "${genome}" "${nprocs}" > ${log} 2> ${err}
echo "pipeline_small.sh finished. Check the logs."

exit 0
