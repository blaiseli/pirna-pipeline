#!/usr/bin/env bash
# Usage: extract_sam_region.sh <mapping_sorted.bam> <region>

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}


sorted_bam=${1}
region=${2}

samtools view -H ${sorted_bam} ${region} > ${sorted_bam%_sorted.bam}_on_${region}.sam || error_exit "SAM header extraction failed"
samtools view ${sorted_bam} ${region} >> ${sorted_bam%_sorted.bam}_on_${region}.sam || error_exit "aligned read extraction failed"


exit 0
