#!/bin/sh
# Usage: do_map_on_TE.sh <library_name>
#TODO: update paths once scripts are relocated

norm_file=mapped_dm/${1}_trimmed_on_D_melanogaster_siRNA.nbseq
echo "mapping the 'bona fide' reads on the canonical transposable elements set"
./map_on_canonical_set.sh ${1} ${norm_file}
