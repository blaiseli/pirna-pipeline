#!/usr/bin/env python
"""
usage: concat_genomes.py [options]

This script will take a series of fasta files and generate two concatenated fasta files.
"Fused_<org_name>_genome.fa" gathers genome portions (scaffolds, chromosomes, etc.).
"ANTI_<org_name>_genome.fa" contains the reverse-complement of these portions
(with portion names prefixed by "ANTI" in the fasta headers).

The program will use as input fasta files the files listed in the file provided
by the option --in_files.
If this option is not set, the program will try to use all files form the
current working directory ending in ".fa", ".fas" or ".fasta".

<org_name> is specified either by the option --org_name either assuming the
name of the current directory is the meaningful information (typically, if you
named the directory to reflect the name of the organism the genome of which you
are concatenating.)

available options:

    --help: prints this help text and exits.

    --in_files <file>: to indicate the name of the file containing the list of the fasta files
    to concatenate.

    --org_name <name>: to indicate the base name to use to name the genome
    files, for instance you could use "X_laevis" if you are concatenating
    Xenopus laevis genomic data, and the output files will be
    "Fused_X_laevis_genome.fa" and ANTI_X_laevis_genome.fa.
    Default is to use the name of the current working directory (don't worry
    about the warning that will be displayed if that's your choice).

    --exclude <chr1>,<chr2>,...: to exclude some chromosomes from the concatenated genomes.

"""


import getopt
import os
opj = os.path.join
opb = os.path.basename
import re
import sys
write = sys.stdout.write
import warnings
def formatwarning(message, category, filename, lineno, line):
    return "%s:%s: %s:%s" % (filename, lineno, category.__name__, message)
warnings.formatwarning = formatwarning

def oui(question):
    """oui(question) returns True if the user answers yes to the question
    <question>."""    
    return (raw_input("\n" + question + " (y/n) ") in ["O", "o", "Oui", "oui", "OUI", "Y", "y", "Yes", "yes", "YES", "J", "j", "Ja", "ja", "JA", "S", "s", "Si", "si", "SI", "D", "d", "Da", "da", "DA", "T", "t", "Taip", "taip", "TAIP"])


def complement(base):
    """This function gives the complementary base of <base>, preserving capitalization."""
    if base == "A":
        return "T"
    elif base == "C":
        return "G"
    elif base == "G":
        return "C"
    elif base == "T":
        return "A"
    elif base == "N":
        return "N"
    elif base == "a":
        return "t"
    elif base == "c":
        return "g"
    elif base == "g":
        return "c"
    elif base == "t":
        return "a"
    elif base == "n":
        return "n"
    else:
        #write("%s not a valid base\n" % base)
        #sys.exit(1)
        assert base == "N" or base == "n", "doesn't know how what should be the complement of base %s\n" % base

##############################################################################################
# For better traceability, options and global variables should be stored as class attributes #
##############################################################################################

class Globals(object):
    """This object holds global variables."""

class Options(object):
    """This object contains the values of the global options."""
    excluded = []
    in_files = None
    org_name = None

#################
# main function #
#################

def main():
    try:
        opts, args = getopt.getopt(sys.argv[1:], "io:", ["help", "exclude=", "in_files=", "org_name="])
    except getopt.GetoptError:
        sys.stderr.write(__doc__)
        sys.exit(1)
    for option, argument in opts:
        if option == "--help":
            write(__doc__)
            sys.exit()
        if option == "--exclude":
            Options.excluded = argument.split(",")
        if option == "--in_files":
            Options.in_files = argument
        if option == "--org_name":
            Options.org_name = argument
    
    if Options.org_name is None:
        # Assume that the directory name corresponds to the organism.
        Options.org_name = opb(os.getcwd())
        warnings.warn("--org_name option not set. %s will be used as base for the output files.\n" % Options.org_name)

    if Options.in_files is None:
        fas_files = [f for f in sorted(os.listdir(".")) if (f.endswith(".fa") or f.endswith(".fas") or f.endswith(".fasta"))]

        write("Here are the files ending in .fa, .fas or .fasta found in the current directory:\n")
        write(", ".join(fas_files))
        write("\n")

        if not oui("concatenate them all?"):
            write("Please provide a file containing the list of the files to use, then, using the '--in_files' option.\n")
            return 0
    else:
        with open(Options.in_files, "r") as f:
            fas_files = []
            for line in f:
                fas_files += [name.strip() for name in line.split()]

    with open("Fused_%s_genome.fa" % Options.org_name, "w") as concat:

        headers = []
        # first header to be found should not have an extra "\n" before
        first = True

        for f_name in fas_files:
            # list of cases to complete later
            #if f_name.endswith(".fa") and (f_name.startswith("chr") or f_name.startswith("scaffold_")):
            # fasta headers found in the file
            skip = False
            with open(f_name, "r") as f:
                for line in f:
                    if line.startswith(">"):
                        seq_name = re.sub("\n", "", line[1:])
                        if seq_name not in Options.excluded:
                            headers.append(seq_name)
                            if first:
                                concat.write(">%s\n" % re.sub("^\s*", "", headers[-1]))
                                first = False
                            else:
                                concat.write("\n>%s\n" % re.sub("^\s*", "", headers[-1]))
                            skip = False
                        else:
                            skip = True
                    else:
                        if not skip:
                            concat.write(re.sub("\n", "", line))

        concat.write("\n")
    # now read it to make the reverse-complement
    with open("Fused_%s_genome.fa" % Options.org_name, "r") as concat, open("ANTI_%s_genome.fa" % Options.org_name, "w") as anti:

        # first header to be found should not have an extra "\n" before
        first = True
        num_seq = 0
        for line in concat:
            if line.startswith(">"):
                if first:
                    anti.write(">%s\n" % re.sub("^\s*", "ANTI", headers[num_seq]))
                    first = False
                else:
                    anti.write("\n>%s\n" % re.sub("^\s*", "ANTI", headers[num_seq]))
                num_seq += 1
            else:
                anti.write("".join(reversed(map(complement, re.sub("\n", "", line)))))
        anti.write("\n")
    return 0

if __name__ == "__main__":
    sys.exit(main())
