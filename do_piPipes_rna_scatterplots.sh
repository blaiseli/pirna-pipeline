#!/usr/bin/env bash
# Usage: do_piPipes_rna_scatterplot.sh <lib1> <lib2> <transform_type> [<element_lists> <colours>]
# <transform_type> can be "log2", "log10" or a number (linear scale)
# <element_lists> and <colours> are comma-seprated lists of files and colours respectively.
# The files are lists of elements, one per line.
# The colours are the names of the colours to attribute to the elements in the corresponding file.
# The correspondence between colours and lists of elements is made by shared rank in the comma-separated lists.

if [ ! ${OUT_DIR} ]
then
    scatterdir="scatterplots"
else
    scatterdir="${OUT_DIR}"
fi

if [ ! ${BY} -o ${BY}=="1" ]
then
    by=""
    BY=""
else
    by="-b ${BY}"
    BY="${BY} "
fi

if [ ! ${MIN_COUNTS} ]
then
    MIN_COUNTS="1000"
fi

if [ ! ${MAX_LOGFOLD} ]
then
    max_logfold=""
else
    max_logfold="--max_logfold ${MAX_LOGFOLD}"
fi

if [ ! ${AXIS_FONT} ]
then
    AXIS_FONT="footnotesize"
fi

if [ ! ${MARKER_SIZE} ]
then
    MARKER_SIZE="4"
fi

if [ ! ${TICK_FONT} ]
then
    TICK_FONT="tiny"
fi

if [ ! ${HM_LABEL} ]
then
    HM_LABEL="9"
fi

if [ ! ${CMAP} ]
then
    CMAP=""
else
    CMAP="--cmap ${CMAP}"
fi

if [ ! ${CENTER_CMAP} ]
then
    REMAP=""
else
    REMAP="--remap_cmap"
fi

if [ ! ${PLOT_DIAGONAL} ]
then
    DIAG=""
else
    DIAG="--plot_diagonal"
fi

if [ ! ${NAME_CONV} ]
then
    NAME_CONV=""
else
    if [ -e ${NAME_CONV} ]
    then
        NAME_CONV="--name_conversion ${NAME_CONV}"
    else
        echo "File ${NAME_CONV} not found! Aborting." 1>&2
        exit 1
    fi
fi

if [ ! ${ORDER} ]
then
    ORDER=""
else
    if [ -e ${ORDER} ]
    then
        ORDER="--elements_order ${ORDER}"
    else
        echo "File ${ORDER} not found! Aborting." 1>&2
        exit 1
    fi
fi

if [ ${NO_LINE} ]
then
    NO_LINE="--no_line"
fi

mkdir -p ${scatterdir}

lib1=${1}
lib2=${2}

transform=${3}


if [ $# = 5 ]
then
    el_files=`echo ${4} | tr "," " "`
    element_lists="--element_lists ${el_files}"
    colour_files=`echo ${5} | tr "," " "`
    colours="--colours ${colour_files}"
else
    element_lists=""
    colours=""
fi

col="8"
ref_col="2"

reads="reads"

infile1="piPipes/${lib1}/direct_transcriptome_mapping/results.xprs.normalized"
infile2="piPipes/${lib2}/direct_transcriptome_mapping/results.xprs.normalized"

mkdir -p tmp_data
sed '1s/^/#/' ${infile1} > tmp_data/${lib1}.xprs
sed '1s/^/#/' ${infile2} > tmp_data/${lib2}.xprs

infiles="tmp_data/${lib1}.xprs tmp_data/${lib2}.xprs"

legend1="${lib1} ${reads} (normalized)"
legend2="${lib2} ${reads} (normalized)"
pdffile="${lib1}_vs_${lib2}_${transform}_min_${MIN_COUNTS}"
texfile="${lib1}_vs_${lib2}_${transform}_min_${MIN_COUNTS}_with_names.tex"
foldheat="${lib1}_vs_${lib2}_${transform}_fold_min_${MIN_COUNTS}"

if [[ ${NOHEAT} ]]
then
    foldheat=""
else
    foldheat="-f ${foldheat}"
fi

if [[ ${NOSCATTER} ]]
then
    pdffile=""
else
    pdffile="-p ${pdffile}"
fi

date >> ${scatterdir}/log

echo "--in_files ${infiles} ${colours} ${element_lists}"
echo "${pdffile} ${foldheat} ${CMAP} ${REMAP} ${DIAG} ${max_logfold}"

# pyplot version
plot_scatterplots.py --min_counts ${MIN_COUNTS} ${by} \
    --ref_column ${ref_col} --column $col \
    --in_files ${infiles} ${colours} ${element_lists} \
    --x_axis "${legend1}" \
    --y_axis "${legend2}" \
    -t ${transform} \
    -d ${scatterdir} \
    ${pdffile} ${foldheat} ${CMAP} ${REMAP} ${DIAG} ${max_logfold} \
    --marker_size ${MARKER_SIZE} --hm_label_size ${HM_LABEL} ${NAME_CONV} ${ORDER} \
    >> ${scatterdir}/log \
    || exit 1

if [[ ! ${NOTEX} ]]
then
    # TikZ version, with names appearing as tip tool (in acrobat reader)
    scatterplot_reads.py --min_counts ${MIN_COUNTS} ${by} \
        --ref_column ${ref_col} --column ${col} ${NO_LINE} ${NAME_CONV} \
        --in_files ${infiles} ${colours} ${element_lists} \
        --library_names ${lib1} ${lib2} \
        --x_axis "${legend1}" \
        --y_axis "${legend2}" \
        --x_scale "2.5" \
        --y_scale "2.5" \
        --transform ${transform} \
        --axis_font ${AXIS_FONT} \
        --tick_font ${TICK_FONT} \
        --plot ${scatterdir}/${texfile} \
        >> ${scatterdir}/log \
        || exit 1

    if [ ! -e ${scatterdir}/${texfile} ]
    then
        echo "Missing scatterplot .tex code." 1>&2
        exit 1
    fi

    compile="Yes"
    #compile=""

    if [ ${compile} ]
    then
        curr_dir=`pwd`
        cd ${scatterdir}
        texfot pdflatex ${texfile}
        texfot pdflatex ${texfile}
        cd ${curr_dir}
        echo "Scatterplot written in ${scatterdir}/${texfile%.tex}.pdf"
    fi
fi

exit 0
