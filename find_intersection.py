#!/usr/bin/env python
"""Usage: find_intersection.py <list_1> <list_2>
Finds the elements present in both lists.
The lists have to be provided as text files with one element per line.
The resulting elements are printed in alphabetical order.
"""

import sys

from operator import and_ as intersection
if len(sys.argv) < 3:
    sys.stderr.write("Wrong number of command-line arguments\n")
    sys.stderr.write(__doc__)
    sys.stderr.write("\n")
    sys.exit(1)

sets = []
for filename in sys.argv[1:]:
    with open(filename, "r") as f:
        sets.append(set((line.strip() for line in f)))

for elem in sorted(reduce(intersection, sets)):
    sys.stdout.write("%s\n" % elem)

sys.exit(0)
