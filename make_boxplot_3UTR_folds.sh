#!/bin/sh
#banque1=${1}
#ref1=${2}
#banque2=${3}
#ref2=${4}
#banque3=${5}
#ref3=${6}
#col=$7
#transform=$8
#norm=$9
#genome=${10}
banque1="yb_hom"
ref1="yb_het"
banque2="dmt_771"
ref2="dw_771"
banque3="drosha_dmt"
ref3="drosha_dw"
col=2
transform="log2"
norm="42AB"
genome="D_melanogaster"
plot_dir="boxplots"

mut1="histos_mapped_3UTR/${banque1}/${banque1}_22-29_ref_counts.txt"
cont1="histos_mapped_3UTR/${ref1}/${ref1}_22-29_ref_counts.txt"
mut2="histos_mapped_3UTR/${banque2}/${banque2}_22-29_ref_counts.txt"
cont2="histos_mapped_3UTR/${ref2}/${ref2}_22-29_ref_counts.txt"
mut3="histos_mapped_3UTR/${banque3}/${banque3}_22-29_ref_counts.txt"
cont3="histos_mapped_3UTR/${ref3}/${ref3}_22-29_ref_counts.txt"

case ${norm} in
    mi)
        norm1="mapped_${genome}/${banque1}_on_${genome}_miRNA.nbseq"
        norm1_ref="mapped_${genome}/${ref1}_on_${genome}_miRNA.nbseq"
        norm2="mapped_${genome}/${banque2}_on_${genome}_miRNA.nbseq"
        norm2_ref="mapped_${genome}/${ref2}_on_${genome}_miRNA.nbseq"
        norm3="mapped_${genome}/${banque3}_on_${genome}_miRNA.nbseq"
        norm3_ref="mapped_${genome}/${ref3}_on_${genome}_miRNA.nbseq"
        ;;
    endosi)
        norm1="mapped_${genome}/${banque1}_on_${genome}_siRNA.nbseq"
        norm1_ref="mapped_${genome}/${ref1}_on_${genome}_siRNA.nbseq"
        norm2="mapped_${genome}/${banque2}_on_${genome}_siRNA.nbseq"
        norm2_ref="mapped_${genome}/${ref2}_on_${genome}_siRNA.nbseq"
        norm3="mapped_${genome}/${banque3}_on_${genome}_siRNA.nbseq"
        norm3_ref="mapped_${genome}/${ref3}_on_${genome}_siRNA.nbseq"
        ;;
    42AB)
        norm1="mapped_strict_dm/${banque1}_strict_on_${genome}_piRNA_cluster1.nbseq"
        norm1_ref="mapped_strict_dm/${ref1}_strict_on_${genome}_piRNA_cluster1.nbseq"
        norm2="mapped_strict_dm/${banque2}_strict_on_${genome}_piRNA_cluster1.nbseq"
        norm2_ref="mapped_strict_dm/${ref2}_strict_on_${genome}_piRNA_cluster1.nbseq"
        norm3="mapped_strict_dm/${banque3}_strict_on_${genome}_piRNA_cluster1.nbseq"
        norm3_ref="mapped_strict_dm/${ref3}_strict_on_${genome}_piRNA_cluster1.nbseq"
        ;;
    *)
        norm1="1"
        ref1="1"
        norm2="1"
        ref2="1"
        norm3="1"
        ref3="1"
        ;;
esac
echo ${mut1} ${norm1} 
echo ${cont1} ${norm1_ref} 
echo ${mut2} ${norm2} 
echo ${cont2} ${norm2_ref} 
echo ${mut3} ${norm3} 
echo ${cont3} ${norm3_ref} 
