#!/usr/bin/env bash
# Usage: map_on_pi_clusters.sh <library_name> <G_species>
# Some information helping understanding the script might be found in 00README.txt

# To configure according to the local installation:
genomes_path=${GENOMES}

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}



# library name
name=${1}
echo $name

genome=${2}
echo ${genome}

# file containing the counts used to perform normalizations
#norm_file=${2}
#norm=`cat ${norm_file}`

#ncpu=${3}
# maximum read size
# If this environment variable is not defined, use 30 as default value
[ MAX_LEN ] || MAX_LEN="30"


seq_dir="${name}/mapped_reads_${genome}"


## mapping the unmapped, low quality, mapped but not annotated, and mapped on piRNA clusters, siRNA clusters (for Drosophila ovaries), transposable elements and 3'UTRs
# Now just mapping "piRNA candidates"
# on piRNA clusters (see /extra/Genomes/D_melanogaster/Annotations/Annotations_Chambeyron/README_Annotations_Chambeyron.txt).
#mkdir -p mapped_pi_clusters

picandidates=${seq_dir}/${name}_23-${MAX_LEN}_on_${genome}_piRNA_candidate.fastq.gz
if [ -e ${picandidates%.gz} ]
then
    # Better use already uncompressed version.
    picandidates=${picandidates%.gz}
fi

piuniques=${seq_dir}/${name}_23-${MAX_LEN}_on_${genome}_piRNA_unique.fastq.gz
if [ -e ${piuniques%.gz} ]
then
    # Better use already uncompressed version.
    piuniques=${piuniques%.gz}
fi

sicandidates=${seq_dir}/${name}_21_on_${genome}_siRNA_candidate.fastq.gz
if [ -e ${sicandidates%.gz} ]
then
    # Better use already uncompressed version.
    sicandidates=${sicandidates%.gz}
fi

siuniques=${seq_dir}/${name}_21_on_${genome}_siRNA_unique.fastq.gz
if [ -e ${siuniques%.gz} ]
then
    # Better use already uncompressed version.
    siuniques=${siuniques%.gz}
fi

# Depending on their definition in the annotation module,
# the above piRNA and siRNA candidates may be subsets of pi-si-TE-3UTR.
# The whole set with sizes from 18 to ${MAX_LEN}:
pisiTE3UTR=${seq_dir}/${name}_18-${MAX_LEN}_on_${genome}_pi-si-TE-3UTR.fastq

#refs="/Genomes/${genome}/Annotations/Annotations_Chambeyron/pi_clusters"
#refs="/Genomes/${genome}/Annotations/pi_clusters"
refs="${genomes_path}/${genome}/Annotations/pi_clusters"

echo -e "\nMapping piRNA candidates on piRNA clusters."
map_loosely_on_refs.sh ${name} ${name} ${picandidates} "piRNA_candidate" ${refs} "pi_clusters"

echo -e "\nMapping 23-${MAX_LEN} unique mappers on piRNA clusters."
map_loosely_on_refs.sh ${name} ${name} ${piuniques} "piRNA_unique" ${refs} "pi_clusters"

echo -e "\nMapping siRNA candidates on piRNA clusters."
map_loosely_on_refs.sh ${name} ${name} ${sicandidates} "siRNA_candidate" ${refs} "pi_clusters"

echo -e "\nMapping 21-nt unique mappers on piRNA clusters."
map_loosely_on_refs.sh ${name} ${name} ${siuniques} "siRNA_unique" ${refs} "pi_clusters"

echo -e "\nMapping pi-si-TE-3UTR on piRNA clusters."
map_loosely_on_refs.sh ${name} ${name} ${pisiTE3UTR} "pi-si-TE-3UTR" ${refs} "pi_clusters"


## extracting reads and building size histograms
#
## Will contain the sequences that mapped, in distinct files for distinct annotation groups
#mkdir -p mapped_reads_pi_clusters/${name}
#seq_dir=mapped_reads_pi_clusters/${name}
## Will contain size histogram data for the various annotation groups
#mkdir -p histos_mapped_pi_clusters/${name}
#histo_dir=histos_mapped_pi_clusters/${name}
#
#echo "read_sam.py --min_score "-10" --in_file mapped_pi_clusters/${name}_piRNA_candidate_on_pi_clusters.sam --seq_dir ${seq_dir} --hist_dir ${histo_dir}"
#read_sam.py --min_score "-10" --in_file mapped_pi_clusters/${name}_piRNA_candidate_on_pi_clusters.sam --seq_dir ${seq_dir} --hist_dir ${histo_dir} || error_exit "read_sam.py failed"
#
#echo "read_sam.py --min_score "-10" --in_file mapped_pi_clusters/${name}_siRNA_candidate_on_pi_clusters.sam --seq_dir ${seq_dir} --hist_dir ${histo_dir}"
#read_sam.py --min_score "-10" --in_file mapped_pi_clusters/${name}_siRNA_candidate_on_pi_clusters.sam --seq_dir ${seq_dir} --hist_dir ${histo_dir} || error_exit "read_sam.py failed"
#
#echo "read_sam.py --min_score "-10" --in_file mapped_pi_clusters/${name}_pi-si-TE-3UTR_on_pi_clusters.sam --seq_dir ${seq_dir} --hist_dir ${histo_dir}"
#read_sam.py --min_score "-10" --in_file mapped_pi_clusters/${name}_pi-si-TE-3UTR_on_pi_clusters.sam --seq_dir ${seq_dir} --hist_dir ${histo_dir} || error_exit "read_sam.py failed"
#
exit 0
