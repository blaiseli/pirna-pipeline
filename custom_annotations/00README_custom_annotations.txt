This directory contains files necessary to make the annotation files for the
pirna-pipeline tools.


* pi_clusters_Brennecke_2007.csv:

csv version of table S2 of Brennecke et al. 2007 (doi:10.1016/j.cell.2007.01.043)


* pi_clusters.fa:

Sequences of the piRNA clusters identified by Julius Brennecke and extracted by
Claudia Armenise.
These sequences are used to map reads at several points of the pipeline.


* ovary_si_clusters.txt:

Annotation file for zones generating siRNA in D. melanogaster ovaries, build
from table S1 of Czech et al. 2008 (doi:10.1038/nature07007), keeping only those
of size higher than 100.
Clusters are numbered by decreasing size order.


* P_lArB.fa:

Sequence of the transgene used in Le Thomas et al. 2014 (doi:10.1101/gad.245514.114)
Corresponds to http://flybase.org/cgi-bin/get_construct_seq.cgi?id=FBtp0000160,
stripped from the dots (positions 3875-4564)
