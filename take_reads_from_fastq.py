#!/usr/bin/env python
"""
This script takes as input one fastq file and a selection of reads, and writes
a new fastq file with the reads from the fastq file whose identifiers are in
the selection.

The order of the reads in the fastq file is preserved.

The selection can take various forms.

It can be another fastq file (option -f, --fq_select), a file containing one
read identifier per line (option -l, --list_select), or a file containing a
pickled python set (option -p, --pyset_select).

In case the selection is provided with the option -f or -l, the option -p is
interpreted in another way: It is the name of a pickle file in which to store
the set of read identifiers taken from the selection file.
"""

import argparse
import sys
import warnings
import os
OPJ = os.path.join
OPE = os.path.exists
OPB = os.path.basename
from os.path import splitext
WRITE = sys.stdout.write
def formatwarning(message, category, filename, lineno, line):
    """Formats a warning message."""
    return "%s:%s: %s:%s" % (filename, lineno, category.__name__, message)
warnings.formatwarning = formatwarning

from cPickle import dump, load, HIGHEST_PROTOCOL
from gzip import GzipFile

from string import split

from itertools import imap, ifilter

from Bio.SeqIO.QualityIO import FastqGeneralIterator

import gc

def is_commented(text):
    """Tells whether *text* is commented, i.e. starts with a "#" sign."""
    return text[0] == "#"


def fastq_triplet2id(fastq_triplet):
    """Extracts read ID from triplet returned by FastqGeneralIterator."""
    return split(fastq_triplet[0])[0]

def get_read_ids_from_fastq(fastq_file):
    """Returns the set of """
    return set(imap(fastq_triplet2id, FastqGeneralIterator(fastq_file)))

def fastaq(qname, annot_string, rlen, seq, qual):
    """Returns fasta and fastq formatted sequence."""
    fasta = ">%s (annot=%s) (len=%d)\n%s\n" % (qname, annot_string, rlen, seq)
    fastq = "@%s\n%s\n+\n%s\n" % (qname, seq, qual)
    return fasta, fastq

def fastq(qname, seq, qual):
    """Returns a fastq formatted sequence."""
    return "@%s\n%s\n+\n%s\n" % (qname, seq, qual)

def open_fastq_or_gzipped(filename):
    prefix, ext = splitext(filename)
    if ext == ".gz":
        return GzipFile(filename)
    elif OPE(filename):
        return open(filename)
    else:
        warnings.warn("%s not found.\n" % filename)
        gzipped = "%s.gz" % filename
        if OPE(gzipped):
            warnings.warn("Will use %s\n" % gzipped)
            return GzipFile(gzipped)
        else:
            raise NotImplementedError, "File format not recognized for %s." % filename

##############################################################################################
# For better traceability, options and global variables should be stored as class attributes #
##############################################################################################

class Globals(object):
    """This object holds global variables."""

class Options(object):
    """This object contains the values of the global options."""
    in_file = None
    min_mapq = 0
    min_score = -200
    #normalize = None
    hist_dir = None
    seq_dir = None

#################
# main function #
#################

def main():
    """Main function."""
    WRITE(" ".join(sys.argv) + "\n")
    parser = argparse.ArgumentParser(description=__doc__,
            formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-i", "--in_fastq",
            help="Fastq file from which to select the reads.",
            required=True)
    parser.add_argument("-f", "--fq_select",
            help="Fastq file to take as selection filter.")
    parser.add_argument("-o", "--out_fastq",
            help="Fastq file to write.",
            required=True)
    parser.add_argument("-l", "--list_select",
            type=argparse.FileType(mode="r"),
            help="File containing one read identifier per line that will "
            "be used as selection filter.")
    parser.add_argument("-p", "--pyset_select",
            help="File in which to read or write a pickled set that will "
            "be used as selection filter.\n"
            "If options -f or -l are used, this file will be overwritten.")
    args = parser.parse_args()
    # To avoid repeatedly calling these as string methods:
    strip = str.rstrip

    msg = "\n".join([
        "At least one means of selecting reads should be provided.",
        __doc__])
    assert any([args.list_select, args.fq_select, args.pyset_select]), msg
    selection = set()
    selection_list = set()
    selection_fq = set()
    if not args.pyset_select:
        msg = "".join([
            "No pickle file provided.\n",
            "It is recommended that you write the set of identifiers ",
            "you want to use as a selection filter in a pickle file ",
            "for later re-use.\n",
            "Ignore this if you do not plan to perform various ",
            "selections using the same set of read identifiers.\n"])
        warnings.warn(msg)
        if args.list_select:
            WRITE("Loading read identifiers from %s\n" % args.list_select)
            selection_list = set(imap(strip, ifilter(is_commented, args.list_select.name)))
        if args.fq_select:
            WRITE("Loading read identifiers from %s\n" % args.fq_select)
            with open_fastq_or_gzipped(args.fq_select) as fastq_file:
                selection_fq = get_read_ids_from_fastq(fastq_file)
        selection = selection_list | selection_fq
        # Trying to save memory
        del selection_list
        del selection_fq
        gc.collect()
    else:
        if args.list_select:
            WRITE("Loading read identifiers from %s\n" % args.list_select)
            selection_list = set(imap(strip, ifilter(is_commented, args.list_select.name)))
        if args.fq_select:
            WRITE("Loading read identifiers from %s\n" % args.fq_select)
            with open_fastq_or_gzipped(args.fq_select) as fastq_file:
                selection_fq = get_read_ids_from_fastq(fastq_file)
        selection = selection_list | selection_fq
        # Trying to save memory
        del selection_list
        del selection_fq
        gc.collect()
        if selection:
            WRITE("Pickling read identifiers into %s\n" % args.pyset_select)
            with open(args.pyset_select, "wb") as pickle_file:
                dump(selection, pickle_file, HIGHEST_PROTOCOL)
        else:
            WRITE("Unpickling read identifiers from %s\n" % args.pyset_select)
            with open(args.pyset_select, "rb") as pickle_file:
                selection = load(pickle_file)

    if not selection:
        warnings.warn("Empty selection. No reads will be selected from %s\n" % args.in_fastq)
        return 0
    else:
        WRITE("%d read identifiers will be used for filtering %s\n" % (len(selection), args.in_fastq))

    with open_fastq_or_gzipped(args.in_fastq) as source_reads_file:
        with open(args.out_fastq, "w") as output_reads_file:
            write_fq = output_reads_file.write
            for triplet in FastqGeneralIterator(source_reads_file):
                # triplet has the rest of the fasta header after the space
                # Remove this
                triplet = (split(triplet[0])[0], triplet[1], triplet[2])
                if triplet[0] in selection:
                    write_fq(fastq(*triplet))

    return 0


if __name__ == "__main__":
    sys.exit(main())
