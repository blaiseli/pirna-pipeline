#!/usr/bin/env python
"""This script summarizes the first batch of analyzes of a smal RNA-Seq library
into a table."""

import argparse
import sys
import warnings
def formatwarning(message, category, filename, lineno, line):
    """Generates the formatted warning message."""
    return "%s:%s: %s:%s" % (filename, lineno, category.__name__, message)
warnings.formatwarning = formatwarning

import gzip
import imp
import os
import csv


def nb_lines(file_name):
    """Returns the number of lines of a file <file_name>."""
    return sum(1 for line in open(file_name, "r"))

def compress_and_remove(file_name):
    """Compresses the file <file_name> with gzip
    and then removes the original."""
    with open(file_name, "rb") as normal_version:
        with gzip.open("%s.gz" % file_name, "wb") as compressed_version:
            compressed_version.writelines(normal_version)
    os.remove(file_name)

def main():
    """Main function."""
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        "-l", "--library",
        help="Name of the library that has been analyzed.")
    parser.add_argument(
        "-g", "--genome",
        help="Name of the genome that has been used for the mapping.")
    parser.add_argument(
        "-s", "--seq_dir",
        help="Directory where annotated sequences have been written.")
    parser.add_argument(
        "-a", "--annot_module_file",
        help="Pyhon module used to define annotation groups. "
        "Its name must end in '.py'.")
    #parser.add_argument("-s", "--switch",
    #        help="This is an optional boolean switch.", action="store_true")
    args = parser.parse_args()
    # To avoid repeatedly calling these as string methods:
    strip = str.rstrip
    #split = str.split

    module_name = os.path.basename(args.annot_module_file)[:-3]
    annotation_module = imp.load_source(
        module_name,
        args.annot_module_file)
    Annotation = annotation_module.Annotation

    writer = csv.writer(sys.stdout, dialect="excel-tab")
    writer.writerow(
        ["#Mapping and annotation of %s on %s" % (
            args.library, args.genome)])
    writer.writerow(["Category", "Number_reads", "File", "Comments"])

    with open(
        "%s/trimmed/%s_trimmed.nbseq" % (args.library, args.library), "r") as trim_count_file:
        writer.writerow(
            ["trimmed_reads",
                int(strip(trim_count_file.readline(), "\n")),
                #"trimmed/%s_trimmed.fastq.gz" % args.library,
                "Erased to save space",
                "Count after reads of size <= 6 "
                "(likely trash) were discarded."])

    with open(
        "%s/trimmed/%s_18-30.nbseq" % (args.library, args.library), "r") as small_count_file:
        writer.writerow(
            ["small_RNA_reads",
                int(strip(small_count_file.readline(), "\n")),
                "trimmed/%s_18-30.fastq.gz" % args.library,
                "Trimmed reads of size 18-30, those submitted to mapping."])
    #short_reads_file = "trimmed/%s_18-30.fastq" % args.library
    #nb_reads = nb_lines(short_reads_file) / 4
    #writer.writerow(
    #    ["small_RNA_reads",
    #        nb_reads, short_reads_file,
    #        "Trimmed reads of size 18-30, those submitted to mapping."])

    nb_mappers = 0
    for group in Annotation.groups:
        reads_file = "%s/%s_18-30_on_%s_%s.fastq" % (
            args.seq_dir,
            args.library, args.genome, group)
        nb_reads = nb_lines(reads_file) / 4
        nb_mappers += nb_reads
        compress_and_remove(reads_file)
        writer.writerow(
            [group,
                nb_reads, "%s.gz" % reads_file,
                "Annotations based on best-score alignments of the reads "
                "and some priority rules to break ties "
                "(see Annotation.set_group "
                "in %s for the rules)." % args.annot_module_file])
    writer.writerow(["mappers", nb_mappers])

    group = "low_score"
    reads_file = "%s/%s_18-30_on_%s_%s.fastq" % (
        args.seq_dir,
        args.library, args.genome, group)
    nb_reads = nb_lines(reads_file) / 4
    nb_mappers += nb_reads
    compress_and_remove(reads_file)
    writer.writerow(
        [group, nb_reads, "%s.gz" % reads_file,
            "Reads mapping, but with score not high enough "
            "to be reliably annotated."])

    group = "unmapped"
    reads_file = "%s/%s_18-30_on_%s_%s.fastq" % (
        args.seq_dir,
        args.library, args.genome, group)
    nb_reads = nb_lines(reads_file) / 4
    nb_mappers += nb_reads
    compress_and_remove(reads_file)
    writer.writerow(
        [group, nb_reads, "%s.gz" % reads_file,
            "Reads with sufficient quality, but not mapping."])

    with open("%s/mapped_%s/%s_on_%s_miRNA.nbseq" % (
        args.library, args.genome,
        args.library, args.genome), "r") as miRNA_count_file:
        writer.writerow(
            ["miRNA_reads",
                int(strip(miRNA_count_file.readline(), "\n")),
                "%s/%s_18-30_on_%s_(pre_)miRNA.fastq.gz" % (
                    args.seq_dir, args.library, args.genome),
                "Sould be the same number as the above '(pre_)miRNA'."])

    with open("%s/mapped_%s/%s_on_%s_siRNA.nbseq" % (
        args.library, args.genome,
        args.library, args.genome), "r") as siRNA_count_file:
        siRNA_file = "%s/%s_21_on_%s_siRNA.fastq" % (
            args.seq_dir, args.library, args.genome)
        # Already done in map_and_annotate_small.sh
        #compress_and_remove(siRNA_file)
        writer.writerow(
            ["siRNA_reads", int(strip(siRNA_count_file.readline(), "\n")),
                "%s.gz" % siRNA_file,
                "Reads of size 21 annotated on "
                "%s." % ", ".join(Annotation.siRNA_types)])

    with open("%s/mapped_%s/%s_on_%s_siRNA_candidate.nbseq" % (
        args.library, args.genome,
        args.library, args.genome), "r") as siRNA_count_file:
        siRNA_file = "%s/%s_21_on_%s_siRNA_candidate.fastq" % (
            args.seq_dir, args.library, args.genome)
        # Already done in map_and_annotate_small.sh
        #compress_and_remove(siRNA_file)
        writer.writerow(
            ["siRNA_candidates", int(strip(siRNA_count_file.readline(), "\n")),
                "%s.gz" % siRNA_file,
                "Reads of size 21 from "
                "%s." % ", ".join(Annotation.piRNA_groups)])

    with open("%s/mapped_%s/%s_on_%s_piRNA_candidate.nbseq" % (
        args.library, args.genome,
        args.library, args.genome), "r") as piRNA_count_file:
        piRNA_file = "%s/%s_23-30_on_%s_piRNA_candidate.fastq" % (
            args.seq_dir, args.library, args.genome)
        # Already done in map_and_annotate_small.sh
        #compress_and_remove(siRNA_file)
        writer.writerow(
            ["piRNA_candidates",
                int(strip(piRNA_count_file.readline(), "\n")),
                "%s.gz" % piRNA_file,
                "Reads of size 23-30 from "
                "%s." % ", ".join(Annotation.piRNA_groups)])

    with open("%s/mapped_strict_%s/%s_strict_on_%s_piRNA_cluster1.nbseq" % (
        args.library, args.genome,
        args.library, args.genome), "r") as unique_count_file:
        writer.writerow(
            ["42AB_unique_piRNAs",
                int(strip(unique_count_file.readline(), "\n")),
                "%s/mapped_strict_%s/unique_reads/%s_strict_on_%s_piRNA_cluster1.fastq.gz" % (
                    args.library, args.genome,
                    args.library, args.genome),
                "Reads of size 23-30 mapping perfectly "
                "only in piRNA cluster 42AB."])

if __name__ == "__main__":
    sys.exit(main())
