#!/usr/bin/awk -f
# Usage: fastq2histo.awk <file.fastq>

#BEGIN {print "#length\tcount"}
# Take every second-out-of-four line
# and count its length in the histogram
NR%4==2 {histo[length($0)]++}
# At the end, display the results
END {for (l in histo) print l"\t"histo[l]}
