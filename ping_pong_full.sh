#!/usr/bin/env bash
# vim: set fileencoding=<utf-8> :
# Usage: ping_pong_full.sh <library_name> <ref_name> <signature_files>


# To configure according to the local installation:
genomes_path=${GENOMES}

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}


# library name
name=${1}
echo "Library: ${name}"

#genome=${2}
genome="D_melanogaster"
echo "Genome: ${genome}"

# Reference on which to compute ping-pong
# "all" (or "tot" or "global") or the name of a transposable element (or equivalent like "P_lArB_transgene")
#ref_name=${3}
ref_name=${2}

# File in which to append the name of the ping-pong signature file
signature_files="${3}"
# maximum read size
# If this environment variable is not defined, use 30 as default value
[ MAX_LEN ] || MAX_LEN="30"

# Overlap between F and R partners
overlap=10

pp_dir="${name}/ping_pong_full"
pp_dir_this_ref="${pp_dir}/${ref_name}"
mkdir -p "${pp_dir_this_ref}"


# Determining piRNA candidates file and name to give to the mapping
####################################################################
genome_db="${genomes_path}/${genome}/${genome}_and_TE"

if [ ${ref_name} == "all" -o ${ref_name} == "global" -o ${ref_name} == "tot" ]
then
    # Global ping-pong
    seq_dir=${name}/mapped_reads_${genome}
    candidates_full=${seq_dir}/${name}_23-${MAX_LEN}_on_${genome}_piRNA_candidate.fastq
    ref="${genome}_and_TE"
    # Re-use mapping that was performed when attributing reads to TEs
    mapping_basename="piRNA_candidate_on_TE"
    map_dir="${name}/mapped_${mapping_basename}"
    sam_file="${map_dir}/${name}_${mapping_basename}.sam"
    bam_file="${sam_file%.sam}_sorted.bam"
else
    # Reference-wise ping-pong
    seq_dir="${name}/reads_piRNA_candidate_on_TE"
    candidates_full=${seq_dir}/${name}_piRNA_candidate_on_${ref_name}.fastq
    ref="${ref_name}"
    # Re-map the TE reads on the (TE-complemented) genome
    mapping_basename="piRNA_candidate_remapped_on_${ref}"
    map_dir="${pp_dir_this_ref}/mapped_${mapping_basename}"
    mkdir -p ${map_dir}
    sam_file="${map_dir}/${name}_${mapping_basename}.sam"
    bam_file="${sam_file%.sam}_sorted.bam"
    if [ ! -e ${bam_file} ]
    then
        if [ ! -e ${sam_file} ]
        then
            # If not "all", "global" or "tot", ${ref_name} must be in that genome index
            #genome_db="${genomes_path}/${genome}/${genome}_and_TE"
            if [ -e ${candidates_full} ]
            then
                # No need to use a compressed version
                map_loosely_on_refs.sh ${name} ${pp_dir_this_ref} ${candidates_full} "piRNA_candidate_remapped" ${genome_db} ${ref} "no_process" || error_exit "map_loosely_on_ref.sh failed"
            else
                # At least a compressed version should exist
                map_loosely_on_refs.sh ${name} ${pp_dir_this_ref} ${candidates_full}.gz  "piRNA_candidate_remapped" ${genome_db} ${ref} "no_process" || error_exit "map_loosely_on_ref.sh failed"
            fi
        else
            echo "Using already existing ${sam_file}"
        fi
        echo -e "\nSorting and indexing alignments."
        sam2indexedbam.sh ${sam_file} || error_exit "sam2indexedbam.sh failed"
    else
        echo -e "\nUsing already existing ${bam_file}"
    fi
fi

#signature_file="${pp_dir_this_ref}/${name}_${mapping_basename}_1-80_signature.xls"
#if [[ ! -e signature_file ]]
#then
#    cmd="signature.py \
#        --input ${bam_file} \
#        --inputFormat sam \
#        --minquery 23 \
#        --maxquery ${MAX_LEN} \
#        --mintarget 23 \
#        --maxtarget ${MAX_LEN} \
#        --minscope 1 \
#        --maxscope 80 \
#        --outputOverlapDataframe ${signature_file} \
#        --referenceGenome ${genome_db} \
#        --extract_index \
#        --graph global \
#        --rcode /dev/null"
#    echo ${cmd}
#    eval ${cmd} || error_exit "signature.py failed"
#fi

signature_file="${pp_dir_this_ref}/${name}_${mapping_basename}_1-21_signature.xls"
if [[ ! -e signature_file ]]
then
    cmd="signature.py \
        --input ${bam_file} \
        --inputFormat sam \
        --minquery 23 \
        --maxquery ${MAX_LEN} \
        --mintarget 23 \
        --maxtarget ${MAX_LEN} \
        --minscope 1 \
        --maxscope 21 \
        --outputOverlapDataframe ${signature_file} \
        --referenceGenome ${genome_db} \
        --extract_index \
        --graph global \
        --rcode /dev/null"
    echo ${cmd}
    eval ${cmd} || error_exit "signature.py failed"
fi
# Append the name of the file to a list for later plotting
echo -e "${ref}\t${signature_file}" >> ${signature_files}
#rm -f ${sam_file}

exit 0
