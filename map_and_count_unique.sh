#!/usr/bin/env bash
# note: "for i in {1..142}" doesn't work as expected using sh, bash has to be used
# Usage: map_and_count_unique.sh <library_name> <G_species> <cluster_list_file> <number_of_cpus_to_use>

# TODO: use make or snakemake to better deal with existing files

# To configure according to the local installation:
genomes_path=${GENOMES}

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}


# library name
name=${1}
genome=${2}
genome_db=${genomes_path}/${genome}/${genome}
#genome=`basename ${genome_db}`
# annotation file giving the genomic coordinates of the elements to count
#annot=${3}
#max_clust_nb=${3}
clust_list_file=${3}
# number of cpus to use
ncpu=${4}
# maximum read size
# If this environment variable is not defined, use 30 as default value
[ MAX_LEN ] || MAX_LEN="30"

mapped_dir="${name}/mapped_strict_${genome}"

#########################################################
# strict and thorough mapping on D. melanogaster genome #
#########################################################

mkdir -p ${mapped_dir}

# piRNA candidates. All piRNA_cluster mappers of size 23-${MAX_LEN} should be there.
candidates="${name}/mapped_reads_${genome}/${name}_23-${MAX_LEN}_on_${genome}_piRNA_candidate.fastq.gz"

# The normalization will use the number of 23-${MAX_LEN} nt reads strictly mapping in
# the annotated element and not strictly mapping somewhere else.
#
# We use rather strict mapping criteria because we are not interested in the
# reads that don't map strictly on the annotated element and want to save space
# (option -a generates a lot of data).
#
# We save disk space by using the following option:
# --no-unal Suppress SAM records for reads that failed to align.
# use strict criteria, and run in parallel on $ncpu cpus
# TODO: avoid uncompressing if counts are already done and registered in files.
sam_file="${mapped_dir}/${name}_piRNA_candidate_strict_on_${genome}.sam"
if [ -e ${sam_file}.bz2 ]
then
    echo -e "\nUncompressing existing ${sam_file}.bz2"
    echo "Be sure there is enough disk space for that."
    cmd="lbunzip2 -k ${sam_file}.bz2"
    echo ${cmd}
    eval ${cmd} || error_exit "${cmd} failed"
fi
#if [ ! -e ${mapped_dir}/${name}_18-${MAX_LEN}_strict_on_${genome}_23-${MAX_LEN}.sam ]
#then
if [ ! -e ${sam_file} ]
then
    echo -e  "\nBe careful, you may need to have a lot of available space on the disk on which the ${mapped_dir} folder resides."
    if [ ! -e ${sam_file}.bz2 ]
    then
        #if [ ! -e trimmed/${name}_23-${MAX_LEN}.fastq ]
        #then
        #    echo "putting reads of piRNA size in trimmed/${name}_23-${MAX_LEN}.fastq"
        #    echo "select_fastq_size.sh trimmed/${name}_18-${MAX_LEN}.fastq 23 ${MAX_LEN} trimmed/${name}_23-${MAX_LEN}.fastq"
        #    select_fastq_size.sh trimmed/${name}_18-${MAX_LEN}.fastq 23 ${MAX_LEN} trimmed/${name}_23-${MAX_LEN}.fastq || error_exit "select_fastq_size.sh failed"
        #fi
        echo -e "\nMapping stringently but exhaustively ${candidates} on ${genome_db}"
        echo "bowtie2 --seed 123 -p ${ncpu} -t -a --mp 10,2 --score-min L,0,-0.2 --no-unal -x ${genome_db} -U ${candidates} -S ${sam_file}"
        bowtie2 --seed 123 -p ${ncpu} -t -a --mp 10,2 --score-min L,0,-0.2 --no-unal -x ${genome_db} -U ${candidates} -S ${sam_file} || error_exit "bowtie2 failed"
    else
        echo -e "\nDecompressing existing ${sam_file}.bz2 file"
        cmd="lbunzip2 -k ${sam_file}.bz2"
        echo ${cmd}
        eval ${cmd} || error_exit "${cmd} failed"
    fi
else
    echo -e "\nUsing already existing ${sam_file} file"
fi
## The file might be too large due to small reads mapping on many places.
## Filter the reads before counting the annotations:
## creating the filtered file with the original header:
#echo "Making a ${mapped_dir}/${name}_18-${MAX_LEN}_strict_on_${genome}_23-${MAX_LEN}.sam"
#echo "samtools view -SH ${mapped_dir}/${name}_18-${MAX_LEN}_strict_on_${genome}.sam > ${mapped_dir}/${name}_18-${MAX_LEN}_strict_on_${genome}_23-${MAX_LEN}.sam"
#samtools view -SH ${mapped_dir}/${name}_18-${MAX_LEN}_strict_on_${genome}.sam > ${mapped_dir}/${name}_18-${MAX_LEN}_strict_on_${genome}_23-${MAX_LEN}.sam || error_exit "samtools view failed"
## appending the reads matching on 23 to ${MAX_LEN} nt
#echo "awk '(length($10) > 22 && length($10) < 31){print $0}' ${mapped_dir}/${name}_18-${MAX_LEN}_strict_on_${genome}.sam >> ${mapped_dir}/${name}_18-${MAX_LEN}_strict_on_${genome}_23-${MAX_LEN}.sam"
#awk '(length($10) > 22 && length($10) < 31){print $0}' ${mapped_dir}/${name}_18-${MAX_LEN}_strict_on_${genome}.sam >> ${mapped_dir}/${name}_18-${MAX_LEN}_strict_on_${genome}_23-${MAX_LEN}.sam || error_exit "awk failed"
#if [ ! -e ${mapped_dir}/${name}_18-${MAX_LEN}_strict_on_${genome}.sam.bz2 ]
#then
#    echo "Compressing ${mapped_dir}/${name}_18-${MAX_LEN}_strict_on_${genome}.sam file with lbzip2"
#    # testing: lbzip2 vs lrzip
#    # TODO: compare sizes
#    #-rw-r--r-- 1 bli bli  24G Mar  4 21:14 dw_771_trimmed_strict_on_${genome}.sam.bz2
#    #-rw-r--r-- 1 bli bli 746G Mar  4 21:14 dw_771_trimmed_strict_on_${genome}.sam
#    # lrzip is too slow
#    # not using option -k: the original is suppressed
#    echo "lbzip2 ${mapped_dir}/${name}_18-${MAX_LEN}_strict_on_${genome}.sam"
#    lbzip2 ${mapped_dir}/${name}_18-${MAX_LEN}_strict_on_${genome}.sam || error_exit "lbzip2 failed"
#else
#    echo "Removing ${mapped_dir}/${name}_18-${MAX_LEN}_strict_on_${genome}.sam file, of which a compressed ${mapped_dir}/${name}_18-${MAX_LEN}_strict_on_${genome}.sam.bz2 version exists"
#    rm -f ${mapped_dir}/${name}_18-${MAX_LEN}_strict_on_${genome}.sam
#fi
#else
#    echo "Using already existing ${mapped_dir}/${name}_23-${MAX_LEN}_strict_on_${genome}_23-${MAX_LEN}.sam file"
#fi

#
# count the 23-${MAX_LEN}-sized reads strictly mapping on the annotated element and nowhere else
#
# To store the reads
mkdir -p ${mapped_dir}/unique_reads
#time count_annotation.py --in_file ${mapped_dir}/${name}_trimmed_strict_on_${genome}.sam --annot ${annot} --max_edit_proportion 0.0 --read_range 23,${MAX_LEN} --check_ali_ends_only --record_count ${count_file}
#time count_annotation.py --in_file ${mapped_dir}/${name}_trimmed_strict_on_${genome}_23-${MAX_LEN}.sam --annot ${annot} --max_edit_proportion 0.0 --read_range 23,${MAX_LEN} --check_ali_ends_only --record_count ${count_file}
#for i in {1..142}; do
#for i in {1..${MAX_LEN}}; do
#for i in `seq 1 ${max_clust_nb}`; do
for i in `cat ${clust_list_file}`; do
    counts_file="${mapped_dir}/${name}_strict_on_${genome}_piRNA_cluster${i}.nbseq"
    if [ ! -e ${counts_file} ]
    then
        fastq_file="${mapped_dir}/unique_reads/${name}_strict_on_${genome}_piRNA_cluster${i}.fastq"
        echo -e "\nCounting unique mappers of piRNA cluster ${i}."
        #cmd="count_annotation.py --in_file ${sam_file} --out_file ${fastq_file} --annot ${genomes_path}/${genome}/Annotations/Annotations_Chambeyron/piRNA_clusters/piRNA_cluster${i}_annotation.bed --max_edit_proportion 0.0 --read_range 23,${MAX_LEN} --record_count ${counts_file}"
        cmd="count_annotation.py --in_file ${sam_file} --out_file ${fastq_file} --annot ${genomes_path}/${genome}/Annotations/piRNA_clusters/piRNA_cluster${i}_annotation.bed --max_edit_proportion 0.0 --read_range 23,${MAX_LEN} --record_count ${counts_file}"
        echo ${cmd}
        time eval ${cmd} || error_exit "${cmd} failed"
        echo -e "Compressing ${fastq_file}"
        pigz ${fastq_file} || error_exit "pigz failed"
    fi
    echo "Number of unique mappers for piRNA_cluster${i} stored in ${counts_file}."
done

# Saving space
if [ ! -e ${sam_file}.bz2 ]
then
    echo -e "\nCompressing ${sam_file} with lbzip2"
    cmd="lbzip2 ${sam_file}"
    echo ${cmd}
    eval ${cmd} || error_exit "${cmd} failed"
else
        echo -e "\nRemoving ${sam_file} file, of which a compressed ${sam_file}.bz2 version exists"
        rm -f ${sam_file}
fi

exit 0
